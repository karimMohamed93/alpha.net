<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'alpha');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '12344321');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?>;$+gJmh|YWl6@uE*3Q`Z(XZ0r0P CghaYzn#j/ jzI`(5`tT]+j6?Be/M,YtdL');
define('SECURE_AUTH_KEY',  'QcIo*R 1BH^yJ|^3M7gC4AoC3QP<!]bvl0yUl!HV}IHQ$Sb6q>iRgj%Re(mD3(Z9');
define('LOGGED_IN_KEY',    'Za0+);~sQ1T!NLdAQ5YtV!Ar[,bO (Q?hc>Kc>th_$if=)yZ(M(|}cDE0axq!;P!');
define('NONCE_KEY',        'gWN~L.KV]w7 Jl(eHazkMU3>x[=67sqJ[[ U:X!&1Oa?W;f1oWSG[(To!6wvHA$s');
define('AUTH_SALT',        '{t4^1YTrIS-!B3|dOvUz/KkIKa%9oxL|*$V[}K[&qH~ir&]6B=A)~R` >K&4ZhBW');
define('SECURE_AUTH_SALT', 'Z^g&2$Xg}=o R}jd<+O>P,4$O<xM^Y|.cK~UiiAs3]Fz.<~v_BxR>~P-W/f]3$R9');
define('LOGGED_IN_SALT',   'mcbcj@]+ZSbW@JD]L^?(Ria;Zv*VRRKhE}c<_Sw~:r q-$j[PLp5{/t7kt*UVpqI');
define('NONCE_SALT',       '_&i >31..`)jljST? (Kj_]#~kx D)X:9YD{,PlUzveY:HTXT]H!n2sM2Fy+Q0F,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';
define ('WPLANG', 'ar');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
