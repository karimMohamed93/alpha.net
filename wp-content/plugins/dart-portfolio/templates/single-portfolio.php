<?php
/**
 * The Template for displaying all single Portfolio posts.
 * 
 * @package themeplaza_portfolio
 * @subpackage templates
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header();
?>
<div class="container">
    <div class="row">
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="tp-content col-md-9">
            <article id="post-<?php the_ID(); ?>" <?php post_class('tp-single-portfolio-item'); ?>>
                <div class="entry-content">
                    <?php the_post_thumbnail(); ?>
                    <h1><?php the_title(); ?></h1>  
        			<?php the_content(); ?>
                </div>
                 <div class="categories">
                    <?php the_taxonomies(); ?> 
                </div>
                <?php edit_post_link( __( 'Edit Post', THEMEPLAZAPORTFOLIO_VERSION ), '<span class="edit-link">', '</span><br /><br />' ); ?>
                <br />
        		<nav class="nav-single">
        			<h3 class="assistive-text"><?php _e( 'Post navigation', THEMEPLAZAPORTFOLIO_VERSION ); ?></h3>
        			<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', THEMEPLAZAPORTFOLIO_VERSION ) . '</span> %title' ); ?></span>
        			<span class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', THEMEPLAZAPORTFOLIO_VERSION ) . '</span>' ); ?></span>
        		</nav><!-- .nav-single -->

        		<?php // comments_template( '', true ); ?>

            </article><!-- #post -->

        </div>
    <?php endwhile; // end of the loop. ?>

    	<div class="col-md-3 tp-sidebar">
            <?php get_sidebar(); ?>
    	</div>
    </div>
</div>

<?php get_footer(); ?>