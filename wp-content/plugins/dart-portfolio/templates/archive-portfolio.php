<?php
/**
 * The Template for displaying Portfolio posts.
 * 
 * @package themeplaza_portfolio
 * @subpackage templates
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header();
?>
<div class="container">
    <div class="row tp-portfolio">
        <div class="col-md-9">
            <div class="tp-portfolio block-grid">
            <?php if (is_page()) { $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts('post_type=portfolio&paged='.$paged); } ?>
            <?php if (have_posts()): 

                $taxonomies = get_object_taxonomies( 'portfolio' );
                foreach ( $taxonomies as $tax_name ) {
                    $categories = get_categories('taxonomy='.$tax_name);
                    $i = 0; $len = count( $categories );
                    $sep = '';
                    foreach ($categories as $category) {
                        if ($i == 0) { ?><ul class="tp-portfolio-filter"><?php }
                        if ($i > 0) { $sep = '<span class="separator">/</span>'; }
                            echo '<li class="'.$category->category_nicename.'"><a href="'.get_term_link($category->slug, 'portfolio_category').'">'.$sep.$category->cat_name.'</a></li>';
                        if ($i == $len - 1) { echo '</ul>'; }
                        $i++;
                    }
                }
            
                while (have_posts()) : the_post(); ?>
                    <div class="col-md-4" data-type="<?php foreach(get_the_terms($post->ID, 'portfolio_category') as $term) echo esc_attr($term->slug).' ' ?>" data-id="id-<?php echo($post->post_name) ?>">
                        <article id="post-<?php the_ID(); ?>" <?php post_class('portfolio-item'); ?>>
                        	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="tp-portfolio-item-link">
                        		<div class="image">
                            		<?php if ( has_post_thumbnail()) { ?>
                            		    <?php the_post_thumbnail(); ?>
                            		<?php } else { ?>
                                        <img src="http://dummyimage.com/600x600/dddddd/454545.jpg&text=Space+For+Image" alt="">
                                    <?php } ?>
                        		</div>
                        		<div class="tp-title">
                        			<h3><?php the_title(); ?></h3>
                        		</div>
                        		<div class="tp-entry-content">
                        			<?php the_excerpt(); ?>
                        		</div>
                            </a>
                        </article>
                    </div>
                <?php endwhile; ?>
            

            <?php else: ?>

                <article id="post-0" class="post no-results not-found">
                	<div class="entry-header">
                		<h1 class="entry-title"><?php _e( 'Nothing Found', THEMEPLAZAPORTFOLIO_DOMAIN ); ?></h1>
                	</div>

                	<div class="entry-content">
                		<p><?php _e( 'Apologies, but no results were found.', THEMEPLAZAPORTFOLIO_DOMAIN ); ?></p>
                	</div><!-- .entry-content -->
                </article><!-- #post-0 -->
            <?php endif; ?>
            </div>
            <?php themeplaza_portfolio_pagination(); ?>
        </div>
        <div class="col-md-3">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>