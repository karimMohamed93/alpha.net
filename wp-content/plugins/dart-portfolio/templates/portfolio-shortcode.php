<?php
/**
 * @package themeplaza_portfolio
 * @subpackage templates
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */
/**
 * [portfolio limit="6" featured="" class="" filter="true" column="col-sm-3"]
 * @param int $limit          	Portfolio items limit for this shortcode
 * @param boolean $featured   	Show only featured porfolios. This 
 *                             		property can be set in portfolio item's admin page
 * @param string $class  		CSS custom class
 * @param boolean $filter  		Portfolios will be filterable or not
 * @param string $column		The Layout. One of the boostrap's col-*-* classes
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

extract(shortcode_atts(array(
	"limit" => 6,
	"featured" => false,
	'class'=> '',
	'filter' => true,
	'column' => 'col-sm-3'
), $atts));

global $post;
$randomId=  mt_rand(0, 10000);

$args = array('post_type' => 'portfolio', 'taxonomy'=> 'portfolio_category', 'showposts' => $limit, 'posts_per_page' => $limit, 'orderby' => 'date','order' => 'DESC');

if ($featured)
{
	$args['meta_key'] = '_portfolio_featured'; 
}

$query = new WP_Query($args);
?>
<div class="<?php echo esc_attr($class); ?> works tp-portfolio-page">
<?php if($filter): ?>
	<div class="row text-center">
		<div class="isotope-nav">
			<ul data-option-key="filter" class="isotope-filter-<?php echo esc_attr($randomId) ?> isotope-filter option-set list-inline text-center">
				<li><a href="isotope-filter" class="filter active" data-filter="*">
					<?php echo __('Show All', THEMEPLAZAPORTFOLIO_DOMAIN) ?>
				</a></li>
				<?php  
					$cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true);
					$MyWalker = new ThemeplazaPortfolioWalker();
					$argsfilter = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'echo' => '0');
					$categories = wp_list_categories ($argsfilter);
					echo $categories;
				?>
			</ul>
		</div>		
	</div>
<?php endif;  ?>	
	<div class="isotope projects isotope-<?php echo esc_attr($randomId) ?>">
<?php 
	if ($query->have_posts()):  
		while ($query->have_posts()) : 							
			$query->the_post();
			$custom = get_post_custom($post->ID);

			// Get the portfolio item categories
			$cats = wp_get_object_terms($post->ID, 'portfolio_category');
			
			$cat_slugs = '';
			$potfolio_cats = '';					   						
			if ($cats):
				foreach( $cats as $cat ) {
					$cat_slugs .= $cat->slug . " ";
					$potfolio_cats .= ucfirst($cat->slug) . " / ";
				}
			endif;
			
			$thumbnail = get_the_post_thumbnail($post->ID, 'portfolio-thumb'); 
			$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			$url = !empty($url) ? $url : "http://dummyimage.com/600x600/dddddd/454545.jpg&text=Space+For+Image";
?>
			<div   class="<?php echo esc_attr($column) ?> isotope-item isotope-item-<?php echo esc_attr($randomId) ?> <?php echo esc_attr($cat_slugs) ?>">			
				<div class="project">
					<div class="project-cover">
						<a href="<?php echo esc_url($url); ?>">
							<span class="fa fa-plus"></span>						
						
							<?php if (!empty($thumbnail)): 
								echo $thumbnail; 
							else : ?>
								<img src = "<?php echo esc_url($url) ?>" alt="" />
							<?php endif; ?>
						</a>
					</div>
					<div class="project-details">
						<?php $proj_url = get_permalink($post->ID); ?>
                    	<h4><a href="<?php echo esc_attr($proj_url) ?>"><?php echo get_the_title($post->ID) ?></a></h4>
                    	<h5><?php echo trim(trim($potfolio_cats), "/") ?></h5>
						<p><?php echo get_the_excerpt($post->ID) ?></p>
						<div class="tp-date"><?php echo get_the_time('F j, Y', $post->ID) ?></div>
						<span class="read-more"><a href="<?php echo esc_attr($proj_url) ?>">Read more</a></span>
					</div>
				</div>
			</div>
										
		<?php endwhile; 
	endif;?>
	</div>
</div>
<script>
jQuery(window).load(function() {
	var $isotope_selectors = jQuery(".isotope-filter-<?php echo esc_attr($randomId) ?>>li .filter");

    if($isotope_selectors!="undefined"){
      	var $portfolio = jQuery(".isotope-<?php echo esc_attr($randomId) ?>");
      	$portfolio.isotope({
       	 	itemSelector : ".isotope-item-<?php echo esc_attr($randomId) ?>",
        	layoutMode : "fitRows"
     	});
      
      	$isotope_selectors.on("click", function(){
	        $isotope_selectors.removeClass("active");
	        jQuery(this).addClass("active");
	        var selector = jQuery(this).attr("data-filter");
	        $portfolio.isotope({ filter: selector });
	        return false;
     	});
    }
}); 
</script>

<?php wp_reset_query(); ?>