<?php
/**
 * Admin functions for the plugin.
 *
 * @package themeplaza_portfolio
 * @subpackage admin
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Set up the admin functionality. */
add_action( 'admin_menu', 'themeplaza_portfolio_admin_setup' );

/* Adds new submenu for sorting of portfolio items */
add_action( 'admin_menu', 'themeplaza_portfolio_register_portfolio_menu' );

/* Adds support of AJAX for the sorting page*/
add_action( 'wp_ajax_themeplaza_portfolio_portfolio_update_post_order', 'themeplaza_portfolio_portfolio_update_post_order' );

/* Adds hooks to perform checking */
add_action( 'restrict_manage_posts','themeplaza_portfolio_portfolio_filter_list' );
add_filter( 'parse_query','themeplaza_portfolio_perform_filtering' );

/**
 * Adds actions where needed for setting up the plugin's admin functionality.
 * @return void
 */
function themeplaza_portfolio_admin_setup() {

	/* Custom columns on the edit portfolio items screen. */
	add_filter( 'manage_edit-portfolio_columns', 'themeplaza_portfolio_edit_portfolio_columns' );
	add_action( 'manage_portfolio_posts_custom_column', 'themeplaza_portfolio_manage_portfolio_columns', 10, 2 );
}

/**
 * Sets up custom columns on the portfolio items edit screen.
 *
 * @access public
 * @param  array  $columns
 * @return array
 */
function themeplaza_portfolio_edit_portfolio_columns( $columns ) {

	unset( $columns['title'] );

	$new_columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Portfolio Item', THEMEPLAZAPORTFOLIO_DOMAIN )
	);

	if ( current_theme_supports( 'post-thumbnails' ) )
		$new_columns['thumbnail'] = __( 'Thumbnail', THEMEPLAZAPORTFOLIO_DOMAIN );

	return array_merge( $new_columns, $columns );
}

/**
 * Displays the content of custom portfolio item columns on the edit screen.
 *
 * @access public
 * @param  string  $column
 * @param  int     $post_id
 * @return void
 */
function themeplaza_portfolio_manage_portfolio_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'thumbnail' :

			if ( has_post_thumbnail() )
				the_post_thumbnail( array( 40, 40 ) );
			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}



/**
 * Add new submenu for sorting.
 *
 * @access public
 * @return void
 */
function themeplaza_portfolio_register_portfolio_menu() {
	add_submenu_page(
		'edit.php?post_type=portfolio',
		'Order portfolio',
		'Sort items',
		'edit_pages', 'themeplaza_portfolio-portfolio-order',
		'themeplaza_portfolio_portfolio_order_page'
	);
}

/**
 * Callback function for add_submenu_page function
 * Returns the output for the sorting page
 *
 * @access public
 * @return void
 */
function themeplaza_portfolio_portfolio_order_page() 
{
	?></pre>
	<div class="wrap">
        <h2>Sort Items</h2>
        Simply drag the items up or down and they will be saved in that order.
        
        <?php $slides = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order' ) ); ?>
        <table id="sortable-table-portfolio" class="wp-list-table widefat fixed posts">
            <thead>
                <tr>
                    <th class="column-order">Order</th>
                    <th class="column-title">Title</th>
                    <th class="column-thumbnail">Thumbnail</th>
         
                </tr>
            </thead>
            <tbody data-post-type="portfolio">
				<?php if( $slides->have_posts() )  : ?>
                    <?php while ($slides->have_posts()): $slides->the_post(); ?>
                        <tr id="post-<?php the_ID(); ?>">
                            <td class="column-order"><img title="" src="<?php echo THEMEPLAZAPORTFOLIO_URI . 'assets/images/move-icon.png'; ?>" alt="Move Icon" width="32" height="32" /></td>
                            <td class="column-title"><strong><?php the_title(); ?></strong></td>
                    		<td class="column-thumbnail"><?php the_post_thumbnail( 'blog-thumb' ); ?></td>
                         </tr>
                    <?php endwhile; ?>
                <?php else : ?>        
                    <p>No portfolio items found, make sure you <a href="post-new.php?post_type=portfolio">create one</a>.</p>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>	
            </tbody>
            <tfoot>
                <tr>
                    <th class="column-order">Order</th>
                    <th class="column-title">Title</th>
                    <th class="column-thumbnail">Thumbnail</th>
                </tr>
            </tfoot>
        </table>
 	</div>
	<pre>
	<!-- .wrap -->	
	<?php 
}

/**
 * Handle function for AJAX
 * Saves the order of sorted elements
 *
 * @access public
 * @return void
 */
function themeplaza_portfolio_portfolio_update_post_order() {
	$post_type     = $_POST['postType'];
	$order        = $_POST['order'];

	if ($post_type !== "portfolio") {
		wp_die("YOU NEED PERMISSIONS FOR THIS KIND OF OPERATION");
	}

	foreach( $order as $menu_order => $post_id )
	{
		$post_id         = intval( str_ireplace( 'post-', '', $post_id ) );
		$menu_order     = intval($menu_order);
		wp_update_post( array( 'ID' => $post_id, 'menu_order' => $menu_order ) );
	}

	wp_die(false);
}

/**
 * Adds dropdown with categories
 *
 * @access public
 * @return void
 */
function themeplaza_portfolio_portfolio_filter_list() {
	global $typenow, $wp_query;
	if ($typenow=='portfolio') {
		wp_dropdown_categories(array(
		   'show_option_all' => 'Show All Portfolio Category',
		   'taxonomy' => 'portfolio_category',
		   'name' => 'portfolio_category',
		   'orderby' => 'name',
		   'selected' =>( isset( $wp_query->query['portfolio_category'] ) ? $wp_query->query['portfolio_category'] : '' ),
		   'hierarchical' => false,
		   'depth' => 3,
		   'show_count' => false,
		   'hide_empty' => true,
		));
	}
}

/**
 * Sets query variables
 *
 * @access public
 * @param object $query Instance of WP_Query
 * @return void
 */
function themeplaza_portfolio_perform_filtering( $query ){
   $qv = &$query->query_vars;
   if ( isset($qv['portfolio_category'] ) && is_numeric( $qv['portfolio_category'] ) ) {
      $term = get_term_by( 'id', $qv['portfolio_category'], 'portfolio_category' );
      $qv['portfolio_category'] = $term->slug;
   }
}

?>