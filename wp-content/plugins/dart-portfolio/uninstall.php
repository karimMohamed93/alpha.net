<?php
/**
 * Uninstall procedure for the plugin.
 *
 * @package themeplaza_portfolio
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Set the text domain */
define( 'THEMEPLAZAPORTFOLIO_DOMAIN', 'themeplaza_portfolio' );

/* Make sure we're actually uninstalling the plugin. */
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
	wp_die( sprintf( __( '%s should only be called when uninstalling the plugin.', THEMEPLAZAPORTFOLIO_DOMAIN ), '<code>' . __FILE__ . '</code>' ) );

/* === Remove capabilities added by the plugin. === */

/* Get the administrator role. */
$role =& get_role( 'administrator' );

/* If the administrator role exists, remove added capabilities for the plugin. */
if ( !empty( $role ) ) {

	$role->remove_cap( 'read_portfolio_items' );
	$role->remove_cap( 'delete_portfolio_items' );
	$role->remove_cap( 'edit_portfolio_items' );
}

?>