<?php
/**
 * File for registering custom taxonomies.
 *
 * @package themeplaza_portfolio
 * @subpackage includes
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Register taxonomies on the 'init' hook. */
add_action( 'init', 'themeplaza_portfolio_register_taxonomies' );

/**
 * Register taxonomies for the plugin.
 *
 * @access public
 * @return void.
 */
function themeplaza_portfolio_register_taxonomies() {

	/* Get the plugin settings. */
	$settings = themeplaza_portfolio_get_default_settings();

	/* Set up the arguments for the portfolio taxonomy. */
	$args = array(
		'public'            => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'show_tagcloud'     => false,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'query_var'         => true,
		'capabilities' 	   	=> array(
			'manage_terms' => 'edit_portfolio_items',
			'edit_terms'   => 'edit_portfolio_items',
			'delete_terms' => 'edit_portfolio_items',
			'assign_terms' => 'edit_portfolio_items',
		),

		/* The rewrite handles the URL structure. */
		'rewrite' => array(
			'slug'         => 'portfolio',
			'with_front'   => true,
			'hierarchical' => true,

		),

		/* Labels used when displaying taxonomy and terms. */
		'labels' => array(
			'name'                       => __( 'Portfolio Categories', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'singular_name'              => __( 'Portfolio Category', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'menu_name'                  => __( 'Portfolio Categories', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'name_admin_bar'             => __( 'Portfolio Category', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'search_items'               => __( 'Search Portfolio Categories', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'popular_items'              => __( 'Popular Portfolio Categories',  THEMEPLAZAPORTFOLIO_DOMAIN ),
			'all_items'                  => __( 'All Portfolio Categories', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'edit_item'                  => __( 'Edit Portfolio Category', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'view_item'                  => __( 'View Portfolio Category', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'update_item'                => __( 'Update Portfolio Category', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'add_new_item'               => __( 'Add New Portfolio Category', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'new_item_name'              => __( 'New Portfolio Category Name', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'separate_items_with_commas' => __( 'Separate Portfolio Categories with commas', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'add_or_remove_items'        => __( 'Add or remove Portfolio Categories', THEMEPLAZAPORTFOLIO_DOMAIN ),
			'choose_from_most_used'      => __( 'Choose from the most used Portfolio Categories', THEMEPLAZAPORTFOLIO_DOMAIN ),
		)
	);

	/* Register the 'portfolio' taxonomy. */
	register_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );
}

?>