<?php
/**
 * Various functions, filters, and actions used by the plugin.
 *
 * @package themeplaza_portfolio
 * @subpackage includes
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Loads necessary scripts and styles */
add_action( 'admin_enqueue_scripts', 'themeplaza_portfolio_load_assets' );
add_action('wp_enqueue_scripts', 'themeplaza_portfolio_frontend_scripts_styles'); 

/* Filters the post type permalink. */
add_filter('post_type_link', 'themeplaza_portfolio_post_type_link_filter_function', 10, 2 );

// Adds templates for single and archive pages
add_action( 'template_redirect', 'themeplaza_portfolio_template_redirect' ); 

// Adds the shortcode
add_shortcode('portfolio', 'themeplaza_portfolio_shortcode');

/**
 * Returns the default settings for the plugin.
 *
 * @access public
 * @return array
 */
function themeplaza_portfolio_get_default_settings() {

	$settings = array(
		'portfolio_root'      		=> 'portfolio',
		'portfolio_base'      		=> 'portfolio',          // defaults to 'portfolio_root'
		'portfolio_item_base' 		=> '%portfolio%',
		'portfolio_category_base'	=> '%portfolio_category%',
		'portfolio_category_root'   => 'portfolio_category',
	);

	return $settings;
}

/**
 * Enqueues scripts and styles
 *
 * @access public
 * @return void
 */
function themeplaza_portfolio_load_assets( $hook_suffix ) {
	if ( false === strpos( $hook_suffix, 'themeplaza_portfolio' ) )
		return;

    wp_enqueue_style( 'themeplaza_portfolio_admin', THEMEPLAZAPORTFOLIO_URI . 'assets/css/themeplaza-portfolio-admin.css', array(), THEMEPLAZAPORTFOLIO_VERSION, 'all' );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'jquery-ui-sortable' );
    wp_enqueue_script( 'themeplaza_portfolio_admin', THEMEPLAZAPORTFOLIO_URI . 'assets/js/themeplaza-portfolio-admin.js', array('jquery', 'jquery-ui-sortable'), THEMEPLAZAPORTFOLIO_VERSION, true );
}

/**
 * Portfolio Frontend styles and scripts
 * @access public
 * @return void                                            
 */
function themeplaza_portfolio_frontend_scripts_styles(){
	wp_enqueue_script( 'jquery' );

    wp_register_style('themeplaza_portfolio', THEMEPLAZAPORTFOLIO_URI.'assets/css/themeplaza_portfolio.css', array(), THEMEPLAZAPORTFOLIO_VERSION, 'all' );
    wp_enqueue_style( 'themeplaza_portfolio');

    wp_register_script('themeplaza_portfolio_isotope', THEMEPLAZAPORTFOLIO_URI.'assets/js/jquery.isotope.min.js', array('jquery'), THEMEPLAZAPORTFOLIO_VERSION, true);
    wp_enqueue_script('themeplaza_portfolio_isotope');
}

/**
 * Filter on 'post_type_link' to allow users to use '%portfolio_category%' (the 'portfolio' taxonomy) in their 
 * portfolio item URLs.
 *
 * @access public
 * @param  string $post_link
 * @param  object $post
 * @return string
 */
function themeplaza_portfolio_post_type_link_filter_function( $post_link, $post ) {

	if ( 'portfolio' !== $post->post_type )

		return $post_link;
	if ( $terms = get_the_terms( $post->ID, 'portfolio_category' ) ) {
		$post_link = str_replace( '%portfolio_category%', current( $terms )->slug, $post_link );
	}
	else {
		$post_link = str_replace( '%portfolio_category%', 'item', $post_link );
	}

	return $post_link;
}

/**
 * Portfolio template files - POST and ARCHIVE
 * @access public
 * @return void                                     
 */

if ( ! function_exists( 'themeplaza_portfolio_template_redirect' ) ) {
	function themeplaza_portfolio_template_redirect() {
		/*global $wp_query, $post, $posts;
		if( get_query_var( 'portfolio')) {

              if ( file_exists( get_template_directory().'/single-portfolio.php' )) {
                  include( get_template_directory().'/single-portfolio.php' );
              } else {
                  include( THEMEPLAZAPORTFOLIO_DIR.'templates/single-portfolio.php' );
              }
			exit();
		}
		elseif( get_query_var( 'portfolio_category' ) || get_query_var( 'post_type' ) == 'portfolio' ) {

			if ( file_exists( get_template_directory().'/taxonomy-portfolio_category.php' )) {
				include( get_template_directory().'/taxonomy-portfolio_category.php' );
			} else {
				include( THEMEPLAZAPORTFOLIO_DIR.'templates/archive-portfolio.php' );
			}
			exit();
		}
		//  ||  (!get_query_var( 'portfolio' ) && !get_query_var( 'taxonomy' )) */

	}
}

/**
 * Portfolio SHORTCODE 
 * @access public
 * @return string            
 */
function themeplaza_portfolio_shortcode( $atts ) {
	ob_start();
	if ( file_exists( get_template_directory().'/themeplaza/portfolio/portfolio-shortcode.php' )) {
        include( get_template_directory().'/themeplaza/portfolio/portfolio-shortcode.php' );
    } else {
        include( THEMEPLAZAPORTFOLIO_DIR.'templates/portfolio-shortcode.php' );
    }
    return ob_get_clean();
}

/**
 * Pagination 
 * @access public
 * @return string 
 */
function themeplaza_portfolio_pagination() {
    if (function_exists('themeplaza_portfolio_pagination')) {
        global $wp_query;
        $big = 999999999;
        echo '<div class="themeplaza_portfolio_paging">'.paginate_links(array(
            'type' => 'list',
            'current' => 0,
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages
        )).'</div>';
    } else {
        wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', THEMEPLAZAPORTFOLIO_DIR ), 'after' => '</div>' ) );
    }
}


