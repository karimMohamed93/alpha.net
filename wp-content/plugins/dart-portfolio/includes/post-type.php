<?php
/**
 * File for registering custom post types.
 *
 * @package themeplaza_portfolio
 * @subpackage includes
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ThemeplazaPortfolioCPT
{	
	function __construct()
	{
		/* Register custom post types on the 'init' hook. */
		add_action( 'init', array( &$this, 'tp_register_post_type' ) );	
	}

	/**
	 * Registers post types needed by the plugin.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function tp_register_post_type() {

		/* Get the plugin settings. */
		$settings = themeplaza_portfolio_get_default_settings();

		/* Set up the arguments for the portfolio item post type. */
		$args = array(
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-portfolio',
			'can_export'          => true,
			'hierarchical'        => false,
			'has_archive'         => false,
			'query_var'           => true,
			'capability_type'     => 'post',
			'_builtin'				=> false,
			'map_meta_cap'        => false,
			'capabilities' => array(

				// meta caps (don't assign these to roles)
				'edit_post'              => 'edit_portfolio_items',
				'read_post'              => 'read_portfolio_items',
				'delete_post'            => 'delete_portfolio_items',

				// primitive caps used outside of map_meta_cap()
				'edit_posts'             => 'edit_portfolio_items',
				'edit_others_posts'      => 'edit_portfolio_items',
				'publish_posts'          => 'read_portfolio_items',
				'read_private_posts'     => 'read_portfolio_items',

				// primitive caps used inside of map_meta_cap()
				'read'                   => 'read_portfolio_items',
				'delete_posts'           => 'delete_portfolio_items',
				'delete_private_posts'   => 'delete_portfolio_items',
				'delete_published_posts' => 'delete_portfolio_items',
				'delete_others_posts'    => 'delete_portfolio_items',
				'edit_private_posts'     => 'edit_portfolio_items',
				'edit_published_posts'   => 'edit_portfolio_items'
			),

			/* The rewrite handles the URL structure. */
			'rewrite' => array(
				'slug'       => "{$settings['portfolio_root']}/{$settings['portfolio_category_base']}",
				'with_front' => false,
				'pages'      => true,
				'feeds'      => true,
				'ep_mask'    => EP_PERMALINK,
			),

			/* What features the post type supports. */
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'comments',
				'thumbnail',
				'revisions'
			),

			/* Labels used when displaying the posts. */
			'labels' => array(
				'name'               => __( 'Portfolio Items',                   THEMEPLAZAPORTFOLIO_DOMAIN ),
				'singular_name'      => __( 'Portfolio Item',                    THEMEPLAZAPORTFOLIO_DOMAIN ),
				'menu_name'          => __( 'Portfolio',                         THEMEPLAZAPORTFOLIO_DOMAIN ),
				'name_admin_bar'     => __( 'Portfolio Item',                    THEMEPLAZAPORTFOLIO_DOMAIN ),
				'add_new'            => __( 'Add New',                           THEMEPLAZAPORTFOLIO_DOMAIN ),
				'add_new_item'       => __( 'Add New Portfolio Item',            THEMEPLAZAPORTFOLIO_DOMAIN ),
				'edit_item'          => __( 'Edit Portfolio Item',               THEMEPLAZAPORTFOLIO_DOMAIN ),
				'new_item'           => __( 'New Portfolio Item',                THEMEPLAZAPORTFOLIO_DOMAIN ),
				'view_item'          => __( 'View Portfolio Item',               THEMEPLAZAPORTFOLIO_DOMAIN ),
				'search_items'       => __( 'Search Portfolio',                  THEMEPLAZAPORTFOLIO_DOMAIN ),
				'not_found'          => __( 'No portfolio items found',          THEMEPLAZAPORTFOLIO_DOMAIN ),
				'not_found_in_trash' => __( 'No portfolio items found in trash', THEMEPLAZAPORTFOLIO_DOMAIN ),
				'all_items'          => __( 'Portfolio Items',                   THEMEPLAZAPORTFOLIO_DOMAIN ),
			)
		);

		/**
		 * Portfolio update messages.
		 *
		 * See /wp-admin/edit-form-advanced.php
		 *
		 * @param array $messages Existing portfolio update messages.
		 *
		 * @return array Amended portfolio update messages with new CPT update messages.
		 */
		function portfolio_updated_messages( $messages ) {
			$post             = get_post();
			$post_type        = get_post_type( $post );
			$post_type_object = get_post_type_object( $post_type );

			$messages['portfolio'] = array(
				0  => '', // Unused. Messages start at index 1.
				1  => __( 'Portfolio Item updated.', THEMEPLAZAPORTFOLIO_DOMAIN ),
				2  => __( 'Custom field updated.', THEMEPLAZAPORTFOLIO_DOMAIN ),
				3  => __( 'Custom field deleted.', THEMEPLAZAPORTFOLIO_DOMAIN ),
				4  => __( 'Portfolio Item updated.', THEMEPLAZAPORTFOLIO_DOMAIN ),
				/* translators: %s: date and time of the revision */
				5  => isset( $_GET['revision'] ) ? sprintf( __( 'Portfolio Item restored to revision from %s', THEMEPLAZAPORTFOLIO_DOMAIN ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6  => __( 'Portfolio Item published.', THEMEPLAZAPORTFOLIO_DOMAIN ),
				7  => __( 'Portfolio Item saved.', THEMEPLAZAPORTFOLIO_DOMAIN ),
				8  => __( 'Portfolio Item submitted.', THEMEPLAZAPORTFOLIO_DOMAIN ),
				9  => sprintf(
					__( 'Portfolio Item scheduled for: <strong>%1$s</strong>.', THEMEPLAZAPORTFOLIO_DOMAIN ),
					// translators: Publish box date format, see http://php.net/date
					date_i18n( __( 'M j, Y @ G:i', THEMEPLAZAPORTFOLIO_DOMAIN ), strtotime( $post->post_date ) )
				),
				10 => __( 'Portfolio Item draft updated.', THEMEPLAZAPORTFOLIO_DOMAIN )
			);

			if ( $post_type_object->publicly_queryable ) {
				$permalink = get_permalink( $post->ID );

				$view_link = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ), __( 'View Portfolio Item', THEMEPLAZAPORTFOLIO_DOMAIN ) );
				$messages['portfolio'][1] .= $view_link;
				$messages['portfolio'][6] .= $view_link;
				$messages['portfolio'][9] .= $view_link;

				$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
				$preview_link = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ), __( 'Preview Portfolio Item', THEMEPLAZAPORTFOLIO_DOMAIN ) );
				$messages['portfolio'][8]  .= $preview_link;
				$messages['portfolio'][10] .= $preview_link;
			}

			return $messages;
		}

		/* Register the portfolio item post type. */
		register_post_type( 'portfolio', $args );
		add_filter( 'post_updated_messages', 'portfolio_updated_messages' );
	}
}

new ThemeplazaPortfolioCPT;

?>