<?php // Dart related shortcode files

define ( 'JS_PATH' , get_template_directory_uri().'/library/functions/shortcodes/shortcode.js');

function dart_addbuttons() {
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
		return;

	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "add_alc_custom_tinymce_plugin");
		add_filter('mce_buttons', 'register_alc_custom_button');
	}
}
function register_alc_custom_button($buttons) {
	array_push(
		$buttons,
		"dart"
	);
	return $buttons;
}

function add_alc_custom_tinymce_plugin($plugin_array) {
	$plugin_array['dartShortcodes'] = JS_PATH;
	return $plugin_array;
}
add_action('init', 'dart_addbuttons');

function sumtips_add_dfe_buttons($buttons)
{
	$buttons[] = 'separator'; //Add separator (optional)
	$buttons['dartShortcodes'] = array(
		'title' => esc_html__('Dart Shortcodes', "dart"), //Button Title
		'onclick' => "tinyMCE.execCommand('alc_dart');", //Command to execute
		'both' => true // Show in visual mode. Set 'true' to show in both visual and HTML mode
	);

	return $buttons;
}
add_filter( 'wp_fullscreen_buttons', 'sumtips_add_dfe_buttons' );
/********************* PANEL **********************/

function alc_panel( $atts, $content = null ) {
	extract(shortcode_atts(array(
		"anim"=>'',
		"color" => 'panel-default',
		"head"=>'',
		"footer"=>'',
		"class"=>''
	), $atts));
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$out = '<div class="panel '.$color.' '.$anim.' '.$class.'">';
	if ($head) $out.='<div class="panel-heading"><span class="panel-title">'.$head.'</span></div>';
	$out.='<div class="panel-body">'.do_shortcode ($content).'</div>';
	if($footer) $out.= '<div class="panel-footer"><span class="panel-title">'.$footer.'</span></div>';
	$out.='</div>';
	return $out;
}
add_shortcode('panel', 'alc_panel');

/**************************************************/

/***************** PROGRESS BAR *******************/

function alc_progressbar( $atts, $content = null ) {
	extract(shortcode_atts(array(
		"anim"=>'',
		"color"=>'info',
		"customcolor" => '',
		"meter" => '10',
		"style" => '',
		"animated" => '',
		"title" => '',
		"class"=>''
	), $atts));
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	if ($customcolor) $customcolor = '; background-color:'.$customcolor.' !important';
	$out='';
	$out.='<div class="progress '.$style.' '.$animated.' '.$class.'  '.$anim.'">
  			<div class="progress-bar progress-bar-'.$color.'  " role="progressbar" aria-valuenow="'.$meter.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$meter.'% '.$customcolor.'">';
	if($title){
		$out.=$title;
	}
	$out.='</div>
		</div>';
	return $out;
}
add_shortcode('progressbar', 'alc_progressbar');

/************************************************/


/************** Circular Progress Bar ***********/

function alc_circle( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'anim' => '',
		'title' => '',
		'meter' => '',
		'class' => '',
		'piebarcolor' => '#ef1e25',
		'pietrackcolor' => '#f2f2f2',
	), $atts));

	$randomID=  mt_rand(0, 10000);
	$anim=(!empty($anim)) ? 'animation '.$anim : '';

	$out = '<div class="skills '.$anim.' '.$class.'">
				<div class="pie-'.$randomID.' percentage easyPieChart" data-percent="'.$meter.'">
					<span>'.$meter.'</span>%
					<canvas width="165" height="165"></canvas>
				</div>
				<small>'.$title.'</small>
			</div>';
	$out.='<script type="text/javascript">
				jQuery(document).ready(function(){
					if(jQuery(".pie-'.$randomID.'").length){
						jQuery(".pie-'.$randomID.'").easyPieChart({
							animate: 5000,
							barColor:"'.$piebarcolor.'",
							trackColor:"'.$pietrackcolor.'",
							onStep: function(value) {
								this.$el.find("span").text(~~value);
							}
						});
					}
				})
			</script>';
	return $out;
}
add_shortcode('circle', 'alc_circle');

/**************************************************/

/*************** Dropdown buttons *****************/

function alc_dropbutton_group( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => '',
		'type'	=> '',
		'color' => '',
		'anim'=>'',
		'class'=>'',
	), $atts));
	$GLOBALS['dropbutton_count'] = 0;
	$randomId = mt_rand(0, 100000);
	$return = '';
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	do_shortcode( $content );
	$counter = 1;
	if( is_array( $GLOBALS['dropbuttons'] ) ){
		foreach( $GLOBALS['dropbuttons'] as $dropbutton ){
			$dropbuttons[] = '<li><a href="'.$dropbutton['url'].'">'.do_shortcode($dropbutton['title']).'</a></li>';
			if ($dropbutton['divider'] == 1)
			{
				$dropbuttons[] = '<li class="divider"></li>';
			}
		}

		if ($type == 'split')
		{
			$return.='
			<div class="btn-group '.$anim.' '.$class.'">
				<button class="btn '.$color.' ">'.$title.'</button>
				<button class="btn dropdown-toggle '.$color.'" data-toggle="dropdown">
					<span class="caret "></span>
				</button>';
		}
		else
		{
			$return.= '
			<div class="btn-group '.$anim.' '.$class.'">
				 <a class="btn btn-default dropdown-toggle '.$color.'" data-toggle="dropdown" href="#">'.$title.'<span class="caret iconright"></span></a>';
		}
		$return.= '<ul class="dropdown-menu" id="'.$randomId.'">'.implode( "\n", $dropbuttons ).'</ul>';
		$return.= '</div>';
		unset($GLOBALS['dropbuttons']);
	}
	return $return;
}
add_shortcode( 'dropbuttongroup', 'alc_dropbutton_group' );

/**************************************************/


/**************** DROPDOWN BUTTON *****************/

function alc_dropbutton( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => '',
		'url' => '',
		'divider' => '',
	), $atts));

	$x = $GLOBALS['dropbutton_count'];
	$GLOBALS['dropbuttons'][$x] = array( 'title' => $title, 'url' => $url, 'divider' => $divider, 'content' =>  $content );

	$GLOBALS['dropbutton_count']++;
}

add_shortcode( 'dropbutton', 'alc_dropbutton' );

/************************************************/

/******************* BUTTONS ********************/

function alc_button( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'size' => 'btn-lg',
		'link' => '#',
		'color' => 'btn-default',
		'status'=>'',
		'target' => '',
		'ipos'=>'icon-left',
		'icon'=>'',
		'anim'=>'',
		'class'=>'',
	), $atts));

	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$target = ($target) ? ' target="_blank"' : '';
	if($icon && $ipos=='icon-right'){
		$output=do_shortcode($content).'<i class="fa '.$icon.' '.$ipos.' "></i>';
	}elseif($icon && $ipos=='icon-left'){
		$output='<i class="fa '.$icon.' '.$ipos.' "></i>'.do_shortcode($content);
	}else{
		$output=do_shortcode($content);
	}
	$out = '<a href="'.$link.'" '.$target.' class="btn  '.$size.' '.$color.' '.$status.' '.$class.' '.$anim.'" >'.$output.'</a>';
	return $out;
}
add_shortcode('button', 'alc_button');

/************************************************/


/****************** TABS ************************/

function alc_tab_group( $atts, $content ){
	extract(shortcode_atts(array(
		'style'=>'nav-tabs',
		'anim'=>'',
		'class'=>'',
	), $atts));
	$GLOBALS['tab_count'] = 0;
	do_shortcode( $content );

	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$randomId = mt_rand(0, 100000);
	$counter = 0;
	if( is_array( $GLOBALS['tabs'] ) ){
		foreach( $GLOBALS['tabs'] as $tab ){
			$active = ($counter == 0) ? ' active' : '';
			$tabs[] = '<li class="'.$active.'"><a href="#tabs-'.$randomId.'" data-toggle="tab">';
			if($style=='nav-stacked'){
				if($tab['icon']) {$tabs[].='<span class="tab-icon pull-left"><i class="fa '.$tab['icon'].' pull"></i></span>';}
				$tabs[].='<div class="tab-info">
							  	<h3>'. $tab['title'].'</h3>
							  	<p>'. $tab['sectitle'].'</p>
						  	</div>';
			}else{
				$tabs[].= $tab['title'];
			}
			$tabs[].='</a></li>';
			$tabcontent[] = '<div id="tabs-'.$randomId.'" class="tab-pane '.$active.'">'.do_shortcode($tab['content']).'</div>';
			$randomId++;
			$counter ++;
		}
		$return='';
		if($style=='nav-stacked'){
			$return.='<div class="featured-tab clearfix">';
			$return.= '<ul class="nav nav-tabs '. $style.' '.$anim.' '.$class.' col-md-4">'.implode( "\n", $tabs ).'</ul>';
			$return.= '<div class="tab-content col-md-8">'.implode( "\n", $tabcontent ).'</div>';
			$return.='</div>';
		}
		else{
			$return.='<div class="tab-default">';
			$return.= '<ul class="nav '. $style.' '.$anim.' '.$class.'">'.implode( "\n", $tabs ).'</ul>';
			$return.= '<div class="tab-content">'.implode( "\n", $tabcontent ).'</div>';
			$return.='</div>';
		}
		unset($GLOBALS['tabs']);
	}
	return $return;
}
add_shortcode( 'tabgroup', 'alc_tab_group' );
add_shortcode( 'av_tabs', 'alc_tab_group' );



function alc_tab( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => 'Tab %d',
		'sectitle'=>'',
		'icon'=>''

	), $atts));

	$x = $GLOBALS['tab_count'];
	$GLOBALS['tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['tab_count'] ), 'content' =>  $content, 'icon'=>$icon, 'sectitle'=>$sectitle );

	$GLOBALS['tab_count']++;
}
add_shortcode( 'tab', 'alc_tab' );
add_shortcode( 'av_tab_item', 'alc_tab' );


/************************************************/
/*************** Vertical Navigation ************/
function alc_vernav_group( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => '',
		'anim'=>'',
		'class'=>'',
	), $atts));
	$GLOBALS['vernav_count'] = 0;
	do_shortcode( $content );
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$return = '<div class="vernav '.$anim.' '.$class.'" style="max-width: 340px; padding: 8px 0"><ul class="nav nav-list">';
	if (!empty($title)) $return.='<li class="nav-header">'.$title.'</li>';
	if( is_array( $GLOBALS['vernavs'] ) ){
		foreach( $GLOBALS['vernavs'] as $vernav ){
			$vernavs[] = ' 
				<li><a href="'.$vernav['link'].'">'.$vernav['title'].'</a></li>';
		}
		$return.=implode( "\n", $vernavs );
		$return.= '</ul></div>';
		unset($GLOBALS['vernavs']);
	}
	return $return;
}
add_shortcode( 'vernavgroup', 'alc_vernav_group' );


function alc_vernav( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => 'Nav %d',
		'link'	=> ''
	), $atts));

	$x = $GLOBALS['vernav_count'];
	$GLOBALS['vernavs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['vernav_count'] ), 'content' =>  $content, 'link' =>  $link );

	$GLOBALS['vernav_count']++;
}
add_shortcode( 'vernav', 'alc_vernav' );

/*************************************************/


/***************** ACCORDION ********************/


function alc_accordion_group( $atts, $content ){
	extract(shortcode_atts(array(
		'type'=>'',
		'anim'=>'',
		'class'=>'',
	), $atts));
	$GLOBALS['accordion_count'] = 0;
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$counter = 0;
	$accId=  mt_rand(0, 100);
	do_shortcode( $content );

	if($type==1){
		$acc='data-parent="#accordion-'.$accId.'"';
	}
	else{
		$acc="";
	}

	if( is_array( $GLOBALS['accordions'] ) ){
		foreach( $GLOBALS['accordions'] as $accordion ){
			$randomId=  mt_rand(0, 10000);
			$active = ($counter == 0) ? ' ' : 'collapsed';
			$activeContent=($counter==0 )? 'in' : '';
			$accordions[] = '
				<div class="panel panel-default">		
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-toggle collapsed-icon  '.$active.' " '.$acc.'  data-toggle="collapse" href="#acc-'.$randomId.'">';
			$accordions[].= $accordion['title'].'
							</a>
						</h4>
					</div>    
					<div id="acc-'.$randomId.'" class="panel-collapse collapse '.$activeContent.'">
						<div class="panel-body">
							'.do_shortcode($accordion['content']).'
						</div>
					</div>
				</div>';
			$counter++;
		}
		$return='<div class="panel-group '.$anim.' '.$class.'" id="accordion-'.$accId.'">'.implode( "\n", $accordions ).'</div>';
		unset($GLOBALS['accordions']);
	}
	return $return;
}

add_shortcode( 'accordiongroup', 'alc_accordion_group' );
/***************/

function alc_accordion( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => 'accordion %d',
	), $atts));

	$x = $GLOBALS['accordion_count'];
	$GLOBALS['accordions'][$x] = array( 'title' => sprintf( $title, $GLOBALS['accordion_count'] ), 'content' =>  $content);

	$GLOBALS['accordion_count']++;
}

add_shortcode( 'accordion', 'alc_accordion' );
/************************************************/


/*************** TESTIMONIALS ********************/

function alc_testimonial_group( $atts, $content ){
	extract(shortcode_atts(array(
		'anim'=>'',
		'class'=>'',
		'nav'=>'true',
		'direction'=>'left',
		'auto'=>'true',
		'interval'=>'6000'

	), $atts));
	$GLOBALS['testimonial_count'] = 0;
	$counter = 0;
	$randomId = mt_rand(0, 100000);
	do_shortcode( $content );

	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$return='<div id="testimonial-carousel-'.$randomId.'" class="owl-carousel owl-theme text-center testimonial-slide '.$anim.' '.$class.'">';
	if( is_array( $GLOBALS['testimonials'] ) ){
		foreach( $GLOBALS['testimonials'] as $testimonial ){
			$imglink='';
			if(function_exists('vc_map')){
				$limage = wp_get_attachment_image_src($testimonial['photo'], 'full');
				$imglink.=$limage[0];
			}else{
				$imglink=$testimonial['photo'];
			}
			$testimonials[] = '<div class="item">
												<div class="testimonial-thumb">
												  <img class="img-circle" src="'.$imglink.'" alt="'.$testimonial['title'].'">
												</div>
												<div class="testimonial-content">
												  <h3 class="name">'.$testimonial['title'].'<span class="'.$testimonial['color'].'">'.$testimonial['position'].'</span></h3>
												  <p class="testimonial-text '.$testimonial['color'].'">'.do_shortcode($testimonial['content']).' </p>
												</div>
										</div>';
			$counter++;
		}
		$return.= implode( "\n", $testimonials );
		unset($GLOBALS['testimonials']);
	}
	$return.='</div>';
	if($nav=='true'){
		$return.='<div class="customNavigation dart-carousel-controller">
				<a class="prev left">
					<i class="fa fa-chevron-left"></i>
				</a>
				<a class="next right">
				  <i class="fa fa-chevron-right"></i>
				</a>
			</div>';
	}
	$rtl= weblusive_get_option('rtl_mode');
	if($rtl){
		$rtl='true';
	}else{
		$rtl='false';
	}
	$return.='<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#testimonial-carousel-'.$randomId.'").owlCarousel({
			nav : '.$nav.', // Show next and prev buttons
			slideSpeed : 600,
			pagination:false,
			singleItem:true,
			autoplay: '.$auto.',
			autoplayTimeout:'.$interval.',
			rtl	:'.$rtl.',
			navText: "",
			items : 1
		});
	});
	// Custom Navigation Events
    var owl = jQuery("#testimonial-carousel-'.$randomId.'");

    // Custom Navigation Events
    jQuery(".next").click(function(){
      owl.trigger("owl.next");
    })
    jQuery(".prev").click(function(){
      owl.trigger("owl.prev");
    })

	</script>';

	return $return;
}

add_shortcode( 'testimonialgroup', 'alc_testimonial_group' );
add_shortcode( 'av_testimonial', 'alc_testimonial_group' );


function alc_testimonial( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => '',
		'position' => '',
		'photo'=>'',
		'color'=>''
	), $atts));

	$x = $GLOBALS['testimonial_count'];
	$GLOBALS['testimonials'][$x] = array( 'title' => sprintf( $title, $GLOBALS['testimonial_count'] ), 'position' => $position, 'photo' => $photo, 'color' => $color,  'content' =>  $content );

	$GLOBALS['testimonial_count']++;
}

add_shortcode( 'testimonial', 'alc_testimonial' );
add_shortcode( 'av_testimonial_item', 'alc_testimonial' );
/************************************************/

/******************* Alertbox *******************/

function alc_alert( $atts, $content = null ) {
	extract(shortcode_atts(array(
		"type"=>'alertdefault',
		"color" => 'alert-warning',
		"anim"=>'',
		"class"=>''
	), $atts));
	do_shortcode($content);
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$out = '<div class="alert '.$type.' '.$color.' '.$class.' '.$anim.'" role="alert">';
	if($type!=='alertdefault') {$out.='<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';}
	$out.=do_shortcode($content).'</div>';
	return $out;
}
add_shortcode('alert', 'alc_alert');

/************************************************/


/***********  VIDEOS  ****************/

function alc_video($atts, $content=null) {
	extract(shortcode_atts(array(
			'site' => 'youtube',
			'id' => '',
			'width' => '',
			'height' => '',
			'autoplay' => '0',
			"anim"=>'',
			"class"=>''
		), $atts)
	);
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	if ( $site == "youtube" ) { $src = 'http://www.youtube.com/embed/'.$id.'?autoplay='.$autoplay; }
	else if ( $site == "vimeo" ) { $src = 'http://player.vimeo.com/video/'.$id.'?autoplay='.$autoplay; }
	else if ( $site == "dailymotion" ) { $src = 'http://www.dailymotion.com/embed/video/'.$id.'?autoplay='.$autoplay; }
	else if ( $site == "veoh" ) { $src = 'http://www.veoh.com/static/swf/veoh/SPL.swf?videoAutoPlay='.$autoplay.'&permalinkId='.$id; }
	else if ( $site == "bliptv" ) { $src = 'http://a.blip.tv/scripts/shoggplayer.html#file=http://blip.tv/rss/flash/'.$id; }
	else if ( $site == "viddler" ) { $src = 'http://www.viddler.com/embed/'.$id.'e/?f=1&offset=0&autoplay='.$autoplay; }

	if ( $id != '' ) {
		return '<div class="flex-video '.$anim.' '.$class.'"><iframe width="'.$width.'" height="'.$height.'" src="'.$src.'" class="vid iframe-'.$site.'"></iframe></div>';
	}
}
add_shortcode('tp_video','alc_video');

/************************************************/



/****************** SLIDER ********************/
function alc_slider( $atts, $content ){
	$GLOBALS['slideritem_count'] = 0;
	extract(shortcode_atts(array(
		"anim"=>'',
		"class"=>'',
		"type"=>'single',
		"nav"=>'true',
		"pag"=>'true',
		"auto"=>'true',
		"speed" => '3000',
		"sitems"=>'3',
	), $atts));
	do_shortcode( $content );
	$randomId = mt_rand(0, 100000);
	$return = '';
	$anim=(!empty($anim)) ? 'animation '.$anim : '';

	if(isset($GLOBALS['sitems']) && is_array( $GLOBALS['sitems'] ) ){
		$icount = 0;
		foreach( $GLOBALS['sitems'] as $item ){
			$imglink='';
			if(function_exists('vc_map')){
				$limage = wp_get_attachment_image_src($item['image'], 'full');
				$imglink.=$limage[0];
			}else{
				$imglink=$item['image'];
			}
			$panes[]='';
			if($type=='single'){
				$panes[].='<li><img src="'.$imglink.'" alt="slider-img" />
							<div class="bx-caption">'.do_shortcode($item['content']).'</div>
						</li>';
			}else{
				$panes[].='<figure class="item"><img src="'.$imglink.'" alt="carousel-img" /></figure>';
			}
			$icount ++ ;
		}
		$randomId = mt_rand(0, 100000);
		if($type=='single'){
			$return.='<div class="page-slider-wrapper '.$class.' '.$anim.'">
						<ul class="bxslider-'.$randomId.'" >
							'.implode( "\n", $panes ).'
						</ul>
					</div>';
		}else{
			$return.='<div id="slide-carousel-'.$randomId.'" class="owl-carousel owl-theme text-center slide-carousel '.$class.' '.$anim.'">
						'.implode( "\n", $panes ).'
				    </div>';
		}

		unset($GLOBALS['sitems']);
	}
	$rtl= weblusive_get_option('rtl_mode');
	if($rtl){
		$rtl='true';
	}else{
		$rtl='false';
	}
	$return.='
	<script type="text/javascript">
		jQuery(document).ready(function(){';
	if($type=='single'){
		$return.='jQuery(".bxslider-'.$randomId.'").bxSlider({
						mode: "fade",
						controls: '.$nav.',
						pager: '.$pag.',
						auto: '.$auto.',';
		if($auto=='true'){$return.='speed:'.$speed.',';}
		$return.='captions: true
					});';
	}else{
		$return.='
			jQuery("#slide-carousel-'.$randomId.'").owlCarousel({
				rtl	:'.$rtl.',
				nav :'.$nav.', // Show next and prev buttons
				navText: ["<i class=\"fa fa-chevron-left\"></i>", "<i class=\"fa fa-chevron-right\"></i>"],
				slideSpeed : 800,
				pagination:false,
				items : '.$sitems.',
				autoplay: '.$auto.',
				autoplayTimeout:'.$speed.',
				rewindNav: true,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,2],
				stopOnHover:true
			});';
	}
	$return.='})
	</script>';
	return $return;
}
add_shortcode('slider', 'alc_slider' );

/****/

function alc_slideritem( $atts, $content ){
	extract(shortcode_atts(array(
		'image' => '',
		'title' => '',
	), $atts));

	$x = $GLOBALS['slideritem_count'];
	$GLOBALS['sitems'][$x] = array( 'image' => $image, 'title' => $title, 'content' =>  $content );

	$GLOBALS['slideritem_count']++;

}
add_shortcode( 'slideritem', 'alc_slideritem' );

/************************************************/


/*******************Carousel********************/

function alc_carousel( $atts, $content ){
	$GLOBALS['caritem_count'] = 0;
	extract(shortcode_atts(array(
		'style'=>'',
		'nav' => 'true',
		'auto' => 'true',
		'speed'=>'4000',
		'sitems' => '3',
		"anim"=>'',
		"class"=>''
	), $atts));
	$randomId = mt_rand(0, 100000);
	$panes = array();
	$return = '';

	do_shortcode ($content);
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	if(isset( $GLOBALS['caritems']) && is_array( $GLOBALS['caritems'] ) ){
		foreach( $GLOBALS['caritems'] as $item ){
			$panes[]='';
			if($style=='logo'){
				$panes[].='<figure class="item client_logo">'.$item['content'].' </figure>';
			}else{
				$panes[].= '<div class="item">'.$item['content'].'</div>';
			}

		}
		$carClass=($style=='logo') ? 'text-center client-carousel' : 'team-carousel team-page';
		$return.='<div id="carousel-'.$randomId.'" class="owl-carousel owl-theme '.$carClass.' '.$class.' '.$anim.'" >
						'.implode( "\n", $panes ).'
				</div>';
		unset($GLOBALS['caritems']);
	}

	$rtl= weblusive_get_option('rtl_mode');
	if($rtl){
		$rtl='true';
	}else{
		$rtl='false';
	}
	$return.='
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#carousel-'.$randomId.'").owlCarousel({
				rtl	:'.$rtl.',
				nav : '.$nav.', // Show next and prev buttons
				navText: ["<i class=\"fa fa-chevron-left\"></i>", "<i class=\"fa fa-chevron-right\"></i>"],
				slideSpeed : 800,
				pagination:false,
				items : '.$sitems.',
				autoplay: '.$auto.',
				autoplayTimeout:'.$speed.',
				rewindNav: true,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,2],
				stopOnHover:true
			});
		});
	</script>';
	return $return;
}

add_shortcode('carousel', 'alc_carousel' );
add_shortcode('av_carousel', 'alc_carousel' );

/***/

function alc_caritem( $atts, $content ){
	extract(shortcode_atts(array(
		'title' => '',
	), $atts));
	$x = $GLOBALS['caritem_count'];
	$GLOBALS['caritems'][$x] = array('title' => $title, 'content' =>  do_shortcode ($content) );
	$GLOBALS['caritem_count']++;
}
add_shortcode( 'caritem', 'alc_caritem' );
add_shortcode( 'av_caritem', 'alc_caritem' );

/************************************************/


/*************** Contact details ****************/

function alc_contact( $atts, $content = null ) {
	extract(shortcode_atts(array(
		"address" => '',
		"tel" => '',
		"email" => '',
		"website"=>'',
		"anim"=>'',
		"class"=>''
	), $atts));
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$out = '
	<ul class="contact-list '.$class.' '.$anim.'">';
	if ($address) $out.='<li><i class="fa fa-home"></i>'.$address.'</li>';
	if ($tel) $out.='<li ><i class="fa fa-phone"></i>'.$tel.'</li>';
	if ($email) $out.='<li><i class="fa fa-envelope-o"></i>'.$email.'</li>';
	if ($website) $out.='<li ><i class="fa fa-globe"></i>'.$website.'</li>';
	$out.='</ul>';
	return $out;
}
add_shortcode('contact', 'alc_contact');

/************************************************/


/************ FEATURED BLOCK****************/
function alc_fblock($atts, $content=NULL){
	extract(shortcode_atts(array(
		'anim'=>'',
		'type'=>'default',
		'title'=>'',
		'tcuscolor' => '',
		'icon'=>'',
		'image' => '',
		'icuscolor' => '',
		'icusbgcolor' => '',
		'link'=>'',
		'class'=>'',
		'divider'=>'',
	), $atts));
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$tcuscolor = (isset($tcuscolor) && !empty($tcuscolor))  ? $tcuscolor : '';
	$icusbgcolor = (isset($icusbgcolor) && !empty($icusbgcolor))  ? ' background-color:'.$icusbgcolor.' !important;' : '';
	$icuscolor = (isset($icuscolor) && !empty($icuscolor))  ? ' color:'.$icuscolor.' !important;' : '';
	$out = '';
	$imglink='';
	if(function_exists('vc_map')){
		$limage = wp_get_attachment_image_src($image, 'full');
		$imglink.=$limage[0];
	}else{
		$imglink.=$image;
	}
	if ($type == 'default') {
		$out.='<div class="service-content '.$divider.' '.$anim.' '.$class.'">';
		if($link){$out.='<a href="'.$link.'">';}
		$out.='<span class="service-icon">';
		if($imglink){
			$out.='<img alt="'.$title.'" class="fb-image img-circle" src="'.$imglink.'" />';
		}
		elseif($icon){
			$out.='<i class="fa '.$icon.' img-circle" style="visibility: visible; '.$icuscolor.' '.$icusbgcolor.'"></i>';
		}
		$out.='</span>';
		if($link){$out.='</a>';}
		$out.='<h3 style="color:'.$tcuscolor.' !important">'.$title.'</h3>
					<p>'.do_shortcode($content).'</p>
				</div>';
	}elseif ($type=='alter') {
		if($divider) {$out.='<div class="featured2 '.$divider.'">';}
		$out.='<div class="feature-box '.$anim.' '.$class.'">
					<div class="feature-icon pull-left">';
		if($imglink){
			$out.='<img alt="'.$title.'" class="fb-image img-circle" src="'.$imglink.'" />';
		}
		elseif($icon){
			$out.='<i class="fa '.$icon.'" style="'.$icuscolor.'"></i>';
		}
		$out.='	
				    </div>
					<div class="feature-box-content">
						<h3 style="color:'.$tcuscolor.' !important">'.$title.'</h3>
						<p>'.do_shortcode($content).'</p>
					</div>
				</div>';
		if($divider) {$out.='</div>';}
	}
	return $out;
}
add_shortcode('fblock', 'alc_fblock');

/*********************************************************/

/***************TITLE BLOCK***************************/
function alc_tblock($atts, $content=NULL){
	extract(shortcode_atts(array(
		'anim'=>'',
		'type'=>'default',
		'title'=>'',
		'sectext'=>'',
		'tag'=>'h1',
		'tcuscolor' => '',
		'class'=>'',
	), $atts));
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$tcuscolor = (isset($tcuscolor) && !empty($tcuscolor))  ? $tcuscolor : '';
	$out='';
	if($type=='default'){
		$out.='<div class="text-center '.$class.' '.$anim.'">
				<'.$tag.' class="title tblock" style="color:'.$tcuscolor.' !important"><span>'.$title.'</span></'.$tag.'>
			  </div>';
	}elseif ($type=='alter') {
		$out.='<'.$tag.' class="page-title '.$class.' '.$anim.'" style="color:'.$tcuscolor.' !important">'.$title.'</'.$tag.'>';
	}elseif ($type=='onepage') {
		$out.='<div class="text-center heading '.$class.' '.$anim.'">
			    		<'.$tag.' class="onetitle" style="color:'.$tcuscolor.' !important">'.$title.'</'.$tag.'>
			    		<h3 class="onesubtitle" style="color:'.$tcuscolor.' !important">'.$sectext.'</h3>
			    	</div>';
	}
	return $out;
}

add_shortcode('tblock', 'alc_tblock');

/******************************************************/


/******************** REVEAL BOX **********************/

function alc_reveal($atts, $content=NULL){
	extract(shortcode_atts(array(
		'type'=>'btn',
		'size'=>'btn-lg',
		'color'=>'btn-default',
		'button'=>'',
		'revtitle'=>'',
		'noclosebutton' => 'false',
		'class'=>''
	), $atts));
	$randomId=  mt_rand(0, 100000);

	$out='<a href="#myModal'.$randomId.'"  role="button" data-toggle="modal" class="'.$type.' '.$color.' '.$size.' '.$class.'">'.$button.'</a>';
	$out.='<div id="myModal'.$randomId.'"  class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
            <div class="modal-dialog">
            <div class="modal-content">        
            <div class="modal-header">
		<h4 id="myModalLabel">'.$revtitle.'</h4>
	  </div>
	   <div class="modal-body">
		<p>'.do_shortcode($content).'</p>
	  </div>';
	if ($noclosebutton == 'false'):
		$out.='<div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">'.esc_html__('Close', 'dart').'</button></div>';
	endif;
	$out.='</div>
          </div>
	 </div>
	';
	return $out;
}

add_shortcode('reveal', 'alc_reveal');

/*************************************************/


/************** PORTFOLIO LISTING ***************/

function alc_portlisting($atts, $content=NULL){
	extract(shortcode_atts(array(
		"limit" => 6,
		"featured" => 0,
		'class'=>'',
		'sitems' => '3',
		'type' => ''
	), $atts));
	global $post;
	$return = '';
	$counter = 0;
	$randomId=  mt_rand(0, 10000);
//	$isActive = 'active';
	$args = array('post_type' => 'portfolio', 'taxonomy'=> 'portfolio_category', 'showposts' => $limit, 'posts_per_page' => $limit, 'orderby' => 'date','order' => 'DESC');

	if ($featured)
	{
		$args['meta_key'] = '_portfolio_featured';
		$args['meta_value'] = '1';
	}

	$query = new WP_Query($args);

	$return.='<div class="owl-carousel owl-theme portlisting-'.$randomId.' '.$class.' portshort" id="portfolio-carousel">';
	if ($query->have_posts()):
		while ($query->have_posts()) :
			$query->the_post();
			$custom = get_post_custom($post->ID);
			$link = ''; $thumbnail = get_the_post_thumbnail($post->ID, 'portfolio-3-col');
			$overlay=isset($custom["_portfolio_item_overlay"][0]) ? $custom["_portfolio_item_overlay"][0] : '';
			//if ($counter == 0 || $counter % 4 == 0): $return.='<div class="item"><div class="row">'; endif;
			$return.='<div class="item">
											<div class="thumbnail">';

			$return.='<div class="caption">
												<div class="caption-content">
												<span>';
			if (!empty($custom['_portfolio_video'][0])) : $link = $custom['_portfolio_video'][0];
				$return.='<a href="'.$link .'" data-rel="prettyPhoto" title="'.get_the_title().'">
																<i class="fa fa-film"></i>
															</a>';
				if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :
					$return.='<a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title().'">
																<i class="fa fa-link"></i>
															</a>';
				endif;
			elseif (isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '') :
				$return.='<a href="'.$custom['_portfolio_link'][0].'"  title="'.get_the_title().'">
																<i class="fa fa-external-link"></i>
															</a>';
				if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :
					$return.='<a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title().'">
																<i class="fa fa-link"></i>
															</a>';
				endif;
			elseif (isset($custom['_portfolio_no_lightbox'][0]) && $custom['_portfolio_no_lightbox'][0] != '') : $link = get_permalink(get_the_ID());
				$return.='<a href="'.$link.'"  title="'.get_the_title().'">
																<i class="fa fa-link"></i>
															</a>';
			else :
				$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
				$link = $full_image[0];
				$return.='<a data-rel="prettyPhoto" href="'.$link.'" title="'.get_the_title().'">
																<i class="fa fa-search"></i>
															</a>';
				if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :
					$return.='<a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title().'">
																<i class="fa fa-link"></i>
															</a>';
				endif;
			endif;
			$return.='</span>
													<h3 class="">'.get_the_title().'</h3>
													<p>'.limit_words(get_the_excerpt(), 15).'</p>
												</div>
											</div>';
			if (!empty($thumbnail)):
				$return.=$thumbnail;
			else :
				$return.='<img src = "http://placehold.it/263x351.jpg" alt="' . esc_html__('No image', 'dart') . '" />';
			endif;
			$return.='</div>	                                                
									</div>';
			//if ($counter >0 && ($counter+1) % 4 == 0): $return.='</div></div>'; endif;
			$counter ++; endwhile;
	endif;
	$return.='</div>';
	$rtl= weblusive_get_option('rtl_mode');
	if($rtl){
		$rtl='true';
	}else{
		$rtl='false';
	}
	$return.='
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".portlisting-'.$randomId.'").owlCarousel({
				rtl	:'.$rtl.',
				navigationText: ["<i class=\"fa fa-chevron-left\"></i>", "<i class=\"fa fa-chevron-right\"></i>"],
				nav : true, // Show next and prev buttons
				
				slideSpeed : 800,
				pagination:false,
				items : '.$sitems.',
				rewindNav: true,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,2],
				stopOnHover:true
			});';
	$return.='
		});
	</script>';

	return $return;
}

add_shortcode('portlist', 'alc_portlisting');
/*************************************************/

function alc_portrelated($atts, $content=NULL){
	extract(shortcode_atts(array(
		"limit" => 3,
		"featured" => 0,
		'anim'=>'',
		'class'=>'',
		'postid' => ''
	), $atts));
	global $post;
	$return = '';
	$counter = 0;
	$isActive = '';
	$randomId=  mt_rand(0, 10000);

	$terms = wp_get_object_terms($postid, 'portfolio_category');
	if (count($terms)) {
		$post_ids = get_objects_in_term($terms[0]->term_id, 'portfolio_category');
		$post = get_post($postid);
		$args = array('post_type' => 'portfolio', 'taxonomy'=> 'portfolio_category', 'showposts' => $limit, 'posts_per_page' => $limit, 'orderby' => 'date','order' => 'DESC', 'post__in' => $post_ids, 'exclude' => $postid, 'term' => $terms[0]->slug);

		if ($featured)
		{
			$args['meta_key'] = '_portfolio_featured';
			$args['meta_value'] = '1';
		}

		$query = new WP_Query($args);
		$return.='<div class="owl-carousel owl-theme portlisting-'.$randomId.' '.$class.' portshort" id="portfolio-carousel">';
		if ($query->have_posts()):
			while ($query->have_posts()) :
				$query->the_post();
				$custom = get_post_custom($post->ID);
				$link = ''; $thumbnail = get_the_post_thumbnail($post->ID, 'portfolio-3-col');
				$overlay=isset($custom["_portfolio_item_overlay"][0]) ? $custom["_portfolio_item_overlay"][0] : '';
				//if ($counter == 0 || $counter % 4 == 0): $return.='<div class="item'.$isActive.'">'; endif;
				$return.='<div class="item">
											<div class="thumbnail">';

				$return.='<div class="caption">
												<div class="caption-content">
												<span>';
				if (!empty($custom['_portfolio_video'][0])) : $link = $custom['_portfolio_video'][0];
					$return.='<a href="'.$link .'" data-rel="prettyPhoto" title="'.get_the_title().'">
																<i class="fa fa-film"></i>
															</a>';
					if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :
						$return.='<a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title().'">
																<i class="fa fa-link"></i>
															</a>';
					endif;
				elseif (isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '') :
					$return.='<a href="'.$custom['_portfolio_link'][0].'"  title="'.get_the_title().'">
																<i class="fa fa-external-link"></i>
															</a>';
					if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :
						$return.='<a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title().'">
																<i class="fa fa-link"></i>
															</a>';
					endif;
				elseif (isset($custom['_portfolio_no_lightbox'][0]) && $custom['_portfolio_no_lightbox'][0] != '') : $link = get_permalink(get_the_ID());
					$return.='<a href="'.$link.'"  title="'.get_the_title().'">
																<i class="fa fa-link"></i>
															</a>';
				else :
					$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
					$link = $full_image[0];
					$return.='<a data-rel="prettyPhoto" href="'.$link.'" title="'.get_the_title().'">
																<i class="fa fa-search"></i>
															</a>';
					if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :
						$return.='<a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title().'">
																<i class="fa fa-link"></i>
															</a>';
					endif;
				endif;
				$return.='</span>
													<h3 class="">'.get_the_title().'</h3>
													<p>'.limit_words(get_the_excerpt(), 15).'</p>
												</div>
											</div>';
				if (!empty($thumbnail)):
					$return.=$thumbnail;
				else :
					$return.='<img src = "http://placehold.it/263x351.jpg" alt="' . esc_html__('No image', 'dart') . '" />';
				endif;
				$return.='</div>	                                                
									</div>';
				//if ($counter >0 && ($counter+1) % 4 == 0): $return.='</div>'; endif;
				$counter ++; endwhile;
		endif;
		$return.='</div>';

		$rtl= weblusive_get_option('rtl_mode');
		if($rtl){
			$rtl='true';
		}else{
			$rtl='false';
		}
		$return.='
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery(".portlisting-'.$randomId.'").owlCarousel({
					rtl	:'.$rtl.',
					nav : true, // Show next and prev buttons
					navigationText: ["<i class=\"fa fa-chevron-left\"></i>", "<i class=\"fa fa-chevron-right\"></i>"],
					slideSpeed : 800,
					pagination:false,
					items : 4,
					rewindNav: true,
					itemsDesktop : [1199,3],
					itemsDesktopSmall : [979,2],
					stopOnHover:true
				});';
		$return.='
			});
		</script>';
	}
	return $return;
}

add_shortcode('portrelated', 'alc_portrelated');
/*************************************************/


/************** SHOP PRODUCT CAROUSEL ************/

function alc_productcar($atts, $content=NULL){

	static $id =0;
	$id++;
	$woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 3);
	extract(shortcode_atts(array(
			'anim'	=> '',
			'prod_ids'		=> '',
			'prod_tags'		=> '',
			'cat_ids' =>'',
			'cat_slugs' => '',
			'limit'	=> '12',
			'automatic' => "false",
			'items' => "4",
			'interval' => "2500",
			'showarrows' => "true",
			'orderby'       => 'menu_order',
			'order'         => 'desc',
			'class'=>''
		), $atts)
	);

	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$return = '';
	$counter = 0;
	if($automatic=='true'){
		$out=' data-carousel-autoplay="'.$interval.'"';
	}else{
		$out='data-carousel-autoplay="false"';
	}
	if($cat_slugs !='') {

		if($cat_slugs!='') {
			//get the products based on the cat slugs
			$loop = new WP_Query(array(
				'orderby' => $orderby,
				'order'   => $order,
				'post_type'	=> 'product',
				'posts_per_page'=>$limit,
				'tax_query' => array(
					array(
						'taxonomy' => 'product_cat',
						'field' => 'slug',
						'terms'=>explode(',',$cat_slugs)

					))
			));

		}

	}elseif($cat_ids !='') {

		if($cat_ids!='') {
			//get the products based on the cat IDs
			$loop = new WP_Query(array(
				'orderby' => $orderby,
				'order'   => $order,
				'post_type'	=> 'product',
				'posts_per_page'=>$limit,
				'tax_query' => array(
					array(
						'taxonomy' => 'product_cat',
						'field' => 'id',
						'terms'=>explode(',',$cat_ids)
					))
			));

		}

	} elseif($prod_ids !='') {

		$prod_arr = array(); //stores the product ids

		if($prod_ids !='') {
			$prod_arr = explode(',',$prod_ids);
		}

		$loop = new WP_Query(array('orderby' => $orderby, 'posts_per_page'=>$limit, 'order' => $order, 'post_type' => 'product','post__in'=>$prod_arr));

	}elseif($prod_tags !='') { //for product tags

		$prod_arr = array(); //stores the product tags

		if($prod_tags !='') {
			$prod_arr = explode(',',$prod_tags);
		}

		$loop = new WP_Query(array('post_type'	=> 'product', 'posts_per_page'=>$limit, 'tax_query' => array(
				array(
					'orderby' => $orderby,
					'order'   => $order,
					'taxonomy' => 'product_tag',
					'field' => 'slug',
					'terms' => $prod_arr,
					'operator'=> 'IN' //Or 'AND' or 'NOT IN'
				)))
		);
	} else { //all products

		$loop = new WP_Query(array('post_type'	=> 'product', 'posts_per_page'=>$limit, 'orderby' => $orderby, 'order'   => $order));
	}
	ob_start();

	?>

	<?php if ( $loop->have_posts() ) : $randomId = mt_rand(0, 100000);?>
		<div class="carousel-box <?php echo $anim.' '.$class?> woocommerce shop-carousel">
			<ul class="carousel carousel-simple" <?php echo $out?> data-carousel-items="<?php echo $items?>" data-carousel-nav="<?php echo $showarrows?>" data-carousel-pagination="false">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php woocommerce_get_template_part( 'content', 'product' ); ?>
				<?php endwhile; // end of the loop. ?>
			</ul>
		</div>

	<?php else: ?>
		<p class="no-products"><?php esc_html_e('No products found.', 'dart')?></p>
	<?php endif;?>

	<?php wp_reset_postdata();

	return ob_get_clean();

}

add_shortcode('productcar', 'alc_productcar');

function shop_special_products( $atts ){
	global $woocommerce_loop;

	extract( shortcode_atts( array(
		"type" 			=> 1,
		'limit'      	=> 12,
		'columns'       => 4,
		'orderby'       => 'menu_order',
		'order'         => 'asc',
		'automatic' 	=> 'false',
		'items' 		=> "4",
		'interval' 		=> "2500",
		'showarrows' 	=> "false",
		'anim'			=> '',
		'class'			=>'',
	), $atts ) );

	$args = array(
		'post_type' => 'product',
		'post_status' => 'publish',
		'ignore_sticky_posts'   => 1,
		'posts_per_page' => $limit,
	);
	if($automatic=='true'){
		$out=' data-carousel-autoplay="'.$interval.'"';
	}else{
		$out='data-carousel-autoplay="false"';
	}
	if ($type == 1){ // Products on Sale
		$args['orderby'] = $orderby;
		$args['order' ] = $order;
		$args['meta_query'] = array(
			array(
				'key' => '_visibility',
				'value' => array('catalog', 'visible'),
				'compare' => 'IN'
			),
			array(
				'key' => '_sale_price',
				'value' => 0,
				'compare' => '>',
				'type' => 'NUMERIC'
			)
		);
	}
	elseif ($type == 2){ // Best selling products
		$args['meta_key'] 	= 'total_sales';
		$args['orderby']	= 'meta_value';
		$args['meta_query'] = array(
			array(
				'key' => '_visibility',
				'value' => array( 'catalog', 'visible' ),
				'compare' => 'IN'
			)
		);
	}

	elseif ($type == 3){ // Featured products
		$args['orderby'] = $orderby;
		$args['order' ] = $order;
		$args['meta_query'] = array(
			array(
				'key' => '_visibility',
				'value' => array('catalog', 'visible'),
				'compare' => 'IN'
			),
			array(
				'key' => '_featured',
				'value' => 'yes'
			)
		);
	}

	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	ob_start();


	$loop = new WP_Query( $args );

	?>


	<?php if ( $loop->have_posts() ) : $randomId = mt_rand(0, 100000);?>
		<div class="carousel-box <?php echo $anim.' '.$class?> woocommerce shop-carousel special-carousel">
			<ul class="carousel carousel-simple" <?php echo $out?> data-carousel-items="<?php echo $items?>" data-carousel-nav="<?php echo $showarrows?>" data-carousel-pagination="false">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php woocommerce_get_template_part( 'content', 'product' ); ?>
				<?php endwhile; // end of the loop. ?>
			</ul>
		</div>
	<?php else: ?>
		<p class="no-products"><?php esc_html_e('No products found.', 'dart')?></p>
	<?php endif;?>

	<?php wp_reset_postdata();

	return ob_get_clean();
}
add_shortcode('shop_special_products', 'shop_special_products');
/*************************************************/


/****** SHOW POSTS BY CATEGORY AND COUNT ********/

function alc_list_posts( $atts )
{
	extract( shortcode_atts( array(
		'category' => '',
		'limit' => '5',
		'order' => 'DESC',
		'orderby' => 'date',
		'anim'=>'',
		'class'=>''
	), $atts) );
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$return = '';

	$query = array();

	if ( $category != '' )
		$query[] = 'category=' . $category;

	if ( $limit )
		$query[] = 'numberposts=' . $limit;

	if ( $order )
		$query[] = 'order=' . $order;

	if ( $orderby )
		$query[] = 'orderby=' . $orderby;

	$posts_to_show = get_posts( implode( '&', $query ) );
	$return.='<div class="bloglisting '.$class.' '.$anim.'">';
	foreach ($posts_to_show as $ps) {
		$day = get_the_time('d', $ps->ID);
		$month = get_the_time('M', $ps->ID);
		$thumbnail = get_the_post_thumbnail($ps->ID, 'blog-thumb-2');
		$postmeta = get_post_custom($ps->ID);
		$return.='<div class="col-sm-6 col-xs-6">
							<div class="media recent-post">';
		if (!empty($thumbnail) && 'gallery' !== get_post_format( $ps->ID )):
			$return.='<a href="' . get_permalink($ps->ID) . '">' . $thumbnail . '</a>';
		elseif (( function_exists( 'get_post_format' ) && 'video' == get_post_format( $ps->ID ) )):
			global $wp_embed;
			$post_embed = '';
			$video = isset($postmeta["_blog_video"]) ? $postmeta["_blog_video"][0] : '';
			$videoself = isset($postmeta["_blog_video_selfhosted"]) ? $postmeta["_blog_video_selfhosted"][0] : '';
			if ($video || $videoself):
				$return.='<div class="full-video">';
				if ($video):
					$post_embed = $wp_embed->run_shortcode('[embed width="500" height="400"]'.$video.'[/embed]');
				else:
					$post_embed = do_shortcode($videoself);
				endif;
				$return.=$post_embed;
				$return.='</div>';
			else:
				$return.='<div class="alert alert-danger fade in">
											Video post format was chosen but no url or embed code provided. Please fix this by providing it.
										</div>';
			endif;
		elseif(( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $ps->ID ) )):
			$return.='<div class="page-slider-wrapper">';
			$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($ps->ID), 'full', false);
			$argsThumb = array(
				'order'          => 'ASC',
				'posts_per_page'  => 99,
				'post_type'      => 'attachment',
				'post_parent'    => $ps->ID,
				'post_mime_type' => 'image',
				'post_status'    => null,
				//'exclude' => get_post_thumbnail_id()
			);
			$return.='<ul class="bxslider">';
			$attachments = get_posts($argsThumb);
			if ($attachments) {
				foreach ($attachments as $attachment) {
					$image = wp_get_attachment_url($attachment->ID, 'full', false, false);
					$alt = $attachment->post_excerpt;
					$return.= '<li><img src="'.$image.'" alt="'.$alt.'"></li>';
				}
			}
			$return.='</ul>
										</div>';
		else:
			$return.='<a href="' . get_permalink($ps->ID) . '" >
												<img src = "http://placehold.it/267x186" alt="' . esc_html__('No image', 'dart') . '" />
										</a>';
		endif;
		$return.='<div class="blog-date">
								<span class="date">'.$day.'
										<span>'.$month.'</span>
									</span>
								</div>
								<div class="media-body post-body">
									<h3><a href="'.get_permalink( $ps->ID ).'">'.$ps->post_title.'</a></h3>
									<p class="post-meta">
										<span class="post-meta-author">Posted by: '.get_the_author().'</span>
										<span class="post-meta-comments">'.get_comments_number($ps->ID ).' comments</span>
									</p>
									<div class="post-excerpt">
										<p>'.limit_words(get_the_excerpt(), 15).'</p>
										<a href="'.get_permalink( $ps->ID ).'" class="read-more">Read More <i class="fa fa fa-long-arrow-right"></i></a>
									</div>
								</div>
							</div><!-- end media -->
	   	 				</div>';

	}
	$return.='</div>';


	return $return;
}

add_shortcode('list_posts', 'alc_list_posts');

/************************************************/


/**************** RELATED POSTS *****************/
function related_posts_shortcode( $atts ) {
	extract(shortcode_atts(array(
		'count' => '3',
		'title' => 'More useful tips',
	), $atts));
	global $post, $wp_embed;
	$retval = '';
	$current_cat = get_the_category($post->ID);
	$current_cat = $current_cat[0]->cat_ID;
	$this_cat = '';
	$tag_ids = array();
	$tags = get_the_tags($post->ID);
	if ($tags) {
		foreach($tags as $tag) {
			$tag_ids[] = $tag->term_id;
		}
	}
	else {
		$this_cat = $current_cat;
	}
	$args = array(
		'post_type' => get_post_type(),
		'numberposts' => $count,
		'orderby' => 'date',
		'order' => 'DESC',
		'tag__in' => $tag_ids,
		'cat' => $this_cat,
		'exclude' => $post->ID
	);
	$dtwd_related_posts = get_posts($args);
	if ( empty($dtwd_related_posts) ) {
		$args['tag__in'] = '';
		$args['cat'] = $current_cat;
		$dtwd_related_posts = get_posts($args);
	}
	//if ( empty($dtwd_related_posts) ) {return;}
	$post_list = '';

	if ( $dtwd_related_posts ) {
		foreach($dtwd_related_posts as $r) {
			$get_meta = get_post_custom($r->ID);
			$video = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : '';
			$thumbnail = get_the_post_thumbnail($r->ID, 'blog-medium');
			$retval .= '
			<div class="col-md-4 col-sm-4">
				<div class="related-post-media">';
			if ($video):
				$retval.='<div class="flex-video">'.$wp_embed->run_shortcode('[embed width="262" height="197"]'.$video.'[/embed]').'</div>';
			else:
				if ($thumbnail !== ''):
					$retval.=$thumbnail;
				else:
					$retval.='<img src = "http://placehold.it/550x403" alt="'.esc_html__('No Image', 'dart').'" />';
				endif;
			endif;
			$retval .= '</div>
				<div class="related-post-content">
					<div class="related-post-title"><a href="'.get_permalink($r->ID).'">'.wptexturize($r->post_title).'</a></div>
					<div class="related-post-meta"><span class="meta-date">'.get_the_time('d M Y', $r->ID).'</span></div>
				</div>
			</div>';
		}
	}
	else {
		$retval .= '<div class="col-md-12"><p>'.esc_html__('No related posts for this one', 'dart').'</div>';
	}

	return $retval;
}
add_shortcode('related_posts', 'related_posts_shortcode');

/*************** SOCIAL BUTTONS *****************/
function alc_social($atts, $content=NULL){
	extract( shortcode_atts( array(
		'anim'=>'',
		'class'=>'',
		'target' => '_blank'
	), $atts) );
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$GLOBALS['socbuttoncount']=0;
	$out = '';
	if ($target) $target = 'target="'.$target.'"';
	do_shortcode ($content);
	if(isset($GLOBALS['soc_buttons']) && is_array($GLOBALS['soc_buttons'])){
		foreach ($GLOBALS['soc_buttons'] as $soc){
			$soclink=$soc['link'];
			$socicon=$soc['icon'];
			$soc_buttons[]="<li><a href=\"$soclink\" $target><i class=\"fa fa-$socicon\"></i></a></li>";
		}
		$out='<ul class="social-icons '.$anim.' '.$class.'">'.implode("\n", $soc_buttons).'</ul>';
		unset($GLOBALS['soc_buttons']);
	}
	return $out;
}

add_shortcode('social', 'alc_social');

/*********************/
function alc_soc_button($atts, $content=NULL){
	extract(shortcode_atts(array(
		'icon'=>'bitbucket',
		'link'=>''
	), $atts));
	//do_shortcode ($content);
	$x= $GLOBALS['socbuttoncount'];
	$GLOBALS['soc_buttons'][$x]=array('icon'=> $icon, 'link'=>$link);
	$GLOBALS['socbuttoncount']++;

}

add_shortcode('soc_button', 'alc_soc_button');
/**************************************************/


/***************** TEAM MEMBERS *******************/
function al_teammember($atts, $content=NULL){
	extract(shortcode_atts(array(
		'style'=>'',
		'name'=>'',
		'position'=>'',
		'photo'=>'',
		'imgstyle'=>'',
		'anim'=>'',
		'class'=>'',
		'cusbgover'=>''
	), $atts));

	$GLOBALS['sbcount']=0;

	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$cusbgover = (isset($cusbgover) && !empty($cusbgover))  ? ' background-color:'.$cusbgover.' !important;' : '';
	do_shortcode ($content);
	$tmClass=($imgstyle=='img-circle')? 'team-wrapper' : 'page-team-wrapper';
	$imgClass=($imgstyle=='img-circle')? 'team-img-wrapper' : 'team-image-wrapper';
	$hoveClass=($imgstyle=='img-circle')? 'team-img-wrapper-hover' : 'team-image-wrapper-hover';
	$out= '<div class="'.$tmClass.' '.$style.' '.$anim.' '.$class.'">
				<div class="'.$imgClass.'">
					<div class="'.$hoveClass.'" style="'.$cusbgover.'">
						<div class="social-icons">';
	if(isset($GLOBALS['tmsocbuttons']) && is_array($GLOBALS['tmsocbuttons'])){
		foreach ($GLOBALS['tmsocbuttons'] as $soc){
			$tmname = ucwords(substr($soc['tmicon'], 3));
			$tmsocbuttons[]='<a href="'.$soc['tmlink'].'"><i class="fa '.$soc['tmicon'].'"></i></a>';
		}
		$out.=implode("\n", $tmsocbuttons);

	}
	$image = $photo;
	if(function_exists('vc_map')) {
		$attachment = wp_get_attachment_image_src($photo, 'full');
		$image = $attachment[0];
	}
	$out.='</div>
					</div>
					<img alt="'.$name.'" src="'.$image.'" class="'.$imgstyle.'" />
				</div>
				<div class="team-content">
				    <h3 class="name">
				      '.$name.' 
				      <span>'.$position.'</span>
					</h3>
					<p class="team-text">'.do_shortcode ($content).'</p>
				</div>
			</div>';
	unset($GLOBALS['tmsocbuttons']);

	return $out;
}

add_shortcode('teammember', 'al_teammember');
add_shortcode('vc_teammember', 'al_teammember');

function al_tmsocbutton($atts, $content=NULL){
	extract(shortcode_atts(array(
		'tmicon'=>'',
		'tmlink'=>''
	), $atts));

	$x = $GLOBALS['sbcount'] ;
	$GLOBALS['tmsocbuttons'][$x]=array('tmicon'=> $tmicon, 'tmlink'=>$tmlink);
	$GLOBALS['sbcount']++;
}

add_shortcode('tmsocbutton', 'al_tmsocbutton');


/**************************************************/

/********************List*************/

function alc_list( $atts, $content ){
	extract(shortcode_atts(array(
		'anim' => '',
		'type' => 'order',
		'style'=>'',
		'class' => ''
	), $atts));
	$return = '';
	$GLOBALS['listitem_count'] = 0;
	$counter=0;
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	do_shortcode($content);
	if($type=='order') {
		$tag='<ol ';
		$tagEnd='</ol>';
	}else{
		$tag='<ul ';
		$tagEnd='</ul>';
	}
	if(isset( $GLOBALS['listitems'] ) && is_array( $GLOBALS['listitems'] ) ){
		foreach( $GLOBALS['listitems'] as $listitem ){
			$listitems[]='<li>';
			if($listitem['link']){
				$listitems[].='<a href="'.$listitem['link'].'">'.do_shortcode($listitem['content']).'</a>';
			}else{
				$listitems[].=do_shortcode($listitem['content']);
			}
			$listitems[].='</li>';
			$counter++;
		}
		$return.= $tag.' class="'.$style.' '.$anim.' '.$class.' '.$type.'">'.implode( "\n", $listitems ).$tagEnd;
		unset($GLOBALS['listitems']);
	}
	return $return;
}
add_shortcode('list', 'alc_list');
add_shortcode('av_list', 'alc_list');

/************************/
function alc_list_item( $atts, $content = null){
	extract(shortcode_atts(array(
		'title' => '',
		'link' => ''
	), $atts));

	$x = $GLOBALS['listitem_count'];
	$GLOBALS['listitems'][$x] = array('link'=>$link, 'content'=>$content);

	$GLOBALS['listitem_count']++;
}
add_shortcode('listitem', 'alc_list_item');
add_shortcode('av_list_item', 'alc_list_item');

/*********************Blockquote************************/

function alc_blockquote( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'author'=>'',
		'anim'=>'',
		'class'=>'',
		'company' => '',
		'pos' => '',
		'bcuscolor'=>''
	), $atts));
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$bcuscolor = (isset($bcuscolor) && !empty($bcuscolor))  ? 'style="border-color:'.$bcuscolor.'"' : '';
	$out='<blockquote class="'.$pos.' '.$class.''.$anim.'" '.$bcuscolor.'>
			<p>'.do_shortcode($content).'</p>
			<small>'.$author.'<cite title="Source Title"> '.$company.'</cite></small>
		</blockquote>';
	return $out;
}
add_shortcode('blockquote', 'alc_blockquote');
/****************** Lightbox ********************/

function alc_lightbox( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'type'=>'image',
		'thumbnail'=>'',
		'src'=>'',
		'anim'=>'',
		'class'=>'',
	), $atts));
	$out = '';
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$imglink='';
	if(function_exists('vc_map')){
		$limage = wp_get_attachment_image_src($thumbnail, 'full');
		$imglink.=$limage[0];
	}else{
		$imglink=$thumbnail;
	}
	$iframeAdd=($type=='iframe') ? '?iframe=true' : '';
	$out.='<a href="'.$src.''.$iframeAdd.'" class="'.$class.'" data-rel="prettyPhoto"  data-lightbox="'.$type.'" title="'.do_shortcode($content).'"><img src="'.$imglink.'" alt="" class="img-responsive thumbnail '.$anim.'"></a>';

	return $out;
}
add_shortcode('lightbox', 'alc_lightbox');
/*****************************************/


/**********************PARALLAX*************************/
function alc_fullbg($atts, $content=NULL){
	extract(shortcode_atts(array(
		'bgcolor'=>'',
		'bgrepeat'=>'no-repeat',
		'bgimage'=>'',
		'class'=>'',
		'padding' => '',
		'custompadding' => '',
		'notopborder' => '',
		'scrollspeed' => '0.1',
		'nobottomborder' => '',
		'videourl' => '',
		'mute' => 'true',
		'autoplay' => 'true',
		'loop' => 'false',
		'showcontrols' => 'true',
		'quality' => 'default',
		"height" => '300',
		'type' => ''
	), $atts));
	$GLOBALS['sbcount']=0;

	do_shortcode ($content);
	$randomId = mt_rand(0, 100000);
	$imglink='';
	if(function_exists('vc_map')){
		$limage = wp_get_attachment_image_src($bgimage, 'full');
		$imglink.=$limage[0];
	}else{
		$imglink=$bgimage;
	}
	$bgcolor = (isset($bgcolor) && $bgcolor!=='') ? ' background-color:'.$bgcolor.' !important;' : '';
	$bgimage = (isset($imglink) && $imglink!=='') ? ' background-image:url('.$imglink.') !important;' : '';
	$bgrepeat = (isset($bgrepeat) && $bgrepeat!=='')? ' background-repeat:'.$bgrepeat.' !important;' : '';
	$height = empty($height) ? '' : 'style="height:'.$height.'px;"';

	$elements = $bgcolor.$bgimage.$bgrepeat;
	$custompadding = (isset($custompadding) && !empty($custompadding))  ? ' padding:'.$custompadding.'px !Important' : '';
	$out = '';
	if ($type == 'parallax'){
		$out.= '
			<div class="fullsize parallax-bg">
				<div class="parallax-wrapper parallax-background '.$class.'" data-stellar-background-ratio="'.$scrollspeed.'" style="'.$elements.'">
					<div class="parallax-wrapper-inner '.$padding.'"  style="'.$custompadding.'" id="'.$randomId.'">'.do_shortcode($content).'</div>
				</div>
			</div>';
	}
	else{
		$condpad = ($type == 'wvideo') ? '' : $padding;
		$out.= '<div class="fullsize fullsize-background '.$notopborder.' '.$nobottomborder.' '.$condpad.'" style="'.$elements.' '.$custompadding.'">';
		if($type == 'wvideo'){ $out.='<div id="ytwrapper'.$randomId.'" '.$height.'><div id="ytvideo'.$randomId.'" class="player '.$padding.'" style="display:block !important; margin: auto '.$custompadding.'" data-property="{showControls : '.$showcontrols.', videoURL:\''.$videourl.'\', containment:\'#ytwrapper'.$randomId.'\', startAt:0, mute:'.$mute.', autoPlay:'.$autoplay.', loop:'.$loop.', opacity:1,quality:\''.$quality.'\'}">';}
		$out.= do_shortcode($content);
		if($type == 'wvideo') {$out.='</div></div>';}
		$out.='</div>';
		$out.='<script>
			jQuery(function(){
				
				jQuery("#ytvideo'.$randomId.'").mb_YTPlayer();
			});
			</script>';
	}

	return $out;
}

add_shortcode('fullbg', 'alc_fullbg');

/*********************************************************/

/********************* GOOGLE MAP **********************/

function mapme($attr) {
	wp_enqueue_script('Google-map-api');

	$number = mt_rand(0, 100000);
	$randomId = "googlemap".$number;
	// default atts
	$attr = shortcode_atts(array(
		'lat'   => '0',
		'lon'    => '0',
		'z' => '14',
		'w' => '300',
		'h' => '300',
		'maptype' => 'ROADMAP',
		'address' => '',
		'marker' => '',
		'markerimage' => '',
		'traffic' => 'no',
		'infowindow' => '',
		'scrollwheel' => 'false',
		'class' => ''
	), $attr);
	$imglink='';
	if(function_exists('vc_map')){
		$limage = wp_get_attachment_image_src($attr['markerimage'], 'full');
		$imglink.=$limage[0];
	}else{
		$imglink=$attr['markerimage'];
	}
	$center = empty($attr['lat']) ? $attr['address'] : '['.$attr['lat'].', '.$attr['lon'].']';
	$traffic = ($attr['traffic'] == 'yes') ? '.trafficlayer()' : '';

	$returnme = '<div class="map-split gm-shortcode '.$attr['class'].'"><div id="' .$randomId . '" style="width:' . $attr['w'] . 'px;height:' . $attr['h'] . 'px;" ></div></div>
  	
  
  	<script type="text/javascript">
		var center = "'.$center.'";
		jQuery.gmap3(false);
		function initMap() {
			jQuery("#'.$randomId.'").gmap3({
				address:"'.$attr['address'].'",
				 scrollwheel: '.$attr['scrollwheel'].',
				zoom:'.$attr['z'].',
				mapTypeId : google.maps.MapTypeId.'.$attr['maptype'].'
			})
			.marker({
				address:"'.$attr['address'].'", 
				icon: "'.$imglink.'"
			})
			.infowindow({content: "'.$attr['infowindow'].'"})
			.then(function (infowindow) {
				var map = this.get(0);
				var marker = this.get(1);
				marker.addListener("click", function() {infowindow.open(map, marker);});
			 })'.$traffic.';
		}	
		</script>';

	return $returnme;
}
add_shortcode('map', 'mapme');

/************ Service BLOCK****************/
function alc_sblock($atts, $content=NULL){
	extract(shortcode_atts(array(
		'anim' => '',
		'type' => 'default',
		'title' => '',
		'tcuscolor'=>'',
		'icon' => '',
		'icuscolor'=>'',
		'link' => '',
		'class' => ''
	), $atts));
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$tcuscolor = (isset($tcuscolor) && !empty($tcuscolor))  ? $tcuscolor : '';
	$icuscolor = (isset($icuscolor) && !empty($icuscolor))  ? ' color:'.$icuscolor.' !important;' : '';
	$out='';
	if($type=='default'){
		$out.='<div class="'.$anim.' '.$class.'">
					<div class="service-box">';
		if($link){$out.='<a href="'.$link.'">';}
		$out.='<span class="service-icon pull-left"><i class="fa '.$icon.'" style="'.$icuscolor.'"></i></span>';
		if($link){$out.='</a>';}
		$out.='<div class="service-box-content">
								<h3 style="color:'.$tcuscolor.' !important">'.$title.'</h3>
								<p>'.do_shortcode($content).'</p>
							</div>
					</div>
				</div>';
	}elseif ($type=='alter') {
		$out.='<div class="'.$anim.' '.$class.'">
					<div class="service-box text-center">
					    <span class="service-icon"><i class="fa '.$icon.'" style="'.$icuscolor.'"></i></span>
					    <div class="service-box-content">
						    <h3 style="color:'.$tcuscolor.' !important">'.$title.'</h3>
						    <p>'.do_shortcode($content).'</p>
						</div>
					</div>
				</div>';
	}
	return $out;
}
add_shortcode('sblock', 'alc_sblock');
/*********************Pricing Table***************/
function alc_pricing($atts, $content=NULL){
	extract(shortcode_atts(array(
		'type' =>'',
		'anim' => '',
		'title' => '',
		'currency'=>'',
		'price'=>'',
		'period'=>'',
		'fbutton'=>'',
		'fbutlink'=>'',
		'class'=>''
	), $atts));
	$GLOBALS['column_count'] = 0;
	$counter = 0;
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	do_shortcode($content);
	$out='';
	$out.='<div class="plan text-center '.$type.' '.$anim.' '.$class.'">';
	if($title){
		$out.='<span class="plan-name">'.$title.'</span>';

	}
	if($price){
		$out.='<p class="plan-price"><sup class="currency">'.$currency.'</sup><strong>'.$price.'</strong><small>'.$period.'</small></p>';
	}
	if( is_array( $GLOBALS['columns'] ) ){
		foreach( $GLOBALS['columns'] as $column ){
			$columns[]='<li>'.do_shortcode($column['content']).'</li>';
			$counter++;
		}
		$out.='<ul class="list-unstyled">'.implode( "\n", $columns ).'</ul>';
		unset($GLOBALS['columns']);
	}
	if($fbutton){
		$out.='<a class="btn btn-primary" href="'.$fbutlink.'">'.$fbutton.'</a>';
	}
	$out.='</div>';


	return $out;
}
add_shortcode('av_pricing', 'alc_pricing');

function alc_column( $atts, $content = null){
	extract(shortcode_atts(array(
		'colcontent' => ''
	), $atts));

	$x = $GLOBALS['column_count'];
	$GLOBALS['columns'][$x] = array( 'content'=>$content);

	$GLOBALS['column_count']++;
}
add_shortcode('av_column', 'alc_column');
/******************** Divider **********************/
function alc_divider( $atts, $content = null ) {
	extract(shortcode_atts(array(
			'type'=>'',
			'size'=>'gap-20',
			'class'=>''
		), $atts)
	);
	if ($type=='blank-spacer') {
		$out='<div class="blank-spacer '.$size.' '.$class.'"></div>';
	}else{
		$out='<hr class="'.$class.'">';
	}

	return $out;
}
add_shortcode('divider', 'alc_divider');
/*******************************************/
/*********************** COLUMNS *************************/

function dart_one_whole( $atts, $content = null ) {
	return '<div class="col-md-12">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_whole', 'dart_one_whole');

function dart_one_half( $atts, $content = null ) {

	return '<div class="col-md-6">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_half', 'dart_one_half');


function dart_one_third( $atts, $content = null ) {
	return '<div class="col-md-4">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_third', 'dart_one_third');


function dart_two_third( $atts, $content = null ) {
	return '<div class="col-md-8">' . do_shortcode($content) . '</div>';
}
add_shortcode('two_third', 'dart_two_third');


function dart_one_fourth( $atts, $content = null ) {

	return '<div class="col-md-3">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_fourth', 'dart_one_fourth');

function dart_three_fourth( $atts, $content = null ) {
	return '<div class="col-md-9">' . do_shortcode($content) . '</div>';
}
add_shortcode('three_fourth', 'dart_three_fourth');

function dart_one_sixth( $atts, $content = null ) {
	return '<div class="col-md-2">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_sixth', 'dart_one_sixth');

function dart_five_twelveth( $atts, $content = null ) {
	return '<div class="col-md-5">' . do_shortcode($content) . '</div>';
}
add_shortcode('five_twelveth', 'dart_five_twelveth');

function dart_seven_twelveth( $atts, $content = null ) {
	return '<div class="col-md-7">' . do_shortcode($content) . '</div>';
}
add_shortcode('seven_twelveth', 'dart_seven_twelveth');

function dart_five_sixth( $atts, $content = null ) {
	return '<div class="col-md-10">' . do_shortcode($content) . '</div>';
}
add_shortcode('five_sixth', 'dart_five_sixth');

function dart_row( $atts, $content = null ) {
	return '<div class="container"><div class="row">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('row', 'dart_row');

function dart_inner_row( $atts, $content = null ) {
	return '<div class="row">' . do_shortcode($content) . '</div>';
}
add_shortcode('inner_row', 'dart_inner_row');
/************************************************/


/******************** CLEAR *********************/

function alc_clear($atts, $content = null) {
	return '<div class="clearfix"></div>';
}
add_shortcode('clear', 'alc_clear');


/******** SHORTCODE SUPPORT FOR WIDGETS *********/

if (function_exists ('shortcode_unautop')) {
	add_filter ('widget_text', 'shortcode_unautop');
}
add_filter ('widget_text', 'do_shortcode');


?>