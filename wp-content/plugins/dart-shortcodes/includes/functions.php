<?php
function dart_vcSetAsTheme() {
	vc_set_as_theme();
}

if(function_exists('vc_remove_element')){
	
}

// Visual composer theme-added shortcodes

if(function_exists('vc_map')){
	$theme = wp_get_theme(); // gets the current theme
	if($theme->get('Name') == 'Dart' || $theme->get('Name') == 'Dart Child') {

		$attributes = array(
			'type' => 'dropdown',
			'heading' => "Layout",
			'param_name' => 'newlayout',
			'value' => array("Fluid", "Fixed"),
		);
		vc_add_param('vc_row', $attributes);

		require_once(get_template_directory() . '/vc_templates/progress.php');
		require_once(get_template_directory() . '/vc_templates/button.php');
		require_once(get_template_directory() . '/vc_templates/alert.php');
		require_once(get_template_directory() . '/vc_templates/contact.php');
		require_once(get_template_directory() . '/vc_templates/fblock.php');
		require_once(get_template_directory() . '/vc_templates/tblock.php');
		require_once(get_template_directory() . '/vc_templates/modal.php');
		require_once(get_template_directory() . '/vc_templates/teammember.php');
		require_once(get_template_directory() . '/vc_templates/portlisting.php');
		require_once(get_template_directory() . '/vc_templates/bloglisting.php');
		require_once(get_template_directory() . '/vc_templates/panel.php');
		require_once(get_template_directory() . '/vc_templates/fullbg.php');
		require_once(get_template_directory() . '/vc_templates/av_testimonial.php');
		require_once(get_template_directory() . '/vc_templates/circle.php');
		require_once(get_template_directory() . '/vc_templates/promo.php');
		require_once(get_template_directory() . '/vc_templates/blockquote.php');
		require_once(get_template_directory() . '/vc_templates/divider.php');
		require_once(get_template_directory() . '/vc_templates/lightbox.php');
		require_once(get_template_directory() . '/vc_templates/av_tabs.php');
		require_once(get_template_directory() . '/vc_templates/slider.php');
		require_once(get_template_directory() . '/vc_templates/av_list.php');
		require_once(get_template_directory() . '/vc_templates/pricingtable.php');
		require_once(get_template_directory() . '/vc_templates/portrelated.php');
		require_once(get_template_directory() . '/vc_templates/av_carousel.php');
		require_once(get_template_directory() . '/vc_templates/av_social.php');
		require_once(get_template_directory() . '/vc_templates/av_map.php');

		add_action('vc_before_init', 'dart_vcSetAsTheme');
	}
}
?>