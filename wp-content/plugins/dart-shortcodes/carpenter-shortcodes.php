<?php 
/*
Plugin Name: Dart Shortcodes
Description: Additional shortcodes for visual composer
Version: 1.0.0
Author: UIUXAesthetics
Author URI: http://weblusive.com
\
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Carpenter_Shortcodes {
	
	/**
	 * Initialization
	 * @return void
	 */
	public static function init()
	{		
		/* Set the constants needed by the plugin. */
		add_action( 'plugins_loaded', array( get_called_class(), 'constants' ), 1 );

		/* Load the functions files. */
		add_action( 'plugins_loaded', array( get_called_class(), 'includes' ), 3 );	
	}

	/**
	 * Defines constants used by the plugin.
	 * @return void
	 */
	public static function constants() {


		/* Set constant path to the plugin directory. */
		define( 'MAIN_DIR', plugin_dir_path( __FILE__ ));


		/* Set the constant path to the includes directory. */
		define( 'MAIN_INCLUDES', MAIN_DIR . trailingslashit( 'includes' ) );

	}	

	public static function includes() {
		require_once MAIN_INCLUDES.'shortcodes.php';
		require_once MAIN_INCLUDES.'functions.php';
	}
}

Carpenter_Shortcodes::init();