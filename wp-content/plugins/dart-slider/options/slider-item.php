<?php 
/**
 * Options of the slider item.
 *
 * @package themeplaza_slider
 * @subpackage options
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */
?>

<ul id="weblusive-slider-items">
	<?php
	if( $slider ){
	$i=0;
	foreach( $slider as $slide ):
		$i++; ?>
		<li id="listItem_<?php echo $i ?>"  class="ui-state-default zebra-table postbox <?php if($i != count($slider)) echo 'closed';?>">
			<div class="handlediv" title="Click to toggle"><br></div>
			<h3 class="hint-title">Slide <?php echo $i; ?> Parameters</h3>
			<div class="widget-content option-item inside">

			<!-- Set your settings after this -->

				<div class="slider-img"><?php echo wp_get_attachment_image( $slide['id'] , 'thumbnail' );  ?></div>		
				<label for="custom_slider[<?php echo $i ?>][caption]">
					<span style="float:left" >Slide Text:</span>
					<textarea type="text" name="custom_slider[<?php echo $i ?>][caption]" id="custom_slider[<?php echo $i ?>][caption]"><?php if(array_key_exists('caption', $slide)) echo ($slide['caption']); ?></textarea>
				</label>
				
				<label for="custom_slider[<?php echo $i ?>][link]">
					<span style="float:left" >Slide Link (Optional):</span>
					<input type="text" name="custom_slider[<?php echo $i ?>][link]" id="custom_slider[<?php echo $i ?>][link]" value="<?php if(array_key_exists('link', $slide)) echo stripslashes( $slide['link']); ?>">
				</label>

			<!-- Set your settings before this -->

				<input id="custom_slider[<?php echo $i ?>][id]" name="custom_slider[<?php echo $i ?>][id]" value="<?php  echo $slide['id']  ?>" type="hidden" />
				<a class="del-cat"></a>
			</div>
		</li>
	<?php endforeach; 
	}else{
		echo ' <p> Use the button above to add slides.</p>';
	}?>
</ul>

<?php 
	// Sava data
	array_push( ThemeplazaSliderAdmin::$fields, "custom_slider");
 ?>