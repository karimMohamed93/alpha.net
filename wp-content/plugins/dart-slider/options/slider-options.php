<?php 
/**
 * General options of the slider.
 *
 * @package themeplaza_slider
 * @subpackage options
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

	global $post;
?>

<!-- Set your settings after this -->
	

<div class="option-item" id="slider-autoslide">
	<span class="label">Auto slide</span>
	<?php
		$get_meta = get_post_custom($post->ID);
		$meta_box_value = get_post_meta($post->ID, "autoslide", true);
		$autoslide = isset( $get_meta["autoslide"][0] ) ? esc_attr( $get_meta["autoslide"][0] ) : '';
	?>
	<select id="autoslide" name="autoslide">
		<option value="false" <?php if( isset($autoslide) && $autoslide == 'false'):?>selected="selected"<?php endif?>>No</option>
		<option value="true" <?php if( isset($autoslide) && $autoslide == 'true'):?>selected="selected"<?php endif?>>Yes</option>
	</select>
	
</div>

<div class="option-item" id="slider-interval">
	<span class="label">Sliding interval (in milliseconds)</span>
	<?php
		$get_meta = get_post_custom($post->ID);
		$meta_box_value = get_post_meta($post->ID, "interval", true);
		$interval = isset( $get_meta["interval"][0] ) ? esc_attr( $get_meta["interval"][0] ) : '5000';
	?>
	<input name="interval" id="interval" type="number" value="<?php echo $interval ?>" />
	
</div>

<!-- Set your settings before this -->

<?php 
	/* Don't forget to save the slider's option by adding 
	it's ID into the array ThemeplazaSliderAdmin::$fields */
	array_push( ThemeplazaSliderAdmin::$fields, "autoslide", "interval" /*, ID-2, ID-3, ...*/ );
?>