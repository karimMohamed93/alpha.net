<?php
/**
 * @package themeplaza_slider
 * @subpackage templates
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */
/**
 * [themeplaza_slider sid=""]
 * @param int $sid          	 The ID of slider
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

extract(shortcode_atts(array(
	"sid" => "",
), $atts));

if ( empty( $sid ) ) {
	?>
	<p class="themeplaza-error">Please Set Slider ID</p>
	<?php 
}
		
$custom_slider_args = array( 'post_type' => 'themeplaza_slider', 'p' => $sid );
$custom_slider = new WP_Query( $custom_slider_args );
		
while ( $custom_slider->have_posts() ) : $custom_slider->the_post();
	$custom = get_post_custom($sid);

	$slider = isset ($custom["custom_slider"]) ? unserialize( $custom["custom_slider"][0]) : '';
	$slider_width = isset ($custom["slider-width"]) ? 'width:'.stripslashes($custom["slider-width"][0]).'px; ' : '';
	$slider_height = isset ($custom["slider-height"]) ? 'height:'.stripslashes($custom["slider-height"][0]).'px; ' : '';
	$slider_dimensions = $slider_width || $slider_height ? 'style="' .$slider_width . $slider_height . '" ' : '';

	if( $slider ):?>
	<div class="row">
		<div class="col-md-12">
			<div id="main-slide" class="flexslider headcarousel slide" data-ride="carousel" style="<?php echo esc_attr( $slider_width ); ?>">
				<ul class="slides slider" >
				<?php 
					foreach( $slider as $slide ):
						$image = wp_get_attachment_image_src( $slide['id'], 'full'); 
						$finalimage = empty($image) ? '' : $image[0];
						$caption = empty($slide['caption']) ? '' : $slide['caption'] ;
						$link = empty($slide['link']) ? '' : $slide['link'] ;
						?>
						<li>	
							<div class="flex-slider-image">
								<img src="<?php echo esc_url( $finalimage ); ?>" class="img-responsive" alt="<?php echo esc_attr( $slide['id'] ); ?>"  <?php echo esc_attr( $slider_dimensions ); ?>/>
							</div>	
							<div class="slide-text">
								<?php if( $link ):?>
									<a class="flex-slider-read-more" href="<?php echo esc_url( $link ) ?>">
								<?php endif?>
									<?php if($caption):?>
										<h2 class="col-md-12 text-center flex-slider-caption slider-caption">
											<?php echo esc_html( $caption ) ?>
										</h2>
									<?php endif; ?>
								<?php if( $link ):?>
									</a>
								<?php endif?>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
			
	<script type="text/javascript">
		jQuery(document).ready(function($) {
		   	var slider; // Global slider value to force playing and pausing by direct access of the slider control
			var canSlide = true; // Global switch to monitor video state
	  		// Call fitVid before FlexSlider initializes, so the proper initial height can be retrieved.
	  		slider = $(".flexslider").fitVids()
	    		.flexslider({
	      			animation: 'fade',
					contorlNav:true,
					directionNav:true,
					prevText:'',
					nextText:'',
					slideshowSpeed: 3000,
					animationSpeed: 500,
					animationLoop: true,
					direction: 'horizontal',
					slideshow: true,	 
			    	//smoothHeight: true,
			    	pauseOnHover: true,
	  			});
	  	});	
	</script>
</div>
	<?php else:?>
		<div class="col-md-12">
			<p class="warning">
				<i class="icon-warning-sign"></i>
				<?php _e('No slider items were found in the selected slider. Please make sure to create some via "Slider" section in your admin panel.', 'Smaug');?>
			</p>
		</div>
	<?php endif;
endwhile; 

wp_reset_query();