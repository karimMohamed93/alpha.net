<?php
/**
 * Admin functions for the plugin.
 *
 * @package themeplaza_slider
 * @subpackage admin
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
* 
*/
class ThemeplazaSliderAdmin {
	/**
	 * Slider options
	 * @var array
	 */
	public static $fields = array();

	public static function init()
	{
		/* Set up the admin functionality. */
		add_action( 'admin_menu', array( get_called_class(), 'admin_setup') );

		/* Adds meta box */
		add_action("add_meta_boxes_themeplaza_slider", array( get_called_class(), "add_slider_options") );

		/* Savs meta fields of slider */
		add_action('save_post', array( get_called_class(), 'save_slide') );

		/* Changes slides images' url */
		add_action('wp_loaded', array( get_called_class(), 'change_slide_pic_url') );
	}

	/**
	 * Adds actions where needed for setting up the plugin's admin functionality.
	 * @return void
	 */
	public static function admin_setup()
	{
		/* Custom columns on the edit slider items screen. */
		add_filter( 'manage_edit-themeplaza_slider_columns', array( get_called_class(), 'edit_slider_columns') );
		add_action( 'manage_themeplaza_slider_posts_custom_column', array( get_called_class(), 'manage_slider_columns'), 10, 2 );
	}

	/**
	 * Sets up custom columns on the slider items edit screen.
	 *
	 * @access public
	 * @param  array  $columns
	 * @return array
	 */
	public static function edit_slider_columns( $columns ) {
		$new_columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Slider Title', THEMEPLAZA_SLIDER_DOMAIN ),
			'slides' => __( 'Number of Slides', THEMEPLAZA_SLIDER_DOMAIN ),
			'date' => __( 'Date', THEMEPLAZA_SLIDER_DOMAIN )
		);

		return $new_columns;
	}

	/**
	 * Displays the content of custom slider item columns on the edit screen.
	 *
	 * @access public
	 * @param  string  $column
	 * @param  int     $post_id
	 * @return void
	 */
	public static function manage_slider_columns( $column, $post_id ) {
		global $post;

		switch ($column) {
			case "slides":
				$custom_slider_args = array( 'post_type' => 'themeplaza_slider', 'p' => $post->ID );
				$custom_slider = new WP_Query( $custom_slider_args );
				while ( $custom_slider->have_posts() ) {
					$custom_slider->the_post();
					$custom = get_post_custom($post->ID);
					if( !empty($custom["custom_slider"][0])){
						$slider = unserialize( $custom["custom_slider"][0] );
						$number = count($slider);
						echo $number;
					}
					else echo 0;
				}
			break;
		}
	}

	/**
	 * Adds container for slider options
	 * 
	 * @access public
	 * @return void
	 */
	public static function add_slider_options(){
	   	add_meta_box("themeplaza_slider_slides", "Slider Slides",  array( get_called_class(), 'slider_slides' ), "themeplaza_slider", "normal", "high");
	}

	/**
	 * Adds the options of slider
	 * 
	 * @access public
	 * @return void
	 */	
	public static function slider_slides() {
		global $post;
		$custom = get_post_custom($post->ID);
		
		$slider = isset($custom["custom_slider"]) ? unserialize( $custom["custom_slider"][0]) : '';

		?>
	  	<div class="weblusivepanel-item">
		<h3>Slider Options</h3>

		
		<?php 
			if ( file_exists( get_template_directory().'/themeplaza/slider/options/slider-options.php' )) {
	       	 	include( get_template_directory().'/themeplaza/slider/options/slider-options.php' );
		    } else {
		        include THEMEPLAZA_SLIDER_DIR . 'options/slider-options.php';
		    }
		?>
		
		</div>
		<p class="important"><i class="dashicons dashicons-flag"></i><span><?php _e("Please make sure to save each slider item created after creating. <br>In this way you can see rest options", THEMEPLAZA_SLIDER_DOMAIN); ?></span></p>
		<input id="upload_add_slide" type="button" class="mpanel-save" value="Add New Slide" />
		
		<?php 
			if ( file_exists( get_template_directory().'/themeplaza/slider/options/slider-item.php' )) {
	       	 	include( get_template_directory().'/themeplaza/slider/options/slider-item.php' );
		    } else {
		        include THEMEPLAZA_SLIDER_DIR . 'options/slider-item.php';
		    }
		?>
		<?php if( $slider ):?>
			<script> var nextCell = <?php echo $i+1 ?> ;</script>
		<?php endif;

		$themeplaza_slider_filds = array();
		foreach (ThemeplazaSliderAdmin::$fields as $field) {
			array_push( $themeplaza_slider_filds, esc_attr( $field ) );
		}

		$custom_fields_ids = get_post_meta( $post->ID, 'themeplaza_slider_filds' );

		if( empty( $custom_fields_ids ) ) {
			add_post_meta($post->ID, 'themeplaza_slider_filds', $themeplaza_slider_filds);
		}
	}

	/**
	 * Saves slider options
	 * 
	 * @access public
	 * @return void
	 */	
	public static function save_slide( $post_id ){
        if(get_post_type() != 'themeplaza_slider')
            return;
		$custom_fields_ids = get_post_meta( $post_id, 'themeplaza_slider_filds' );
  
		if(!empty($custom_fields_ids)) {
			$custom_fields_ids = $custom_fields_ids[0];
			
		  	foreach ( $custom_fields_ids as $field_id ) {
                $field_value = isset($_POST[ $field_id ]) ? $_POST[ $field_id ] : '';

		  		if( isset($field_value) && $field_value != "" ){
					update_post_meta( $post_id, $field_id , $field_value );	
				}
				else{ 
					if ( isset( $post_id ) )
					delete_post_meta( $post_id, $field_id );
				}
		  	}
		}
	}

    private function _sanitize_recursive($func, $array) {
        if(is_array($array)) {
            foreach ($array as $key => $val) {
                if ( is_array( $array[$key] ) ) {
                    $array[$key] = self::_sanitize_recursive($func, $array[$key]);
                } else {
                    $array[$key] = call_user_func( $func, $val );
                }
            }
        } else {
            $array = call_user_func( $func, $array );
        }

        return $array;
    }

	/**
	 * @access public
	 * @return void
	 */	
	public static function change_slide_pic_url() {
		$upload_attr = wp_upload_dir();
		$qry_args = array(
			'post_type' => 'themeplaza_slider', // Change to match your post_type
			'posts_per_page' => -1, // ALL posts
		);
		$all_posts = new WP_Query( $qry_args );

		foreach ($all_posts->posts as $key) {
			$custom_slide = get_post_meta( $key->ID, 'custom_slider' );
		
			if(isset($custom_slide) && !empty($custom_slide)) {
				$custom_slide = $custom_slide[0];
				for ($i = 1; $i <= count($custom_slide); $i++) {
					if( isset($custom_slide[$i]) && is_array($custom_slide[$i]) && array_key_exists('image', $custom_slide[$i])) {
						$image_name = basename($custom_slide[$i]['image']);

						if (!file_exists($upload_attr['path'] . '/' . $image_name)) {

							// Use the WordPress API to upload the file
							$upload = wp_upload_bits($image_name, null, file_get_contents($custom_slide[$i]['image']));

							if (isset($upload['error']) && $upload['error']) {
								wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
							} else {
								$custom_slide[$i]['image'] = $upload['url'];
							} // end if/else
						} else {
							$custom_slide[$i]['image'] = $upload_attr['url'] . '/' . $image_name;
						}
					}
				}
				update_post_meta($key->ID, 'custom_slider', $custom_slide);
		}
		}
	}
}

ThemeplazaSliderAdmin::init();	