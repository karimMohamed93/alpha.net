/**
 * @package themeplaza_slider
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

jQuery(document).ready(function($) {
	jQuery(function() {
		jQuery( "#weblusive-slider-items" ).sortable({placeholder: "ui-state-highlight"});
	});

	function custom_slider_uploader(field) {
		var button = "#upload_"+field;
		jQuery(button).click(function() {
			window.restore_send_to_editor = window.send_to_editor;
			tb_show('', 'media-upload.php?referer=weblusive-settings&amp;type=image&amp;TB_iframe=true&amp;post_id=0');
			weblusive_set_slider_img(field);
			return false;
		});

	}
	function weblusive_set_slider_img(field) {
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			
			if(typeof imgurl == 'undefined') 
				imgurl = jQuery(html).attr('src');
			
			
			classes = jQuery('img', html).attr('class');
			if(typeof classes != 'undefined')
				id = classes.replace(/(.*?)wp-image-/, '');
			
			if(typeof classes == 'undefined'){ 
				classes = jQuery(html).attr('class');
				if(typeof classes != 'undefined')
					id = classes.replace(/(.*?)wp-image-/, '');
			}
			if (typeof nextCell === "undefined") var nextCell = 0;	
			jQuery('#weblusive-slider-items').append('<li id="listItem_'+ nextCell +'" class="ui-state-default zebra-table postbox"><div class="handlediv" title="Click to toggle"><br></div><h3 class="hint-title">New Slide parameters (Update to continue)</h3><div class="widget-content option-item inside"><div class="new-slider-img"><img src="'+imgurl+'" alt=""></div><input id="custom_slider['+ nextCell +'][id]" name="custom_slider['+ nextCell +'][id]" value="'+id+'" type="hidden" /><a class="del-cat"></a></div></li>');
			nextCell ++ ;
			tb_remove();
			window.send_to_editor = window.restore_send_to_editor;
		}
	};
	
	custom_slider_uploader("add_slide");	

	// Del Cats ##############################################
	jQuery(document).on("click", ".del-cat" , function(){
		jQuery(this).parent().parent().addClass('removered').fadeOut(function() {
			jQuery(this).remove();
		});
	});
});