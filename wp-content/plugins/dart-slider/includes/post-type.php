<?php
/**
 * File for registering custom post types.
 *
 * @package themeplaza_slider
 * @subpackage includes
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ThemeplazaSliderCPT
{	
	function __construct()
	{
		/* Register custom post types on the 'init' hook. */
		add_action( 'init', array( &$this, 'tp_register_post_type' ) );	
	}

	/**
	 * Registers post types needed by the plugin.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function tp_register_post_type() {

		/* Get the plugin settings. */
		$settings = themeplaza_slider_get_default_settings();

		/* Set up the arguments for the slider item post type. */
		$args = array(
			'description'         => '',
			'public'              => false,
			'publicly_queryable'  => false,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-images-alt2',
			'can_export'          => true,
			'hierarchical'        => false,
			'has_archive'         => false,
			'query_var'           => 'themeplaza_slider',
			'capability_type'     => 'themeplaza_slider',
			'map_meta_cap'        => false,
			'capabilities' => array(

				// meta caps (don't assign these to roles)
				'edit_post'              => 'edit_slider_items',
				'read_post'              => 'read_slider_items',
				'delete_post'            => 'delete_slider_items',

				// primitive caps used outside of map_meta_cap()
				'edit_posts'             => 'edit_slider_items',
				'edit_others_posts'      => 'edit_slider_items',
				'publish_posts'          => 'read_slider_items',
				'read_private_posts'     => 'read_slider_items',

				// primitive caps used inside of map_meta_cap()
				'read'                   => 'read_slider_items',
				'delete_posts'           => 'delete_slider_items',
				'delete_private_posts'   => 'delete_slider_items',
				'delete_published_posts' => 'delete_slider_items',
				'delete_others_posts'    => 'delete_slider_items',
				'edit_private_posts'     => 'edit_slider_items',
				'edit_published_posts'   => 'edit_slider_items'
			),

			/* The rewrite handles the URL structure. */
			'rewrite' => array(
				'slug'       => !empty( $settings['slider_item_base'] ) ? "{$settings['slider_root']}/{$settings['slider_item_base']}" : $settings['slider_root'],
				'with_front' => false,
				'pages'      => true,
				'feeds'      => false,
				'ep_mask'    => EP_PERMALINK,
			),

			/* What features the post type supports. */
			'supports' => array(
				'title',
			),

			/* Labels used when displaying the posts. */
			'labels' => array(
				'name'               => __( 'Sliders',                   THEMEPLAZA_SLIDER_DOMAIN ),
				'singular_name'      => __( 'Slider',                    THEMEPLAZA_SLIDER_DOMAIN ),
				'menu_name'          => __( 'Sliders',                   THEMEPLAZA_SLIDER_DOMAIN ),
				'name_admin_bar'     => __( 'Slider',                    THEMEPLAZA_SLIDER_DOMAIN ),
				'add_new'            => __( 'Add New',                   THEMEPLAZA_SLIDER_DOMAIN ),
				'add_new_item'       => __( 'Add New Slider',            THEMEPLAZA_SLIDER_DOMAIN ),
				'edit_item'          => __( 'Edit Slider',               THEMEPLAZA_SLIDER_DOMAIN ),
				'new_item'           => __( 'New Slider',                THEMEPLAZA_SLIDER_DOMAIN ),
				'view_item'          => __( 'View Slider',               THEMEPLAZA_SLIDER_DOMAIN ),
				'search_items'       => __( 'Search Slider',             THEMEPLAZA_SLIDER_DOMAIN ),
				'not_found'          => __( 'No sliders found',          THEMEPLAZA_SLIDER_DOMAIN ),
				'not_found_in_trash' => __( 'No sliders found in trash', THEMEPLAZA_SLIDER_DOMAIN ),
				'all_items'          => __( 'Sliders',                   THEMEPLAZA_SLIDER_DOMAIN ),
			)
		);

		/**
		 * Slider update messages.
		 *
		 * See /wp-admin/edit-form-advanced.php
		 *
		 * @param array $messages Existing slider update messages.
		 *
		 * @return array Amended slider update messages with new CPT update messages.
		 */
		function slider_updated_messages( $messages ) {
			$post             = get_post();
			$post_type        = get_post_type( $post );
			$post_type_object = get_post_type_object( $post_type );

			$messages['themeplaza_slider'] = array(
				0  => '', // Unused. Messages start at index 1.
				1  => __( 'Slider updated.', THEMEPLAZA_SLIDER_DOMAIN ),
				2  => __( 'Custom field updated.', THEMEPLAZA_SLIDER_DOMAIN ),
				3  => __( 'Custom field deleted.', THEMEPLAZA_SLIDER_DOMAIN ),
				4  => __( 'Slider updated.', THEMEPLAZA_SLIDER_DOMAIN ),
				/* translators: %s: date and time of the revision */
				5  => isset( $_GET['revision'] ) ? sprintf( __( 'Slider restored to revision from %s', THEMEPLAZA_SLIDER_DOMAIN ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6  => __( 'Slider published.', THEMEPLAZA_SLIDER_DOMAIN ),
				7  => __( 'Slider saved.', THEMEPLAZA_SLIDER_DOMAIN ),
				8  => __( 'Slider submitted.', THEMEPLAZA_SLIDER_DOMAIN ),
				9  => sprintf(
					__( 'Slider scheduled for: <strong>%1$s</strong>.', THEMEPLAZA_SLIDER_DOMAIN ),
					// translators: Publish box date format, see http://php.net/date
					date_i18n( __( 'M j, Y @ G:i', THEMEPLAZA_SLIDER_DOMAIN ), strtotime( $post->post_date ) )
				),
				10 => __( 'Slider draft updated.', THEMEPLAZA_SLIDER_DOMAIN )
			);

			if ( $post_type_object->publicly_queryable ) {
				$permalink = get_permalink( $post->ID );

				$view_link = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ), __( 'View Slider', THEMEPLAZA_SLIDER_DOMAIN ) );
				$messages['themeplaza_slider'][1] .= $view_link;
				$messages['themeplaza_slider'][6] .= $view_link;
				$messages['themeplaza_slider'][9] .= $view_link;

				$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
				$preview_link = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ), __( 'Preview Slider', THEMEPLAZA_SLIDER_DOMAIN ) );
				$messages['themeplaza_slider'][8]  .= $preview_link;
				$messages['themeplaza_slider'][10] .= $preview_link;
			}

			return $messages;
		}

		/* Register the slider item post type. */
		register_post_type( 'themeplaza_slider', $args );
		add_filter( 'post_updated_messages', 'slider_updated_messages' );
	}
}

new ThemeplazaSliderCPT;

?>