<?php
/**
 * Various functions, filters, and actions used by the plugin.
 *
 * @package themeplaza_slider
 * @subpackage includes
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Loads necessary scripts and styles */
add_action( 'admin_enqueue_scripts', 'themeplaza_slider_load_assets' );
add_action( 'wp_enqueue_scripts', 'themeplaza_slider_frontend_scripts_styles'); 

// Adds the shortcode
add_shortcode('themeplaza_slider', 'themeplaza_slider_shortcode');

/**
 * Returns the default settings for the plugin.
 *
 * @access public
 * @return array
 */
function themeplaza_slider_get_default_settings() {

	$settings = array(
		'slider_root'      		=> 'slider',
		'slider_base'      		=> '',          // defaults to 'slider_root'
		'slider_item_base' 		=> '%slider%',
	);

	return $settings;
}

/**
 * Enqueues scripts and styles
 *
 * @access public
 * @return void
 */
function themeplaza_slider_load_assets( $hook_suffix ) {
    wp_enqueue_style( 'themeplaza_slider_admin', THEMEPLAZA_SLIDER_URI . 'assets/css/themeplaza-slider-admin.css', array(), THEMEPLAZA_SLIDER_VERSION, 'all' );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'jquery-ui-sortable' );
    wp_enqueue_script( 'themeplaza_slider_admin', THEMEPLAZA_SLIDER_URI . 'assets/js/themeplaza-slider-admin.js', array('jquery', 'jquery-ui-sortable'), THEMEPLAZA_SLIDER_VERSION, true );

    //wp_register_script( 'themeplaza-admin-slider', get_template_directory_uri() . '/library/admin-panel/js/jquery.ui.slider.js', array( 'jquery', 'jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-sortable' ) , THEMEPLAZA_SLIDER_VERSION, true );  
   // wp_enqueue_script( 'themeplaza-admin-slider' );  
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_enqueue_style('thickbox');
}

/**
 * Frontend styles and scripts
 * @access public
 * @return void                                            
 */
function themeplaza_slider_frontend_scripts_styles(){
	wp_enqueue_script( 'jquery' );

    wp_register_style('themeplaza_slider', THEMEPLAZA_SLIDER_URI.'assets/css/themeplaza_slider.css', array(), THEMEPLAZA_SLIDER_VERSION, 'all' );
    wp_enqueue_style( 'themeplaza_slider');

	wp_register_style('themeplaza_slider_flex', THEMEPLAZA_SLIDER_URI.'assets/css/flexslider.css', array(), THEMEPLAZA_SLIDER_VERSION, 'all' );
    wp_enqueue_style( 'themeplaza_slider_flex');

 	wp_register_script('themeplaza_slider_fitvid', THEMEPLAZA_SLIDER_URI.'assets/js/jquery.fitvid.js', array('jquery'), THEMEPLAZA_SLIDER_VERSION, true);
    wp_enqueue_script('themeplaza_slider_fitvid');

    wp_register_script('themeplaza_slider_flex', THEMEPLAZA_SLIDER_URI.'assets/js/jquery.flexslider-min.js', array('jquery', 'themeplaza_slider_fitvid'), THEMEPLAZA_SLIDER_VERSION, true);
    wp_enqueue_script('themeplaza_slider_flex');
}

/**
 * SHORTCODE
 * @access public
 * @return string            
 */
function themeplaza_slider_shortcode( $atts ) {
	ob_start();
	if ( file_exists( get_template_directory().'/themeplaza/slider/slider-shortcode.php' )) {
        include( get_template_directory().'/themeplaza/slider/slider-shortcode.php' );
    } else {
        include( THEMEPLAZA_SLIDER_DIR.'templates/slider-shortcode.php' );
    }
    return ob_get_clean();
}
