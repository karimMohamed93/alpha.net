<?php 
/**
 * Plugin Name: Dart Slider
 * Description: This plugin allows you to manage, edit, and create unlimited number of sliders.
 * Version: 1.0.0
 * Author: UIUXAesthetics
 * Text Domain: themeplaza_slider
 * Domain Path: /locale/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * @package themeplaza_slider
 * @version 1.0.0
 * @since 1.0.0
 * @author Weblusive <info@weblusive.com>
 * @copyright Copyright (c) 2015, Weblusive LLC.
 */
class ThemeplazaSlider {
	
	/**
	 * Initialization
	 * @return void
	 */
	public static function init()
	{		
		/* Set the constants needed by the plugin. */
		add_action( 'plugins_loaded', array( get_called_class(), 'constants' ), 1 );

		/* Internationalize the text strings used. */
		add_action( 'plugins_loaded', array( get_called_class(), 'i18n' ), 2 );

		/* Load the functions files. */
		add_action( 'plugins_loaded', array( get_called_class(), 'includes' ), 3 );

		/* Load the admin files. */
		add_action( 'plugins_loaded', array( get_called_class(), 'admin' ), 4 );

		/* Register activation hook. */
		register_activation_hook( __FILE__, array( get_called_class(), 'activation' ) );		
	}

	/**
	 * Defines constants used by the plugin.
	 * @return void
	 */
	public static function constants() {
		/* Set the text domain */
		define( 'THEMEPLAZA_SLIDER_DOMAIN', 'themeplaza_slider' );

		/* Set the version */
		define( 'THEMEPLAZA_SLIDER_VERSION', '1.0.0' );

		/* Set constant path to the plugin directory. */
		define( 'THEMEPLAZA_SLIDER_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

		/* Set the constant path to the plugin directory URI. */
		define( 'THEMEPLAZA_SLIDER_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );

		/* Set the constant path to the includes directory. */
		define( 'THEMEPLAZA_SLIDER_INCLUDES', THEMEPLAZA_SLIDER_DIR . trailingslashit( 'includes' ) );

		/* Set the constant path to the admin directory. */
		define( 'THEMEPLAZA_SLIDER_ADMIN', THEMEPLAZA_SLIDER_DIR . trailingslashit( 'admin' ) );
	}	

	/**
	 * Loads the translation files.
	 * @return void
	 */	
	public static function i18n()
	{
		/* Load the translation of the plugin. */
		load_plugin_textdomain( THEMEPLAZA_SLIDER_DOMAIN, false, THEMEPLAZA_SLIDER_DIR . '/locale/' );
	}

	/**
	 * Loads the initial files needed by the plugin.
	 * @return void
	 */
	public static function includes() {
		foreach ( glob( THEMEPLAZA_SLIDER_INCLUDES . "*.php" ) as $file ) {
		    require_once $file;
		}
	}

	/**
	 * Loads the admin functions and files.
	 * @return void
	 */
	public static function admin() {
		if ( is_admin() ) {
			foreach ( glob( THEMEPLAZA_SLIDER_ADMIN . "*.php" ) as $file ) {
		    	require_once $file;
			}	
		}
	}

	/**
	 * Method that runs only when the plugin is activated.
	 * @return void
	 */
	public static function activation() {
		/* Get the administrator role. */
		$role = get_role( 'administrator' );

		// /* If the administrator role exists, add required capabilities for the plugin. */
		if ( !empty( $role ) ) {

			$role->add_cap( 'read_slider_items' );
			$role->add_cap( 'delete_slider_items' );
			$role->add_cap( 'edit_slider_items' );
		}
	}
}

ThemeplazaSlider::init();