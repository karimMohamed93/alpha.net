<?php
/**
 * Uninstall procedure for the plugin.
 *
 * @package weblusive_demo_importer
 * @version 1.0.0
 * @since 1.0.0
 * @author Weblusive <info@weblusive.com>
 * @copyright (c) 2016, Weblusive.com
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Set the text domain */
define( 'WEBLUSIVE_DEMO_IMPORTER_DOMAIN', 'weblusive_demo_importer' );

/* Make sure we're actually uninstalling the plugin. */
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
	wp_die( sprintf( __( '%s should only be called when uninstalling the plugin.', WEBLUSIVE_DEMO_IMPORTER_DOMAIN ), '<code>' . __FILE__ . '</code>' ) );

/* === Remove capabilities added by the plugin. === */

/* Get the administrator role. */
$role =& get_role( 'administrator' );

/* If the administrator role exists, remove added capabilities for the plugin. */
if ( !empty( $role ) ) {

	$role->remove_cap( 'read_demo_importer_items' );
	$role->remove_cap( 'delete_demo_importer_items' );
	$role->remove_cap( 'edit_demo_importer_items' );
}

?>