<?php
/**
 * Various functions, filters, and actions used by the plugin.
 *
 * @package weblusive_demo_importer
 * @subpackage includes
 * @version 1.0.0
 * @since 1.0.0
 * @author Weblusive <info@weblusive.com>
 * @copyright (c) 2016, Weblusive LLC.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Loads necessary scripts and styles */
add_action( 'admin_enqueue_scripts', 'weblusive_demo_importer_load_assets' );
add_action( 'wp_enqueue_scripts', 'weblusive_demo_importer_frontend_scripts_styles'); 

// Adds the shortcode
add_shortcode('weblusive_demo_importer', 'weblusive_demo_importer_shortcode');

/**
 * Returns the default settings for the plugin.
 *
 * @access public
 * @return array
 */
function weblusive_demo_importer_get_default_settings() {

	$settings = array(
		'faq_root'      		=> 'faq',
		'faq_base'      		=> '',          // defaults to 'faq_root'
		'faq_item_base' 		=> '%faq%',
	);

	return $settings;
}

/**
 * Enqueues scripts and styles
 *
 * @access public
 * @return void
 */
function weblusive_demo_importer_load_assets( $hook_suffix ) {
	//wp_enqueue_style( 'weblusive_demo_importer_css_admin', WEBLUSIVE_DEMO_IMPORTER_URI . 'assets/css/admin.css', array(), WEBLUSIVE_DEMO_IMPORTER_VERSION, 'all' );
    wp_enqueue_script( 'jquery');
	// wp_enqueue_script( 'weblusive_demo_importer_js_admin', WEBLUSIVE_DEMO_IMPORTER_URI . 'assets/js/admin.js', array('jquery'), WEBLUSIVE_DEMO_IMPORTER_VERSION, true );

}

/**
 * Frontend styles and scripts
 * @access public
 * @return void                                            
 */
function weblusive_demo_importer_frontend_scripts_styles(){
	//	wp_enqueue_script('jquery');
	//  wp_enqueue_style('weblusive_demo_importer', WEBLUSIVE_DEMO_IMPORTER_URI.'assets/css/weblusive_demo_importer.css', array(), WEBLUSIVE_DEMO_IMPORTER_VERSION, 'all' );

}
