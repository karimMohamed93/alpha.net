<?php
/**
 * Admin functions for the plugin.
 *
 * @package weblusive_demo_importer
 * @subpackage admin
 * @version 1.0.0
 * @since 1.0.0
 * Author: Weblusive

 * @copyright Copyright (c) 2016, Weblusive.com
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Set up the admin functionality. */
add_action( 'admin_menu', 'weblusive_demo_importer_admin_setup' );


/**
 * Adds actions where needed for setting up the plugin's admin functionality.
 * @return void
 */
function weblusive_demo_importer_admin_setup() {
	
	
}



?>