<?php 
/**
 * Plugin Name: Weblusive Demo Importer
 * Description: This plugin allows you to import the theme's sample content and make it look as showcased in the demo
 * Version: 1.0.0
 * Author: Weblusive
 * Text Domain: weblusive_demo_importer
 * Domain Path: /locale/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * @package weblusive_demo_importer
 * @version 1.0.0
 * @since 1.0.0
 * @author Weblusive <info@weblusive.com>
 * @copyright (c) 2016, Weblusive LLC.
 */
class WeblusiveDemoImporter {
	
	/**
	 * Initialization
	 * @return void
	 */
	public static function init()
	{		
		/* Set the constants needed by the plugin. */
		add_action( 'plugins_loaded', array( get_called_class(), 'constants' ), 1 );

		/* Internationalize the text strings used. */
		add_action( 'plugins_loaded', array( get_called_class(), 'i18n' ), 2 );

		/* Load the functions files. */
		add_action( 'plugins_loaded', array( get_called_class(), 'includes' ), 3 );

		/* Load the admin files. */
		add_action( 'plugins_loaded', array( get_called_class(), 'admin' ), 4 );

		/* Register activation hook. */
		register_activation_hook( __FILE__, array( get_called_class(), 'activation' ) );		
	}

	/**
	 * Defines constants used by the plugin.
	 * @return void
	 */
	public static function constants() {
		/* Set the text domain */
		define( 'WEBLUSIVE_DEMO_IMPORTER_DOMAIN', 'weblusive_demo_importer' );

		/* Set the version */
		define( 'WEBLUSIVE_DEMO_IMPORTER_VERSION', '1.0.0' );

		/* Set constant path to the plugin directory. */
		define( 'WEBLUSIVE_DEMO_IMPORTER_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

		/* Set the constant path to the plugin directory URI. */
		define( 'WEBLUSIVE_DEMO_IMPORTER_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );

		/* Set the constant path to the includes directory. */
		define( 'WEBLUSIVE_DEMO_IMPORTER_INCLUDES', WEBLUSIVE_DEMO_IMPORTER_DIR . trailingslashit( 'includes' ) );

		/* Set the constant path to the admin directory. */
		define( 'WEBLUSIVE_DEMO_IMPORTER_ADMIN', WEBLUSIVE_DEMO_IMPORTER_DIR . trailingslashit( 'admin' ) );
	}	

	/**
	 * Loads the translation files.
	 * @return void
	 */	
	public static function i18n()
	{
		/* Load the translation of the plugin. */
		load_plugin_textdomain( WEBLUSIVE_DEMO_IMPORTER_DOMAIN, false, WEBLUSIVE_DEMO_IMPORTER_DIR . '/locale/' );
	}

	/**
	 * Loads the initial files needed by the plugin.
	 * @return void
	 */
	public static function includes() {
		foreach ( glob( WEBLUSIVE_DEMO_IMPORTER_INCLUDES . "*.php" ) as $file ) {
		    require_once $file;
		}
	}

	/**
	 * Loads the admin functions and files.
	 * @return void
	 */
	public static function admin() {
		if ( is_admin() ) {
			foreach ( glob( WEBLUSIVE_DEMO_IMPORTER_ADMIN . "*.php" ) as $file ) {
		    	require_once $file;
			}	
		}
	}

	/**
	 * Method that runs only when the plugin is activated.
	 * @return void
	 */
	public static function activation() {
		/* Get the administrator role. */
		$role = get_role( 'administrator' );

		// /* If the administrator role exists, add required capabilities for the plugin. */
		if ( !empty( $role ) ) {

		
		}
	}
}

WeblusiveDemoImporter::init();