<?php
/**
 * The Template for displaying all single FAQ posts.
 * 
 * @package themeplaza_faq
 * @subpackage templates
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header();
?>
<div class="section  no-padding-bottom faq-single">
	<div class="inner">
		<div class="container">
			<div class="row">
				<?php while (have_posts()) : the_post(); ?>
					<div class="tp-content col-sm-8">
						<div id="post-<?php the_ID(); ?>" <?php post_class('tp-single-faq-item'); ?>>
							<div class="entry-content">
								<?php the_post_thumbnail(); ?>
								<h1><?php the_title(); ?></h1>  
								<?php the_content(); ?>
							</div>
							<?php edit_post_link(__('Edit Post', THEMEPLAZA_FAQ_VERSION), '<span class="edit-link">', '</span><br /><br />'); ?>


							<?php // comments_template( '', true ); ?>

						</div><!-- #post -->

					</div>
				<?php endwhile; // end of the loop. ?>

				<div class="col-sm-4 tp-sidebar">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>