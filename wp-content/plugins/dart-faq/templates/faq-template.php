<?php
/**
 * The Template for displaying faq posts.
 * 
 * @package themeplaza_faq
 * @subpackage templates
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header();
$counter=1
?>
<div class="section">
	<div class="inner">
<div class="container">
    <div class="row tp-faq">
        <div class="col-sm-8">
            <div class="tp-faq">
            <?php if (is_page()) { $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts('post_type=faq&paged='.$paged); } ?>
            <?php if (have_posts()): 
                while (have_posts()) : the_post(); ?>
                        <div class="faq">
							<div class="meta">
								<span class="question"><?php _e('Que:', THEMEPLAZA_FAQ_DOMAIN); ?> <?php echo $counter?></span>
								<span class="answer"><?php _e('Ans:', THEMEPLAZA_FAQ_DOMAIN); ?>  <?php echo $counter?></span>
							</div>
							<div class="content">
								<h5><?php the_title(); ?></h5>
								<?php the_content(); ?>
							</div>
						</div> 	
						<hr>
                <?php $counter++; endwhile; ?>
            

            <?php else: ?>

                <div id="post-0" class="post no-results not-found">
                	<div class="entry-header">
                		<h1 class="entry-title"><?php _e( 'Nothing Found', THEMEPLAZA_FAQ_DOMAIN ); ?></h1>
                	</div>

                	<div class="entry-content">
                		<p><?php _e( 'Apologies, but no results were found.', THEMEPLAZA_FAQ_DOMAIN ); ?></p>
                	</div><!-- .entry-content -->
                </div><!-- #post-0 -->
            <?php endif; ?>
            </div>
			<nav>
            <?php themeplaza_faq_pagination(); ?>
			</nav>
        </div>
        <div class="col-sm-4">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
	</div>
</div>
<?php get_footer(); ?>