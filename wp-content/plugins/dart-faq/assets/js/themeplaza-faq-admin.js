/**
 * @package themeplaza_faq
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

jQuery(document).ready(function($) {
	jQuery(function($) {
		jQuery('#sortable-table-faq tbody').sortable({
			axis: 'y',
			handle: '.column-order',
			placeholder: 'ui-state-highlight',
			forcePlaceholderSize: true,
			update: function(event, ui) {
				var theOrder = jQuery(this).sortable('toArray');
	
				var data = {
					action: 'themeplaza_faq_faq_update_post_order',
					postType: jQuery(this).attr('data-post-type'),
					order: theOrder
				};
	
				jQuery.post(ajaxurl, data, function(response) {
					if(response) {
						jQuery(".wrap h2").after('<p class="faq-alert">' + response + '</p>');
					}
				});
			}
		}).disableSelection();
	
	});
});