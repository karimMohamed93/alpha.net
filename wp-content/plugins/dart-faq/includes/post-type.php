<?php
/**
 * File for registering custom post types.
 *
 * @package themeplaza_faq
 * @subpackage includes
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ThemeplazaFaqCPT
{	
	function __construct()
	{
		/* Register custom post types on the 'init' hook. */
		add_action( 'init', array( &$this, 'tp_register_post_type' ) );	
	}

	/**
	 * Registers post types needed by the plugin.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function tp_register_post_type() {

		/* Get the plugin settings. */
		$settings = themeplaza_faq_get_default_settings();

		/* Set up the arguments for the slider item post type. */
		$args = array(
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-format-status',
			'can_export'          => true,
			'hierarchical'        => false,
			'has_archive'         => $settings['faq_root'],
			'query_var'           => 'faq',
			'capability_type'     => 'faq',
			'map_meta_cap'        => false,
			'capabilities' => array(

				// meta caps (don't assign these to roles)
				'edit_post'              => 'edit_faq_items',
				'read_post'              => 'read_faq_items',
				'delete_post'            => 'delete_faq_items',

				// primitive caps used outside of map_meta_cap()
				'edit_posts'             => 'edit_faq_items',
				'edit_others_posts'      => 'edit_faq_items',
				'publish_posts'          => 'read_faq_items',
				'read_private_posts'     => 'read_faq_items',

				// primitive caps used inside of map_meta_cap()
				'read'                   => 'read_faq_items',
				'delete_posts'           => 'delete_faq_items',
				'delete_private_posts'   => 'delete_faq_items',
				'delete_published_posts' => 'delete_faq_items',
				'delete_others_posts'    => 'delete_faq_items',
				'edit_private_posts'     => 'edit_faq_items',
				'edit_published_posts'   => 'edit_faq_items'
			),

			/* The rewrite handles the URL structure. */
			'rewrite' => array(
				'slug'       => !empty( $settings['faq_item_base'] ) ? "{$settings['faq_root']}/{$settings['faq_item_base']}" : $settings['faq_root'],
				'with_front' => false,
				'pages'      => true,
				'feeds'      => false,
				'ep_mask'    => EP_PERMALINK,
			),

			/* What features the post type supports. */
			'supports' => array(
				'title',
				'editor',
				'excerpt',
			),

			/* Labels used when displaying the posts. */
			'labels' => array(
				'name'               => __( 'Faqs',						THEMEPLAZA_FAQ_DOMAIN ),
				'singular_name'      => __( 'Faq',						THEMEPLAZA_FAQ_DOMAIN ),
				'menu_name'          => __( 'Faqs',						THEMEPLAZA_FAQ_DOMAIN ),
				'name_admin_bar'     => __( 'faq',						THEMEPLAZA_FAQ_DOMAIN ),
				'add_new'            => __( 'Add New',                  THEMEPLAZA_FAQ_DOMAIN ),
				'add_new_item'       => __( 'Add New Faq',				THEMEPLAZA_FAQ_DOMAIN ),
				'edit_item'          => __( 'Edit Faq',					THEMEPLAZA_FAQ_DOMAIN ),
				'new_item'           => __( 'New Faq',					THEMEPLAZA_FAQ_DOMAIN ),
				'view_item'          => __( 'View Faq',					THEMEPLAZA_FAQ_DOMAIN ),
				'search_items'       => __( 'Search Faq',				THEMEPLAZA_FAQ_DOMAIN ),
				'not_found'          => __( 'No faq found',				THEMEPLAZA_FAQ_DOMAIN ),
				'not_found_in_trash' => __( 'No faqs found in trash',	THEMEPLAZA_FAQ_DOMAIN ),
				'all_items'          => __( 'Faqs',						THEMEPLAZA_FAQ_DOMAIN ),
			)
		);

		/**
		 * Faq update messages.
		 *
		 * See /wp-admin/edit-form-advanced.php
		 *
		 * @param array $messages Existing slider update messages.
		 *
		 * @return array Amended slider update messages with new CPT update messages.
		 */
		function faq_updated_messages( $messages ) {
			$post             = get_post();
			$post_type        = get_post_type( $post );
			$post_type_object = get_post_type_object( $post_type );

			$messages['faq'] = array(
				0  => '', // Unused. Messages start at index 1.
				1  => __( 'Faq updated.', THEMEPLAZA_FAQ_DOMAIN ),
				2  => __( 'Custom field updated.', THEMEPLAZA_FAQ_DOMAIN ),
				3  => __( 'Custom field deleted.', THEMEPLAZA_FAQ_DOMAIN ),
				4  => __( 'Faq updated.', THEMEPLAZA_FAQ_DOMAIN ),
				/* translators: %s: date and time of the revision */
				5  => isset( $_GET['revision'] ) ? sprintf( __( 'Faq restored to revision from %s', THEMEPLAZA_FAQ_DOMAIN ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6  => __( 'Faq published.', THEMEPLAZA_FAQ_DOMAIN ),
				7  => __( 'Faq saved.', THEMEPLAZA_FAQ_DOMAIN ),
				8  => __( 'Faq submitted.', THEMEPLAZA_FAQ_DOMAIN ),
				9  => sprintf(
					__( 'Faq scheduled for: <strong>%1$s</strong>.', THEMEPLAZA_FAQ_DOMAIN ),
					// translators: Publish box date format, see http://php.net/date
					date_i18n( __( 'M j, Y @ G:i', THEMEPLAZA_FAQ_DOMAIN ), strtotime( $post->post_date ) )
				),
				10 => __( 'Faq draft updated.', THEMEPLAZA_FAQ_DOMAIN )
			);

			if ( $post_type_object->publicly_queryable ) {
				$permalink = get_permalink( $post->ID );

				$view_link = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ), __( 'View Faq', THEMEPLAZA_FAQ_DOMAIN ) );
				$messages['faq'][1] .= $view_link;
				$messages['faq'][6] .= $view_link;
				$messages['faq'][9] .= $view_link;

				$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
				$preview_link = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ), __( 'Preview Faq', THEMEPLAZA_FAQ_DOMAIN ) );
				$messages['faq'][8]  .= $preview_link;
				$messages['faq'][10] .= $preview_link;
			}

			return $messages;
		}

		/* Register the slider item post type. */
		register_post_type( 'faq', $args );
		add_filter( 'post_updated_messages', 'faq_updated_messages' );
	}
}

new ThemeplazaFaqCPT;

?>