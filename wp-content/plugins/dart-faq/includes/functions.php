<?php
/**
 * Various functions, filters, and actions used by the plugin.
 *
 * @package themeplaza_faq
 * @subpackage includes
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Loads necessary scripts and styles */
add_action( 'admin_enqueue_scripts', 'themeplaza_faq_load_assets' );
add_action( 'wp_enqueue_scripts', 'themeplaza_faq_frontend_scripts_styles'); 

// Adds the shortcode
add_shortcode('themeplaza_faq', 'themeplaza_faq_shortcode');

/**
 * Returns the default settings for the plugin.
 *
 * @access public
 * @return array
 */
function themeplaza_faq_get_default_settings() {

	$settings = array(
		'faq_root'      		=> 'faq',
		'faq_base'      		=> '',          // defaults to 'faq_root'
		'faq_item_base' 		=> '%faq%',
	);

	return $settings;
}

/**
 * Enqueues scripts and styles
 *
 * @access public
 * @return void
 */
function themeplaza_faq_load_assets( $hook_suffix ) {
    wp_enqueue_style( 'themeplaza_faq_admin', THEMEPLAZA_FAQ_URI . 'assets/css/themeplaza-faq-admin.css', array(), THEMEPLAZA_FAQ_VERSION, 'all' );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'jquery-ui-sortable' );
    wp_enqueue_script( 'themeplaza_faq_admin', THEMEPLAZA_FAQ_URI . 'assets/js/themeplaza-faq-admin.js', array('jquery', 'jquery-ui-sortable'), THEMEPLAZA_FAQ_VERSION, true );

}

/**
 * Frontend styles and scripts
 * @access public
 * @return void                                            
 */
function themeplaza_faq_frontend_scripts_styles(){
	wp_enqueue_script( 'jquery' );

    wp_register_style('themeplaza_faq', THEMEPLAZA_FAQ_URI.'assets/css/themeplaza_faq.css', array(), THEMEPLAZA_FAQ_VERSION, 'all' );
    wp_enqueue_style( 'themeplaza_faq');

}



/**
 * faq template files - POST and ARCHIVE
 * @access public
 * @return void                                     
 */

if ( ! function_exists( 'themeplaza_faq_template_redirect' ) ) {
	function themeplaza_faq_template_redirect() {
		global $wp_query, $post, $posts;
		if( get_query_var( 'faq' )  ) {
              if ( file_exists( get_template_directory().'/single-faq.php' )) {
                  include( get_template_directory().'/single-faq.php' );
              } else {
                  include( THEMEPLAZA_FAQ_DIR.'templates/single-faq.php' );
              }
			exit();
		}
		elseif( get_query_var( 'faq_category' ) || get_query_var( 'post_type' ) == 'faq' ) {
              if ( file_exists( get_template_directory().'/faq-template.php' )) {
                  include( get_template_directory().'/faq-template.php' );
              } else {
                 include( THEMEPLAZA_FAQ_DIR.'templates/faq-template.php' );
              }
			exit();
		}
	}
}

/**
 * faq SHORTCODE 
 * @access public
 * @return string            
 */
function themeplaza_faq_shortcode( $atts ) {
	ob_start();
	if ( file_exists( get_template_directory().'/themeplaza/faq/faq-template.php' )) {
        include( get_template_directory().'/themeplaza/faq/faq-template.php' );
    } else {
        include( THEMEPLAZA_FAQ_DIR.'templates/faq-template.php' );
    }
    return ob_get_clean();
}

/**
 * Pagination 
 * @access public
 * @return string 
 */
function themeplaza_faq_pagination() {
    if (function_exists('themeplaza_faq_pagination')) {
        global $wp_query;
        $big = 999999999;
        echo '<div class="themeplaza_faq_paging">'.paginate_links(array(
            'type' => 'list',
            'current' => 0,
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages
        )).'</div>';
    } else {
        wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', THEMEPLAZA_FAQ_DIR ), 'after' => '</div>' ) );
    }
}
