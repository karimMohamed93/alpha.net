<?php
/**
 * Admin functions for the plugin.
 *
 * @package themeplaza_faq
 * @subpackage admin
 * @version 1.0.0
 * @since 1.0.0
 * @author Grig <grigpage@gmail.com>
 * @copyright Copyright (c) 2015, Themeplaza
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/* Set up the admin functionality. */
add_action( 'admin_menu', 'themeplaza_faq_admin_setup' );

/* Adds new submenu for sorting of faq items */
add_action( 'admin_menu', 'themeplaza_faq_register_faq_menu' );

/* Adds support of AJAX for the sorting page*/
add_action( 'wp_ajax_themeplaza_faq_faq_update_post_order', 'themeplaza_faq_faq_update_post_order' );



/**
 * Adds actions where needed for setting up the plugin's admin functionality.
 * @return void
 */
function themeplaza_faq_admin_setup() {

	/* Custom columns on the edit faq items screen. */
	add_filter( 'manage_edit-faq_columns', 'themeplaza_faq_edit_faq_columns' );
	add_action( 'manage_faq_posts_custom_column', 'themeplaza_faq_manage_faq_columns', 10, 2 );
}

/**
 * Sets up custom columns on the faq items edit screen.
 *
 * @access public
 * @param  array  $columns
 * @return array
 */
function themeplaza_faq_edit_faq_columns( $columns ) {

	unset( $columns['title'] );

	$new_columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Faq Item', THEMEPLAZA_FAQ_DOMAIN )
	);

	return array_merge( $new_columns, $columns );
}

/**
 * Displays the content of custom faq item columns on the edit screen.
 *
 * @access public
 * @param  string  $column
 * @param  int     $post_id
 * @return void
 */
function themeplaza_faq_manage_faq_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}



/**
 * Add new submenu for sorting.
 *
 * @access public
 * @return void
 */
function themeplaza_faq_register_faq_menu() {
	add_submenu_page(
		'edit.php?post_type=faq',
		'Order faq',
		'Sort items',
		'edit_pages', 'themeplaza_faq-faq-order',
		'themeplaza_faq_faq_order_page'
	);
}

/**
 * Callback function for add_submenu_page function
 * Returns the output for the sorting page
 *
 * @access public
 * @return void
 */
function themeplaza_faq_faq_order_page() 
{
	?></pre>
	<div class="wrap">
        <h2>Sort Items</h2>
        Simply drag the items up or down and they will be saved in that order.
        
        <?php $slides = new WP_Query( array( 'post_type' => 'faq', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order' ) ); ?>
        <table id="sortable-table-faq" class="wp-list-table widefat fixed posts">
            <thead>
                <tr>
                    <th class="column-order">Order</th>
                    <th class="column-title">Title</th>
         
                </tr>
            </thead>
            <tbody data-post-type="faq">
				<?php if( $slides->have_posts() )  : ?>
                    <?php while ($slides->have_posts()): $slides->the_post(); ?>
                        <tr id="post-<?php the_ID(); ?>">
                            <td class="column-order"><img title="" src="<?php echo THEMEPLAZA_FAQ_URI . 'assets/images/move-icon.png'; ?>" alt="Move Icon" width="32" height="32" /></td>
                            <td class="column-title"><strong><?php the_title(); ?></strong></td>
                         </tr>
                    <?php endwhile; ?>
                <?php else : ?>        
                    <p>No faq items found, make sure you <a href="post-new.php?post_type=faq">create one</a>.</p>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>	
            </tbody>
            <tfoot>
                <tr>
                    <th class="column-order">Order</th>
                    <th class="column-title">Title</th>
                </tr>
            </tfoot>
        </table>
 	</div>
	<pre>
	<!-- .wrap -->	
	<?php 
}

/**
 * Handle function for AJAX
 * Saves the order of sorted elements
 *
 * @access public
 * @return void
 */
function themeplaza_faq_faq_update_post_order() {
	$post_type     = $_POST['postType'];
	$order        = $_POST['order'];

	if ($post_type !== "faq") {
		wp_die("YOU NEED PERMISSIONS FOR THIS KIND OF OPERATION");
	}

	foreach( $order as $menu_order => $post_id )
	{
		$post_id         = intval( str_ireplace( 'post-', '', $post_id ) );
		$menu_order     = intval($menu_order);
		wp_update_post( array( 'ID' => $post_id, 'menu_order' => $menu_order ) );
	}

	wp_die(false);
}



?>