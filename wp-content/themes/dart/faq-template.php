<?php /* Template Name: FAQ */ 

get_header();?>

<?php
	$get_meta = get_post_custom($post->ID);
	$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
	get_template_part( 'inner-header', 'content'); 

	$hideform = weblusive_get_option('faq_hide_contact');
?>
<section class="main-wrapper faq-page pagecustom-<?php echo $post->ID?>"> 
	<div class="container">
		<div class="row">
			<div class="<?php if(!($hideform)) :?> col-lg-8 col-md-8 col-sm-12 col-xs-12<?php else :?>col-md-12<?php endif; ?>">
				<?php echo getPageContent($post->ID); ?>
				<div class="panel-group" id="accordion">
					<?php query_posts('post_type=faq&orderby=menu_order&order=ASC&showposts=999'); get_template_part( 'loop', 'faq' );?>
				</div>
			</div>
			<?php
			
			if (!($hideform)):?>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-12">
						<h3 class="widget-title">
							<?php $ctitle = weblusive_get_option('faq_contact_title'); 
							if (!empty($ctitle)) echo $ctitle;?>
						</h3>
						<?php $fcontext = weblusive_get_option('faq_contact_text'); 
							if (!empty($fcontext)) :?>
						<p class="faq-form-text">
							 <?php	echo htmlspecialchars_decode(do_shortcode($fcontext));?>
						</p>
						<?php endif; ?>
					</div>
				</div>
				<!-- Include contact template -->
					<?php
					$hideform = weblusive_get_option('faq_hide_contact');
					if (!($hideform)) {
						$mailto = weblusive_get_option('faq_contact_address');
						if (empty($mailto)) {
							echo '<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></a><p><strong>' . esc_html__('You have not set an email address yet.',  'dart').'</strong> <br /> ' . esc_html__('Please enter it via  dart settings => FAQ settings section in your admin panel. Once done, the contact form will appear instead of this message.', 'dart') . '</p></div>';
						} else {
							include_once('faq-form.php');
						}
					}
					?>
					<!-- END contact template -->
			</div>
			<?php endif; ?>
		</div>
		<div class="gap-40"></div>
    </div>
</section>
<?php get_footer();?>