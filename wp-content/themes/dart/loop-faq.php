<?php /*** The loop that displays FAQ posts.***/ 

$counter=1; 
 if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
			<a class="accordion-toggle <?php if ($counter > 1):?>collapsed <?php endif?>collapsed-icon" data-parent="#accordion" data-toggle="collapse" href="#collapse<?php echo $counter?>">
				<?php the_title(); ?>
			</a>
			</h4>
		</div>
		<div id="collapse<?php echo $counter?>" class="panel-collapse collapse <?php if ($counter == 1):?>in<?php endif?>">
			<div class="panel-body">
				<?php the_content(); ?>
			</div>
		</div>
	</div>	
<?php $counter++; endwhile; ?>