<?php /* Template Name: Blog */

get_header();
$catid = get_query_var('cat');
$cat = get_category($catid);
$blogLayout =  weblusive_get_option('blog_layout');
isset ($blogLayout) ? $blogLayout : $blogLayout=='regular';
$get_meta = get_post_custom($post->ID);
$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
get_template_part( 'inner-header', 'content'); 

?>

<div class="pagecustom-<?php echo $post->ID?>" id="blog-page"> 
	<div class="container">	
		<div class="row">
			<?php if ($weblusive_sidebar_pos == 'left'):?><div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><div class="sidebar sidebar-left"><?php get_sidebar(); ?></div></div><?php endif?>
			<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>content col-lg-12 col-md-12 col-sm-12 col-xs-12<?php else:?>col-lg-8 col-md-8 col-sm-12 col-xs-12<?php endif?>" >
				<?php 
					$temp = $wp_query;
					$wp_query= null;
					$wp_query = new WP_Query();
					$pp = get_option('posts_per_page');
					$wp_query->query('posts_per_page='.$pp.'&paged='.$paged);			
					get_template_part( 'loop', 'index' );
					wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'dart' ), 'after' => '</div>' ) ); 
				?>
			</div>
			<?php if ($weblusive_sidebar_pos == 'right'): ?><div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><div class="sidebar sidebar-right"><?php get_sidebar(); ?></div></div><?php endif?>
		</div>
	</div>
</div>
<?php get_footer(); ?>