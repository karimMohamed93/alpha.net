<?php /** The Template for displaying all single posts. **/

get_header(); 
$get_meta = get_post_custom($post->ID);

$overridepos = isset($get_meta['_weblusive_sidebar_pos']) ? $get_meta['_weblusive_sidebar_pos'] : array (0 => 'full');
$overridesidebar =isset($get_meta['_weblusive_sidebar_post']) ? $get_meta['_weblusive_sidebar_post'] : array (0 => '');

$weblusive_sidebar_pos = ($overridepos[0] == 'default') ?  weblusive_get_option('sidebar_pos') : $overridepos[0];
$sidebar = ($overridesidebar[0] == '') ?  weblusive_get_option('sidebar_post') : $overridesidebar[0];

$format =  get_post_format( $post->ID );
if (empty($format)) $format = 'standard';

$nobio = weblusive_get_option('blog_disable_authorbio');
$norelated = weblusive_get_option('blog_disable_related');
$notags = weblusive_get_option('blog_disable_tags');
$nocomments = weblusive_get_option('blog_disable_comments');
$hidedate = weblusive_get_option('blog_show_date'); 
//get_template_part( 'library/includes/page-head'); 
get_template_part( 'inner-header', 'content'); 
?>


<div id="blog-details"> 
	<div class="container">
		<div class="row">
			<?php if ($weblusive_sidebar_pos == 'left' && !empty($sidebar)): ?><div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><div class="sidebar sidebar-left"><?php get_sidebar(); ?></div></div><?php endif?>
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div class="<?php if (empty($sidebar) || $weblusive_sidebar_pos == 'full'):?>col-lg-12 col-md-12 col-sm-12 col-xs-12<?php else:?>col-lg-8 col-md-8 col-sm-12 col-xs-12<?php endif?>">
				<div class="blog-post post-format-<?php echo $format?>">
					<?php if ($format == 'standard' || $format == 'video' || $format == 'image' || $format == 'gallery'):?>
							<?php if($format == 'standard' || $format == 'image'):?>
							<div class="post-media">  
								<?php $thumbnail = get_the_post_thumbnail(); if(!empty($thumbnail)) : ?>
									<?php the_post_thumbnail('blog-default', array( 'class' => 'img-responsive' )); ?>
								<?php endif?>
							</div>
							<?php elseif ($format == 'video'):?>
							<div class="blog-video-wrapper">  
							<?php	global $wp_embed;
								$post_embed = '';
								$video = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : ''; 
								$videoself = isset($get_meta["_blog_video_selfhosted"]) ? $get_meta["_blog_video_selfhosted"][0] : ''; 
								if ($video): ?>
									<div class="full-video">
										<?php
											if ($video):
												$post_embed = $wp_embed->run_shortcode('[embed width="500" height="400"]'.$video.'[/embed]'); 
											else:
												//$post_embed = do_shortcode($videoself);
											endif;
											echo $post_embed; 
										?>
									</div>	
								<?php else:?>
									
								<?php endif?>
							</div>
							<?php elseif ($format == 'gallery'):?>
							<div class="page-slider-wrapper">
								<?php
								$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); 
								$argsThumb = array(
									'order'          => 'ASC',
									'posts_per_page'  => 99,
									'post_type'      => 'attachment',
									'post_parent'    => $post->ID,
									'post_mime_type' => 'image',
									'post_status'    => null,
									//'exclude' => get_post_thumbnail_id()
								);
								?>
								<ul class="bxslider">
									<?php 
										$attachments = get_posts($argsThumb);
										if ($attachments) {
											foreach ($attachments as $attachment) {
												$image = wp_get_attachment_url($attachment->ID, 'full', false, false);
												$alt = $attachment->post_excerpt;
												echo '<li><img src="'.$image.'" alt="'.$alt.'" ></li>';
											}
										}
										?>
								</ul>
							</div>
							<?php endif?>
					<?php endif?>
					<div class="entry-header clearfix">
						<h2 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
						<div class="post-meta">
							<?php $meta = dart_meta($post); echo $meta ?>
						</div> 
					</div>
					<div class="entry-content">
						<?php the_content()?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'dart' ), 'after' => '</div>' ) ); ?>
					</div>
                    <!-- author box -->
					<?php if (!$nobio):?>
						<?php 
							$photo = get_avatar( get_the_author_meta('email') , 90 );
							$bio = get_the_author_meta('description');
							$website = get_the_author_meta('user_url');
                            $name= get_the_author_meta('first_name');
                            $lastName=get_the_author_meta('last_name');
							
						?>
					<div class="about-author">
						<h3><?php esc_html_e('About the Author', 'dart') ?></h3>
						<div class="author-img pull-left">
							<a href="<?php echo $website?>"><?php echo $photo; ?></a>
						</div>
						<div class="author-info">
							<h3><?php echo $name.' '.$lastName; ?></h3>
							<p><?php echo $bio?></p>
						</div>
					</div>              
					<?php endif?>
				
				<?php if (!$nocomments):?>
					<div class="gap-30"></div>
					<div id="post-comments">
						<?php if( 'open' == $post->comment_status):?> 
							<?php comments_template(); ?>
						<?php endif?>
						<?php $test = false; if ($test) {comment_form();} ?>
					</div>
				<?php endif?>
				</div>
			</div>
			<?php endwhile; ?>	
			<?php if ($weblusive_sidebar_pos == 'right'  && !empty($sidebar)): ?><div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><div class="sidebar sidebar-right"><?php get_sidebar(); ?></div></div><?php endif?>
        </div>
    </div>
</div>
<?php get_footer(); ?>