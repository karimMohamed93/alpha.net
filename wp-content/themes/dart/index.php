<?php /** The default template for pages. **/ ?>

<?php get_header(); 
	wp_reset_postdata();

	 $args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'hierarchical' => 1,
		'post_type' => 'page',
		'post_status' => 'publish'
	);

	$pages = get_pages($args); 
	
	$pageCounter = 1;
	$onepager = 0;
	$al_options = get_option('al_general_settings');
	
	$walker = new pages_from_nav;
	$menu_items = wp_nav_menu( array('container' => false,'items_wrap' => '%3$s', 'depth' => 0, 'menu' =>'primary_nav', 'theme_location' =>'primary_nav', 'echo'  => false,'walker' => $walker));
	$menu_items = strip_tags ($menu_items);
	
	$pages = explode(',', trim($menu_items));
	$allpages = get_pages();

	foreach ( $allpages as $pag ) {
		$get_meta = get_post_custom($pag->ID);
		$pageType = isset ($get_meta['_weblusive_page_type']) ? $get_meta['_weblusive_page_type'][0] : '';
		if ($pageType == '2') $onepager++;
	}
	
	
	if (!empty($pages[0]) && $onepager > 0):	
	?>
	<div class="main-wrapper">	
		<?php 	
		foreach ( $pages as $pag ) {
			
			$page = get_page($pag);
			
			$template = get_post_meta( $page->ID, '_wp_page_template', true );
			$get_meta = get_post_custom($page->ID);
			$layout = isset ($custom['_page_layout']) ? $custom['_page_layout'][0] : '1';
					
			$slug = slugify($page->post_title);
			
			$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
			$headline = isset( $get_meta['_weblusive_altpagetitle'][0]) ? $get_meta["_weblusive_altpagetitle"][0] : '';
			$title = empty($headline) ?  $page->post_title :  $headline;
			$titleColor=isset( $get_meta['_weblusive_page_title_color'][0]) ? $get_meta["_weblusive_page_title_color"][0] : '';
			$hideTilte=isset( $get_meta["_weblusive_page_title_hide"][0]) ? $get_meta["_weblusive_page_title_hide"][0] : '';
			$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
			$pageType = isset ($get_meta['_weblusive_page_type']) ? $get_meta['_weblusive_page_type'][0] : '1';
			$notop = isset( $get_meta['_weblusive_page_notoppadding'][0]) ? ' nopaddingtop' : '';
			$nobottom = isset( $get_meta['_weblusive_page_nobottompadding'][0]) ? ' nopaddingbottom' : '';
			
			
			
			if ($pageType == '2')
			{
				switch ($template)
				{
					case 'homepage-template.php':
						$content = $page->post_content;
						?>
						<section id="<?php echo $slug?>" class="dart-homepage imagepar <?php echo $nobottom.$notop; ?>  waypoint pagecustom-<?php echo $page->ID?>">
								<?php  get_template_part( 'library/includes/page-head' ); ?>
								<?php dynamic_sidebar('HomePage Top sidebar'); ?>
							<?php echo do_shortcode($content) ?>
						</section>
						<?php 
					break;
					case 'default': 
					case 'page.php':
						$content = $page->post_content;
						?>
						<section id="<?php echo $slug?>" class="imagepar  waypoint  <?php echo $nobottom.$notop; ?> pagecustom-<?php echo $page->ID?>">
								<?php //get_template_part( 'library/includes/page-head' );  ?>
								<div class="container ">
									<?php if(!$hideTilte) : ?>
									    <div class="row wow bounceIn">
											<div class="col-md-12">
												<div class='text-center heading'>
													<h2 class="onetitle <?php echo $titleColor; ?>"><?php echo htmlspecialchars_decode($title)?></h2>
													<h3 class="onesubtitle <?php echo $titleColor; ?>"><?php echo htmlspecialchars_decode($tagline)?></h3>
												</div>
											</div>
										</div>
										<?php endif;?>
								</div>
							<?php if(function_exists('vc_map')) : ?>
								<div class="container">
							<?php endif;?>
									<div class="row">
										<?php echo do_shortcode($content) ?>
									</div>
							<?php if(function_exists('vc_map')) : ?>
								</div>
							<?php endif;?>
						</section>
						<?php 
					break; 
					case 'portfolio-template-2columns.php':
					case 'portfolio-template-3columns.php':
					case 'portfolio-template-4columns.php':
						$page_template_name = get_post_meta( $page->ID,'_wp_page_template',true); 
						$content = $page->post_content;
						$pageId = $page->ID;
						$itemsize = '263x351';	
						$itemlayout = 'portfolio-4-cols';	
						$colnumber = 4;
						$thumbsize = 'portfolio-4-cols';
					
						?>
						<section id="<?php echo $slug?>" class="imagepar  portfolio-section  <?php echo $nobottom.$notop; ?> pagecustom-<?php echo $page->ID?>">
							<div class="container" <?php if (isset($customBg) && !empty($customBg[0])):?>style="background-image:url(<?php echo $customBg[0]?>)"<?php endif?>>
							<?php if(!$hideTilte) : ?>
							<div class="row animation bounceIn">
								<div class="col-md-12">
									<div class='text-center heading'>
										<h2 class="onetitle <?php echo $titleColor; ?>"><?php echo htmlspecialchars_decode($title)?></h2>
										<h3 class="onesubtitle <?php echo $titleColor; ?>"><?php echo htmlspecialchars_decode($tagline)?></h3>
									</div>
								</div>
							</div>
							<?php endif;?>
							<div class="row animation fadeInUp">
								<div class="col-lg-12">
									<div class="carousel slide" id="portfolio-carousel">
										<div class="carousel-inner">
										<?php 
										$items_per_page = 777;
										$loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => $items_per_page)); 
										$cats = get_post_meta($pageId, "_page_portfolio_cat", $single = true);
										if( $cats == '' ): ?>
											<p><?php esc_html_e('No categories selected. To fix this, please login to your WP Admin area and set
												the categories you want to show by editing this page and setting one or more categories 
												in the multi checkbox field "Portfolio Categories".', 'dart')?>
											</p>
										<?php else: ?>		
											<?php 
												// If the user hasn't set a number of items per page, then use JavaScript filtering
												if( $items_per_page == 777 ) : endif; 
												$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
												//  query the posts in selected terms
												$portfolio_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'portfolio_category');
											 ?>
											 <?php if (!empty($portfolio_posts_to_query)):			
												$wp_query = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'menu_order', 'order' => 'ASC', 'post__in' => $portfolio_posts_to_query, 'paged' => $paged, 'showposts' => $items_per_page) ); 
												
												if ($wp_query->have_posts()):  ?>
												<?php $counter=1; while ($wp_query->have_posts()) : 							
													$wp_query->the_post();
													$custom = get_post_custom($post->ID);
														 
													// Get the portfolio item categories
													$cats = wp_get_object_terms($post->ID, 'portfolio_category');
																		   
																			
													if ($cats):
														$cat_slugs = '';
														foreach( $cats as $cat ) {$cat_slugs .= $cat->slug . " ";}
													endif;
													?>
										
													<?php	
														$link = ''; 
														$attachments = ''; 
														$thumbnail = get_the_post_thumbnail($post->ID, $thumbsize); 
														$animation = isset($custom["_portfolio_item_animation"][0]) ? $custom["_portfolio_item_animation"][0] : 'fade'; 
													?>
													<?php if($counter % 4 == 1 ) echo '<div class="item"><div class="row">'; ?>
													<div class="col-xs-6 col-sm-3 col-md-3">   
														<div class="thumbnail">
															<div class="caption">
																<div class="caption-content">
																	<span>
																		<?php if (!$attachments) : ?>
																			<?php if (!empty($custom['_portfolio_video'][0])) : $link = $custom['_portfolio_video'][0]; ?>
																				<a href="<?php echo $link ?>" data-rel="prettyPhoto" title="<?php the_title() ?>">
																					<i class="fa fa-film"></i>
																				</a>
																				<?php if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :?>
																					<a href="<?php echo  get_permalink(get_the_ID()); ?>" title="<?php the_title() ?>">
																						<i class="fa fa-link"></i>
																					</a>
																				<?php endif; ?>
																			<?php elseif (isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '') : ?>
																				<a href="<?php echo $custom['_portfolio_link'][0] ?>" title="<?php the_title() ?>">
																					<i class="fa fa-external-link"></i>
																				</a>
																				<?php if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :?>
																					<a href="<?php echo  get_permalink(get_the_ID()); ?>" title="<?php the_title() ?>">
																						<i class="fa fa-link"></i>
																					</a>
																				<?php endif; ?>
																			<?php elseif (isset($custom['_portfolio_no_lightbox'][0]) && $custom['_portfolio_no_lightbox'][0] != '') : $link = get_permalink(get_the_ID()); ?>
																				<a href="<?php echo $link; ?>" title="<?php the_title() ?>">
																					<i class="fa fa-link"></i>
																				</a>
																			<?php else : ?>
																				<?php
																					$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
																					$link = $full_image[0];
																					?>
																					<a data-rel="prettyPhoto" href="<?php echo $link ?>" title="<?php the_title() ?>">
																						<i class="fa fa-search"></i>
																					</a>
																					<?php if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :?>
																					<a href="<?php echo  get_permalink(get_the_ID()); ?>" title="<?php the_title() ?>">
																						<i class="fa fa-link"></i>
																					</a>
																				<?php endif; ?>
																			<?php endif; ?>       
																		<?php endif; ?>
																	</span>
																	<h3><?php the_title() ?></h3>
																	<p><?php echo limit_words(get_the_excerpt(), 5) ?></p>
																</div>
															</div>
															<?php if (!empty($thumbnail)): ?>
																	<?php the_post_thumbnail($thumbsize, array('class' => 'cover')); ?>
																<?php else :?>
																	 <img src="http://placehold.it/<?php echo $itemsize?>" alt="<?php _e ('No preview image', 'dart') ?>" />
																<?php endif?>
														</div>
													</div> 
													<?php if($counter % 4==0 ) echo '</div></div>'; ?>
													<?php $counter++; ?>
												<?php endwhile; ?>
											<?php endif;?>
										<?php endif;?>
									<?php endif?>
								</div>
								<div class="dart-carousel-controller">
									<a data-slide="prev" href="#portfolio-carousel" class="left"><i class="fa fa-chevron-left"></i></a>
									<a data-slide="next" href="#portfolio-carousel" class="right"><i class="fa fa-chevron-right"></i></a>
								</div>
							</div></div></div>
							<?php echo do_shortcode(getPageContent($pageId)); ?>
							</div>
						</section>
						<?php 
					break;
					
					case 'blog-template.php':
					$newId = $page->ID;
					$get_meta = get_post_custom($newId);
						$overridepos = isset($get_meta['_weblusive_sidebar_pos']) ? $get_meta['_weblusive_sidebar_pos'] : array (0 => 'full');
						$overridesidebar =isset($get_meta['_weblusive_sidebar_post']) ? $get_meta['_weblusive_sidebar_post'] : array (0 => '');

						$weblusive_sidebar_pos = ($overridepos[0] == 'default') ?  weblusive_get_option('sidebar_pos') : $overridepos[0];
						$sidebar = ($overridesidebar[0] == '') ?  weblusive_get_option('sidebar_post') : $overridesidebar[0];
						?>
						<section id="<?php echo $slug?>" class="imagepar blog-section blog-mini <?php echo $nobottom.$notop; ?> pagecustom-<?php echo $page->ID?>" <?php if (isset($customBg) && !empty($customBg[0])):?>style="background-image:url(<?php echo $customBg[0]?>)"<?php endif?>>
							<div class="container ">
								<div class="row">
									<?php if(!$hideTilte) : ?>
									<div class="col-md-12">
										<div class="text-center heading">
											<h2 class="onetitle <?php echo $titleColor; ?>"><?php echo htmlspecialchars_decode($title)?></h2>
											<h3 class="onesubtitle <?php echo $titleColor; ?>"><?php echo htmlspecialchars_decode($tagline)?></h3>
										</div>
									</div>
									<?php endif;?>
									<div class="col-md-12">
										<?php 
											$postlimit = weblusive_get_option('blog_op_posts');
											if (empty($postlimit)) $postlimit = 4;
									
											$blog = get_page($newId);
											$temp = $wp_query;
											$wp_query= null;
											$wp_query = new WP_Query();
											$wp_query->query('posts_per_page='.$postlimit.'&paged='.$paged);			
											get_template_part( 'loop-mini', 'index' );
											wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'dart' ), 'after' => '</div>' ) ); 
											?>
										<?php
											wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'dart' ), 'after' => '</div>' ) ); 
										?>
									</div>
									<div class="container-fluid">
										<div class="row">
											<?php 
												setup_postdata($blog);
												the_content() ;
												rewind_posts();
											?>
										</div>
									</div>
								</div>
							</div>
						</section>
						<?php 
						
					break;
					
				}
			}
			
			$pageCounter++;
		}
	?>

	</div>
	<?php else: ?>
		<?php $get_meta = get_post_custom($post->ID);
		$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'right';
		get_template_part( 'inner-header', 'content'); 
		?>

		<div class="content-dedicated pagecustom-<?php echo $post->ID?>">
			<div class="container">
				<div class="row">			
					<?php if ($weblusive_sidebar_pos == 'left'): ?><div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><div class="sidebar sidebar-left"><?php get_sidebar(); ?></div></div><?php endif?>
					<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>content col-lg-12 col-md-12 col-sm-12 col-xs-12<?php else:?>col-lg-8 col-md-8 col-sm-12 col-xs-12<?php endif?>">
						<?php 
						$temp = $wp_query;
						$wp_query= null;
						$wp_query = new WP_Query();
						$pp = get_option('posts_per_page');
						$wp_query->query('posts_per_page='.$pp.'&paged='.$paged);			
						get_template_part( 'loop', 'index' );
					
						wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'dart' ), 'after' => '</div>' ) ); 
						?>
					</div>
					
					<?php if ($weblusive_sidebar_pos == 'right'): ?><div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><div class="sidebar sidebar-right"><?php get_sidebar(); ?></div></div><?php endif?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	<?php endif;?>
<?php get_footer();?>