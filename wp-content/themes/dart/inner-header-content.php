<?php
$get_meta = get_post_custom($post->ID);
$hidetitles = weblusive_get_option('hide_titles');
$hidebreadcrumbs = weblusive_get_option('hide_breadcrumbs');
$title = get_the_title($post->ID);
$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
$alternativetitle = isset( $get_meta['_weblusive_altpagetitle'][0]) ? $get_meta["_weblusive_altpagetitle"][0] : '';
$hideheading = isset( $get_meta['_weblusive_hide_innerheading'][0]) ? $get_meta["_weblusive_hide_innerheading"][0] : '';
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 

$layout = weblusive_get_option('innerheading_layout');
if (!$layout){
	$layout = "1";
}
$bg = weblusive_get_option('innerheading_background_type'); 
$bgimage=weblusive_get_option('innerheading_background_image');
$postBg = isset( $get_meta['_weblusive_header_bg'][0]) ? $get_meta["_weblusive_header_bg"][0] : '';
$pHead=isset( $get_meta['_weblusive_post_head'][0]) ? $get_meta["_weblusive_post_head"][0] : '';
$image_id = get_post_thumbnail_id($post->ID);  
$image_url = wp_get_attachment_image_src($image_id,'full');  
$image_url = $image_url[0];
$bgimg=get_template_directory_uri() .'/images/banner/banner1.jpg' ;
if($image_id && is_page()){
	$bgimg=$image_url;
}elseif ($bgimage) {
	$bgimg=$bgimage;
}elseif ($postBg) {
	$bgimg=$postBg;
}
if(!$bgimg){
	$bgimg=get_template_directory_uri() .'/images/banner/banner1.jpg' ;
}
?>
<div class="pageHead">
<div id="inner-header" <?php if($pHead!=='none'):?>style="background: url(<?php echo $bgimg;?>) no-repeat; padding-top: 200px"<?php endif;?>>
	<?php if($pHead == 'none' && $bgimg):?>
	<img src="<?php echo $bgimg;?>" alt="" />
	<?php  else : ?>
	<div class="container">
		<?php get_template_part( 'library/includes/page-head' ); ?>
	</div>
	<?php endif; ?>
</div>

<?php if (!$hideheading):?>
<section id="inner-title">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12">
				<div class="inner-title-content">
				<?php if (!$hidetitles):?>	
					<h1>
					<?php 
						if ($term){
							echo $term->name;									
						}else{
							echo ($alternativetitle == '') ?  $title : $alternativetitle; 
						}
					?>
					</h1>
					<?php if($tagline):?><h4><?php echo htmlspecialchars_decode($tagline); ?></h4><?php endif; ?>
					<?php endif; ?>
		        	<?php if (!$hidebreadcrumbs):?>
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					<?php endif?>
	          	</div>
	        </div>
		</div>
	</div>
</section>
<?php endif; ?>
</div>
<div class="gap-40"></div>

