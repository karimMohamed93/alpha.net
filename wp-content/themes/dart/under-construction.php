<?php /* Template Name: Under Construction */ ?>

<?php get_header(); ?>
<div class="uc-header-wrapper">
	<div class="text-center logo">
		<?php
		$logo = weblusive_get_option('logo');
		$logosettings = weblusive_get_option('logo_setting');
		?>
		<?php if ($logosettings == 'logo' && !empty($logo)): ?>
			<img src="<?php echo $logo ?>" alt="<?php echo get_bloginfo('name') ?>" id="uc-logo-image" />
		<?php else: ?>
			<?php echo get_bloginfo('name') ?>
		<?php endif ?>
		
	</div>   
</div>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<?php
		$custombg = weblusive_get_option('uc_custombg');
		($custombg) ? $custombg=$custombg : '' ;
	?>
	<section class="main-wrapper uc-wrapper" <?php if ($custombg):?> style="background-image:url('<?php echo $custombg?>')"<?php endif?> data-stellar-background-ratio="0.4"> 
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<?php $maintext = weblusive_get_option( 'uc_text' ); if($maintext):?>
						<h1 class="fancy-title text-center color-white"><span><?php echo htmlspecialchars_decode(do_shortcode($maintext)) ?></span></h1>
					<?php endif; ?>
					<div class="lead text-center color-white"><?php the_content() ?></div>
					<div class="white-space space-small"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="row clock">
						<?php
						$date = '';
						$dateset = weblusive_get_option('uc_launchdate');
						$ccolor = weblusive_get_option('uc_timercolor');
						if ($dateset)
							$date = explode('/', $dateset);
						if ($date):
						?>	
							<div class="countdown styled" ></div>
						<?php else: ?>
							<div class="alert alert-danger fade in">
								<a class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></a>
								<p><strong><?php esc_html_e('Please set a correct launching date value via dart settings => Under constr. section in your admin panel.', 'dart') ?></strong></p>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-6 col-md-offset-3">
					<?php $newsletter = weblusive_get_option( 'uc_enable_newsletter' ); if($newsletter):?>
						<?php if(shortcode_exists('email-newsletter-plugin')):?>
							<?php echo do_shortcode('[email-newsletter-plugin]')?>
						<?php else:?>
							<div class="alert alert-danger fade in">
								<a class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></a>
								<p><strong><?php esc_html_e('Newsletter plugin usage was enabled via admin panel but the plugin was not found. Please install the email newsletter plugin.', 'dart') ?></strong></p>
							</div>
						<?php endif?>
					<?php endif?>
				</div>
			</div>
		</div>
	</section>
<?php endwhile; ?>	
<?php if($date):?>
<script>
	jQuery(document).ready(function(){
        var endDate = "<?php echo $date[0] ?>/<?php echo $date[1] ?>/<?php echo $date[2] ?>  00:00:00";
        jQuery('.countdown.styled').countdown({
          date: endDate,
          render: function(data) {
            jQuery(this.el).html("<div class='col-md-2 col-md-offset-2'><div class='counter-block' <?php if ($ccolor): ?>style='color:<?php echo $ccolor ?> !important'<?php endif ?>>" + this.leadingZeros(data.days, 3) + " <span>days</span></div></div>\n\<div class='col-md-2'><div class='counter-block' <?php if ($ccolor): ?>style='color:<?php echo $ccolor ?> !important'<?php endif ?>>" + this.leadingZeros(data.hours, 2) + " <span>hrs</span></div></div>\n\<div class='col-md-2'><div class='counter-block' <?php if ($ccolor): ?>style='color:<?php echo $ccolor ?> !important'<?php endif ?>>" + this.leadingZeros(data.min, 2) + " <span>min</span></div></div>\n\<div class='col-md-2'><div class='counter-block' <?php if ($ccolor): ?>style='color:<?php echo $ccolor ?> !important'<?php endif ?>>" + this.leadingZeros(data.sec, 2) + " <span>sec</span></div></div>");
          }
        });
	})
</script>
<?php endif?>
<?php get_footer();?>