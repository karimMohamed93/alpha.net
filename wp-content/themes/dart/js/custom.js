/*
  1. Fixed header
  2. Site search
  3. Main slideshow
  4. Owl Carousel
      a. Testimonial
      b. Clients
      c. Team
  5. Back to top
  6. Skills
  7. BX slider
      a. Blog Slider
      b. Portfolio item slider
  8. Isotope
  9. Animation (wow)
  10. Flickr
  
*/
jQuery(function($) {
  "use strict";

  /* ----------------------------------------------------------- */
  /*  Fixed header
  /* ----------------------------------------------------------- */

	function fixedHeader()
	{
		var windowWidth = $(window).width();
		if(windowWidth > 120 ){
			$(window).on('scroll', function(){
				if( $(window).scrollTop()>100 ){
					$('.main-nav').addClass('header-fixed animatation-active slideInDown');
				} else {
					$('.main-nav').removeClass('header-fixed animatation-active slideInDown');
				}
			});
		}else{
			$('.main-nav').addClass('fixed-menu animatation-active slideInDown');
		}
	}

	fixedHeader();
 
	$('.footer-title span').each(function(index, element) {
		var heading = $(element), word_array, last_word, first_part;

		word_array = heading.html().split(/\s+/); // split on spaces
		last_word = word_array.pop();             // pop the last word
		first_part = word_array.join(' ');        // rejoin the first words together

		heading.html([first_part, ' <strong>', last_word, '</strong>'].join(''));
	});
/* ---------------------------------------------------
	Smooth Scroll to Section
-------------------------------------------------- */

    $(function() {
        $('a.page-scroll[href*=#]:not([href=#])').click(function() {
			//added
			$('a.page-scroll').parent('li').removeClass('active');
			$(this).parent('li').addClass('active');
			//****
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
				
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top-62
                    }, 1000, 'easeInOutExpo');
                    return false;
                }
            }
        });
    });
	
	
	$(function() {
		$(".dropdown-menu>li>a" ).hover(
			function() {
				$(this).addClass( 'open' );
				$(this).parents().eq(2).addClass( 'active' );
			}, function() {
				$(this).parents().eq(2).removeClass( 'active');
				$(this).removeClass( 'open' );
			}
		);
	});
	$(function() {
		$(".dropdown-toggle").hover(
			function() {
				$(this).addClass( 'open' );
			}, function() {
				$(this).removeClass( 'open' );
			}
		);
		$('.dropdown-toggle').on('click', function() {
			$(this).toggleClass( 'open' );
		});
	});
	
		
  /* ----------------------------------------------------------- */
  /*  Site search
  /* ----------------------------------------------------------- */

  $('.navbar-nav .fa-search').on('click', function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $('.site-search .container').toggleClass('open');
  })

  $('.site-search .close').on('click', function() {
    $('.site-search .container').removeClass('open');
  })

  /* ----------------------------------------------------------- */
  /*  Portfolio Carousel
  /* ----------------------------------------------------------- */
	$('#portfolio-carousel .item:first-child').addClass('active');

      /* ----------------------------------------------------------- */
      /*  Back to top
      /* ----------------------------------------------------------- */

       $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
      // scroll body to 0px on click
      $('#back-to-top').click(function () {
          $('#back-to-top').tooltip('hide');
          $('body,html').animate({
              scrollTop: 0
          }, 800);
          return false;
      });
      
      $('#back-to-top, .ToolTip').tooltip('hide');
 
      /* ----------------------------------------------------------- */
      /*  BX slider
      /* ----------------------------------------------------------- */


       //Page slider About, Service
      $('.bxslider').bxSlider({
        mode: 'fade',
        autoControls: true,
        captions: true
      });
	  

      $('.bx-product-slider').bxSlider({
        pagerCustom: '#bx-pager'
      });


      /* ----------------------------------------------------------- */
      /*  Isotope
      /* ----------------------------------------------------------- */


      // portfolio filter
      $(window).load(function(){

        var $isotope_selectors = $('#isotope-filter.non-paginated>li>a');

        if($isotope_selectors!='undefined'){
          var $portfolio = $('.isotope');
          $portfolio.isotope({
            itemSelector : '.col-sm-3, .col-sm-4, .col-sm-6',
            layoutMode : 'fitRows'
          });
          
          $isotope_selectors.on('click', function(){
            $isotope_selectors.removeClass('active');
            $(this).addClass('active');
            var selector = $(this).attr('data-filter');
            $portfolio.isotope({ filter: selector });
            return false;
          });
        }
      });
	  
	  //Parallax

	$(function(){
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 0,
		});
	});
	

	/* ----------------------------------------------------------- */
	/*  Animation
	/* ----------------------------------------------------------- */	
		
	$('.animation').waypoint(function(direction) {
	  $(this).addClass('animation-active');
	}, { 	offset: '100%',
	triggerOnce: true });
	
	//PrettyPhoto
	$("a[data-rel^='prettyPhoto']").prettyPhoto();
});
/* ---------------------------------------------------
	Quick contact form widget
-------------------------------------------------- */

function checkemail(emailaddress){
	"use strict";
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i); 
	return pattern.test(emailaddress); 
}

jQuery(document).ready(function(){ 
	jQuery('.empty-widget-title').parent().parent('h4').css('display', 'none');
	jQuery('.empty-widget-title').parent('.footer-widget-title').css('display', 'none');
	jQuery('.sidebar-widget ul.drop-menu').removeClass('drop-menu').css('display', 'block');
	"use strict";
	jQuery('#registerErrors, .widgetinfo').hide();		
	var $messageshort = false;
	var $emailerror = false;
	var $nameshort = false;
	var $namelong = false;
	
	jQuery('#contactFormWidget input#wformsend').click(function(){ 
		var $name = jQuery('#wname').val();
		var $email = jQuery('#wemail').val();
		var $message = jQuery('#wmessage').val();
		var $contactemail = jQuery('#wcontactemail').val();
		var $contacturl = jQuery('#wcontacturl').val();
		var $subject = jQuery('#wsubject').val();
	
		if ($name !== '' && $name.length < 3){ $nameshort = true; } else { $nameshort = false; }
		if ($name !== '' && $name.length > 30){ $namelong = true; } else { $namelong = false; }
		if ($email !== '' && checkemail($email)){ $emailerror = true; } else { $emailerror = false; }
		if ($message !== '' && $message !== 'Message' && $message.length < 3){ $messageshort = true; } else { $messageshort = false; }
		
		jQuery('#contactFormWidget .loading').animate({opacity: 1}, 250);
		
		if ($name !== '' && $nameshort !== true && $namelong !== true && $email !== '' && $emailerror !== false && $message !== '' && $messageshort !== true && $contactemail !== ''){ 
			jQuery.post($contacturl, 
				{type: 'widget', contactemail: $contactemail, subject: $subject, name: $name, email: $email, message: $message}, 
				function(/*data*/){
					jQuery('#contactFormWidget .loading').animate({opacity: 0}, 250);
					jQuery('.form').fadeOut();
					jQuery('#bottom #wname, #bottom #wemail, #bottom #wmessage').css({'border':'0'});
					jQuery('.widgeterror').hide();
					jQuery('.widgetinfo').fadeIn('slow');
					jQuery('.widgetinfo').delay(2000).fadeOut(1000, function(){ 
						jQuery('#wname, #wemail, #wmessage').val('');
						jQuery('.form').fadeIn('slow');
					});
				}
			);
			
			return false;
		} else {
			jQuery('#contactFormWidget .loading').animate({opacity: 0}, 250);
			jQuery('.widgeterror').hide();
			jQuery('.widgeterror').fadeIn('fast');
			jQuery('.widgeterror').delay(3000).fadeOut(1000);
			
			if ($name === '' || $name === 'Name' || $nameshort === true || $namelong === true){ 
				jQuery('#wname').css({'border-left':'4px solid #red'}); 
			} else { 
				jQuery('#wname').css({'border-left':'4px solid #929DAC'}); 
			}
			
			if ($email === '' || $email === 'Email' || $emailerror === false){ 
				jQuery('#wemail').css({'border-left':'4px solid red'});
			} else { 
				jQuery('#wemail').css({'border-left':'4px solid #929DAC'}); 
			}
			
			if ($message === '' || $message === 'Message' || $messageshort === true){ 
				jQuery('#wmessage').css({'border-left':'4px solid red'}); 
			} else { 
				jQuery('#wmessage').css({'border-left':'4px solid #929DAC'}); 
			}
			
			return false;
		}
	});
});