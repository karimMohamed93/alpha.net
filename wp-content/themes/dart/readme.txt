
Version 2.0 List of changes:
----------------------------

This is a major version with HUGE list of improvements under the hood.
Please refer to the documentation's main page for critical points highlighted.

The theme is now fully compatible with Wordpress 4.8+ and Woocommerce Latest version
Fixed variety of shortcode issues, also enhanced some
Improved admin panel
Fixed sample content
Added option to import Revolution Slider with 1 click
CSS fixes
Ensured full compatibility with Visual Composer 5.2+
Added option to insert your Google Map API Key (the maps no more work as is in new domains unless a key is provided)

IMPORTANT NOTE: We have removed the native shortcodes version in favor of Visual Composer as the new versions of Wordpress do not support the classic shortcodes functionality


Version 1.8 List of changes:
----------------------------
Added Full compatiblity with Wordpress 4.3+
Added Plugin activation option for easier installations / updates
Fixed the deprecated woocommerce files issue
Fixed the deprecated widget file issue
Minor fixes and improvements



Version 1.7 List of changes:
----------------------------
Minor fixes and improvements
Updated prettyphoto to fix the XSS vulnerability bug.



Version 1.6 changes:
----------------------------
Fixed the Google map issue
Fixed portfolio visibility issue in some SEO plugins
Updated the bundled plugins to latest versions



Version 1.5 changes:
----------------------------
Bug fixes and improvements



Version 1.4 changes:
----------------------------
1. Added visual composer sample content for one-page variation
2. Fixed carousel issue with RTL version
3. Improved the 1-click installer
4. Fixed one page dropdown menus not working in mobile issue
5. Fixed some elements colors not changing when global color is changed bug
6. Updated font awesome to latest version
7. Updated Revolution slider and Visual composer plugins to the latest versions
8. Added option to specify custom image for featured icon blocks
9. Fixed some responsive mode issues
10. Minor fixes and improvements



Version 1.3 Changes:
----------------------------
1. Fixed quick contact widget not working correctly issue
2. Added option in admin panel to change the go to top button image
3. Added class to hold active state of parent element when hovering over submenu
4. Minor styling fixes and improvements



Version 1.2 Changes:
----------------------------
1. Added PSD files
2. Added dark variation
3. Fixed Woocommerce cart page promo code text field not working issue.



Version 1.1 Changes:
----------------------------
1. Fixed some minor issues
2. Added a sample page with Revolution slider
3. Added revolution slider export file into sample contents folder


Refer to documentation folder on download package for theme documentation.


