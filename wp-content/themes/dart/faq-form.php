<?php /* FAQ page contact form template */ ?> 
<div class="faq-form">
		<form method="POST" class="faqform" action="<?php echo site_url()?>/library/faq-mailer.php">
			<div id="appstatus"></div>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control" type="text" id="faqname" name="faqname" placeholder="<?php esc_html_e('Name', 'dart')?>">
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control" type="text" id="faqemail" name="faqemail" placeholder="<?php esc_html_e('Email', 'dart')?>">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="form-group">
						<input class="form-control" type="text" id="faqsubject" name="faqsubject" placeholder="<?php esc_html_e('Suject', 'dart')?>">
					</div> 
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="form-group">
						<textarea class="form-control" rows="5" id="faqmessage" name="faqmessage" placeholder="<?php esc_html_e('Message...', 'dart')?>"></textarea>
					</div> 
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-left">
					<div class="form-group">
						<button class="btn btn-primary btn-lg" type="submit"  id="faqsubmit" name="faqsubmit"><?php esc_html_e('Submit', 'dart')?></button>
					</div> 
				</div>
			</div>
		</form>      
</div>
<script type="text/javascript">
	<!-- Contact form validation
	jQuery(document).ready(function(){
		"use strict";
		jQuery(".faqform").validate({
			submitHandler: function() {
			
				var postvalues =  jQuery(".faqform").serialize();
				
				jQuery.ajax
				({
					type: "POST",
					url: "<?php echo get_template_directory_uri()?>/library/faq-mailer.php",
					data: postvalues,
					success: function(response)
					{
						jQuery("#appstatus").html(response).fadeIn('normal');
						jQuery('#appstatus').delay(5000).fadeOut(1000, function(){ 
							jQuery('#faqname, #faqemail, #faqmessage').val("");
							jQuery('input[type="text"], textarea').each(function(){
							jQuery(this).attr({'data-value': jQuery(this).attr('placeholder')});
							jQuery(this).removeAttr('placeholder');
							jQuery(this).attr({'value': jQuery(this).attr('data-value')});
						});
					});
				   }
				 });
				return false;
				
			},
			focusInvalid: true,
			focusCleanup: false,
			//errorLabelContainer: jQuery("#registerErrors"),
			rules: 
			{
				faqname: {required: true},
				faqemail: {required: true, minlength: 6,maxlength: 50, email:true},
				faqmessage: {required: true, minlength: 6}
			},
			
			messages: 
			{
				faqname: {required: "<?php esc_html_e( 'Name is required', 'dart' ); ?>"},
				faqemail: {required: "<?php esc_html_e( 'E-mail is required', 'dart' ); ?>", email: "<?php esc_html_e( 'Please provide a valid e-mail', 'dart' ); ?>", minlength:"<?php esc_html_e( 'E-mail address should contain at least 6 characters', 'dart' ); ?>"},
				faqmessage: {required: "<?php esc_html_e( 'Message is required', 'dart' ); ?>"}
			},
			
			errorPlacement: function(error, element) 
			{
				error.insertBefore(element);
				jQuery('<span class="errorarr"></span>').insertBefore(element);
			},
			invalidHandler: function()
			{
				//jQuery("body").animate({ scrollTop: 0 }, "slow");
			}
			
		});
	});
	-->
</script>