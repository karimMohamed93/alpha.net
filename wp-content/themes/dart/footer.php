<section id="footer-wrapper">
	<?php 
		$footer_widget_count = weblusive_get_option('footer_widgets');
		if($footer_widget_count && $footer_widget_count !== 'none'): ?>
		<footer id="footer">
			<div class="container">
				<div class="row">
				<?php 
					if($footer_widget_count !== 'none'):
					for($i = 1; $i<= $footer_widget_count; $i++){
						if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Widget ".$i) ) :endif;
					}			
				?>
				<?php endif; ?>
				</div>
			</div>	
		</footer>
	<?php endif; ?>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-5 wow fadeInLeft">
					<?php
						$locations = get_nav_menu_locations();
						if (isset($locations['footer_nav']) && 0 !== $locations['footer_nav']) {
							$footermenu = wp_nav_menu( 
							array( 
								'theme_location' => 'footer_nav',
								'menu' =>'footer_nav', 
								'container'=>'', 
								'depth' => 1, 
								//'echo' => false,
								'menu_class' => 'footer-bottom-menu'
							)); 
						}	
						//if (!empty($footermenu)) echo $footermenu;
					?>
				</div>
				<div class="col-md-2">
					<?php if(!weblusive_get_option('hide_footer_top')):
						$topimg = weblusive_get_option('footer_gotop_icon');
						if(!$topimg) $topimg = get_template_directory_uri() .'/images/to-top.png' ;?>
						<a id="back-to-top" href="#" class="scroll-up back-to-top" role="button" title="<?php esc_html_e('Click to return to the top of page', 'dart')?>" data-toggle="tooltip" data-placement="left">
							<img class="wow flipInY" src="<?php echo $topimg; ?>" alt="to top">
						</a>
					<?php endif;?>
				</div>
				<div class="col-md-5 wow fadeInRight">
					<div class="copyright-info">
						<?php echo  htmlspecialchars_decode(do_shortcode(weblusive_get_option('footer_copyright')))?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div> <!--boddy inner-->
		
<?php if(weblusive_get_option('footer_code')) echo  htmlspecialchars_decode(weblusive_get_option('footer_code')); ?>
<?php wp_footer()?>
</body>
</html>
