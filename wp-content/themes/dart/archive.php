<?php get_header();

$sidebar = weblusive_get_option('sidebar_archive');
$sidebarPos =  weblusive_get_option('sidebar_pos');
$pagelayout = weblusive_get_option('blog_layout');

$get_meta = get_post_custom($post->ID);
$hidetitles = weblusive_get_option('hide_titles');
$hidebreadcrumbs = weblusive_get_option('hide_breadcrumbs');
$title = get_the_title($post->ID);
$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
$alternativetitle = isset( $get_meta['_weblusive_altpagetitle'][0]) ? $get_meta["_weblusive_altpagetitle"][0] : '';
$hideheading = isset( $get_meta['_weblusive_hide_innerheading'][0]) ? $get_meta["_weblusive_hide_innerheading"][0] : '';
$bg = weblusive_get_option('innerheading_background_type'); 
$bgimage=weblusive_get_option('innerheading_background_image'); 
if ($bgimage) {
	$bgimg=$bgimage;
}else{
	$bgimg=get_template_directory_uri() .'/images/banner/banner1.jpg' ;
}
?>

<div id="inner-header">
	<img src="<?php echo $bgimg;?>" alt ="banner" />
</div>
<?php if (!$hidetitles):?>
<!-- Subpage title start -->
<section id="inner-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner-title-content">
				<?php if (!$hidetitles):?>	
					<h2>
						<?php if ( is_day() ) : ?>
						<?php printf( esc_html__( 'Daily Archives: %s', 'dart' ), get_the_date() ); ?>
						<?php elseif ( is_month() ) : ?>
						<?php printf( esc_html__( 'Monthly Archives: %s', 'dart' ), get_the_date('F Y') ); ?>
						<?php elseif ( is_year() ) : ?>
						<?php printf( esc_html__( 'Yearly Archives: %s', 'dart' ), get_the_date('Y') ); ?>
						<?php elseif ( is_category() ) : ?>
						<?php single_cat_title();?>
						<?php else : ?>
						<?php esc_html_e( 'Blog Archives', 'dart' ); ?>
						<?php endif; ?>
					</h2>
					<?php endif; ?>
		        	<?php if (!$hidebreadcrumbs):?>
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					<?php endif?>
	          	</div>
	        </div>
		</div>
	</div>
</section>
<?php endif; ?>
<div class="gap-40"></div>
<section id="main-container"> 
	<div class="container" >	
		<div class="row">
			<?php if (!empty($sidebar) && $sidebarPos==='left'):?><div id="sidebar" class="col-md-4"><div class="sidebar sidebar-left"><?php get_sidebar(); ?></div></div><?php endif?>
			<div class="<?php if ($sidebarPos == 'full' || empty($sidebar)):?>col-md-12<?php else:?>col-md-8<?php endif?>">
				<?php
					
					if ( have_posts() ) the_post();
					rewind_posts();       
					
					get_template_part( 'loop', 'archive' );
				?>
			</div>	
			<?php if (!empty($sidebar) && $sidebarPos==='right') : ?><div id="sidebar" class="col-md-4"><div class="sidebar sidebar-right"><?php get_sidebar(); ?></div></div><?php endif?>
		</div>
    </div>
</section>
<?php get_footer(); ?>