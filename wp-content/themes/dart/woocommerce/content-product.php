<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


global $product;
// Ensure visibility

if ( ! $product || ! $product->is_visible() )
	return;

?>
<li <?php post_class(); ?>>
	<div class="shop-product">
		<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		<div class="overlay-wrapper">
			<a href="<?php the_permalink(); ?>">
				<?php do_action( 'woocommerce_before_shop_loop_item_title' );?>
			</a>
			<div class="product-links">
				<ul>
					<li class="cart"><?php do_action( 'woocommerce_after_shop_loop_item' ); ?></li>
					<li class="view"><a href="<?php the_permalink(); ?>" title="<?php esc_html_e('View product', 'dart')?>"><i class="fa fa-list-ul"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="shop-product-info text-center">
			<h3 class="product-title"><?php the_title(); ?></h3>
			<a href="<?php the_permalink(); ?>">	
				<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
			</a>
		</div>
	</div>
	<div class="clearfix"></div>
</li>