<!DOCTYPE html>
<html class="no-js" <?php $rtl =  weblusive_get_option('rtl_mode'); if($rtl):?>dir="rtl"<?php endif?> <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php $favicon = weblusive_get_option('favicon'); if(!empty($favicon)):?>
		<link rel="shortcut icon" href="<?php echo $favicon ?>" /> 
 	<?php endif?>

	<?php if(weblusive_get_option('header_code')) echo  htmlspecialchars_decode(weblusive_get_option('header_code')); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> data-spy="scroll" data-target="#header" data-offset="62">
<!-- Page Main Wrapper -->
<div class="body-inner <?php if(weblusive_get_option('layout_type')=='boxed'):?>container<?php endif?>">
	
<?php if (!is_page_template('under-construction.php')):?>
<!-- Header Container -->

<?php 
	$topbar_enable =  weblusive_get_option('topbar_enable'); 
	$menualignment = weblusive_get_option('main_navigation_alignment');
	if (!$menualignment) $menualignment = 'nav-left';
	$disSticky=weblusive_get_option('disable_sticky_menu');
?>
<header id="header" class="<?php if(!$disSticky) echo 'navbar-fixed-top';?> main-nav" role="banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if ($topbar_enable):?>
					<!-- Header Top Container -->
				<div class="top-info">
					<?php 
					$ltopsidebar = weblusive_get_option('topbar_lsidebar_enable');
					$rtopsidebar = weblusive_get_option('topbar_rsidebar_enable');
				?>
				<div class="row">
					<div class="col-md-6 leftTop-sidebar">
						<?php if ($ltopsidebar): ?>
							<?php dynamic_sidebar('Left Topbar sidebar'); ?>
						<?php endif ?>
					</div>
					<div class="col-md-6 rightTop-sidebar">
						<?php if ($rtopsidebar): ?>
							<?php dynamic_sidebar('Right Topbar sidebar'); ?>
						<?php endif ?>
					</div>
				</div>
				
				</div>
				<!-- /Header Top Container -->
				<?php endif ?>
				<!-- Logo start -->
				<div class="navbar-header">
					<?php 
						$logo = weblusive_get_option('logo'); 
						$logosettings =  weblusive_get_option('logo_setting');
					?>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only"><?php esc_html_e('Toggle navigation', 'dart')?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo home_url() ?>">
						<?php if($logosettings == 'logo' && !empty($logo)):?>
							<img src="<?php echo $logo ?>" alt="<?php echo get_bloginfo('name')?>" class="img-responsive" />
						<?php else:?>
							<?php echo get_bloginfo('name') ?>
						<?php endif?>
					</a>                    
				</div><!--/ Logo end -->
				<nav class="collapse navbar-collapse clearfix" role="navigation">
					<?php 
						$walker = new dart_onepage_Walker;
						if(function_exists('wp_nav_menu')):
						wp_nav_menu( 
							array( 
								'theme_location' => 'primary_nav',
								'menu' =>'primary_nav', 
								'container'=>'', 
								'depth' => 4, 
								'menu_id' => 'main-menu',
								'menu_class' => 'nav navbar-nav navbar-right',
								'walker' => $walker
							)  
						); 
						else:?>
						<ul id="main-menu" class="sm sm-default"><?php wp_list_pages('title_li=&depth=4'); ?></ul>
						<?php endif; ?>	
				</nav>
				<?php 
				$nosearchbox = weblusive_get_option('disable_header_searchbox');
				if(!$nosearchbox):?>
				<div class="site-search">
					<div class="container">
					<form action="<?php echo site_url() ?>" method="get" id="nav-search-form" class="searchbox">
						<input type="text" class="searchbox-input" onkeyup="buttonUp();" name="s" id="search" placeholder="<?php esc_html_e('Type what you want and enter', 'dart')?>" value="<?php get_search_query() ?>">
						<input name="post_type" type="hidden" value="post" />
						<button type="submit" class="fa fa-search"></button>
						<span class="close">&times;</span>
					</form>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>
<?php endif;?>