<?php
/******************* DEFINE NAME/VERSION ********************/

$themename = "Dart";
$themefolder = "dart";

define ('theme_name', $themename );
define ('theme_ver' , 1 );

/************************************************************/


/********************* DEFINE MAIN PATHS ********************/

define('dart_PLUGINS',  get_template_directory() . '/library/includes/' ); // Shortcut to the /addons/ directory
define('dart_SHORTCODES',  get_template_directory() . '/library/functions/shortcodes/ui.php' ); // Shortcut to the /addons/ directory

$adminPath 	=  get_template_directory() . '/library/admin-panel/';
$funcPath 	=  get_template_directory() . '/library/functions/';
$incPath 	=  get_template_directory() . '/library/includes/';
$tgmPath 	=  get_template_directory() . '/plugin-activation/';

require_once ($funcPath . 'helper-functions.php');
require_once ($incPath . 'the_breadcrumb.php');
require_once ($incPath . 'portfolio_walker.php');
require_once ($funcPath . 'widgets.php');
require_once ($tgmPath . 'class-tgm-plugin-activation.php');

include (get_template_directory() . '/library/admin-panel/admin-ui.php');
include (get_template_directory() . '/library/admin-panel/admin-functions.php');
include (get_template_directory() . '/library/admin-panel/post-options.php');
include (get_template_directory() . '/library/customizer/customizer.php');

/************************************************************/

/*********** LOAD ALL REQUIRED SCRIPTS AND STYLES ***********/

function dart_load_scripts(){

	wp_enqueue_script('bootstrap',  get_template_directory_uri(). '/js/bootstrap.js', array('jquery'), '3.0.1' );
	wp_enqueue_script('countdown', get_template_directory_uri() .'/js/jquery.countdown.js', array('jquery'), '3.2', true);
	wp_enqueue_script('easing-script',  get_template_directory_uri(). '/js/jquery.easing.1.3.js', array('jquery'), '3.2', true);	
	wp_enqueue_script('isotope', get_template_directory_uri() .'/js/jquery.isotope.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('owl-carousel',  get_template_directory_uri(). '/js/owl.carousel.js', array('jquery'), '3.0.1' );
	wp_enqueue_script('flexslider',  get_template_directory_uri(). '/js/jquery.flexslider-min.js', array('jquery'), '3.0.1' );
	wp_enqueue_script('bxslider',  get_template_directory_uri(). '/js/jquery.bxslider.min.js', array('jquery'), '3.0.1' );
	wp_enqueue_script('prettyphoto', get_template_directory_uri() .'/js/jquery.prettyPhoto.js', array('jquery'), '3.2', true);
	wp_enqueue_script('waypoint', get_template_directory_uri() .'/js/jquery.waypoints.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('pie', get_template_directory_uri() .'/js/jquery.easy-pie-chart.js', array('jquery'), '3.2', true);	
	wp_enqueue_script('flickr', get_template_directory_uri().'/js/jflickrfeed.min.js', array('jquery'), '3.2', true);   
	wp_enqueue_script('Validate',  get_template_directory_uri().'/js/jquery.validate.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('smooth-scroll',  get_template_directory_uri() .'/js/smoothscroll.js');
	wp_enqueue_script('stellar', get_template_directory_uri().'/js/jquery.stellar.min.js', array('jquery'), '3.2', true);
	wp_enqueue_script('youtube-player',  get_template_directory_uri().'/js/jquery.mb.YTPlayer.js', array('jquery'), '3.2', true);        
	wp_enqueue_script('custom-scripts', get_template_directory_uri() .'/js/custom.js', array('jquery'), '3.2', true);

	$google_map_key =  weblusive_get_option('google_map_key');
	if($google_map_key){
		wp_enqueue_script('Google-map',  get_template_directory_uri().'/js/gmap3.min.js', array('jquery'), '3.2', false);
		wp_register_script('Google-map-api',  "https://maps.googleapis.com/maps/api/js?key=$google_map_key&amp;callback=initMap", '', '', true);
	}
}

function dart_load_styles(){
	wp_enqueue_style('bootstrap',  get_template_directory_uri().'/css/bootstrap.min.css');
	//wp_enqueue_style('owl-carousel',  get_template_directory_uri().'/css/owl.carousel.css');
	wp_enqueue_style('owl-carousel-styles',  get_template_directory_uri().'/js/assets/owl.carousel.css');
	wp_enqueue_style('flexslider',  get_template_directory_uri().'/css/flexslider.css');
	wp_enqueue_style('bxslider',  get_template_directory_uri().'/css/jquery.bxslider.css');
	wp_enqueue_style('lightbox-styles',  get_template_directory_uri().'/css/prettyPhoto.css');
	wp_enqueue_style('font-awesome',  get_template_directory_uri().'/css/font-awesome.min.css');
	wp_enqueue_style('animate',  get_template_directory_uri().'/css/animate.css');
	wp_enqueue_style('YT-styles',  get_template_directory_uri().'/css/YTPlayer.css');
	wp_enqueue_style('main-styles', get_stylesheet_directory_uri().'/style.css');
	wp_enqueue_style('shotcodes_styles',  get_template_directory_uri().'/css/shotcodes_styles.css');
	$mainStyle=  weblusive_get_option('color_scheme');
	if($mainStyle=='dark'){
		wp_enqueue_style('dark-styles', get_stylesheet_directory_uri().'/style-dark.css');
	}
	$rtl = weblusive_get_option('rtl_mode');
	if($rtl){
		wp_enqueue_style('rtl-styles',  get_template_directory_uri().'/css/rtl.css');
	}
	
	wp_enqueue_style('dynamic-styles',  get_template_directory_uri().'/css/dynamic-styles.php');
}


add_action( 'wp_enqueue_scripts', 'dart_load_styles' );
add_action( 'wp_enqueue_scripts', 'dart_load_scripts' );

/*add_filter('locale', 'wp_my_setLocale');
function wp_my_setLocale($locale) {
    if ( is_admin() ) {
        return 'ar';
    }

    return $locale;
}
if( ! is_admin() ) { // not admin area
        // set RTL locale
        $locale = 'en_US';
    }
    */

// Load Google Fonts
function dart_fonts_url() {
	$font_url = '';

	$latinext = weblusive_get_option('typography_latin_extended');
	$cyrillic = weblusive_get_option('typography_cyrillic');
	$cyrillicext = weblusive_get_option('typography_cyrillic_extended');
	$greek = weblusive_get_option('typography_latin_extended');
	$greekext = weblusive_get_option('typography_greek_extended');

	$extensions = array_filter(array('latin-ext' => $latinext, 'cyrillic' => $cyrillic, 'cyrillic-ext' => $cyrillicext, 'greek' => $greek, 'greek-ext' => $greekext));
	$subset = 'latin, ';
	if($extensions){
		foreach ($extensions as $key => $val) {
			$subset.= ($key.', ');
		}
	}
	$subset = substr($subset, 0,-2);


	$query_args = array(
		'family' => urlencode('Raleway:800,700,500,400,600'),
		'subset' => $subset
	);

	/*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
	if ( 'off' !== _x( 'on', 'Google font: on or off', 'dart' ) ) {
		$font_url = add_query_arg($query_args, "//fonts.googleapis.com/css" );
	}
	$font_url = esc_url($font_url);
	return $font_url;
}

function dart_fonts() {
	wp_enqueue_style( 'carpenter-fonts', dart_fonts_url(), array(), '1.0.0' );
}
add_action( 'wp_enqueue_scripts', 'dart_fonts' );

/************************************************************/

add_action( 'after_setup_theme', 'dart_setup' );
function dart_setup() {
	/************** ADD SUPPORT FOR LOCALIZATION ***************/

	load_theme_textdomain( 'dart',  get_template_directory() . '/languages' );

	$locale = get_locale();

	$locale_file =  get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) ){
		require_once( $locale_file );
	}
	/************************************************************/
	
	
	/*************** ADD SUPPORT FOR POST FORMATS ***************/
	
	add_theme_support('post-formats', array('image', 'gallery', 'video', 'audio', 'link', 'quote') );
	add_theme_support( 'title-tag' );
	
	/************* ADD SUPPORT FOR WORDPRESS 3 MENUS ************/

	add_theme_support( 'menus' );

	//Register Navigations
	add_action( 'init', 'dart_custom_menus' );
	function dart_custom_menus() {
		register_nav_menus(
			array(
				'primary_nav' => esc_html__( 'Primary Navigation', 'dart'),
				'footer_nav' => esc_html__( 'Footer Navigation', 'dart'),
			)
		);
	}
	$nosearchbox = weblusive_get_option('disable_header_searchbox'); 
	$hideCart=weblusive_get_option('disable_header_cart'); 
	$megamenuenabled = class_exists( 'mega_main_init' );
	if(class_exists('Woocommerce') && !$hideCart):
		add_filter( 'wp_nav_menu_items', 'dart_add_cart_link', 10, 2);
	endif;
	if (!$nosearchbox && !$megamenuenabled):
		add_filter( 'wp_nav_menu_items', 'add_custom_link', 10, 2);
	endif;
	
	function dart_add_cart_link( $items, $args )
	{
		$out = "";
		$rtl = weblusive_get_option('rtl_mode');
		if($args->theme_location == 'primary_nav'){
				global $woocommerce;
				$out.='<li class="nav-single">
						<a class="cart-contents" href="'.$woocommerce->cart->get_cart_url().'" title="'.esc_html__('View your shopping cart', 'dart').'"><i class="fa fa-shopping-cart"></i></a>
					</li>';	
			}
		if ($rtl){
			return $out.$items;
		}
		else{
			return $items.$out;
		}
	}
	function add_custom_link( $items, $args )
	{
		$box = "";
		$rtl = weblusive_get_option('rtl_mode');
		if($args->theme_location == 'primary_nav'){
			$box.= '
			<li class="nav-single"><button class="fa fa-search"></button></li>';
		}
		if ($rtl){
			return $box.$items;
		}
		else{
			return $items.$box;
		}
	}
	/************************************************************/


	/**************** ADD SUPPORT FOR POST THUMBS ***************/

	add_theme_support( 'post-thumbnails');

	// Define various thumbnail sizes
	add_image_size('portfolio-4-cols', 292, 300, true); 
	add_image_size('portfolio-3-cols', 450, 600, true); 
	add_image_size('portfolio-2-cols', 600, 850, true);
	
	add_image_size('blog-default', 1225, 550, true);
	add_image_size('blog-medium', 550, 400, true);
	add_image_size('blog-small', 245, 180, true);
	add_image_size('blog-thumb', 70, 65, true);
	add_image_size('blog-thumb-2', 267, 186, true);
	
	/************************************************************/
}

/******* FIX THE PORTFOLIO CATEGORY PAGINATION ISSUE ********/

$option_posts_per_page = get_option( 'posts_per_page' );
add_action( 'init', 'dart_modify_posts_per_page', 0);
function dart_modify_posts_per_page() {
    add_filter( 'option_posts_per_page', 'dart_option_posts_per_page' );
}
function dart_option_posts_per_page( $value ) {
    global $option_posts_per_page;
    if ( is_tax( 'portfolio_category') ) {
		$items_per_page = weblusive_get_option ('portfolio_category_ppp');
		if ($items_per_page)
		{
			return $items_per_page;
		}
		else
		{
			return 4;
		}
    } else {
        return $option_posts_per_page;
    }
}

/************************************************************/
function dart_meta($post){
	

	$hidecomments = weblusive_get_option('blog_show_comments'); 
	$hidecategory = weblusive_get_option('blog_show_category'); 
	$hideauthor = weblusive_get_option('blog_show_author'); 
	$hidedate = weblusive_get_option('blog_show_date'); 
	?>
		<?php if(!$hidedate): $day = get_the_time('d'); $month = get_the_time('m'); $year = get_the_time('Y');?>
			<span class="post-meta-date"><i class="fa fa-clock-o"></i><a href="<?php echo  get_day_link( $year, $month, $day ); ?>"> <?php echo date( get_option('date_format'), strtotime( get_the_date() ) ) ?></a></span>
		<?php endif?>
		<?php if(!$hideauthor): ?>
			<span class="post-meta-author"><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></span>
		<?php endif?>
		<?php if(!$hidecategory): ?>
		 <?php $category = get_the_category();  if(isset($category[0])): ?>
			<span class="post-meta-cats">
				<i class="fa fa-tags"></i>
				<a href="<?php echo get_category_link($category[0]->term_id )?>"><?php echo $category[0]->cat_name?></a>
			</span>
		 <?php endif;?>
		<?php endif?>
		 <?php if( 'open' == $post->comment_status && !$hidecomments) : ?>
		<span class="post-meta-comments">
			<i class="fa fa-comment"></i>
			 <?php comments_popup_link( esc_html__( '0', 'dart' ), esc_html__( '1', 'dart' ), esc_html__( '% ', 'dart' )); ?>                              
		 </span>
		 <?php endif?>
	<?php
}

function weblusive_get_option( $name ) {
	$get_options = get_option( 'weblusive_options' );
	
	if( !empty( $get_options[$name] ))
		return $get_options[$name];
		
	return false ;
}

//Docs Url
$docs_url = "http://dartwp.weblusive-themes.com/theme-assets/documentation/";

// Redirect To Theme Options Page on Activation
if (is_admin() && isset($_GET['activated'])){
	wp_redirect(admin_url('admin.php?page=panel'));
}

/************************************************************/


/****************** REGISTER SIDEBARS ***********************/

add_filter('widget_text', 'do_shortcode');
add_filter('the_excerpt', 'do_shortcode');

add_action( 'widgets_init', 'dart_widgets_init' );
function dart_widgets_init() {
	$before_widget =  '<div id="%1$s" class="widget sidebar-widget %2$s">';
	$after_widget  =  '</div>';
	$before_title  =  '<h3 class="widget-title">';
	$after_title   =  '</h3>';
					
	register_sidebar( array(
		'name' =>  esc_html__( 'Primary Widget Area', 'dart' ),
		'id' => 'primary-widget-area',
		'description' => esc_html__( 'The Primary widget area', 'dart' ),
		'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
	) );
	
	$footer_widget_count = weblusive_get_option('footer_widgets');
	if($footer_widget_count !== 'none')
	{
		$columns = 'column3';
		switch($footer_widget_count)
		{
			case '4':
			$columns = 'col-md-3';
			break;
			case '3':
			$columns = 'col-md-4';
			break;
			case '2':
			$columns = 'col-md-6';
			break;
		}
		for($i = 1; $i<= $footer_widget_count; $i++)
		{
			//unregister_sidebar('Footer Widget '.$i);
			if ( function_exists('register_sidebar') )
			register_sidebar(array(
				'name' => 'Footer Widget '.$i,
				'id'	=> 'footer-sidebar-'.$i,
				'before_widget' => '<div class="'.$columns.'"><div class="footer-widget">',
				'after_widget' => '</div></div>',
				'before_title' => '<h3 class="footer-title"><span>',
				'after_title' => '</span></h3>',
			));
		}
	}
	
	//Custom Sidebars
	$sidebars = weblusive_get_option( 'sidebars' ) ;
	if($sidebars){
		foreach ($sidebars as $sidebar) {
			register_sidebar( array(
				'name' => $sidebar,
				'id' => sanitize_title($sidebar),
				'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
			) );
		}
	}
	$ltopsidebar = weblusive_get_option('topbar_lsidebar_enable');
	if ($ltopsidebar){
		register_sidebar(array(
			'name' =>  esc_html__('Left Topbar sidebar', 'dart' ),
			'id'	=> 'topbar-lsidebar',
			'before_widget' => '<div class="topbar-sidebar">',
			'after_widget' => '</div>',
			'before_title' => '<span class="topbar-title">',
			'after_title' => '</span>',
		));
	}
	$rtopsidebar = weblusive_get_option('topbar_rsidebar_enable');
	if ($rtopsidebar){
		register_sidebar(array(
			'name' =>  esc_html__('Right Topbar sidebar', 'dart' ),
			'id'	=> 'topbar-rsidebar',
			'before_widget' => '<div class="topbar-sidebar">',
			'after_widget' => '</div>',
			'before_title' => '<span class="topbar-title">',
			'after_title' => '</span>',
		));
	}
		register_sidebar(array(
			'name' =>  esc_html__('HomePage Top sidebar', 'dart' ),
			'id'	=> 'home-sidebar',
			'before_widget' => '<div class="home-sidebar"><div class="container">',
			'after_widget' => '</div></div>',
			'before_title' => '<span class="topbar-title">',
			'after_title' => '</span>',
		));
	
	
}
/************************************************************/


/****************** CUSTOM LOGIN LOGO ***********************/

function dart_login_logo(){
	if( weblusive_get_option('dashboard_logo') )
    echo '<style  type="text/css"> h1 a {  background-image:url('.weblusive_get_option('dashboard_logo').')  !important; } </style>';  
}  
add_action('login_head',  'dart_login_logo'); 

/************************************************************/


/******************** CUSTOM GRAVATAR ***********************/

function dart_custom_gravatar ($avatar) {
	$weblusive_gravatar = weblusive_get_option( 'gravatar' );
	
	
	if($weblusive_gravatar){
		$custom_avatar = weblusive_get_option( 'gravatar' );
		$avatar[$custom_avatar] = "Custom Gravatar";
	}
	return $avatar;
}
add_filter( 'avatar_defaults', 'dart_custom_gravatar' ); 

/************************************************************/


/********************* CUSTOM TAG CLOUDS ********************/

function dart_custom_tag_cloud_widget($args) {
	$args['number'] = 0; //adding a 0 will display all tags
	$args['largest'] = 18; //largest tag
	$args['smallest'] = 12; //smallest tag
	$args['unit'] = 'px'; //tag font unit
	$args['format'] = 'list'; //ul with a class of wp-tag-cloud
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'dart_custom_tag_cloud_widget' );

/************************************************************/


/****************** ENABLE SESSIONS *************************/

function dart_admin_init() {
	if (!session_id())
	session_start();
}

add_action('init', 'dart_admin_init');

/************************************************************/


/******************* FILL EMPTY WIDGET TITLE ****************/

function dart_fill_widget_title($title){
	if( empty( $title ) )
		return '';
	else return $title;
}
add_filter('widget_title', 'dart_fill_widget_title');

add_theme_support( 'automatic-feed-links' );
if ( ! isset( $content_width ) ) $content_width = 960;

/************************************************************/

if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	wp_enqueue_script( 'comment-reply' );
}


/****************** WOOCommerce HOOKS ***********************/

add_theme_support( 'woocommerce' );

if ( function_exists('WC')){ 
	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

	add_filter( 'woocommerce_breadcrumb_defaults', 'dart_woocommerce_breadcrumbs' );
	function dart_woocommerce_breadcrumbs() {
		return array(
				'delimiter'   => '',
				'wrap_before' => '<ul class="breadcrumb">',
				'wrap_after'  => '</ul>',
				'before'      => '<li>',
				'after'       => '</li>',
				'home'        => _x( 'Home', 'breadcrumb', 'dart' ),
			);
	}

	function dart_woopagination(){
		$perpage = weblusive_get_option('products_per_page');
		$prodperpage = 8;
		if (isset($perpage) && !empty($perpage)){
			$prodperpage =  $perpage;
		}
		return $prodperpage;
	}

	add_filter( 'loop_shop_per_page', 'dart_woopagination', 20 );


	add_action( 'woocommerce_before_shop_loop_item_title', 'dart_woo_ribbonwrapper'); 
	function dart_woo_ribbonwrapper(){
		global $product;
	 
		if ( !$product->is_in_stock() ) {
			echo '<div class="ribbon-wrapper"><div class="ribbon">' . esc_html__( 'Out Of Stock', 'dart' ) . '</div></div>';
		}
	};


	add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_new_badge', 30 ); 	// The new badge function
	function woocommerce_show_product_loop_new_badge() {
		$postdate 		= get_the_time( 'Y-m-d' );			// Post date
		$postdatestamp 	= strtotime( $postdate );			// Timestamped post date
		$newness 		= 10; 	// Newness in days as defined by option

		if ( ( time() - ( 60 * 60 * 24 * $newness ) ) < $postdatestamp ) { // If the product was published within the newness time frame display the new badge
			echo '<div class="ribbon-wrapper"><div class="ribbon bg-color-purple"><span>' . esc_html__( 'New', 'dart' ) . '</span></div></div>';
		}
	}

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
	add_action( 'woocommerce_single_product_summary',  'woocommerce_template_single_sharing', 35 );

	
}

/* Remove related products */
function wc_remove_related_products( $args ) {
	return array();
}

// Change number or products per row to 3
add_filter('loop_shop_columns', 'dart_shop_loop_columns');
if (!function_exists('dart_shop_loop_columns')) {
	function dart_shop_loop_columns() {
		$products_per_row = weblusive_get_option('products_per_row');

		if(!$products_per_row) $products_per_row = 3;
		return $products_per_row;
	}
}

$hiderelated = weblusive_get_option('hide_related_products');
if ($hiderelated){
	add_filter('woocommerce_related_products_args','wc_remove_related_products', 10); 
}


/************** Don't remove i and span tags(for icons) *************/
function override_mce_options($initArray) {
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
}
add_filter('tiny_mce_before_init', 'override_mce_options');

/*********************slugfy function************/


function slugify($text) {
	// replace non letter or digits by -
	$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	// trim
	$text = trim($text, '-');

	// transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	// lowercase
	$text = strtolower($text);

	// remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);

	if (empty($text))
	{
		return 'n-a';
	}

	return $text;
}


/*************** Gallery Fixes ****************/

/* ------------------------------------------------------------------*/
/* ADD PRETTYPHOTO REL ATTRIBUTE FOR LIGHTBOX */
/* ------------------------------------------------------------------*/
 
add_filter('wp_get_attachment_link', 'dart_rc_add_rel_attribute');
function dart_rc_add_rel_attribute($link) {
	global $post;
	return str_replace('<a href', '<a data-rel="prettyPhoto" href', $link);
}

function dart_fix_gallery($output, $attr) {
    global $post;

    static $instance = 0;
    $instance++;


    /**
     *  will remove this since we don't want an endless loop going on here
     */
    // Allow plugins/themes to override the default gallery template.
    //$output = apply_filters('post_gallery', '', $attr);

    // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
    if ( isset( $attr['orderby'] ) ) {
        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
        if ( !$attr['orderby'] )
            unset( $attr['orderby'] );
    }

    extract(shortcode_atts(array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'itemtag'    => 'li',
        'icontag'    => 'p',
        'captiontag' => 'small',
        'columns'    => 3,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => ''
    ), $attr));

    $id = intval($id);
    if ( 'RAND' == $order )
        $orderby = 'none';

    if ( !empty($include) ) {
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( !empty($exclude) ) {
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
        $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    } else {
        $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    }

    if ( empty($attachments) )
        return '';

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment )
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }

    $itemtag = tag_escape($itemtag);
    $captiontag = tag_escape($captiontag);
    $columns = intval($columns);
    $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
    $float = is_rtl() ? 'right' : 'left';

    $selector = "gallery-{$instance}";

    $gallery_style = $gallery_div = '';
    if ( apply_filters( 'use_default_gallery_style', true ) )
       
    $size_class = sanitize_html_class( $size );
    $gallery_div = "<ul id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
    $output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

    $i = 0;
    foreach ( $attachments as $id => $attachment ) {
        $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

        $output .= "<{$itemtag} class='gallery-item'>";
        $output .= "<{$icontag} class='gallery-icon'>$link</{$icontag}>";
        $output .= "</{$itemtag}>";
    }

  

    $output .= "</ul>\n";
    return $output;
}
add_filter("post_gallery", "dart_fix_gallery",10,2);


/****************** TGM Activation *******************/

add_action( 'tgmpa_register', 'dart_register_required_plugins' );

function dart_register_required_plugins() {

	$plugins = array(
		array(
			'name'               => esc_html__('Dart Demo Importer', 'dart'), // The plugin name.
			'slug'               => 'weblusive-demo-importer', // The plugin slug (typically the folder name).
			'source'             => esc_url('http://dartwp.weblusive-themes.com/theme-assets/weblusive-demo-importer.zip'), // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '1.0.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
		),

		array(
			'name'               => esc_html__('Revolution Slider Plugin', 'dart'), // The plugin name.
			'slug'               => 'revslider', // The plugin slug (typically the folder name).
			'source'             => esc_url('http://avendor.weblusive-themes.com/revslider.zip'), // The plugin source.
			'required'           => false, // If false, the plugin is only 'recommended' instead of required.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => 'http://avendor.weblusive-themes.com/revslider.zip', // If set, overrides default API URL and points to an external URL.
		),


		array(
			'name'               => esc_html__('Visual Composer', 'dart'),// The plugin name.
			'slug'               => 'js_composer', // The plugin slug (typically the folder name).
			'source'             => esc_url('http://avendor.weblusive-themes.com/js_composer.zip'), // The plugin source.
			'required'           => false, // If false, the plugin is only 'recommended' instead of required.
			'external_url'       => 'http://avendor.weblusive-themes.com/js_composer.zip', // If set, overrides default API URL and points to an external URL.
		),

		array(
			'name'               => esc_html__('Dart Shortcodes Plugin','dart'), // The plugin name.
			'slug'               => 'dart-shortcodes', // The plugin slug (typically the folder name).
			'source'             => esc_url('http://dartwp.weblusive-themes.com/theme-assets/dart-shortcodes.zip'), // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '1.0.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
		),

		array(
			'name'               => esc_html__('Dart Portfolio Plugin', 'dart'),// The plugin name.
			'slug'               => 'dart-portfolio', // The plugin slug (typically the folder name).
			'source'             => esc_url('http://dartwp.weblusive-themes.com/theme-assets/dart-portfolio.zip'), // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '1.0.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
		),


		array(
			'name'               => esc_html__('Dart FAQ Plugin', 'dart'),// The plugin name.
			'slug'               => 'dart-faq', // The plugin slug (typically the folder name).
			'source'             => esc_url('http://dartwp.weblusive-themes.com/theme-assets/dart-faq.zip'), // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '1.0.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
		),

		array(
			'name'               => esc_html__('Dart Slider Plugin', 'dart'),// The plugin name.
			'slug'               => 'dart-slider', // The plugin slug (typically the folder name).
			'source'             => esc_url('http://dartwp.weblusive-themes.com/theme-assets/dart-slider.zip'), // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '1.0.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
		),

		array(
			'name'      => esc_html__('Contact Form 7', 'dart'),
			'slug'      => 'contact-form-7',
			'required'  => false,
		),


		array(
			'name'      => esc_html__('Easy Twitter Widget', 'dart'),
			'slug'      => 'easy-twitter-feed-widget',
			'required'  => false,
		),
	);
    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
	$config = array(
		'id'           => 'dart-activation',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'dart-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',
	);


    tgmpa( $plugins, $config );

}
?>