<?php  header("Content-type: text/css; charset: UTF-8"); 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$logo_mt = weblusive_get_option('logo_margin_top');
$logo_ml = weblusive_get_option('logo_margin_left');
 if($logo_mt || $logo_ml): ?>
.navbar-brand{
	<?php if($logo_mt):?>margin-top:<?php echo $logo_mt?>px;<?php endif?>
	<?php if($logo_ml):?>margin-left:<?php echo $logo_ml?>px;<?php endif?>
}
$preloadbg = weblusive_get_option('custom_preloader_bg'); $preloadbgcolor = weblusive_get_option('custom_preloader_bgcolor'); if ($preloadbg || $preloadbgcolor):?>
#page-preloader{
	<?php if($preloadbg):?>background-image:url(<?php echo $preloadbg?>);<?php endif?>
	<?php if($preloadbgcolor):?>background-color:<?php echo $preloadbgcolor?>;<?php endif?>
}
<?php endif; 
 $css =  weblusive_get_option('css'); if (isset($css)) echo htmlspecialchars_decode($css ) , "\n";
if( weblusive_get_option('css_tablets') ) : ?>
@media (min-width: 768px) and (max-width: 979px) { 
<?php echo htmlspecialchars_decode( weblusive_get_option('css_tablets') ) , "\n";?>
}
<?php endif; ?>
<?php if( weblusive_get_option('css_wide_phones') ) : ?>
@media (max-width: 767px) {
<?php echo htmlspecialchars_decode( weblusive_get_option('css_wide_phones') ) , "\n";?>
}
<?php endif; ?>
<?php if( weblusive_get_option('css_phones') ) : ?>
@media (max-width: 480px) {
<?php echo htmlspecialchars_decode( weblusive_get_option('css_phones') ) , "\n";?>
}
<?php endif; ?>
<?php
$args = array(
	'sort_order' => 'ASC',
	'sort_column' => 'menu_order',
	'hierarchical' => 1,
	'post_type' => 'page',
	'post_status' => 'publish'
);
$pages = get_pages($args);
foreach ( $pages as $pag ) :
	$page = get_page($pag);
	$get_meta = get_post_custom($page->ID);
	
	$bgrepeat = isset ($get_meta['_weblusive_page_bgrepeat']) ? $get_meta['_weblusive_page_bgrepeat'][0] : '';
	$bgcolor = isset ($get_meta['_weblusive_page_bgcolor']) ? $get_meta['_weblusive_page_bgcolor'][0] : '';
	$bgimage = isset ($get_meta['_weblusive_page_bg']) ? $get_meta['_weblusive_page_bg'][0] : '';	
	$color = isset ($get_meta['_weblusive_page_color']) ? $get_meta['_weblusive_page_color'][0] : '';
	
	echo '.pagecustom-'.$page->ID.'{';
		echo $bgrepeat === '' ? '' : 'background-repeat:'.$bgrepeat.' !important;';
		echo $bgcolor === '' ? '' : 'background-color:'.$bgcolor.' !important;';
		echo $bgimage === '' ? '' : 'background-image:url('.$bgimage.') !important;';
		echo 'background-position:center top';
	echo '}';
	if ($color!== ''):?>
		.pagecustom-<?php echo $page->ID?> * {color:<?php echo $color ?>}
	<?php endif; ?>	
<?php endforeach ?>