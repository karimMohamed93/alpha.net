<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
function set_html_content_type() {
	return 'text/html';
}

if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'widget'){
	$mailFromName = $mailFromEmail = $mailFromWebsite = $message = $mailTo = '';
	if (isset($_REQUEST['contactemail'])){ $mailTo = $_REQUEST['contactemail']; }
	if (isset($_REQUEST['name'])){ $mailFromName = $_REQUEST['name']; }
	if (isset($_REQUEST['email'])){ $mailFromEmail = $_REQUEST['email']; }
	if (isset($_REQUEST['message'])){ $message = $_REQUEST['message']; }
	if (isset($_REQUEST['wcontactwebsite'])){ $mailFromWebsite = $_REQUEST['wcontactwebsite']; }
	$subject = 'E-mail from website visitor';
	
	
	//$msg = "This message was sent from: $mailFromWebsite \n\nby: $mailFromName \n\nEmail: $mailFromEmail \n\nSubject: $subject \n\nText of message: $message";
	$headers = 'From: '.$mailFromName.' <'.$mailFromEmail.'>' . "\r\n";
	add_filter('wp_mail_content_type', 'set_html_content_type');
	mail($mailTo, $subject, $message, $headers);
	remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
}
?>