<?php

/**** WIDGETS AREA ****/

/* ***************************************************** 
 * Plugin Name: dart Social widget
 * Description: Display social links.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class alc_social_widget extends WP_Widget {

	// Widget setup.
	function __construct()
	{
		parent::__construct( /* Base ID */'alc-social-widget', /* Name */esc_html__('dart - Social links', 'dart'), array('description' => esc_html__('Display social links', 'dart')) );
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		
		$social_target = apply_filters('social_target', $instance['social_target']);
		$social_size = isset($instance['social_size']) ? apply_filters('social_size', $instance['social_size']) : '';
		$social_placement = isset($instance['social_placement']) ? apply_filters('social_placement', $instance['social_placement']) : '';
		
		$bitbucket = apply_filters('bitbucket', $instance['bitbucket']);
		$dribbble = isset($instance['dribbble']) ? apply_filters('dribbble', $instance['dribbble']) : '';
		$facebook = apply_filters('facebook', $instance['facebook']);
		$flickr = apply_filters('flickr', $instance['flickr']);
		$github = apply_filters('github', $instance['github']);
		$google = apply_filters('google', $instance['google']);
		$instagram = apply_filters('instagram', $instance['instagram']);
		$linkedin = apply_filters('linkedin', $instance['linkedin']);
		$pinterest = apply_filters('pinterest', $instance['pinterest']);
		$skype = apply_filters('skype', $instance['skype']);
		$twitter = apply_filters('twitter', $instance['twitter']);
		$youtube = apply_filters('youtube', $instance['youtube']);
		
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		
		$socials = array(
			'bitbucket' => $bitbucket, 
			'dribbble' => $dribbble, 
			'facebook' => $facebook, 
			'flickr' => $flickr, 
			'github' => $github, 
			'google-plus' => $google, 
			'instagram' => $instagram, 
			'linkedin' => $linkedin, 
			'pinterest' => $pinterest, 
			'skype' => $skype, 
			'twitter' => $twitter, 
			'youtube' => $youtube
		);
		$social_target = ($social_target === '0') ? '' : ' target="_blank"';
		$fullmarkup = array();
		foreach ($socials as $name => $link)
		{
			if ($link !== '')
			{
				$fullmarkup[] = '<li><a href="'.$link.'" '.$social_target.' class="ToolTip" data-toggle="tooltip" data-placement="top" title="'.ucfirst($name).'"><span class="fa fa-'.$name.' '.$social_size.'"></span></a></li>';
			}
		}
		echo '<div class="social-widget '.$social_placement.'"><ul class="list-inline">'.implode( "\n", $fullmarkup ).'</ul></div>'.$after_widget;
		
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['social_target'] = strip_tags($new_instance['social_target']);
		$instance['social_placement'] = strip_tags($new_instance['social_placement']);
		$instance['social_size'] = strip_tags($new_instance['social_size']);
		$instance['bitbucket'] = strip_tags($new_instance['bitbucket']);
		$instance['dribbble'] = strip_tags($new_instance['dribbble']);
		$instance['facebook'] = strip_tags($new_instance['facebook']);
		$instance['flickr'] = strip_tags($new_instance['flickr']);
		$instance['github'] = strip_tags($new_instance['github']);
		$instance['google'] = strip_tags($new_instance['google']);
		$instance['instagram'] = strip_tags($new_instance['instagram']);
		$instance['linkedin'] = strip_tags($new_instance['linkedin']);
		$instance['pinterest'] = strip_tags($new_instance['pinterest']);
		$instance['skype'] = strip_tags($new_instance['skype']);
		$instance['twitter'] = strip_tags($new_instance['twitter']);
		$instance['youtube'] = strip_tags($new_instance['youtube']);
		return $instance;
	}

	function form($instance) {
		$defaults = array(
		'title' => 'Social links',
		'social_target' => '',
		'social_placement' => 'top-social',
		'social_size' => '',
		'bitbucket' => '',
		'dribbble' => '',
		'facebook' => '',
		'flickr' => '',
		'github' => '',
		'google' => '',
		'instagram' => '',
		'linkedin' => '',
		'pinterest' => '',
		'skype' => '',
		'twitter' => '',
		'youtube' => '',
		);
		
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:', 'dart'); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('social_target'); ?>"><?php esc_html_e('Target window:', 'dart'); ?></label>
			<select id="<?php echo $this->get_field_id('social_target'); ?>" name="<?php echo $this->get_field_name('social_target'); ?>" class="widefat">
				<option value="0" <?php if( $instance['social_target'] == 0):?>selected="selected"<?php endif?>>Same window</option> 
				<option value="1" <?php if( $instance['social_target'] == 1):?>selected="selected"<?php endif?>>New window</option> 
			</select>
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('social_placement'); ?>"><?php esc_html_e('Placement:', 'dart'); ?></label>
			<select id="<?php echo $this->get_field_id('social_placement'); ?>" name="<?php echo $this->get_field_name('social_placement'); ?>" class="widefat">
				<option value="social-top" <?php if( $instance['social_placement'] == 'social-top'):?>selected="selected"<?php endif?>>Header</option> 
				<option value="social-footer" <?php if( $instance['social_placement'] == 'footer-social'):?>selected="selected"<?php endif?>>Footer</option> 
				<option value="social-sidebar" <?php if( $instance['social_placement'] == 'social-sidebar'):?>selected="selected"<?php endif?>>Sidebar</option> 
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('social_size'); ?>"><?php esc_html_e('Icon size:', 'dart'); ?></label>
			<select id="<?php echo $this->get_field_id('social_size'); ?>" name="<?php echo $this->get_field_name('social_size'); ?>" class="widefat">
				<option value="fa-lg" <?php if( $instance['social_size'] == ''):?>selected="selected"<?php endif?>>Regular</option> 
				<option value="fa-2x" <?php if( $instance['social_size'] == 'fa-2x'):?>selected="selected"<?php endif?>>2X size</option>
				<option value="fa-3x" <?php if( $instance['social_size'] == 'fa-3x'):?>selected="selected"<?php endif?>>3X size</option>
				<option value="fa-4x" <?php if( $instance['social_size'] == 'fa-4x'):?>selected="selected"<?php endif?>>4X size</option>
				<option value="fa-5x" <?php if( $instance['social_size'] == 'fa-5x'):?>selected="selected"<?php endif?>>5X size</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('bitbucket'); ?>"><?php esc_html_e('Bitbucket:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('bitbucket'); ?>" type="text" name="<?php echo $this->get_field_name('bitbucket'); ?>" value="<?php echo $instance['bitbucket']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('dribbble'); ?>"><?php esc_html_e('Dribble:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('dribbble'); ?>" type="text" name="<?php echo $this->get_field_name('dribbble'); ?>" value="<?php echo $instance['dribbble']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('facebook'); ?>"><?php esc_html_e('Facebook:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('facebook'); ?>" type="text" name="<?php echo $this->get_field_name('facebook'); ?>" value="<?php echo $instance['facebook']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('flickr'); ?>"><?php esc_html_e('Flickr:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('flickr'); ?>" type="text" name="<?php echo $this->get_field_name('flickr'); ?>" value="<?php echo $instance['flickr']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('github'); ?>"><?php esc_html_e('Github:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('github'); ?>" type="text" name="<?php echo $this->get_field_name('github'); ?>" value="<?php echo $instance['github']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('google'); ?>"><?php esc_html_e('Google+:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('google'); ?>" type="text" name="<?php echo $this->get_field_name('google'); ?>" value="<?php echo $instance['google']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('instagram'); ?>"><?php esc_html_e('Instagram:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('instagram'); ?>" type="text" name="<?php echo $this->get_field_name('instagram'); ?>" value="<?php echo $instance['instagram']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('linkedin'); ?>"><?php esc_html_e('LinkedIn:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('linkedin'); ?>" type="text" name="<?php echo $this->get_field_name('linkedin'); ?>" value="<?php echo $instance['linkedin']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('pinterest'); ?>"><?php esc_html_e('Pinterest:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('pinterest'); ?>" type="text" name="<?php echo $this->get_field_name('pinterest'); ?>" value="<?php echo $instance['pinterest']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('skype'); ?>"><?php esc_html_e('Skype:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('skype'); ?>" type="text" name="<?php echo $this->get_field_name('skype'); ?>" value="<?php echo $instance['skype']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('twitter'); ?>"><?php esc_html_e('Twitter:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('twitter'); ?>" type="text" name="<?php echo $this->get_field_name('twitter'); ?>" value="<?php echo $instance['twitter']; ?>" class="widefat" />
    	</p>
		<p>
			<label for="<?php echo $this->get_field_id('youtube'); ?>"><?php esc_html_e('Youtube:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('youtube'); ?>" type="text" name="<?php echo $this->get_field_name('youtube'); ?>" value="<?php echo $instance['youtube']; ?>" class="widefat" />
    	</p>
	<?php
	}
}

register_widget('alc_social_widget');


/* ***************************************************** 
 * Plugin Name: dart Flickr
 * Description: Retrieve and display photos from Flickr.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class alc_flickr_widget extends WP_Widget {

	// Widget setup.
	function __construct()
	{
		parent::__construct('alc-flickr-widget', esc_html__('dart - Flickr images', 'dart'), array('description' => esc_html__('Grab and display images from Flickr', 'dart')) );
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$id = $instance['flickr_id'];
		$nr = ($instance['flickr_nr'] != '') ? $nr = $instance['flickr_nr'] : $nr = 16;
		$randomId = mt_rand(0, 100000);
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		echo "
		<script type=\"text/javascript\">
			jQuery(document).ready(function(){
				jQuery('.flickr-widget-$randomId').jflickrfeed({
					limit: ".$nr.",
					qstrings: {
						id: '".$id."'
					},
					itemTemplate: '<li><a href=\"http://www.flickr.com/photos/".$id."\" class=\"thumb-holder\" data-rel=\"prettyPhoto\"><img src=\"{{image_s}}\" alt=\"{{title}}\" /></a></li>'
				});
			});
		</script>";
		echo '<div class="flickr-widget"><ul id="basicuse" class="flickr-list flickr-widget-'.$randomId.'"></ul></div>'.$after_widget;
		
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['flickr_id'] = strip_tags($new_instance['flickr_id']);
		$instance['flickr_nr'] = strip_tags($new_instance['flickr_nr']);
		return $instance;
	}

	function form($instance) {
		$defaults = array(
		'title' => 'Latest From Flickr',
		'flickr_nr' => '16',
		'flickr_id' => '47445714@N03'
		);


		$idGetter = 'http://www.idgettr.com';
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:', 'dart'); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
        
		<p>
			<label for="<?php echo $this->get_field_id('flickr_id'); ?>"><?php esc_html_e('Flickr ID:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('flickr_id'); ?>" type="text" name="<?php echo $this->get_field_name('flickr_id'); ?>" value="<?php echo $instance['flickr_id']; ?>" class="widefat" />
            <small style="line-height:12px;"><a href="<?php echo esc_url($idGetter)?>">Find your Flickr user or group id</a></small>
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('flickr_nr'); ?>"><?php esc_html_e('Number of photos:', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('flickr_nr'); ?>" type="text" name="<?php echo $this->get_field_name('flickr_nr'); ?>" value="<?php echo $instance['flickr_nr']; ?>" class="widefat" />
		</p>
	<?php
	}
}

register_widget('alc_flickr_widget');

/* ***************************************************** 
 * Plugin Name: dart Last Works
 * Description: Retrieve and display latest works (Portfolio).
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class alc_works_widget extends WP_Widget {
	
	// Widget setup.
	function __construct()
	{
		parent::__construct('alc-works-widget', esc_html__('dart - Latest Works', 'dart'), array('description' => esc_html__('Display latest works (Portfolio)', 'dart')) );
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['post_title']);
		
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		$post_count = $instance['post_count'];
		$featured = $instance['featured'];
		$position=$instance['position'];
		
		$args = array('post_type' => 'portfolio', 'taxonomy'=> 'portfolio_category', 'posts_per_page' => $post_count);
		if ($featured)
		{
			$args['meta_key'] = '_portfolio_featured'; 
			$args['meta_value'] = '1';
		}
		$loop = new WP_Query($args);?>
		<ul class="<?php if($position=='sidebar') :?>posts-list unstyled clearfix<?php else :?>divider<?php endif; ?>">
		<?php	while ( $loop->have_posts() ) : $loop->the_post();?>
			
			<li>
				<?php if($position=='sidebar'):?>
				<div class="posts-thumb pull-left"> 
					<?php if(has_post_thumbnail()):?>
						<?php the_post_thumbnail('blog-thumb' ); ?>
					<?php endif?>
				</div>
				<div class="post-content">
					<h4 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h4>
					<p class="post-meta">
						<span class="post-meta-author">Posted by: <?php the_author_posts_link(); ?></span>
						<span class="post-meta-date"><?php the_date()?></span>
					</p>
				</div>
				<div class="clearfix"></div>
				<?php else:?>
				<!-- For footer-->
				<a href="<?php the_permalink()?>"><?php the_title()?></a>
				<?php endif; ?>
				</li><!-- First post end-->
            <?php endwhile;?>
		</ul>
		<?php echo $after_widget; 
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_title'] = strip_tags($new_instance['post_title']);
		$instance['post_count'] = strip_tags($new_instance['post_count']);
		$instance['featured'] = strip_tags($new_instance['featured']);
		$instance['position']=strip_tags($new_instance['position']);
		return $instance;
	}

	function form($instance) {
		$defaults = array(
			'post_title' => 'Recent works',
			'post_count' => '3',
			'featured' => '0',
			'position'=>''
		);
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('post_title'); ?>"><?php esc_html_e('Title', 'dart'); ?></label>
			<input id="<?php echo $this->get_field_id('post_title'); ?>" type="text" name="<?php echo $this->get_field_name('post_title'); ?>" value="<?php echo $instance['post_title']; ?>" class="widefat" />
		</p>
        
         <p>
			<label for="<?php echo $this->get_field_id('featured'); ?>"><?php esc_html_e('Show only featured posts', 'dart'); ?></label> 
			<select id="<?php echo $this->get_field_id('featured'); ?>" name="<?php echo $this->get_field_name('featured'); ?>" class="widefat">
				<option value="0" <?php if( $instance['featured'] == 0):?>selected="selected"<?php endif?>>No</option> 
				<option value="1" <?php if( $instance['featured'] == 1):?>selected="selected"<?php endif?>>Yes</option> 
			</select>
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('post_count'); ?>"><?php esc_html_e('Number of Posts to show', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_count'); ?>" type="text" name="<?php echo $this->get_field_name('post_count'); ?>" value="<?php echo $instance['post_count']; ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('position'); ?>"><?php esc_html_e('Widget Location', 'dart'); ?></label> 
			<select id="<?php echo $this->get_field_id('position'); ?>" name="<?php echo $this->get_field_name('position'); ?>" class="widefat">
				<option value="sidebar" <?php if( $instance['position'] == 'sidebar'):?>selected="selected"<?php endif?>>Sidebar</option> 
				<option value="footer" <?php if( $instance['position'] == 'footer'):?>selected="selected"<?php endif?>>Footer</option> 
			</select>
		</p>
	<?php
	}
}

register_widget('alc_works_widget');



/* ***************************************************** 
 * Plugin Name: dart Blog Posts
 * Description: Retrieve and display latest blog posts.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class alc_blogposts_widget extends WP_Widget {

	// Widget setup.
	function __construct()
	{
		parent::__construct( /* Base ID */'alc-blogposts-widget', esc_html__('dart - Blog Posts', 'dart'), array('description' => esc_html__('Display latest blog posts', 'dart')) );
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = isset($instance['post_title']) ? apply_filters('widget_title', $instance['post_title']) : '';
		
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		$post_count = isset($instance['post_count']) ? $instance['post_count'] : '2';
		//$show_date = isset($instance['show_date']) ? $instance['show_date'] : '';
		//$show_author = isset($instance['show_author']) ? $instance['show_author'] : '';
		$post_category = isset($instance['post_category']) ? $instance['post_category'] : '';
		$post_thumbs = isset($instance['post_thumbs']) ? $instance['post_thumbs'] : '';
		$position = isset($instance['position']) ? $instance['position'] : '';
		
		
		global $post;
		$args = array( 'numberposts' => $post_count);
		if (!empty($post_category))
		$args['category'] = $post_category;
		$myposts = get_posts( $args );
		
		if ($myposts):
		if($position=='sidebar'):?>
			<ul class="sidebar-posts-widget posts-list unstyled clearfix">
				<?php  foreach( $myposts as $post ) :	setup_postdata($post);  ?>          
					<li>
						<?php if ($post_thumbs == 1):?>
						<div class="posts-thumb pull-left"> 
							<?php if(has_post_thumbnail()):?>
								<?php the_post_thumbnail('blog-thumb' ); ?>
							<?php endif?>
						</div>	
						<?php endif; ?>
						<div class="post-content">
							<h4 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h4>
							<p class="post-meta">
									<span class="post-meta-author">Posted by: <?php the_author_posts_link(); ?></span>
									<span class="post-meta-date"><?php the_date()?></span>
							</p>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php elseif ($position=='footer') :?>
			<ul class="list-latest divider">
				<?php  foreach( $myposts as $post ) :	setup_postdata($post);  ?>
					<li>
						<a href="<?php the_permalink()?>"><?php the_title()?></a>
					</li>
				<?php  endforeach;?>
			</ul>
		<?php endif;?>
			<?php endif;?>
        <?php echo $after_widget; 
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_title'] = $new_instance['post_title'];
		$instance['post_count'] = strip_tags($new_instance['post_count']);
		$instance['post_thumbs'] = strip_tags($new_instance['post_thumbs']);
		$instance['post_category'] = strip_tags($new_instance['post_category']);
		$instance['position'] = strip_tags($new_instance['position']);
		
		return $instance;
	}

	function form($instance) {
		$defaults = array(
			'post_title' => 'Latest from the blog',
			'post_count' => '2',
			'post_category' => '',
			'post_thumbs' => '',
			'position'=>''
		);
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('post_title'); ?>"><?php esc_html_e('Title', 'dart'); ?></label>
			<input id="<?php echo $this->get_field_id('post_title'); ?>" type="text" name="<?php echo $this->get_field_name('post_title'); ?>" value="<?php echo $instance['post_title']; ?>" class="widefat" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('post_count'); ?>"><?php esc_html_e('Number of Posts to show', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_count'); ?>" type="text" name="<?php echo $this->get_field_name('post_count'); ?>" value="<?php echo $instance['post_count']; ?>" class="widefat" />
		</p>
        
         <p>
			<label for="<?php echo $this->get_field_id('post_category'); ?>"><?php esc_html_e('Category (Leave Blank to show from all categories)', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_category'); ?>" type="text" name="<?php echo $this->get_field_name('post_category'); ?>" value="<?php echo $instance['post_category']; ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('post_thumbs'); ?>"><?php esc_html_e('Show thumbnails', 'dart'); ?></label>
			<select id="<?php echo $this->get_field_id('post_thumbs'); ?>" name="<?php echo $this->get_field_name('post_thumbs'); ?>" class="widefat">
				<option value="0" <?php if( $instance['post_thumbs'] == 0):?>selected="selected"<?php endif?>>No</option> 
				<option value="1" <?php if( $instance['post_thumbs'] == 1):?>selected="selected"<?php endif?>>Yes</option> 
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('position'); ?>"><?php esc_html_e('Widget Location', 'dart'); ?></label>
			<select id="<?php echo $this->get_field_id('position'); ?>" name="<?php echo $this->get_field_name('position'); ?>" class="widefat">
				<option value="sidebar" <?php if( $instance['position'] == 'sidebar'):?>selected="selected"<?php endif?>>Sidebar</option> 
				<option value="footer" <?php if( $instance['position'] == 'footer'):?>selected="selected"<?php endif?>>Footer</option> 
			</select>
		</p>

	<?php
	}
}

register_widget('alc_blogposts_widget');



/* ***************************************************** 
 * Plugin Name: 3-in-1 Posts
 * Description: Retrieve and display popular/latest posts/latest comments.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class alc_totalposts_widget extends WP_Widget {

	// Widget setup.
	function __construct()
	{
		parent::__construct( /* Base ID */'alc-totalposts-widget', /* Name */esc_html__('Construct Popular/Latest posts/Last comments', 'dart'), array('description' => esc_html__('Retrieve and display popular/latest posts/latest comments', 'dart')) );
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['post_title']);
		
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		$post_count = $instance['post_count'];
		$post_category = $instance['post_category'];
		
		global $post;
		$args = array( 'numberposts' => $post_count);
		if (!empty($post_category))
		$args['category'] = $post_category;
		?>
		<div class="widget-tab">
			<ul id="tab" class="nav nav-tabs">
				<li class="active"><a href="#widget-popular" data-toggle="tab"><?php esc_html_e('Popular', 'dart')?></a></li>
				<li><a href="#widget-recent" data-toggle="tab"><?php esc_html_e('Recent', 'dart')?></a></li>
				<li><a href="#widget-comments" data-toggle="tab"><?php esc_html_e('Comments', 'dart')?></a></li>
			</ul>
			<div id="tab-content" class="tab-content">
				<div id="widget-popular" class="tab-pane active">
					<?php $myposts = get_posts( $args ); if ($myposts):?>
						<ul class="posts-list unstyled clearfix">
							<?php foreach( $myposts as $post ) :	setup_postdata($post);  ?>                 
								<li>
									<div class="posts-thumb pull-left"> 
					                    <?php if('' != get_the_post_thumbnail()):?>
											<?php the_post_thumbnail('blog-thumb' ); ?>
										<?php endif?>
									</div>
									<div class="post-content">
										<h4 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h4>
										<p class="post-meta">
											<span class="post-meta-author">Posted by: <?php the_author_posts_link(); ?></span>
											<span class="post-meta-date"><?php echo the_date('M d, Y'); ?></span>
										</p>
									</div>
									<div class="clearfix"></div>
								</li>
							<?php endforeach;?>
						</ul>
					<?php endif; ?>
				</div>
				
				<div id="widget-recent" class="tab-pane fade">
					<?php  
					wp_reset_query();
					$args ['orderby'] = 'comment_count';
					$myposts = get_posts( $args );  
					if ($myposts):?>
						<ul class="posts-list unstyled clearfix">
							<?php foreach( $myposts as $post ) :	setup_postdata($post);  ?>                 
								<li>
									<div class="posts-thumb pull-left"> 
					                    <?php if('' != get_the_post_thumbnail()):?>
											<?php the_post_thumbnail('blog-thumb' ); ?>
										<?php endif?>
									</div>
									<div class="post-content">
										<h4 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h4>
										<p class="post-meta">
											<span class="post-meta-author">Posted by: <?php the_author_posts_link(); ?></span>
											<span class="post-meta-date"><?php echo the_date('M d, Y'); ?></span>
										</p>
									</div>
									<div class="clearfix"></div>
								</li>
							<?php endforeach;?>
						</ul>
					<?php endif; ?>
				</div>
				
				<div id="widget-comments" class="tab-pane fade">
					<?php  
					global $wpdb;	
					$sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_author_email, comment_date, comment_date_gmt, comment_approved, comment_type, comment_author_url, SUBSTRING(comment_content,1,70) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT 5";
					$comments = $wpdb->get_results($sql);
					if ($comments):?>
						<ul class="posts-list unstyled clearfix">
							<?php foreach ($comments as $comment) :	  ?>                 
								<li>
									<div class="posts-avator pull-left"> 
					                   	<?php echo get_avatar($comment, 72); ?> 
					                </div>
					                <div class="post-content">
					                    <h4 class="entry-title"><a href="<?php echo get_permalink($comment->ID)?>"><?php echo strip_tags($comment->com_excerpt)?></a></h4>
					                    <p class="post-meta">
											<span class="post-meta-author"><a href="<?php echo get_permalink($comment->ID)?>">Posted by: <?php echo strip_tags($comment->comment_author)?></a></span>
											<span class="post-meta-date"><a href="<?php echo get_permalink($comment->ID)?>"><?php echo strip_tags($comment->comment_date)?></a></span>
										</p>
					                </div>
					                <div class="clearfix"></div>
								</li>
							<?php endforeach; 	wp_reset_query();?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
        <?php echo $after_widget; 
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_title'] = strip_tags($new_instance['post_title']);
		$instance['post_count'] = strip_tags($new_instance['post_count']);
		$instance['post_category'] = strip_tags($new_instance['post_category']);
		return $instance;
	}

	function form($instance) {
		$defaults = array(
			'post_title' => 'Blog posts',
			'post_count' => '3',
			'post_category' => ''
		);
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('post_title'); ?>"><?php esc_html_e('Title', 'dart'); ?></label>
			<input id="<?php echo $this->get_field_id('post_title'); ?>" type="text" name="<?php echo $this->get_field_name('post_title'); ?>" value="<?php echo $instance['post_title']; ?>" class="widefat" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('post_count'); ?>"><?php esc_html_e('Number of Posts to show', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_count'); ?>" type="text" name="<?php echo $this->get_field_name('post_count'); ?>" value="<?php echo $instance['post_count']; ?>" class="widefat" />
		</p>
        
         <p>
			<label for="<?php echo $this->get_field_id('post_category'); ?>"><?php esc_html_e('Category (Leave Blank to show from all categories)', 'dart'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_category'); ?>" type="text" name="<?php echo $this->get_field_name('post_category'); ?>" value="<?php echo $instance['post_category']; ?>" class="widefat" />
		</p>
	<?php
	}
}

register_widget('alc_totalposts_widget');


/* ***************************************************** 
 * Plugin Name: dart Contact Widget
 * Description: Display contact widget on footer.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
/**
 * Contact Form Widget Class
 */
class alc_Contact_Form extends WP_Widget {
	
	function alc_Contact_Form() {
		$widget_ops = array('classname' => 'alc_contact_form_entries', 'description' => esc_html__("Contact widget", 'dart') );
		$this->__construct('alc_Contact_Form', esc_html__('dart - Contact Form', 'dart'), $widget_ops);
	}

	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', empty($instance['title']) ? esc_html__('Contact Us', 'dart') : $instance['title'], $instance);
		$email = apply_filters('widget_title', empty($instance['email']) ? esc_html__('', 'dart') : $instance['email'], $instance);
		$success = apply_filters('widget_title', empty($instance['success']) ? esc_html__('Thank you, e-mail sent.', 'dart') : $instance['success'], $instance);
		
		echo $before_widget;
		
		if ( $title ) echo $before_title . $title . $after_title;
        ?>                
			<form action="#" method="post" id="contactFormWidget">
				<div class="container contact-widget-wrapper">
					<div class="row">
						<div class="col-md-6">
							<input type="text" name="wname" id="wname" value="" size="22" placeholder="<?php esc_html_e('Name', 'dart')?>"/>
						</div>
						<div class="col-md-6">
							<input type="text" name="wemail" id="wemail" value="" size="22" placeholder="<?php esc_html_e('Email', 'dart')?>" />
						</div>
					</div>
                    <div class="row">	
						<div class="col-md-12">
							<textarea name="wmessage" id="wmessage"  placeholder="<?php esc_html_e('Message', 'dart')?>"></textarea>
							<input type="submit" id="wformsend" value="<?php esc_html_e('Send', 'dart')?>" class="btn btn-primary" name="wsend" />
						</div>
					</div>
					<div class="loading"></div>
					<div>
						<input type="hidden" name="wcontactemail" id="wcontactemail" value="<?php echo $email?>" />
						<input type="hidden" value="<?php echo home_url()?>" id="wcontactwebsite" name="wcontactwebsite" />
						<input type="hidden" name="wcontacturl" id="wcontacturl" value="<?php echo get_template_directory_uri()?>/library/sendmail.php" />
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="widgeterror"></div>
				<div class="widgetinfo"><i class="fa fa-envelope"></i><?php echo $success?></div>
			</form>
		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['email'] = strip_tags($new_instance['email']);
		$instance['success'] = strip_tags($new_instance['success']);
		return $instance;
	}

	function form( $instance ) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$email = isset($instance['email']) ? esc_attr($instance['email']) : '';
		$success = isset($instance['success']) ? esc_attr($instance['success']) : '';
	?>
	
		<div>
        	<label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:<br />', 'dart'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
		</div>
        <div>
        	<label for="<?php echo $this->get_field_id('email'); ?>"><?php esc_html_e('Email Address:<br />', 'dart'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" /></label></p>
		</div>
        <div>
        	<label for="<?php echo $this->get_field_id('success'); ?>"><?php esc_html_e('Success Message:<br />', 'dart'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('success'); ?>" name="<?php echo $this->get_field_name('success'); ?>" type="text" value="<?php echo $success; ?>" /></label></p>
		</div>
		<div style="clear:both"></div>
<?php
	}
}

register_widget('alc_Contact_Form');
?>