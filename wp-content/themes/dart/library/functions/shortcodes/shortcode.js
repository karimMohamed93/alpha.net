(function() {
	
	
	tinymce.create('tinymce.plugins.Addshortcodes', {
		init : function(ed, url) {
			
			//Add dart shortcodes button
			ed.addButton('dart', {
				title : 'Add dart shortcodes',
				cmd : 'alc_dart',
				image : url + '/images/dart.png'
			});
			ed.addCommand('alc_dart', function() {
				ed.windowManager.open({file : url + '/ui.php?page=dart',  width : 604 ,	height : 520 ,	inline : 1});
			});	
		},
		getInfo : function() {
			return {
				longname : "Weblusive Shortcodes",
				author : 'Weblusive',
				authorurl : 'http://www.weblusive.com/',
				infourl : 'http://www.weblusive.com/',
				version : "1.0"
			};
		}
	});
	tinymce.PluginManager.add('dartShortcodes', tinymce.plugins.Addshortcodes);

	
	
})();

