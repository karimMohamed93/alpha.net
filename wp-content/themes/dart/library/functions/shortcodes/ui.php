<?php

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
$fonturl = esc_url('http://fortawesome.github.io/Font-Awesome/icons/');
$icondir = get_template_directory_uri().'/library/functions/shortcodes/images'; 
$hintimg = get_template_directory_uri().'/library/functions/shortcodes/images/smicon.png'; 


?>
<!DOCTYPE html>
<head>
	<?php 
	wp_print_scripts('media-upload');
	wp_enqueue_script('thickbox');
	wp_enqueue_style('thickbox');
	do_action('admin_print_styles');
	
	?>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/library/functions/shortcodes/main.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/library/admin-panel/js/colorpicker.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/library/admin-panel/js/tipsy.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/library/functions/shortcodes/tabs.js"></script>
	

	
	<script>
	jQuery(document).ready(function() {
		jQuery('.tooltip').tipsy({fade: true, gravity: 'n'});
	});
	</script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/library/admin-panel/style.css" type="text/css" media="all" />
	<link rel='stylesheet' href='shortcode.css' type='text/css' media='all' />
<?php $page = isset($_GET['page']) ? htmlentities($_GET['page']) : 'dart'; if( $page == 'dart' ){
?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('.tabs-1').jQueryTab({

				//classes settings
				tabClass:'darttabs',                // class of the tabs
				accordionClass:'accordion_tabs',            // class of the header of accordion on smaller screens
				contentWrapperClass:'tab_content_wrapper',  // class of content wrapper
				contentClass:'tab_content',         // class of container
				activeClass:'active',               // name of the class used for active tab

				//feature settings
				responsive:true,                // enable accordian on smaller screens
				responsiveBelow:400,             // the breakpoint
				collapsible:true,               // allow all tabs to collapse on accordians
				useCookie: false,                // remember last active tab using cookie
				openOnhover: false,             // open tab on hover
				initialTab: 1,                  // tab to open initially; start count at 1 not 0

				//tabs transition settings      fade, flip, scaleUp, slideLeft, etc.
				tabInTransition: 'fadeIn',              // classname for showing in the tab content
				tabOutTransition: 'fadeOut',            // classname for hiding the tab content

				//accordion transition settings
				accordionTransition: 'slide',           // transitions to use - normal or slide
				accordionIntime:500,                // time for animation IN (1000 = 1s)
				accordionOutTime:400,               // time for animation OUT (1000 = 1s)

				//api functions
				before: function(){},               // function to call before tab is opened
				after: function(){}             // function to call after tab is opened

		});
		
		//jQuery('.tooltip').tipsy({fade: true, gravity: 'n'});
		});


		var shortcode = {
			e: '',
			init: function(e) {
				shortcode.e = e;

			},
			insert: function createdartShortcode(e, page, dialogwidth, dialogheight) {
				e.windowManager.open({url : '<?php echo get_template_directory_uri()?>/library/functions/shortcodes/ui.php?page='+page, width : dialogwidth, height : dialogheight});
				//tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				//tinyMCEPopup.close();
			},
			quickinsert: function createQuickShortcode(e, tag){
				var output = '['+tag+']'+ '[/'+tag+']';
				e.execCommand('mceInsertContent', false, output);
			}
		};
		//tinyMCEPopup.onInit.add(shortcode.init, shortcode);
		
	</script>
	<title>Dart Shortcodes Listing</title>
</head>
<body>
<form id="dartShortcode">
	<div class="tabs-1">
		<ul class="darttabs">
			<li><a href="#tab1">Layout</a></li>
			<li><a href="#tab2">Typography</a></li>
			<li><a href="#tab3">Content</a></li>
			<li><a href="#tab4">Posts Listing</a></li>
		</ul>
		<section class="tab_content_wrapper">
			<article class="tab_content" id="tab1">
				<ul class="shortcode-list">
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'row')" class="mo-help tooltip" title="Must be set to wrap the columns">
							<figure>
								<img src="<?php echo $icondir?>/gfx-row.png" alt="Add Row" /> 
								<figcaption>Row</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'inner_row')" class="mo-help tooltip" title="If you have a row which is wrapped in another row, use this one.">
							<figure>
								<img src="<?php echo $icondir?>/gfx-row-inner.png" alt="Add Inner row" /> 
								<figcaption>Inner row</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'one_whole')" class="mo-help tooltip" title="Full width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-full-column.png" alt="Add Fullwidth column" /> 
								<figcaption>1/1 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'one_half')" class="mo-help tooltip" title="One half width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-half-column.png" alt="Add One half column" /> 
								<figcaption>1/2 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'one_third')" class="mo-help tooltip" title="One third width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-three-column.png" alt="Add One third column" /> 
								<figcaption>1/3 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'one_fourth')" class="mo-help tooltip" title="One fourth width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-fourth-column.png" alt="Add One fourth column" /> 
								<figcaption>1/4 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'one_sixth')" class="mo-help tooltip" title="One sixth width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-six-column.png" alt="Add One sixth column" /> 
								<figcaption>1/6 Column</figcaption>
							</figure>
						</a>
					</li> 
					
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'two_third')" class="mo-help tooltip" title="Two third width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-2-three-column.png" alt="Add Two third column" /> 
								<figcaption>2/3 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'three_fourth')" class="mo-help tooltip" title="Three fourth width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-3-four-column.png" alt="Add Three fourth column" /> 
								<figcaption>3/4 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'five_sixth')" class="mo-help tooltip" title="Five sixth width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-five-sixth-column.png" alt="Add Five sixth column" /> 
								<figcaption>5/6 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'five_twelveth')" class="mo-help tooltip" title="Five twelveth width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-five-twelve-column.png" alt="Add Five twelveth column" /> 
								<figcaption>5/12 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.quickinsert(shortcode.e, 'seven_twelveth')" class="mo-help tooltip" title="Seven twelveth width column">
							<figure>
								<img src="<?php echo $icondir?>/gfx-seven-twelve-column.png" alt="Add Seven twelveth column" /> 
								<figcaption>7/12 Column</figcaption>
							</figure>
						</a>
					</li> 
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'fullbg', 800, 600)" class="mo-help tooltip" title="Regular or parallax 100% width background block">
							<figure>
								<img src="<?php echo $icondir?>/full-width-block.png" alt="Add Fullwidth Background" /> 
								<figcaption>Fullwidth Background</figcaption>
							</figure>
						</a>
					</li> 
			
				</ul>
				<div class="clear"></div>
			</article>
			<article class="tab_content" id="tab2">
				<ul class="shortcode-list">
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'list', 600, 450)" class="mo-help tooltip" title="List.">
							<figure>
								<img src="<?php echo $icondir?>/list.png" alt="Add  list" /> 
								<figcaption>List</figcaption>
							</figure>
						</a>
					</li>
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'tblock', 600, 480)" class="mo-help tooltip" title="Featured heading with variety of parameters">
							<figure>
								<img src="<?php echo $icondir?>/custom-heading.png" alt="Add heading title" /> 
								<figcaption>Custom heading title</figcaption>
							</figure>
						</a>
					</li>
					
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'blockquote', 600, 450)" class="mo-help tooltip" title="Regular blockquote">
							<figure>
								<img src="<?php echo $icondir?>/blockquotes.png" alt="Add Blockquote" /> 
								<figcaption>Blockquote</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'divider', 600, 375)" class="mo-help tooltip" title="Separate content blocks with various type of dividers">
							<figure>
								<img src="<?php echo $icondir?>/divider.png" alt="Add Divider" /> 
								<figcaption>Divider</figcaption>
							</figure>
						</a>
					</li>

				</ul>
				<div class="clear"></div>
			</article>
			<article class="tab_content" id="tab3">
				<ul class="shortcode-list">
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'teammember', 800, 600)" class="mo-help tooltip" title="A convenient way to list team members">
							<figure>
								<img src="<?php echo $icondir?>/team-member.png" alt="Add a team member" /> 
								<figcaption>Team member</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'progress', 600, 450)" class="mo-help tooltip" title="Regular, striped, animated, you choose!">
							<figure>
								<img src="<?php echo $icondir?>/progress-bars.png" alt="Add Progress bar" /> 
								<figcaption>Progress bar</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'circle', 600, 450)" class="mo-help tooltip" title="Special type of stylish progress bars">
							<figure>
								<img src="<?php echo $icondir?>/circular-bar.png" alt="Add Circular progress bar" /> 
								<figcaption>Circular progress bar</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'button', 600, 450)" class="mo-help tooltip" title="Regular button, many parameters to choose from">
							<figure>
								<img src="<?php echo $icondir?>/button.png" alt="Add Regular button" /> 
								<figcaption>Standard Button</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'dropdown', 600, 500)" class="mo-help tooltip" title="Regular button + dropdown menu combination">
							<figure>
								<img src="<?php echo $icondir?>/dropdown.png" alt="Add Dropdown Button" /> 
								<figcaption>Dropdown Button</figcaption>
							</figure>
						</a>
					</li>
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'vernav', 600, 375)" class="mo-help tooltip" title="Useful for creating custom menus to go inside the content">
							<figure>
								<img src="<?php echo $icondir?>/vertical-navigation.png" alt="Add Vertical navigation" /> 
								<figcaption>Vertical navigation</figcaption>
							</figure>
						</a>
					</li>
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'tabs', 600, 450)" class="mo-help tooltip" title="Tabbed content with variety of positions and styles">
							<figure>
								<img src="<?php echo $icondir?>/tabs.png" alt="Add Tab" /> 
								<figcaption>Tab</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'accordion', 600, 450)" class="mo-help tooltip" title="Q&A blocks, terms and definitions are example of accordion shortcode usage">
							<figure>
								<img src="<?php echo $icondir?>/accordion.png" alt="Add Accordion" /> 
								<figcaption>Accordion</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'testimonial', 800, 630)" class="mo-help tooltip" title="Parametrized testimonials">
							<figure>
								<img src="<?php echo $icondir?>/testimonials.png" alt="Add Testimonial" /> 
								<figcaption>Testimonial</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'alert', 600, 450)" class="mo-help tooltip" title="Use for important informative messages">
							<figure>
								<img src="<?php echo $icondir?>/alertbox.png" alt="Add Alert box" /> 
								<figcaption>Alert box</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'slider', 800, 600)" class="mo-help tooltip" title="Image slider">
							<figure>
								<img src="<?php echo $icondir?>/slider.png" alt="Add Slider" /> 
								<figcaption>Slider</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'carousel', 600, 450)" class="mo-help tooltip" title="Highly customizable carousel-style content sliding">
							<figure>
								<img src="<?php echo $icondir?>/carousel.png" alt="Add Carousel" /> 
								<figcaption>Carousel</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'panel', 600, 450)" class="mo-help tooltip" title="Blocks with variety of coloring options">
							<figure>
								<img src="<?php echo $icondir?>/panel.png" alt="Insert a panel" /> 
								<figcaption>Panel</figcaption>
							</figure>
						</a>
					</li>
					
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'fblock', 600, 450)" class="mo-help tooltip" title="Useful for listing services, main features, etc...">
							<figure>
								<img src="<?php echo $icondir?>/featured-block.png" alt="Add featured block" /> 
								<figcaption>Featured block</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'sblock', 600, 450)" class="mo-help tooltip" title="Useful for listing services, main features, etc...">
							<figure>
								<img src="<?php echo $icondir?>/featured-block.png" alt="Add Service block" /> 
								<figcaption>Service block</figcaption>
							</figure>
						</a>
					</li>
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'reveal', 600, 500)" class="mo-help tooltip" title="Can have any type of content assigned to show on a modal window">
							<figure>
								<img src="<?php echo $icondir?>/modal.png" alt="Add modal box" /> 
								<figcaption>Modal box</figcaption>
							</figure>
						</a>
					</li>
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'lightbox', 800, 600)" class="mo-help tooltip" title="Image gallery / videos / iframed content">
							<figure>
								<img src="<?php echo $icondir?>/lightbox.png" alt="Add Lightbox" /> 
								<figcaption>Lightbox</figcaption>
							</figure>
						</a>
					</li> 
					
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'social', 600, 450)" class="mo-help tooltip" title="All major social services to choose from.">
							<figure>
								<img src="<?php echo $icondir?>/social-button.png" alt="Add Social Button" /> 
								<figcaption>Social Button</figcaption>
							</figure>
						</a>
					</li>
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'googlemap', 800, 600)" class="mo-help tooltip" title="Show custom google maps anywhere in your pages/posts">
							<figure>
								<img src="<?php echo $icondir?>/google-map.png" alt="Add Google Map" /> 
								<figcaption>Google Map</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'pricingtable', 600, 450)" class="mo-help tooltip" title="Pricing Tables">
							<figure>
								<img src="<?php echo $icondir?>/accordion.png" alt="Add Pricing Table" /> 
								<figcaption>Pricing Table</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'contact', 600, 450)" class="mo-help tooltip" title="Contact Details">
							<figure>
								<img src="<?php echo $icondir?>/social-button.png" alt="Add Contact details" /> 
								<figcaption>Contact details</figcaption>
							</figure>
						</a>
					</li>
				</ul>
				<div class="clear"></div>
			</article>
			<article class="tab_content" id="tab4">
				<ul class="shortcode-list">
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'portlisting', 600, 450)" class="mo-help tooltip" title="List recent or featured portfolio posts in a carousel.">
							<figure>
								<img src="<?php echo $icondir?>/portfolio-listing.png" alt="Add Portfolio posts listing" /> 
								<figcaption>Portfolio Listing</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'bloglisting', 600, 480)" class="mo-help tooltip" title="List blog posts. Filtering optinons available.">
							<figure>
								<img src="<?php echo $icondir?>/blog-listing.png" alt="Add Blog posts listing" /> 
								<figcaption>Post Listing</figcaption>
							</figure>
						</a>
					</li>
					
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'productcar', 600, 450)" class="mo-help tooltip" title="Works only with Woocommerce plugin installed.">
							<figure>
								<img src="<?php echo $icondir?>/portfolio-listing.png" alt="List recent products" /> 
								<figcaption>Shop carousel (Regular)</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="javascript:shortcode.insert(shortcode.e, 'shop_special_products', 600, 450)" class="mo-help tooltip" title="Works only with Woocommerce plugin installed.">
							<figure>
								<img src="<?php echo $icondir?>/portfolio-listing.png" alt="List products by type" /> 
								<figcaption>Shop carousel (Special)</figcaption>
							</figure>
						</a>
					</li>
				</ul>
				<div class="clear"></div>
			</article>
		</section>
	</div>
</form>
<!--/*************************************/ -->
<?php
} elseif( $page == 'panel' ){
?>
	<script type="text/javascript">
		var AddPanel = {
			e: '',
			init: function(e) {
				AddPanel.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
                var PanelColor=jQuery('#color').val();
				var PanelHead = jQuery('#PanelHead').val();
                var PanelFooter=jQuery('#PanelFooter').val();
				var PanelContent = jQuery('#PanelContent').val();
                var anim=jQuery('#anim').val();
                var addclass=jQuery('#class').val();

				var output = '[panel ';
					if (anim){
						output+= 'anim="'+anim+'" ';
					}
					if(addclass){
						output+='class="'+addclass+'" ';
					}
					if(PanelColor) {
						output += 'color="'+PanelColor+'" ';
					}
					if(PanelHead) {
						output += 'head="'+PanelHead+'" ';
					}
									if(PanelFooter) {
						output += 'footer="'+PanelFooter+'" ';
					}
				output += ']'+PanelContent+'[/panel]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddPanel.init, AddPanel);

	</script>
	<title>Add Panel</title>

</head>
<body>
<form id="GalleryShortcode">
     <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
    <p>
        <label for="color">Panel Color</label>
        <select id="color" name="color">
            <option value="panel-default">Default</option>
            <option value="panel-primary">Primary</option>
            <option value="panel-info">Info</option>
            <option value="panel-success">Success</option>
            <option value="panel-warning">Warning</option>
            <option value="panel-danger">Danger</option>
        </select>
    </p>
    <p>
		<label for="PanelHead">Panel Header</label>
		<input id="PanelHead" name="PanelHead" type="text" value="" />
		
	</p>
        <p>
		<label for="PanelFooter">Panel Footer</label>
		<input id="PanelFooter" name="PanelFooter" type="text" value="" />
		
	</p>
	
	<p>
		<label for="PanelContent">Content </label>
		<textarea id="PanelContent" name="PanelContent" col="20"></textarea>
	</p>
        <p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
		
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:AddPanel.insert(AddPanel.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'progress' ){
?>
	<script type="text/javascript">
		var AddProgress = {
			e: '',
			init: function(e) {
				AddProgress.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
				var ProgressColor = jQuery('#ProgressColor').val();
				var ProgressAnim = jQuery('#ProgressAnim').val();
				var ProgressStyle = jQuery('#ProgressStyle').val();
				var ProgressMeter = jQuery('#ProgressMeter').val();
				var ProgressTitle = jQuery('#ProgressTitle').val();
				var ProgressCustomColor = jQuery('#ProgressCustomColor').val();
				var output = '[progressbar ';
					if(anim){
						output+=' anim="'+anim+'" ';
					}
					if(addclass){
						output+='class="'+addclass+'" ';
					}
					
					if(ProgressColor) {
						output += 'color="'+ProgressColor+'" ';
					}
					if(ProgressCustomColor){
						output += 'customcolor="'+ProgressCustomColor+'" ';
					}
					if(ProgressMeter) {
						output += 'meter="'+ProgressMeter+'" ';
					}
					if(ProgressAnim) {
						output += 'animated="'+ProgressAnim+'" ';
					}
					
					if(ProgressStyle) {
						output += 'style="'+ProgressStyle+'" ';
					}
					if(ProgressTitle) {
						output += 'title="'+ProgressTitle+'" ';
					}
					
				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddProgress.init, AddProgress);

	</script>
	<title>Add Progress bar</title>

</head>
<body>
<form id="GalleryShortcode">
	<script> 
		jQuery(function(){
			jQuery("#animcontent").load("animation.html"); 
			
		});
    </script>
    <p id="animcontent"></p>
	 
        <p>
		<label for="ProgressColor">Color :</label>
		<select id="ProgressColor" name="ProgressColor">
			<option value="info">Info</option>
			<option value="success">Success</option>
			<option value="danger">Danger</option>
			<option value="warning">Warning</option>
			<option value="primary">Primary</option>
		</select>
	</p>
	<p>
		<label for="ProgressCustomColor">Custom bg color:</label>
		<div id="ProgressCustomColorcolorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="ProgressCustomColor" id="ProgressCustomColor" type="text" value="" />			
		<script>
			jQuery(document).ready(function() {
				jQuery('#ProgressCustomColorcolorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#ProgressCustomColorcolorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#ProgressCustomColor').val('#'+hex);
					}
				});
			});
		</script>
	</p>
	<p>
		<label for="ProgressStyle">Style :</label>
		<select id="ProgressStyle" name="ProgressStyle">
			<option value="">Regular</option>
			<option value="progress-striped">Striped</option>	
		</select>
	</p>
	<p>
		<label for="ProgressAnim">Animated :</label>
		<select id="ProgressAnim" name="ProgressAnim">
			<option value="">No</option>
			<option value="active">Yes</option>
		</select>
	</p>
	<p>
		<label for="ProgressMeter">Progress meter :</label>
		<select id="ProgressMeter" name="ProgressMeter">
			<option value="1">1%</option>
			<option value="2">2%</option>
			<option value="3">3%</option>
			<option value="4">4%</option>
			<option value="5">5%</option>
			<option value="6">6%</option>
			<option value="7">7%</option>
			<option value="8">8%</option>
			<option value="9">9%</option>
			<option value="10">10%</option>
			<option value="11">11%</option>
			<option value="12">12%</option>
			<option value="13">13%</option>
			<option value="14">14%</option>
			<option value="15">15%</option>
			<option value="16">16%</option>
			<option value="17">17%</option>
			<option value="18">18%</option>
			<option value="19">19%</option>
			<option value="20">20%</option>
			<option value="21">21%</option>
			<option value="22">22%</option>
			<option value="23">23%</option>
			<option value="24">24%</option>
			<option value="25">25%</option>
			<option value="26">26%</option>
			<option value="27">27%</option>
			<option value="28">28%</option>
			<option value="29">29%</option>
			<option value="30">30%</option>
			<option value="31">31%</option>
			<option value="32">32%</option>
			<option value="33">33%</option>
			<option value="34">34%</option>
			<option value="35">35%</option>
			<option value="36">36%</option>
			<option value="37">37%</option>
			<option value="38">38%</option>
			<option value="39">39%</option>
			<option value="40">40%</option>
			<option value="41">41%</option>
			<option value="42">42%</option>
			<option value="43">43%</option>
			<option value="44">44%</option>
			<option value="45">45%</option>
			<option value="46">46%</option>
			<option value="47">47%</option>
			<option value="48">48%</option>
			<option value="49">49%</option>
			<option value="50">50%</option>
			<option value="51">51%</option>
			<option value="52">52%</option>
			<option value="53">53%</option>
			<option value="54">54%</option>
			<option value="55">55%</option>
			<option value="56">56%</option>
			<option value="57">57%</option>
			<option value="58">58%</option>
			<option value="59">59%</option>
			<option value="60">60%</option>
			<option value="61">61%</option>
			<option value="62">62%</option>
			<option value="63">63%</option>
			<option value="64">64%</option>
			<option value="65">65%</option>
			<option value="66">66%</option>
			<option value="67">67%</option>
			<option value="68">68%</option>
			<option value="69">69%</option>
			<option value="70">70%</option>
			<option value="71">71%</option>
			<option value="72">72%</option>
			<option value="73">73%</option>
			<option value="74">74%</option>
			<option value="75">75%</option>
			<option value="76">76%</option>
			<option value="77">77%</option>
			<option value="78">78%</option>
			<option value="79">79%</option>
			<option value="80">80%</option>
			<option value="81">81%</option>
			<option value="82">82%</option>
			<option value="83">83%</option>
			<option value="84">84%</option>
			<option value="85">85%</option>
			<option value="86">86%</option>
			<option value="87">87%</option>
			<option value="88">88%</option>
			<option value="89">89%</option>
			<option value="90">90%</option>
			<option value="91">91%</option>
			<option value="92">92%</option>
			<option value="93">93%</option>
			<option value="94">94%</option>
			<option value="95">95%</option>
			<option value="96">96%</option>
			<option value="97">97%</option>
			<option value="98">98%</option>
			<option value="99">99%</option>
			<option value="100">100%</option>
		</select>
	</p>
	<p>
		<label for="ProgressTitle">Title :</label>
		<input id="ProgressTitle" name="ProgressTitle" type="text" value="" />
	</p>
   
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:AddProgress.insert(AddProgress.e)">Insert</a></div>
<!--/*************************************/ --> 

<?php } elseif( $page == 'circle' ){
?>
	<script type="text/javascript">
		var circle = {
			e: '',
			init: function(e) {
				circle.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var piebarcolor = jQuery('#piebarcolor').val(); 
				var pietrackcolor = jQuery('#pietrackcolor').val(); 
				var meter = jQuery('#meter').val();
				var title = jQuery('#title').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
		
				var output = '[circle ';
					if (anim){
						output+= 'anim="'+anim+'" ';
					}
					
					if(addclass){
						output+='class="'+addclass+'" ';
					}
					
					if(meter) {
						output += 'meter="'+meter+'" ';
					}
					if(title) {
						output += 'title="'+title+'" ';
					}
					if(piebarcolor) {
						output += 'piebarcolor="'+piebarcolor+'" ';
					}
					if(pietrackcolor) {
						output += 'pietrackcolor="'+pietrackcolor+'" ';
					}
					
					      				
				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(circle.init, circle);

	</script>
	<title>Add Circle Progress bar</title>

</head>
<body>
<form id="GalleryShortcode">
     <script> 
    jQuery(function(){
		jQuery("#animcontent").load("animation.html"); 
		
	});
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="meter">Progress meter :</label>
		<select id="meter" name="meter">
			<option value="1">1%</option>
			<option value="2">2%</option>
			<option value="3">3%</option>
			<option value="4">4%</option>
			<option value="5">5%</option>
			<option value="6">6%</option>
			<option value="7">7%</option>
			<option value="8">8%</option>
			<option value="9">9%</option>
			<option value="10">10%</option>
			<option value="11">11%</option>
			<option value="12">12%</option>
			<option value="13">13%</option>
			<option value="14">14%</option>
			<option value="15">15%</option>
			<option value="16">16%</option>
			<option value="17">17%</option>
			<option value="18">18%</option>
			<option value="19">19%</option>
			<option value="20">20%</option>
			<option value="21">21%</option>
			<option value="22">22%</option>
			<option value="23">23%</option>
			<option value="24">24%</option>
			<option value="25">25%</option>
			<option value="26">26%</option>
			<option value="27">27%</option>
			<option value="28">28%</option>
			<option value="29">29%</option>
			<option value="30">30%</option>
			<option value="31">31%</option>
			<option value="32">32%</option>
			<option value="33">33%</option>
			<option value="34">34%</option>
			<option value="35">35%</option>
			<option value="36">36%</option>
			<option value="37">37%</option>
			<option value="38">38%</option>
			<option value="39">39%</option>
			<option value="40">40%</option>
			<option value="41">41%</option>
			<option value="42">42%</option>
			<option value="43">43%</option>
			<option value="44">44%</option>
			<option value="45">45%</option>
			<option value="46">46%</option>
			<option value="47">47%</option>
			<option value="48">48%</option>
			<option value="49">49%</option>
			<option value="50">50%</option>
			<option value="51">51%</option>
			<option value="52">52%</option>
			<option value="53">53%</option>
			<option value="54">54%</option>
			<option value="55">55%</option>
			<option value="56">56%</option>
			<option value="57">57%</option>
			<option value="58">58%</option>
			<option value="59">59%</option>
			<option value="60">60%</option>
			<option value="61">61%</option>
			<option value="62">62%</option>
			<option value="63">63%</option>
			<option value="64">64%</option>
			<option value="65">65%</option>
			<option value="66">66%</option>
			<option value="67">67%</option>
			<option value="68">68%</option>
			<option value="69">69%</option>
			<option value="70">70%</option>
			<option value="71">71%</option>
			<option value="72">72%</option>
			<option value="73">73%</option>
			<option value="74">74%</option>
			<option value="75">75%</option>
			<option value="76">76%</option>
			<option value="77">77%</option>
			<option value="78">78%</option>
			<option value="79">79%</option>
			<option value="80">80%</option>
			<option value="81">81%</option>
			<option value="82">82%</option>
			<option value="83">83%</option>
			<option value="84">84%</option>
			<option value="85">85%</option>
			<option value="86">86%</option>
			<option value="87">87%</option>
			<option value="88">88%</option>
			<option value="89">89%</option>
			<option value="90">90%</option>
			<option value="91">91%</option>
			<option value="92">92%</option>
			<option value="93">93%</option>
			<option value="94">94%</option>
			<option value="95">95%</option>
			<option value="96">96%</option>
			<option value="97">97%</option>
			<option value="98">98%</option>
			<option value="99">99%</option>
			<option value="100">100%</option>
		</select>
	</p>
	<p>
		<label for="title">Title</label>
		<input type="text" id="title" name="title">
	</p>
	<p>
		<label for="piebarcolor">Bar color :</label>
		<div id="barcolorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="piebarcolor" id="piebarcolor" type="text" value="" />
							
		<script>
		jQuery(document).ready(function() {
			jQuery('#barcolorSelector').ColorPicker({
				onShow: function (colpkr) {
					jQuery(colpkr).fadeIn(500);
					return false;
				},
				onHide: function (colpkr) {
					jQuery(colpkr).fadeOut(500);
					return false;
				},
				onChange: function (hsb, hex, rgb) {
					jQuery('#barcolorSelector div').css('backgroundColor', '#' + hex);
					jQuery('#piebarcolor').val('#'+hex);
				}
			});
		});
		</script>
		
	</p>
	<p>
		<label for="pietrackcolor"> Track color :</label>
		<div id="trackcolorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="pietrackcolor" id="pietrackcolor" type="text" value="" />
							
		<script>
		jQuery(document).ready(function() {
			jQuery('#trackcolorSelector').ColorPicker({
				onShow: function (colpkr) {
					jQuery(colpkr).fadeIn(500);
					return false;
				},
				onHide: function (colpkr) {
					jQuery(colpkr).fadeOut(500);
					return false;
				},
				onChange: function (hsb, hex, rgb) {
					jQuery('#trackcolorSelector div').css('backgroundColor', '#' + hex);
					jQuery('#pietrackcolor').val('#'+hex);
				}
			});
		});
		</script>
		
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:circle.insert(circle.e)">Insert</a></div>
    <!--/*************************************/ -->
<?php } elseif( $page == 'dropdown' ){ ?>

	<script type="text/javascript">
		var DropdownButton = {
			e: '',
			init: function(e) {
				DropdownButton.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[dropbuttongroup ";
				var Type = jQuery('#Type').val();
				var Title = jQuery('#Title').val();
				var anim = jQuery('#anim').val();
				var color = jQuery('#color').val();
				var addclass=jQuery('#class').val();
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				
				if(Type) {
					output+= ' type="'+Type+'"';
				}
				
				if(Title) {
					output+= ' title="'+Title+'"';
				}
				if(color) {
					output+= ' color="'+color+'"';
				}
				
				output += "]";
				
				jQuery("input[id^=dropbutton_title]").each(function(intIndex, objValue) {
				
					output +='[dropbutton';
					output += ' title="'+jQuery(this).val()+'"';
					var obj1 = jQuery('input[id^=dropbutton_url]').get(intIndex);
					output += ' url= "'+obj1.value+'"';
					
					var obj2 = jQuery('select[id^=dropbutton_divider]').get(intIndex);
					output += ' divider= "'+obj2.value+'"]';
				
					output += "[/dropbutton]";
				});
				
				
				output += '[/dropbuttongroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(DropdownButton.init, DropdownButton);

		jQuery(document).ready(function() {
			jQuery("#add-dropbutton").click(function() {
				jQuery('#DropbuttonShortcodeContent').append('<p><label for="dropbutton_title[]">Item Title</label><input id="dropbutton_title[]" name="dropbutton_title[]" type="text" value="" /></p><p><label for="dropbutton_url[]">Item URL</label><input id="dropbutton_url[]" name="dropbutton_url[]" type="text" value="" /></p><p><label for="Content[]">Item Content</label><input id="Content[]" name="Content[]" type="text" value="" /></p><p><label for="dropbutton_divider[]">Insert divider after item</label><select id="dropbutton_divider[]" name="dropbutton_divider[]"><option value="0">No</option><option value="1">Yes</option></select></p>	<hr class="divider" />');
			});
		});
		
	</script>
	<title>Add Dropdown button</title>

</head>
<body>
<form id="DropbuttonShortcode">
    <script> 
    jQuery(function(){
		jQuery("#animcontent").load("animation.html");
		
    });
    </script>
    <p id="animcontent"></p>
	<div id="DropbuttonShortcodeContent">
		<p>
			<label for="Title">Title</label>
			<input id="Title" name="Title" type="text" value="" />
		</p>
		<p>
			<label for="Type">Type :</label>
			<select id="Type" name="Type">
				<option value="">Default</option>
				<option value="split">Split</option>
			</select>		
		</p>
		<p>
			<label for="color">Color :</label>
			<select id="color" name="color">
				<option value="btn-default">Default</option>
				<option value="btn-primary">Primary</option>
				<option value="btn-info">Info</option>
				<option value="btn-success">Success</option>
				<option value="btn-warning">Warning</option>
				<option value="btn-danger">Danger</option>
			</select>
		</p>
				
		<p>
			<label for="class">Extra Class</label>
			<input id="class" name="class" type="text" value="" />
		</p>
		<hr class="divider" />
		<p>
			<label for="dropbutton_title[]">Item Title</label>
			<input id="dropbutton_title[]" name="dropbutton_title[]" type="text" value="" />
		</p>
		<p>
			<label for="dropbutton_url[]">Item URL</label>
			<input id="dropbutton_url[]" name="dropbutton_url[]" type="text" value="" />
		</p>
		<p>
			<label for="dropbutton_divider[]">Insert divider after item</label>
			<select id="dropbutton_divider[]" name="dropbutton_divider[]">
				<option value="0">No</option>
				<option value="1">Yes</option>
			</select>	
		</p>
		
		<hr class="divider" />
	</div>
    
	<strong><a style="cursor: pointer;" id="add-dropbutton">+ Add Item</a></strong>
   
</form>
<div class="mce-foot"><a class="add" href="javascript:DropdownButton.insert(DropdownButton.e)">Insert</a></div>
<!--/*************************************/ --> 

<?php
} elseif( $page == 'button' ){
 ?>
 	<script type="text/javascript">
		var AddButton = {
			e: '',
			init: function(e) {
				AddButton.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var ButtonColor = jQuery('#ButtonColor').val();
				var ButtonSize = jQuery('#ButtonSize').val();
				var ButtonLink = jQuery('#ButtonLink').val();
				var ButtonStatus = jQuery('#ButtonStatus').val();
				var ButtonText = jQuery('#ButtonText').val();
				var ButtonTarget = jQuery('#ButtonTarget').val();
				var icon = jQuery('#icon').val();
				var ipos = jQuery('#ipos').val();
				var anim=jQuery('#anim').val();				
				var addclass=jQuery('#class').val();              
				var output = '[button ';
					if (anim){
						output+= 'anim="'+anim+'" ';
					}
					if(addclass){
						output+='class="'+addclass+'" ';
					}
					if(ButtonColor) {
						output += 'color="'+ButtonColor+'" ';
					}
					if(ButtonSize) {
						output += 'size="'+ButtonSize+'" ';
					}
					
					if(ButtonStatus){
						output += 'status="'+ButtonStatus+'" ';
					}
					
					if(ButtonLink) {
						output += 'link="'+ButtonLink+'" ';
					} 
					if(icon){
							output += 'icon="'+icon+'" ';
					}
					if(ipos){
						output += 'ipos="'+ipos+'" ';
					}
					if(ButtonTarget) {
						output += 'target="_blank" ';
					}
				output += ']'+ButtonText+'[/button]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddButton.init, AddButton);

	</script>
	<title>Add Buttons</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="ButtonColor">Button Color:</label>
		<select id="ButtonColor" name="ButtonColor">
			<option value="btn-default">Default</option>
			<option value="btn-primary">Primary</option>
			<option value="btn-info">Info</option>
			<option value="btn-success">Success</option>
			<option value="btn-warning">Warning</option>
			<option value="btn-danger">Danger</option>
		</select>
	</p>
	<p>
		<label for="ButtonSize">Button Size :</label>
		<select id="ButtonSize" name="ButtonSize">	
			<option value="btn-lg">Large</option>
			<option value="">Default</option>       
			<option value="btn-sm">Small</option>
			<option value="btn-xs">Very Small</option>	
		</select>
	</p>
	
    <p>
		<label for="ButtonStatus">Button Status:</label>
		<select id="ButtonStatus" name="ButtonStatus">
			<option value="">Enabled</option>
			<option value="disabled">Disabled</option>
		</select>
	</p>
        
	<p>
		<label for="ButtonLink">Button Link :</label>
		<input id="ButtonLink" name="ButtonLink" type="text" value="http://" />
		
	</p>
	<p>
		<label for="ButtonTarget">Link Target: </label>
		<select id="ButtonTarget" name="ButtonTarget">
			<option value="_blank">New Window</option>	
			<option value="">Same Window</option>	
		</select>
	</p>
	<p>
		<label for="ButtonText">Button Text :</label>
		<input id="ButtonText" name="ButtonText" type="text" value="" />
	</p>
	<p>
		<label for="icon">Icon :</label>
		<input id="icon" name="icon" type="text" value=""/>
        <small><a href="<?php echo $fonturl ?>" target="blank">Icons list</a></small>
	</p>
	<p>
		<label for="ipos">Icon position :</label>
		<select id="ipos" name="ipos">
			<option value="icon-left">Left</option>	
			<option value="icon-right">Right</option>	
		</select>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:AddButton.insert(AddButton.e)">Insert</a></div>
<!--/*************************************/ -->

<!--/*************************************/ -->


<!--/*************************************/ -->

<?php } elseif( $page == 'tabs' ){ ?>

	<script type="text/javascript">
		var tabs = {
			e: '',
			init: function(e) {
				tabs.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var style=jQuery('#style').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
				
				var output = '[tabgroup  style="'+style+'" ';
					if (anim){
						output+= 'anim="'+anim+'" ';
					}
					if(addclass){
						output+='class="'+addclass+'" ';
					}
					output+= ']';
				jQuery("input[id^=tab_title]").each(function(intIndex, objValue) {
					var sectitle=jQuery('input[id^=sectitle]').get(intIndex);
                    var icon=jQuery('input[id^=tab_icon]').get(intIndex);
					output +='[tab title="'+jQuery(this).val()+'" icon="'+icon.value+'" sectitle="'+sectitle.value+'"]';
					var obj = jQuery('textarea[id^=Content]').get(intIndex);
					output += obj.value;
					output += "[/tab]";
				});
				
				
				output += '[/tabgroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(tabs.init, tabs);

		jQuery(document).ready(function() {
			jQuery("#add-tab").click(function() {
				jQuery('#TabShortcodeContent').append('<p><label for="tab_title[]">Tab Title</label><input id="tab_title[]" name="tab_title[]" type="text" value="" /></p><p><label for="sectitle[]">Secondary title(optional)</label><input id="sectitle[]" name="sectitle[]" type="text" value="" /><small>For Alternative Style</small></p><p><label for="tab_icon[]">Tab Icon</label><input id="tab_icon[]" name="tab_icon[]" type="text" value="" /><small><a href="<?php echo $fonturl ?>" target="blank">Icons list</a></small> </p><p><label for="Content[]">Tab Content</label><textarea  style="height:100px;  width:400px;" id="Content[]" name="Content[]" type="text" value=""></textarea></p>	<hr class="divider" />');
			});
		});

	</script>
	<title>Add Tabs</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
	<p>
        <label for="style">Style</label>
        <select id="style" name="style">
            <option value="nav-tabs">Default</option>
            <option value="nav-stacked">Alternative</option>
        </select>
    </p>
	<div id="TabShortcodeContent">
		<p>
			<label for="tab_title[]">Tab Title</label>
			<input id="tab_title[]" name="tab_title[]" type="text" value="" />
		</p>
		<p>
			<label for="sectitle[]">Secondary title(optional)</label>
			<input id="sectitle[]" name="sectitle[]" type="text" value="" />
			<small>For Alternative Style</small>
		</p>
		<p>
			<label for="tab_icon[]">Tab Icon</label>
			<input id="tab_icon[]" name="tab_icon[]" type="text" value="" />
			<small><a href="<?php echo $fonturl ?>" target="blank">Icons list</a></small>
		</p>
		
		<p>
			<label for="Content[]">Tab Content</label>
			<textarea style="height:100px; width:400px;" id="Content[]" name="Content[]" type="text" value="" ></textarea>
		</p>
		<hr class="divider" />
	</div>
	<strong><a style="cursor: pointer;" id="add-tab">+ Add Tab</a></strong>
    <p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:tabs.insert(tabs.e)">Insert</a></div>
<!--/*************************************/ -->
<?php } elseif( $page == 'vernav' ){ ?>

	<script type="text/javascript">
		var vernav = {
			e: '',
			init: function(e) {
				vernav.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[vernavgroup";
				
				var maintitle = jQuery('#vntitle').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}

				if(maintitle) {
					output += ' title="'+maintitle+'" ';
				}
				output += "]";
				
				jQuery("input[id^=vernav_title]").each(function(intIndex, objValue) {
					output +='[vernav title="'+jQuery(this).val()+'" ';
					var obj2 = jQuery('input[id^=vernav_link]').get(intIndex);
					output += 'link= "'+obj2.value+'"]';
					output += "[/vernav]";
				});
				
				
				output += '[/vernavgroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(vernav.init, vernav);

		jQuery(document).ready(function() {
			jQuery("#add-vernav").click(function() {
				jQuery('#VernavShortcodeContent').append('<p><label for="vernav_title[]">Title</label><input id="vernav_title[]" name="vernav_title[]" type="text" value="" /></p><p><label for="vernav_link[]">URL</label><input id="vernav_link[]" name="vernav_link[]" type="text" value="" /></p>	<hr class="divider" />');
			});
		});

	</script>
	<title>Add Vertical Navigation</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
	<div id="VernavShortcodeContent">
		<p>
			<label for="vntitle">Header title</label>
			<input id="vntitle" name="vntitle" type="text" value="" />
		</p>
		<hr class="divider" />
		<p>
			<label for="vernav_title[]">Title</label>
			<input id="vernav_title[]" name="vernav_title[]" type="text" value="" />
		</p>
		<p>
			<label for="vernav_link[]">URL</label>
			<input id="vernav_link[]" name="vernav_link[]" type="text" value="" />
		</p>
		<hr class="divider" />
	</div>
	<strong><a style="cursor: pointer;" id="add-vernav">+ Add Navigation item</a></strong>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />	
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:vernav.insert(vernav.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'accordion' ){ ?>

	<script type="text/javascript">
		var accordion = {
			e: '',
			init: function(e) {
				accordion.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {				
				var type=jQuery('#type').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();

				var output = '[accordiongroup  type="'+type+'" ';
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				output+= ']';
				jQuery("input[id^=accordion_title]").each(function(intIndex, objValue) {
					output +='[accordion title="'+jQuery(this).val()+'"';
					var obj = jQuery('textarea[id^=Content]').get(intIndex);
					output+=']';
					output += obj.value;
					output += "[/accordion]";
				});
				
				output += '[/accordiongroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
			}
		}
		tinyMCEPopup.onInit.add(accordion.init, accordion);

		jQuery(document).ready(function() {
			jQuery("#add-accordion").click(function() {
				jQuery('#accordionShortcodeContent').append('<p><label for="accordion_title[]">accordion Title</label><input id="accordion_title[]" name="accordion_title[]" type="text" value="" /></p><p><label for="Content[]">accordion Content</label><textarea  style="height:100px;  width:400px;" id="Content[]" name="Content[]" type="text" value=""></textarea></p><hr class="divider" />');
			});
		});

	</script>
	<title>Add accordion</title>

</head>
<body>
<form id="accordionsShortcode">
     <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
    <p>
        <label for="type">Accordion Type</label>
        <select name="type" id="type">
            <option value="1">Accordion</option>
            <option value="2">Toggle</option>
        </select>
    </p>
	<div id="accordionShortcodeContent">
		<p>
			<label for="accordion_title[]">Title</label>
			<input id="accordion_title[]" name="accordion_title[]" type="text" value="" />
		</p>
		<p>
			<label for="Content[]">Content</label>
			<textarea style="height:100px; width:400px;" id="Content[]" name="Content[]" type="text" value="" ></textarea>
		</p>
		<hr class="divider" />
	</div>
	<strong><a style="cursor: pointer;" id="add-accordion">+ Add accordion tab</a></strong>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:accordion.insert(accordion.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'testimonial' ){ ?>
	<script type="text/javascript">
		
		var Testimonial = {
			e: '',
			init: function(e) {
				Testimonial.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var anim=jQuery('#anim').val();
                var addclass=jQuery('#class').val();
				var nav=jQuery('#nav').val();
				var automatic=jQuery('#autoslide').val();  
				var interval=jQuery('#interval').val(); 
				
				var output = '[testimonialgroup  ';
				if (nav){
					output+= 'nav="'+nav+'" ';
				}
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				if (automatic){
					output+= 'auto="'+automatic+'" ';
				}
				if(interval){
					output+='interval="'+interval+'" ';
				}
				output+= ']';
				jQuery("input[id^=authorName]").each(function(intIndex, objValue) {
					output +='[testimonial title="'+jQuery(this).val()+'"';
					var position = jQuery('input[id^=authorPosition]').get(intIndex);
					if (position) output += ' position="'+position.value+'"';
					var color = jQuery('select[id^=textColor]').get(intIndex);
					if (color) output += ' color="'+color.value+'"';
					var photoholder = '#authorphoto'+intIndex+'-img';
					var photo=jQuery(photoholder).val();
					if(photo) output+=' photo="'+photo+'"';
					
					output += "]";
					var obj = jQuery('textarea[id^=Content]').get(intIndex);
					output += obj.value;
					output += "[/testimonial]";
					
				});
				
				
				output += '[/testimonialgroup]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
			}
		}
		tinyMCEPopup.onInit.add(Testimonial.init, Testimonial);
		jQuery(document).ready(function() {
			var counter = 0;
			var photo = '';
			jQuery("#add-testimonial").click(function() {
				counter++;
				photo = 'authorphoto' + counter;
				
				weblusive_styling_uploader(photo);
				
				jQuery('#testimonialShortcodeContent').append('<p><label for="authorName[]">Author Name</label><input id="authorName[]" name="authorName[]" type="text" value="" /></p><p><label for="authorPosition[]">Author Position</label><input id="authorPosition[]" name="authorPosition[]" type="text" value="" /></p><div class="wrap-list"><label for="upload_authorphoto'+counter+'_button">Author Photo:</label><input id="authorphoto'+counter+'-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="authorphoto'+counter+'" value="" /><input id="upload_authorphoto'+counter+'_button" type="button" class="small_button" value="Upload" /><div id="authorphoto'+counter+'-preview" class="img-preview" <?php if(!weblusive_get_option('authorphoto')) echo 'style="display:none;"' ?>><img src="<?php if(weblusive_get_option('authorphoto')) echo weblusive_get_option('authorphoto'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" /><a class="del-img" title="Delete"></a></div><div class="clear"></div></div><p><label for="textColor[]">Text Color: </label><select id="textColor[]" name="textColor[]"><option value="">Default</option><option value="color-white">White</option></select></p><p><label for="Content[]">Text</label><textarea  style="height:100px;  width:400px;" id="Content[]" name="Content[]" type="text" value=""></textarea></p><hr class="divider" />');
			});
		});
	
	</script>
	<title>Insert Testimonial</title>
</head>
<body>
<form id="GalleryShortcode">
     <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
	  jQuery("#interval-wrapper").hide();
	  	jQuery("#autoslide").change(function(){
			var selected = jQuery('#autoslide').val();
			if (selected == 'false'){
				jQuery("#interval-wrapper").hide();
			}
			else{
				jQuery("#interval-wrapper").show();
			}
		});
    });
    </script>
    <p id="animcontent"></p>
	<p>
        <label for="nav">Carousel navigation</label>
        <select id="nav" name="nav">
            <option value="true">Yes</option>
            <option value="false">No</option>
        </select>
    </p>
	<p>
		<label for="autoslide">Automatic sliding</label>
		<select id="autoslide" name="autoslide">
			<option value="false">No</option>
			<option value="true">Yes</option>
		</select>
	</p>
	<div id="carousel-options">
		<p id="interval-wrapper">
			<label for="interval">Sliding interval</label>
			<input id="interval" name="interval" type="text" value="6000" />
		</p>
	</div>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
	<hr class="divider" />
	<div id="testimonialShortcodeContent">
		<p>
			<label for="authorName[]">Author Name</label>
			<input id="authorName[]" name="authorName[]" type="text" value="" />
		</p>
		<p>
			<label for="authorPosition[]">Author Position</label>
			<input id="authorPosition[]" name="authorPosition[]" type="text" value="" />
		</p>
		<div class="wrap-list">
			<label for="upload_authorphoto0_button">Author Photo:</label>
			<input id="authorphoto0-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="authorphoto0" value="" />
			<input id="upload_authorphoto0_button" type="button" class="small_button" value="Upload" />
			<div id="authorphoto0-preview" class="img-preview" <?php if(!weblusive_get_option('authorphoto0')) echo 'style="display:none;"' ?>>
				<img src="<?php if(weblusive_get_option('authorphoto0')) echo weblusive_get_option('authorphoto0'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" />
				<a class="del-img" title="Delete"></a>
			</div>
			<div class="clear"></div>
		</div>
		<p>
			<label for="textColor[]">Text Color: </label>
			<select id="textColor[]" name="textColor[]">
				<option value="">Default</option>
				<option value="color-white">White</option>
			</select>
		</p>
		<p>
			<label for="Content[]">Text : </label>
			<textarea id="Content[]" name="Content[]" col="20"></textarea>
		</p>
		<hr class="divider" />
	</div>
	<strong><a style="cursor: pointer;" id="add-testimonial">+ Add another testimonial</a></strong>
	
</form>
<div class="mce-foot"><a class="add" href="javascript:Testimonial.insert(Testimonial.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'alert' ){ ?>

	<script type="text/javascript">
		var alert = {
			e: '',
			init: function(e) {
				alert.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
                            
                var alertColor=jQuery('#alertColor').val();
				var alertType = jQuery('#alertType').val();
				var Content = jQuery('#Content').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();

				
				var output = '[alert ';
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				if(alertColor){
					output+= 'color="'+alertColor+'" ';
				}
				if(alertType) {
					output += 'type="'+alertType+'"';
				}
			
				output += ']'+Content+'[/alert]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(alert.init, alert);

	</script>
	<title>Add Alert box</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="alertType">Type :</label>
		<select id="alertType" name="alertType">
			<option value="alertdefault">Default</option>
			<option value="alert-dismissible">Dismissible</option>
		</select>
	</p>
	<p>
		<label for="alertColor">Color :</label>
		<select id="alertColor" name="alertColor">
			<option value="alert-info">Info</option>
			<option value="alert-success">Success</option>
			<option value="alert-warning">Warning</option>
			<option value="alert-danger">Danger</option>	
		</select>
	</p>
	<p>
		<label for="Content">Content : </label>
		<textarea id="Content" name="Content" col="20"></textarea>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:alert.insert(alert.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'slider' ){ ?>
	
	<script type="text/javascript">
		var Slider = {
			e: '',
			init: function(e) {
				Slider.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
				var type = jQuery('#type').val();
				var nav = jQuery('#nav').val();
				var pag = jQuery('#pag').val();
				var auto = jQuery('#auto').val();
				var speed = jQuery('#speed').val();
				var sitems = jQuery('#sitems').val();
				
				var output = "[slider ";
			
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				if(type) {
					output += ' type="'+type+'"';
				}
				
				if(nav) {
					output += ' nav="'+nav+'"';
				}
				if(pag) {
					output += ' pag="'+pag+'"';
				}
				if(speed) {
					output += ' speed="'+speed+'"';
				}
				if(auto) {
					output += ' auto="'+auto+'"';
				}
				
				if(sitems) {
					output += ' sitems="'+sitems+'"';
				}
				output += "]";
				
				jQuery("textarea[id^=slide_title]").each(function(intIndex, objValue) {
					output +='[slideritem ';
					//var obj = jQuery('input[id^=slide_image]').get(intIndex);
					var photoholder = '#slideimage'+intIndex+'-img';
					var photo=jQuery(photoholder).val();
					if(photo) output+=' image="'+photo+'" ';
					output+=']'+jQuery(this).val();
					//output += ' image="'+ obj.value +'"]';
					output += "[/slideritem]";
				});
				
				output += '[/slider]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Slider.init, Slider);

		jQuery(document).ready(function() {
			
			var counter = 0;
			var photo = '';
			
			
			
			jQuery("#add-slide").click(function() {
				counter++;
				photo = 'slideimage' + counter;
				weblusive_styling_uploader(photo);
				jQuery('#SlideShortcodeContent').append('<p><label for="slide_title[]">Slide Title</label><textarea id="slide_title[]" name="slide_title[]"></textarea></p><div class="wrap-list"><label for="upload_slideimage'+counter+'_button">Slide image:</label><input id="slideimage'+counter+'-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="slideimage'+counter+'" value="" /><input id="upload_slideimage'+counter+'_button" type="button" class="small_button" value="Upload" /><div id="slideimage'+counter+'-preview" class="img-preview" <?php if(!weblusive_get_option('slideimage')) echo 'style="display:none;"' ?>><img src="<?php if(weblusive_get_option('slideimage')) echo weblusive_get_option('slideimage'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" /><a class="del-img" title="Delete"></a></div><div class="clear"></div></div>	<hr class="divider" />');
			});
		});
		
	</script>
	<title>Add Slider</title>

</head>
<body>

<form id="SliderShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
	  jQuery('.sitems').hide();
	  jQuery("#type").change(function(){
			var selected = jQuery('#type').val();
			if (selected == 'carousel'){
				jQuery(".sitems").show();
				jQuery(".pag").hide(); 
			}else{
				jQuery(".sitems").hide(); 
				jQuery(".pag").show();
			}
		});
    });
    </script>
    
	<div id="SlideShortcodeContent">
			<p id="animcontent"></p>
			<p>
				<label for="type">Type</label>
				<select id="type" name="type">
					<option value="single">Single</option>
					<option value="carousel">Carousel</option>
				</select>
			</p>
			<p>
				<label for="nav">Navigation</label>
				<select id="nav" name="nav">
					<option value="true">Yes</option>
					<option value="false">No</option>
				</select>
			</p>
			<p class="pag">
				<label for="pag">Pagination</label>
				<select id="pag" name="pag">
					<option value="true">Yes</option>
					<option value="false">No</option>
				</select>
			</p>
			<p>
				<label for="auto">Autoplay</label>
				<select id="auto" name="auto">
					<option value="false">No</option>
					<option value="true">Yes</option>
				</select>
			</p>
			<p>
				<label for="speed">Autoplay Speed</label>
				<input id="speed" name="speed" type="text" value="800" />
			</p>
			<p class="sitems">
				<label for="sitems">Slide Items for carousel</label>
				<input id="sitems" name="sitems" type="text" value="3" />
			</p>
			<p>
				<label for="class">Extra Class</label>
				<input id="class" name="class" type="text" value="" />
			</p>
			<hr class="divider" />
		<p>
			<label for="slide_title[]">Slide Title</label>
			<textarea id="slide_title[]" name="slide_title[]"></textarea>
		</p>
		
		<div class="wrap-list">
			<label for="upload_slideimage0_button">Slide image:</label>
			<input id="slideimage0-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="slideimage0" value="" />
			<input id="upload_slideimage0_button" type="button" class="small_button" value="Upload" />
			<div id="slideimage0-preview" class="img-preview" <?php if(!weblusive_get_option('slideimage0')) echo 'style="display:none;"' ?>>
				<img src="<?php if(weblusive_get_option('slideimage0')) echo weblusive_get_option('slideimage0'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" />
				<a class="del-img" title="Delete"></a>
			</div>
			<div class="clear"></div>
		</div>
		<hr class="divider" />
	</div>
	<strong><a style="cursor: pointer;" id="add-slide">+ Add Slide</a></strong>
	
</form>
<div class="mce-foot"><a class="add" href="javascript:Slider.insert(Slider.e)">Insert</a></div>
<!--/*************************************/ -->
<?php } elseif( $page == 'carousel' ){ ?>
	
	<script type="text/javascript">
		var Carousel = {
			e: '',
			init: function(e) {
				Carousel.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
			
				var output = "[carousel ";
				var style=jQuery('#style').val();
				var nav = jQuery('#nav').val();
				var auto = jQuery('#auto').val();
				var speed = jQuery('#speed').val(); 
				var sitems = jQuery('#sitems').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
				
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}	
				if(style){
					output+='style="'+style+'" ';
				}					
				
				if(nav) {
					output += 'nav="'+nav+'" ';
				}
				if(auto) {
					output += ' auto="'+auto+'"';
				}
				if(speed) {
					output += ' speed="'+speed+'"';
				}
				if(sitems) {
					output += ' sitems="'+sitems+'"';
				}
                               
				output += "]";
				
				jQuery("textarea[id^=carousel_content]").each(function(intIndex, objValue) {
					output +='[caritem]'+jQuery(this).val()+'[/caritem]';
				});
				
				output += '[/carousel]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Carousel.init, Carousel);

		jQuery(document).ready(function() {
			jQuery("#interval-holder").hide();
			jQuery("#carouselAuto").change(function(){
				var selected = jQuery('#carouselAuto').val();
				if (selected == 'true'){
					jQuery("#interval-holder").show();
				}
				else{
					jQuery("#interval-holder").hide();
				}
			});
			jQuery("#add-carousel").click(function() {
				jQuery('#SlideShortcodeContent').append('<p><label for="carousel_content[]">Slide Content</label><textarea id="carousel_content[]" name="carousel_content[]" type="text" value="" ></textarea><hr /></p>');
			});
		});
		
	</script>
	<title>Add Carousel slide</title>

</head>
<body>

<form id="CarouselShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
	<div id="SlideShortcodeContent">
			<p>
				<label for="style">Style</label>
				<select id="style" name="style">
					<option value="">Default</option>
					<option value="logo">Logo</option>
				</select>
			</p>
			<p>
				<label for="nav">Navigation</label>
				<select id="nav" name="nav">
					<option value="true">Yes</option>
					<option value="false">No</option>
				</select>
			</p>
			<p>
				<label for="auto">Autoplay</label>
				<select id="auto" name="auto">
					<option value="true">Yes</option>
					<option value="false">No</option>
				</select>
			</p>	
			<p>
				<label for="speed">Autoplay Speed</label>
				<input id="speed" name="speed" type="text" value="800" />
			</p>
			<p>
				<label for="sitems">Slide Items for carousel</label>
				<input id="sitems" name="sitems" type="text" value="3" />
			</p>
			<p>
				<label for="carousel_content[]">Slide Content</label>
				<textarea id="carousel_content[]" name="carousel_content[]" type="text" value="" ></textarea>
			</p>
			<p>
				<label for="class">Extra Class</label>
				<input id="class" name="class" type="text" value="" />
			</p>
			<hr />
		</div>
	<strong><a style="cursor: pointer;" id="add-carousel">+ Add slide</a></strong>
	
</form>
<div class="mce-foot"><a class="add" href="javascript:Carousel.insert(Carousel.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'contact' ){ ?>
	<script type="text/javascript">
		
		var Contact = {
			e: '',
			init: function(e) {
				Contact.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var address = jQuery('#Contactaddress').val();
				var tel = jQuery('#Contacttel').val();
				var email = jQuery('#Contactemail').val();
				var website = jQuery('#Contactweb').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
				
				var output = '[contact ';
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				if(address) {
					output += 'address="'+address+'" ';
				}
				
				if(tel) {
					output += 'tel="'+tel+'" ';
				}
                
				
				if(email) {
					output += 'email="'+email+'" ';
				}
                if(website) {
					output += 'website="'+website+'" ';
				}
				
				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(Contact.init, Contact);

	</script>
	<title>Insert contact details</title>

</head>
<body>

<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="Contactaddress">Address</label>
		<input id="Contactaddress" name="Contactaddress" type="text" value="" />
	</p>
	<p>
		<label for="Contacttel">Telephone</label>
		<input id="Contacttel" name="Contacttel" type="text" value="" />
	</p>
       
	<p>
		<label for="Contactemail">E-mail</label>
		<input id="Contactemail" name="Contactemail" type="text" value="" />
	</p>

	<p>
		<label for="Contactweb">Website</label>
		<input type="text" id="Contactweb" name="Contactweb" />
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
		
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:Contact.insert(Contact.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif($page=='fblock') {?>
    <script type="text/javascript">
        var fblock={
            e: '',
            init: function(e){
                fblock.e=e,
                tinyMCEPopup.resizeToInnerSize();
            },
            insert: function createGalleryShortcode(e){
				var Type=jQuery('#fblockType').val();
                var Title=jQuery('#fblockTitle').val();
				var titleCustomColor=jQuery('#titleCustomColor').val();
                var Icon=jQuery('#icon').val();
				var iconCustomColor=jQuery('#iconCustomColor').val();
				var iconBgCustomColor=jQuery('#iconBgCustomColor').val();
                var Fcontent=jQuery('#fblockContent').val();
                var Link=jQuery('#link').val();
				var divider=jQuery('#divider').val();
				var image=jQuery('#fullimage-img').val();
                var anim=jQuery('#anim').val();
                var addclass=jQuery('#class').val();
		
                var output='[fblock';
                if (anim){
                    output+= ' anim="'+anim+'"';
                }
                if(addclass){
                    output+=' class="'+addclass+'"';
                }
				if(Type){
                    output+=' type="'+Type+'"';
                }
                if(Title){
                    output+=' title="'+Title+'"';
                }
				 if(image){
                    output+=' image="'+image+'"';
                }
				if(titleCustomColor){
                    output+=' tcuscolor="'+titleCustomColor+'"';
                }
				 
                if(Icon){
                    output+=' icon="'+Icon+'"';
                }
                
				if(iconCustomColor){
                    output+=' icuscolor="'+iconCustomColor+'"';
                }
				
				if(iconBgCustomColor){
                    output+=' icusbgcolor="'+iconBgCustomColor+'"';
                }
                if(Link){
                    output+=' link="'+Link+'"';
                }
				if(divider){
                    output+=' divider="'+divider+'"';
                }
				                
                output+=']'+Fcontent+'[/fblock]';
                tinyMCEPopup.execCommand('mceReplaceContent', false, output);
		tinyMCEPopup.close();
            }
        }
        tinyMCEPopup.onInit.add(fblock.init, fblock);
    </script>
    <title>Insert Featured Block</title>
</head>
<body>
    <form id="GalleryShortcode">
         <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
	  jQuery("#fblockType").change(function(){
			var selected = jQuery('#fblockType').val();
			if (selected == 'alter'){
				jQuery("#iconwrap").hide();  
			}else{
				jQuery("#iconwrap").show(); 
			}
		});
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="fblockType">Block Type:</label>
		<select id="fblockType" name="fblockType">
			<option value="default">Default</option>
			<option value="alter">Alternative</option>
		</select>
	</p>
	<p>
		<label for="fblockTitle">Block Title:</label>
		<input type="text" id="fblockTitle">
	</p>
	<p>
		<label for="titleCustomColor"> Title custom color :</label>
		<div id="titleCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="titleCustomColor" id="titleCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#titleCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#titleCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#titleCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
	 <p>
		<label for="icon">Block Icon:</label>
		<input type="text" id="icon">
		<small><a href="<?php echo $fonturl ?>" target="blank">Icons list</a></small>
	</p>
	<p id="image-holder">
		<label for="upload_fullimage_button">Block image:</label>
		<input id="fullimage-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="fullimage" value="" />
		<input id="upload_fullimage_button" type="button" class="small_button" value="Upload" />
		<div id="fullimage-preview" class="img-preview" <?php if(!weblusive_get_option('fullimagefblock')) echo 'style="display:none;"' ?>>
			<img src="<?php if(weblusive_get_option('fullimagefblock')) echo weblusive_get_option('fullimagefblock'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" />
			<a class="del-img" title="Delete"></a>
		</div>
		<div class="clear"></div>
	</p>
	<p>
		<label for="iconCustomColor">Icon custom color :</label>
		<div id="iconCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="iconCustomColor" id="iconCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#iconCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#iconCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#iconCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
	<div id="iconwrap">
	<p>
		<label for="iconBgCustomColor">Icon Background custom color :</label>
		<div id="iconBgCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="iconBgCustomColor" id="iconBgCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#iconBgCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#iconBgCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#iconBgCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
	<p>
		<label for="link">Block Link:</label>
		<input type="text" id="link">
		<small>For Default type</small>
	</p>
	</div>
	<p>
		<label for="divider">Insert Divider:</label>
		<select id="divider" name="divider">
			<option value="">No</option>
			<option value="fdivider">Yes</option>
		</select>
	</p>
	<p>
		<label for="fblockContent">Block Content:</label>
		<textarea id="fblockContent" style="width:200px; height:50px"></textarea>
		
	</p>
	
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
		
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:fblock.insert(fblock.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif($page=='tblock'){ ?>
<script type="text/javascript">
    var tblock={
        e:'',
        init:function(e){
            tblock.e=e;
            tinyMCEPopup.resizeToInnerSize();
        },
        insert:function createGalleryShortcode(e){
            var Title=jQuery('#tblockTitle').val();
			var SecTitle=jQuery('#sectext').val();
			var Tag=jQuery('#tblockTag').val();
			var Type=jQuery('#type').val();
			var titleCustomColor=jQuery('#titleCustomColor').val();
            var anim=jQuery('#anim').val();
            var addclass=jQuery('#class').val();
            var output='[tblock ';
            if (anim){
                output+= 'anim="'+anim+'" ';
            }
            if(addclass){
                output+='class="'+addclass+'" ';
            }
            if(Type){
                output+=' type="'+Type+'"';
            }
			if(titleCustomColor){
                output+=' tcuscolor="'+titleCustomColor+'"';
            }
			if(Title){
                output+=' title="'+Title+'"';
            }
			if(SecTitle){
                output+=' sectext="'+SecTitle+'"';
            }
			if(Tag){
                output+=' tag="'+Tag+'"';
            }

            output+='/]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    }
    tinyMCEPopup.onInit.add(tblock.init, tblock);
</script>
<title>Add Title Block</title>
</head>
<body>
    <form id="GalleryShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
	  jQuery("#sectextwrap").hide(); 
	  jQuery("#type").change(function(){
			var selected = jQuery('#type').val();
			if (selected == 'onepage'){
				jQuery("#sectextwrap").show();  
			}else{
				jQuery("#sectextwrap").hide(); 
			}
		});
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="type">Type:</label>
		<select  id="type" name="type">
			<option value="default">Default</option>
			<option value="alter">Alternative</option>
			<option value="onepage">Alternative 2</option>
		</select>
	</p>
        <p>
            <label for="tblockTitle">Title:</label>
            <input type="text" id="tblockTitle">
        </p>
        <p>
            <label for="tblockTag">Tag:</label>
            <select id="tblockTag" name="tblockTag">
                <option value="h1">H1</option>
                <option value="h2">H2</option>
				<option value="h3">H3</option>
				<option value="h4">H4</option>
				<option value="h5">H5</option>
				<option value="h6">H6</option>
            </select>
        </p>
		 <p id="sectextwrap">
            <label for="sectext">Secondary text</label>
            <input type="text" id="sectext">
        </p>
		<p>
		<label for="titleCustomColor"> Title custom color :</label>
		<div id="titleCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="titleCustomColor" id="titleCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#titleCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#titleCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#titleCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
        <p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
		
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:tblock.insert(tblock.e)">Insert</a></div>
<!--/*************************************/ 

<!--/*************************************/ -->


<?php } elseif($page=='lead'){ ?>
<script type="text/javascript">
    var leadp={
        e:'',
        init:function(e){
            leadp.e=e;
            tinyMCEPopup.resizeToInnerSize();
        },
        insert:function createGalleryShortcode(e){
            var ProgressColor = jQuery('#ProgressCustomColor').val();
            var Color=jQuery('#tblockColor').val();
            var Pos=jQuery('#tblockPos').val();
            var anim=jQuery('#anim').val();
			var content = jQuery('#content').val();
			
            var addclass=jQuery('#class').val();
            
			var output='[lead ';
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				if(Color){
					output+=' color="'+Color+'"';
				}
				if(ProgressColor){
					output+=' customcolor="'+ProgressColor+'"';
				}
				
				if(Pos){
					output+=' position="'+Pos+'"';
				}
				
            output += ']'+content+'[/lead]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    }
    tinyMCEPopup.onInit.add(leadp.init, leadp);
</script>
<title>Add Lead paragraph</title>
</head>
<body>
<form id="GalleryShortcode">
	<script> 
	jQuery(function(){
	  jQuery("#animcontent").load("animation.html"); 
	});
	</script>
	<p id="animcontent"></p>
  
	<p>
		<label for="tblockColor">Color:</label>
		<select id="tblockColor" name="tblockColor">
			<option value="">Default</option>
			<option value="color-info">Info</option>
			<option value="color-success">Success</option>
			<option value="color-warning">Warning</option>
			<option value="color-danger">Danger</option>
			<option value="color-white">white</option>
			<option value="color-dark">Dark</option>
		</select>
	</p>
	<p>
		<label for="ProgressCustomColor">Custom color :</label>
		<div id="ProgressCustomColorcolorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="ProgressCustomColor" id="ProgressCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#ProgressCustomColorcolorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#ProgressCustomColorcolorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#ProgressCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
	<p>
		<label for="tblockPos">Position:</label>
		 <select id="tblockPos" name="tblockPos">
			<option value="text-center">Center</option>
			<option value="">Left</option>
			<option value="text-right">Right</option>
		</select>
	</p>
	<p>
		<label for="content">Content</label>
		<textarea id="content" name="content" col="20"></textarea>
	</p>
   
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:leadp.insert(leadp.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif($page=='reveal') { ?>
<script type="text/javascript">
    var reveal={
        e:'',
        init:function(e){
            reveal.e=e;
            //tinyMcePopup.resizeToInnerSize();
        },
        insert: function createGalleryShortcode(e){
            var ButtonColor = jQuery('#ButtonColor').val();
            var Buttonsize = jQuery('#Buttonsize').val();
            var Buttontype = jQuery('#ButtonType').val();
            var Buttontext = jQuery('#Buttontext').val();
			var closebutton = jQuery('#closebutton').val();
            var RevTitle = jQuery('#revTitle').val();
            var RevContent = jQuery('#revContent').val();
            var addclass=jQuery('#class').val();
            
            var output = '[reveal ';
         
            if(addclass){
                output+='class="'+addclass+'" ';
            }
            if(ButtonColor) {
                output += ' color="'+ButtonColor+'" ';
            }
            if(Buttonsize) {
                output += ' size="'+Buttonsize+'" ';
            }
            if(Buttontype) {
                output += ' type="'+Buttontype+'" ';
            }
            if(Buttontext){
                output+=' button="'+Buttontext+'"';
            }
           
            if(RevTitle){
                output+=' revtitle="'+RevTitle+'"';
            }
			if(closebutton){
                output+=' noclosebutton="'+closebutton+'"';
            }
            

            output += ']'+RevContent+'[/reveal]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
	
	}
}
tinyMCEPopup.onInit.add(reveal.init, reveal);
jQuery(function(){
	jQuery("#button-params").hide();
	jQuery("#ButtonType").change(function(){
		var selected = jQuery('#ButtonType').val();
		if (selected == ''){
			jQuery("#button-params").hide();
		}
		else{
			jQuery("#button-params").show();
		}
	});
});
</script>
<title>Add Modal Box</title>

</head>
<body>
    <form id="GalleryShortcode">
	<p>
		<label for="ButtonType">Button Type:</label>
		<select id="ButtonType" name="ButtonType">
			<option value="btn">Button</option>
			<option value="" selected="selected">Minimal (link-style)</option>
		</select>
	</p>
    <div id="button-params">
		<p>
			<label for="ButtonColor">Button Color:</label>
			<select id="ButtonColor" name="ButtonColor">
				<option value="">None</option>
				<option value="btn-default">Default</option>
				<option value="btn-primary">Primary</option>
				<option value="btn-info">Info</option>
				<option value="btn-success">Success</option>
				<option value="btn-warning">Warning</option>
				<option value="btn-danger">Danger</option>
			</select>
		</p>
		<p>
			<label for="ButtonSize">Button Size :</label>
			<select id="ButtonSize" name="ButtonSize">
				
				<option value="">Default</option>
				<option value="btn-lg">Large</option>
				<option value="btn-sm">Small</option>
				<option value="btn-xs">Very small</option>	
			</select>
		</p>
	</div>
	
	<p>
		<label for="Buttontext">Button Text :</label>
		<input id="Buttontext" name="Buttontext" type="text" value="" />
	</p>
	<hr>
   
	<p>
		<label for="revTitle">Modal Box Title</label>
		<input type="text" id="revTitle" name="revTitle">
	</p>
	<p>
		<label for="revContent">Modal Box Content</label>
		<textarea id="revContent" name="revContent" col="20"></textarea>
	</p>
	<p>
		<label for="closebutton">Hide Bottom close button:</label>
		<select id="closebutton" name="closebutton">
			<option value="false">No</option>
			<option value="true">Yes</option>
		</select>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:reveal.insert(reveal.e)">Insert</a></div>
<!--/*************************************/ -->
<?php } elseif( $page == 'portlisting' ){ ?>

	<script type="text/javascript">
		var portlisting = {
			e: '',
			init: function(e) {
				portlisting.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var limit = jQuery('#portfolioLimit').val();
				var sitems = jQuery('#sitems').val();
				var featured = jQuery('#portfolioFeatured').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
				
				
				var output = '[portlist';
				if (anim){
					output+= ' anim="'+anim+'"';
				}
				if(addclass){
					output+=' class="'+addclass+'"';
				}
				if(sitems) {
					output += ' sitems="'+sitems+'"';
				}
				
				if(limit) {
					output += ' limit="'+limit+'"';
				}
				
				if(featured) {
					output += ' featured="'+featured+'"';
				}
				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(portlisting.init, portlisting);

	</script>
	<title>Add Portfolio Listing</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
		jQuery("#interval-wrapper").hide();
		jQuery("#animcontent").load("animation.html"); 
		jQuery("#carouselAuto").change(function(){
			var selected = jQuery('#carouselAuto').val();
			if (selected == 'false'){
				jQuery("#interval-wrapper").hide();
			}
			else{
				jQuery("#interval-wrapper").show();
			}
			
		});
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="portfolioLimit">Items limit</label>
		<input id="portfolioLimit" name="portfolioLimit" type="Text" value="6" />
	</p>
	<p>
		<label for="sitems">Items on slider</label>
		<input id="sitems" name="sitems" type="Text" value="3" />
	</p>
	<p>
		<label for="portfolioFeatured">Type of items to show</label>
		<select id="portfolioFeatured" name="portfolioFeatured">
			<option value="0">All items</option>
			<option value="1">Only featured items</option>
		</select>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:portlisting.insert(portlisting.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'bloglisting' ){ ?>

	<script type="text/javascript">
		var blogList = {
			e: '',
			init: function(e) {
				blogList.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var limit = jQuery('#blogLimit').val();
				var category = jQuery('#blogCategory').val();
				var order = jQuery('#blogOrder').val();
				var orderby = jQuery('#blogOrderby').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();
				
				
				var output = '[list_posts ';
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				
				
				if(limit) {
					output += ' limit="'+limit+'"';
				}
				if(category) {
					output += ' category="'+category+'"';
				}
				if(order) {
					output += ' order="'+order+'"';
				}
				if(orderby) {
					output += ' orderby="'+orderby+'"';
				}

				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(blogList.init, blogList);

	</script>
	<title>Add Blog Listing</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
		jQuery("#animcontent").load("animation.html"); 
		jQuery("#interval-wrapper").hide();
	  	jQuery("#autoslide").change(function(){
			var selected = jQuery('#autoslide').val();
			if (selected == 'false'){
				jQuery("#interval-wrapper").hide();
			}
			else{
				jQuery("#interval-wrapper").show();
			}
		});
		
		jQuery("#blogLayout").change(function(){
			var selected = jQuery('#blogLayout').val();
			if (selected == '2'){
				jQuery("#carousel-options").hide();
			}
			else{
				jQuery("#carousel-options").show();
			}
		});
		
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="blogLimit">Items limit</label>
		<input id="blogLimit" name="blogLimit" type="Text" value="6" />
	</p>
	<p>
		<label for="blogCategory">Category</label>
		<input id="blogCategory" name="blogCategory" type="Text" value="" />
		<br /><small style="margin-left:150px">Specify category Id or leave blank to display items from all categories.</small>
	</p>
	<p>
		<label for="blogOrder">Posts order</label>
		<select id="blogOrder" name="blogOrder">
			<option value="DESC">Descending</option>
			<option value="ASC">Ascending</option>
		</select>
	</p>
	<p>
		<label for="blogOrderby">Order by:</label>
		<select id="blogOrderby" name="blogOrderby">
			<option value="date">Date</option>
			<option value="id">ID</option>
			<option value="author">Author</option>
			<option value="title">Title</option>
			<option value="comment_count">Number of comments</option>
			<option value="rand">Randomly</option>
		</select>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:blogList.insert(blogList.e)">Insert</a></div>
<!--/*************************************/ -->
<?php } elseif( $page == 'productcar' ){ ?>

	<script type="text/javascript">
		var prodList = {
			e: '',
			init: function(e) {
				prodList.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var productid = jQuery('#productid').val();
				var prodtags = jQuery('#prodtags').val();
				var prodcat = jQuery('#prodcat').val();
				var prodcatslug = jQuery('#prodcatslug').val();
				var limit = jQuery('#prodlimit').val();
				var order = jQuery('#productorder').val();
				var orderby = jQuery('#productorderby').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();				
				var showarrows=jQuery('#showarrows').val(); 
				var automatic=jQuery('#autoslide').val();  
				var interval=jQuery('#interval').val(); 
				var items=jQuery('#items').val(); 
				
				var output = '[productcar';
				if (anim){
					output+= ' anim="'+anim+'" ';
				}
				if(addclass){
					output+=' class="'+addclass+'" ';
				}
				if(items){
					output+=' items="'+items+'" ';
				}
				if(showarrows){
					output+=' showarrows="'+showarrows+'" ';
				}
				if(automatic){
					output+=' automatic="'+automatic+'" ';
				}
				if(interval){
					output+=' interval="'+interval+'" ';
				}
				if(limit) {
					output += ' limit="'+limit+'"';
				}
				if(productid) {
					output += ' prod_ids="'+productid+'"';
				}
				if(prodtags) {
					output += ' prod_tags="'+prodtags+'"';
				}
				if(prodcat) {
					output += ' cat_ids="'+prodcat+'"';
				}
				if(prodcatslug) {
					output += ' cat_slugs="'+prodcatslug+'"';
				}
				if(order) {
					output += ' order="'+order+'"';
				}
				if(orderby) {
					output += ' orderby="'+orderby+'"';
				}

				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(prodList.init, prodList);

	</script>
	<title>Woocommerce products listing</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
		jQuery("#animcontent").load("animation.html"); 
		jQuery("#interval-wrapper").hide();
	  	jQuery("#autoslide").change(function(){
			var selected = jQuery('#autoslide').val();
			if (selected == 'false'){
				jQuery("#interval-wrapper").hide();
			}
			else{
				jQuery("#interval-wrapper").show();
			}
		});
		
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="productid">Product ID(s)</label>
		<input id="productid" name="productid" type="Text" value="" />
		<a href="#" class="mo-help tooltip" title="Separate product IDs with commas. E.g.: 4,18,22"><img src="<?php echo $icondir?>/smicon.png" alt="" /></a>
	</p>
	<p>
		<label for="prodtags">Product tag(s)</label>
		<input id="prodtags" name="prodtags" type="Text" value="" />
		<a href="#" class="mo-help tooltip" title="Separate product tags with commas. E.g.: technology, furniture"><img src="<?php echo $icondir?>/smicon.png" alt="" /></a>
	</p>
	<p>
		<label for="prodcat">Category ID(s)</label>
		<input id="prodcat" name="prodcat" type="Text" value="" />
		<a href="#" class="mo-help tooltip" title="Separate category IDs with commas. E.g.: 4,18,22"><img src="<?php echo $icondir?>/smicon.png" alt="" /></a>
	</p>
	<p>
		<label for="prodcatslug">Category slug(s)</label>
		<input id="prodcatslug" name="prodcatslug" type="Text" value="" />
		<a href="#" class="mo-help tooltip" title="Separate category slugs with commas. E.g.: sofa,chair,table"><img src="<?php echo $icondir?>/smicon.png" alt="" /></a>
	</p>
	<p>
		<label for="prodlimit">Items limit</label>
		<input id="prodlimit" name="prodlimit" type="Text" value="5" />
	</p>
	<p>
		<label for="items">Items to Show</label>
		<input id="items" name="items" type="Text" value="4" />
	</p>
	<p>
		<label for="autoslide">Automatic sliding</label>
		<select id="autoslide" name="autoslide">
			<option value="false">No</option>
			<option value="true">Yes</option>
		</select>
	</p>
	<div id="carousel-options">
		<p id="interval-wrapper">
			<label for="interval">Sliding interval</label>
			<input id="interval" name="interval" type="text" value="2500" />
		</p>
	</div>
	<p>
		<label for="showarrows">Show arrows</label>
		<select id="showarrows" name="showarrows">
			<option value="true">Yes</option>
			<option value="false">No</option>
		</select>
	</p>
	<p>
		<label for="productorder">Order</label>
		<select id="productorder" name="productorder">
			<option value="DESC">Descending</option>
			<option value="ASC">Ascending</option>
		</select>
	</p>
	<p>
		<label for="productorderby">Order by:</label>
		<select id="productorderby" name="productorderby">
			<option value="menu_order">Default sorting</option>
			<option value="popularity">Sort by popularity</option>
			<option value="rating">Sort by average rating</option>
			<option value="date">Sort by newness</option>
			<option value="price">Sort by price: low to high</option>
			<option value="price-desc">Sort by price: high to low</option>
		</select>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:prodList.insert(prodList.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'shop_special_products' ){ ?>

	<script type="text/javascript">
		var prodspecList = {
			e: '',
			init: function(e) {
				prodspecList.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var type = jQuery('#type').val();
				var limit = jQuery('#prodlimit').val();
				var order = jQuery('#productorder').val();
				var orderby = jQuery('#productorderby').val();
				var anim=jQuery('#anim').val();
				var addclass=jQuery('#class').val();				
				var showarrows=jQuery('#showarrows').val(); 
				var automatic=jQuery('#autoslide').val();  
				var interval=jQuery('#interval').val(); 
				var items=jQuery('#items').val(); 
				
				var output = '[shop_special_products';
				if (anim){
					output+= ' anim="'+anim+'" ';
				}
				if(addclass){
					output+=' class="'+addclass+'" ';
				}
				if(items){
					output+=' items="'+items+'" ';
				}
				if(showarrows){
					output+=' showarrows="'+showarrows+'" ';
				}
				if(automatic){
					output+=' automatic="'+automatic+'" ';
				}
				if(interval){
					output+=' interval="'+interval+'" ';
				}
				if(limit) {
					output += ' limit="'+limit+'"';
				}
				if(type) {
					output += ' type="'+type+'"';
				}
			
				if(order) {
					output += ' order="'+order+'"';
				}
				if(orderby) {
					output += ' orderby="'+orderby+'"';
				}

				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(prodspecList.init, prodspecList);

	</script>
	<title>Woocommerce products listing by special type</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
		jQuery("#animcontent").load("animation.html"); 
		jQuery("#interval-wrapper").hide();
	  	jQuery("#autoslide").change(function(){
			var selected = jQuery('#autoslide').val();
			if (selected == 'false'){
				jQuery("#interval-wrapper").hide();
			}
			else{
				jQuery("#interval-wrapper").show();
			}
		});
		
		jQuery("#type").change(function(){
			var selected = jQuery('#type').val();
			if (selected == '2'){
				jQuery("ordertype").hide();
			}
			else{
				jQuery("#ordertype").show();
			}
		});
		
		
		
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="type">Listing type</label>
		<select id="type" name="type">
			<option value="1">On Sale</option>
			<option value="2">Best selling</option>
			<option value="3">Featured</option>
		</select>
	</p>
	<p>
		<label for="prodlimit">Items limit</label>
		<input id="prodlimit" name="prodlimit" type="Text" value="5" />
	</p>
	<p>
		<label for="items">Items to show</label>
		<input id="items" name="items" type="Text" value="4" />
	</p>
	<p>
		<label for="autoslide">Automatic sliding</label>
		<select id="autoslide" name="autoslide">
			<option value="false">No</option>
			<option value="true">Yes</option>
		</select>
	</p>
	<div id="carousel-options">
		<p id="interval-wrapper">
			<label for="interval">Sliding interval</label>
			<input id="interval" name="interval" type="text" value="2500" />
		</p>
	</div>
	<p>
		<label for="showarrows">Show arrows</label>
		<select id="showarrows" name="showarrows">
			<option value="false">No</option>
			<option value="true">Yes</option>
		</select>
	</p>
	<p>
		<label for="productorder">Order</label>
		<select id="productorder" name="productorder">
			<option value="DESC">Descending</option>
			<option value="ASC">Ascending</option>
		</select>
	</p>
	<p id="ordertype">
		<label for="productorderby">Order by:</label>
		<select id="productorderby" name="productorderby">
			<option value="menu_order">Default sorting</option>
			<option value="popularity">Sort by popularity</option>
			<option value="rating">Sort by average rating</option>
			<option value="date">Sort by newness</option>
			<option value="price">Sort by price: low to high</option>
			<option value="price-desc">Sort by price: high to low</option>
		</select>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:prodspecList.insert(prodspecList.e)">Insert</a></div>
<!--/*************************************/ -->


<?php } elseif($page=='social') { ?>
<script type="text/javascript">
    var social={
        e:'',
        init:function(e){
            social.e=e;
            tinyMCEPopup.resizeToInnerSize();
        },
        insert: function createGalleryShortCode(e){
            
            var Icon = jQuery('#icon').val();
            var Link = jQuery('#link').val();
            var anim = jQuery('#anim').val();
			var target = jQuery('#social_target').val();
			var size = jQuery('#social_size').val();
			
            var addclass=jQuery('#class').val();
					
            var output = '[social ';
            if (anim){
                output+= ' anim="'+anim+'" ';
            }
            if(addclass){
                output+=' class="'+addclass+'" ';
            }
			 if(size){
                output+=' size="'+size+'" ';
            }
			  if(target){
                output+=' target="'+target+'" ';
            }
            output+= ']';
            jQuery("select[id^=icon]").each(function(intIndex, objValue) {
		output +='[soc_button icon="'+jQuery(this).val()+'"';
		var obj = jQuery('input[id^=link]').get(intIndex);
		output += ' link="'+obj.value+'" ';
		output += "/]";
            });
				
            output += '[/social]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    }
    tinyMCEPopup.onInit.add(social.init, social);
    jQuery(document).ready(function() {
        jQuery("#add-social").click(function() {
            jQuery('#SocShortcodeContent').append('<p><label for="icon[]">Social Button</label><select id="icon[]" name="icon[]"><option value="bitbucket">Bitbucket</option><option value="dribbble">Dribble</option><option value="facebook">Facebook</option><option value="flickr">Flickr</option><option value="github">Github</option><option value="google-plus">Google+</option><option value="instagram">Instagram</option><option value="linkedin">LinkedIn</option><option value="pinterest">Pinterest</option><option value="skype">Skype</option><option value="stack-exchange">Stackexchange</option>        <option value="tumblr">Tumblr</option><option value="twitter">Twitter</option><option value="vk">Vkontakte</option><option value="youtube">Youtube</option></select></p><p><label for="link[]">Link to:</label><input type="text" id="link[]" name="link[]"></p><hr class="divider" />');
    });
    });
</script>
<title>Add Social Button</title>
</head>
<body>
    <form id="GalleryShortcode">
        <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
		<p id="animcontent"></p>
		<p>
			<label for="social_target">Link target</label>
			<select id="social_target" name="social_target">
				<option value="_blank">Open in new page</option>
				<option value="">Open in same page</option>
			</select>
        </p>
		<p>
			<label for="class">Extra Class (optional)</label>
			<input id="class" name="class" type="text" value="" />
        </p>
        <div id="SocShortcodeContent">
            <p>
				<label for="icon[]">Social Button</label>
				<select id="icon[]" name="icon[]">
					<option value="bitbucket">Bitbucket</option>
					<option value="dribbble">Dribble</option>
					<option value="facebook">Facebook</option>
					<option value="flickr">Flickr</option>
					<option value="github">Github</option>
					<option value="google-plus">Google+</option>
					<option value="instagram">Instagram</option>
					<option value="linkedin">LinkedIn</option>
					<option value="pinterest">Pinterest</option>
					<option value="skype">Skype</option>
					<option value="stack-exchange">Stackexchange</option>        
					<option value="tumblr">Tumblr</option>
					<option value="twitter">Twitter</option>
					<option value="vk">Vkontakte</option>
					<option value="youtube">Youtube</option>
				</select>
            </p>
            <p>
                <label for="link[]">Link to (with http):</label>
                <input type="text" id="link[]" name="link[]">
            </p>
			
            <p>
                <hr class="divider" />  
            </p>
        </div>
        <strong><a style="cursor: pointer;" id="add-social">+ Add Social Button</a></strong>
    </form>
	<div class="mce-foot"><a class="add" href="javascript:social.insert(social.e)">Insert</a></div>
<!--/*************************************/ -->


<?php } elseif($page=='teammember') { ?>
<script type="text/javascript">
    var team={
        e:'',
        init:function(e){
            team.e=e;
            tinyMCEPopup.resizeToInnerSize();
        },
        insert: function createGalleryShortCode(e){
            
            var style=jQuery('#style').val();
			var name = jQuery('#membername').val();
			var position = jQuery('#memberposition').val();
			var photo = jQuery('#memberphoto-img').val();
			var imgstyle=jQuery('#imgstyle').val();
			var desc=jQuery('#memberdesc').val();
			var anim=jQuery('#anim').val();
			var addclass=jQuery('#class').val();
			var BgCustomColor=jQuery('#BgCustomColor').val();

			var output = '[teammember ';
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				if(style) {
					output += ' style="'+style+'"';
				}
				if(name) {
					output += ' name="'+name+'"';
				}
				if(position) {
					output += ' position="'+position+'"';
				}
							
				if(photo) {
					output += ' photo="'+photo+'"';
				} 
				if(imgstyle) {
					output += ' imgstyle="'+imgstyle+'"';
				}
				if(BgCustomColor) {
					output += ' cusbgover="'+BgCustomColor+'"';
				} 
			output += ']';
			
			if(desc) {
				output += desc;
			}
            jQuery("select[id^=tmicon]").each(function(intIndex, objValue) {
				output +='[tmsocbutton tmicon="'+jQuery(this).val()+'"';
				var obj = jQuery('input[id^=tmlink]').get(intIndex);
				output += ' tmlink="'+obj.value+'" ';
				output += "/]";
            });
				
            output += '[/teammember]';
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    }
    tinyMCEPopup.onInit.add(team.init, team);
    jQuery(document).ready(function() {
        jQuery("#add-social").click(function() {
            jQuery('#TeamMemberContent').append('<p><label for="tmicon[]">Social Button</label><select id="tmicon[]" name="tmicon[]"><option value="fa-bitbucket">Bitbucket</option><option value="fa-dribbble">Dribble</option><option value="fa-facebook">Facebook</option><option value="fa-flickr">Flickr</option><option value="fa-github">Github</option><option value="fa-google-plus">Google+</option><option value="fa-instagram">Instagram</option><option value="fa-linkedin">LinkedIn</option><option value="fa-pinterest">Pinterest</option><option value="fa-skype">Skype</option><option value="fa-stack-exchange">Stackexchange</option><option value="fa-tumblr">Tumblr</option><option value="fa-twitter">Twitter</option><option value="fa-vk">Vkontakte</option><option value="fa-youtube">Youtube</option></select></p><p><label for="tmlink[]">Link to:</label><input type="text" id="tmlink[]" name="tmlink[]"></p><hr class="divider" />');
		});
    });
    
</script>
<title>Add Team Member</title>
</head>
<body>
<form id="GalleryShortcode">
	<script> 
	jQuery(function(){
	  jQuery("#animcontent").load("animation.html"); 
	});
	</script>
    <p id="animcontent"></p>
	<div id="TeamMemberContent">
		<p>
			<label for="style">Style</label>
			<select  id="style" name="style">
				<option value="">Default</option>
				<option value="text-center">Centered</option>
			</select>
		</p>
		<p>
			<label for="membername">Name</label>
			<input type="text" id="membername" name="membername" />
		</p>
		<p>
			<label for="memberposition">Position</label>
			<input type="text" id="memberposition" name="memberposition" />
		</p>
		<div class="wrap-list">
			<label for="upload_memberphoto_button">Photo:</label>
			<input id="memberphoto-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="memberphoto" value="" />
			<input id="upload_memberphoto_button" type="button" class="small_button" value="Upload" />
			<div id="memberphoto-preview" class="img-preview" <?php if(!weblusive_get_option('memberphoto')) echo 'style="display:none;"' ?>>
				<img src="<?php if(weblusive_get_option('memberphoto')) echo weblusive_get_option('memberphoto'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" />
				<a class="del-img" title="Delete"></a>
			</div>
			<div class="clear"></div>
		</div>
		<p>
			<label for="imgstyle">Image style</label>
			<select  id="imgstyle" name="imgstyle">
				<option value="img-responsive">Default</option>
				<option value="img-circle">Circle</option>
			</select>
		</p>
		<p>
		<label for="BgCustomColor">Overlay custom color :</label>
		<div id="BgCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="BgCustomColor" id="BgCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#BgCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#BgCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#BgCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
		<p>
			<label for="desc">Member info</label>
			<textarea type="text" id="memberdesc" name="memberdesc" cols="20" rows="10"  ></textarea>
		</p>
		<p>
			<label for="class">Extra Class</label>
			<input id="class" name="class" type="text" value="" />
		</p>
		<hr class="divider" />
		<p>
			<label for="tmicon[]">Social Button</label>
			<select id="tmicon[]" name="tmicon[]">
				<option value="fa-bitbucket">Bitbucket</option>
				<option value="fa-dribbble">Dribble</option>
				<option value="fa-facebook">Facebook</option>
				<option value="fa-flickr">Flickr</option>
				<option value="fa-github">Github</option>
				<option value="fa-google-plus">Google+</option>
				<option value="fa-instagram">Instagram</option>
				<option value="fa-linkedin">LinkedIn</option>
				<option value="fa-pinterest">Pinterest</option>
				<option value="fa-skype">Skype</option>
				<option value="fa-stack-exchange">Stackexchange</option>        
				<option value="fa-tumblr">Tumblr</option>
				<option value="fa-twitter">Twitter</option>
				<option value="fa-vk">Vkontakte</option>
				<option value="fa-youtube">Youtube</option>
			</select>
		</p>
		<p>
			<label for="tmlink[]">Link to (without http):</label>
			<input type="text" id="tmlink[]" name="tmlink[]">
		</p>
		<hr class="divider" />
	</div>
	<strong><a style="cursor: pointer;" id="add-social">+ Add Social Button</a></strong>
	
</form>
<div class="mce-foot"><a class="add" href="javascript:team.insert(team.e)">Insert</a></div>

<!--/*************************************/ -->
<?php } elseif($page=='list') { ?>
<script type="text/javascript">
	var list = {
		e: '',
		init: function(e) {
			list.e = e;
			tinyMCEPopup.resizeToInnerSize();
		},
		insert: function createGalleryShortcode(e) {
			var output = '[list ';
			var anim=jQuery('#anim').val();
			var addclass=jQuery('#class').val();
			var type=jQuery('#type').val();
			var style=jQuery('#style').val();
			if (anim){
				output+= ' anim="'+anim+'"';
			}
			if (type){
				output+= ' type="'+type+'"';
			}
			if (style){
				output+= ' style="'+style+'"';
			}
			if(addclass){
				output+=' class="'+addclass+'"';
			}
			output+=']';
			jQuery("input[id^=itemName").each(function(intIndex, objValue) {
				output +='[listitem ';
				var iconlink=jQuery('input[id^=itemLink]').get(intIndex);
				if(iconlink.value){
					output+=' link="'+iconlink.value+'"';
				}
				output+=']';
				var obj = jQuery('input[id^=itemName]').get(intIndex);
				output += obj.value;
				output += "[/listitem]";
			});
			
			
			output += '[/list]';
			tinyMCEPopup.execCommand('mceReplaceContent', false, output);
			tinyMCEPopup.close();
			
		}
	}
	tinyMCEPopup.onInit.add(list.init, list);

	jQuery(document).ready(function() {
		counter=1;
		
		jQuery("#add-listitem").click(function() {
			jQuery('#ListItemShortcodeContent').append('<p><label for="itemName[]">List Item Name</label><input id="itemName[]" name="itemName[]" type="text" value="" /></p><p><label for="itemLink[]">List Item Link</label><input  id="itemLink[]" name="itemLink[]" type="text" value="" /></p><hr class="divider" />');
			
			counter++;
		});
	});

</script>
<title>Add  List</title>

</head>
<body>
<form id="GalleryShortcode">
	<script> 
		jQuery(function(){
			jQuery("#animcontent").load("animation.html"); 
			
		});
    </script>
  
	<div id="ListItemShortcodeContent">
		<p id="animcontent"></p>
		
		<p>
			<label for="type">List Type</label>
			<select id="type" name="type">
				<option value="order">Ordered</option>
				<option value="unorder">Unordered</option>
				<option value="list-unstyled">Unstyled</option>
				<option value="circle">Circle</option>
			</select>
		</p>
		<p>
			<label for="style">List Style</label>
			<select id="style" name="style">
				<option value="">Unstyle</option>
				<option value="divider divider-list">Divider list</option>
			</select>
		</p>
		<p>
			<label for="class">Extra Class</label>
			<input id="class" name="class" type="text" value="" />
		</p>
		<hr class="divider" />
		<p>
			<label for="itemName[]">List Item Name</label>
			<input id="itemName[]" name="itemName[]" type="text" value="" />
		</p>
		
		<p>
			<label for="itemLink[]">List Item Link</label>
			<input  id="itemLink[]" name="itemLink[]" type="text" value="" />
		</p>		
		<hr class="divider" />
	</div>
	<strong><a style="cursor: pointer;" id="add-listitem">+ Add  List Item</a></strong>
	
</form>
<div class="mce-foot"><a class="add" href="javascript:list.insert(list.e)">Insert</a></div>
<!--/*************************************/ -->
<?php } elseif( $page == 'blockquote' ){
?>
	<script type="text/javascript">
		var blockquote = {
			e: '',
			init: function(e) {
				blockquote.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var author=jQuery('#author').val();
				var company=jQuery('#company').val();
				var content = jQuery('#content').val();
				var bcuscolor = jQuery('#BCustomColor').val();
				var anim=jQuery('#anim').val();     
				var position=jQuery('#pos').val(); 
				var addclass=jQuery('#class').val();        
				var output = '[blockquote';
				if(author) {
					output += ' author="'+author+'"';
				}
                
				if(company) {
					output += ' company="'+company+'"';
				}
				
				if (anim){
					output+= ' anim="'+anim+'"';
				}
				if (position){
					output+= ' pos="'+position+'"';
				}
				if (bcuscolor){
					output+= ' bcuscolor="'+bcuscolor+'"';
				}
				if(addclass){
					output+=' class="'+addclass+'"';
				}
				output += ']'+content+'[/blockquote]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(blockquote.init, blockquote);

	</script>
	<title>Add Blockquote</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="pos">Position:</label>
		 <select id="pos" name="pos">
			<option value="">Left</option>
			<option value="pull-right">Right</option>
		</select>
	</p>
	<p>
		<label for="author">Author :</label>
		<input type="text" id="author" name="author"/>
	</p>
	<p>
		<label for="company">Company :</label>
		<input type="text" id="company" name="company"/>
	</p>
	<p>
		<label for="content">Content : </label>
		<textarea id="content" name="content" col="20"></textarea>
	</p>
	<p>
		<label for="BCustomColor">Border custom color :</label>
		<div id="BCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="BCustomColor" id="BCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#BCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#BCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#BCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
    <p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
    </p>
</form>
<div class="mce-foot"><a class="add" href="javascript:blockquote.insert(blockquote.e)">Insert</a></div>
<!--********************************************-->
<?php } elseif( $page == 'divider' ){
?>
<script type="text/javascript">
	var divider = {
		e: '',
		init: function(e) {
			divider.e = e;
			tinyMCEPopup.resizeToInnerSize();
		},
		insert: function createGalleryShortcode(e) {
			var type=jQuery('#type').val();
			var size = jQuery('#size').val();			   
			var addclass=jQuery('#class').val(); 
			var output = '[divider ';
				
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				if(type) {
					output += 'type="'+type+'" ';
				}
				if(size) {
					output += 'size="'+size+'" ';
				}
				
			output += '/]';
			tinyMCEPopup.execCommand('mceReplaceContent', false, output);
			tinyMCEPopup.close();
			
		}
	}
	tinyMCEPopup.onInit.add(divider.init, divider);

</script>
<title>Add Divider</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){ 
	  
		jQuery("#sizewrap").hide(); 
		jQuery("#type").change(function(){
			var selected = jQuery('#type').val();
			if (selected == 'blank-spacer'){
				jQuery("#sizewrap").show(); 
			}else{
				jQuery("#sizewrap").hide(); 
			}
		});
    });
    </script>
	<p>
		<label for="type">Type:</label>
		<select  id="type" name="type">
			<option value="">Default</option>
			<option value="blank-spacer">Blank Spacer</option>
		</select>
	</p>
	<p id="sizewrap">
		<label for="size">Size:</label>
		<select id="size" name="size">
			<option value="gap-20">Small</option>
			<option value="gap-30">Medium</option>
			<option value="gap-40">Large</option>
		</select>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:divider.insert(divider.e)">Insert</a></div>
<!--/*************************************/ -->

<?php } elseif( $page == 'tooltip' ){
?>
	<script type="text/javascript">
		var tooltip = {
			e: '',
			init: function(e) {
				tooltip.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var type=jQuery('#type').val();
				var butcolor=jQuery('#butcolor').val();
				var titleCustomColor=jQuery('#titleCustomColor').val();
				var buttext=jQuery('#buttext').val();
				var butlink=jQuery('#butlink').val();
				var toolpos=jQuery('#toolpos').val();
				var tooltext=jQuery('#tooltext').val();
				var anim=jQuery('#anim').val();     
				var addclass=jQuery('#class').val();        
                                
				var output = '[tooltip ';
					if (anim){
						output+= 'anim="'+anim+'" ';
					}
					if(addclass){
						output+='class="'+addclass+'" ';
					}
					if(type) {
						output += 'type="'+type+'" ';
					}
                    if(butcolor) {
						output += 'butcolor="'+butcolor+'" ';
					}
					if(titleCustomColor) {
						output += 'cuscolor="'+titleCustomColor+'" ';
					}
                    if(buttext) {
						output += 'buttext="'+buttext+'" ';
					}
                    if(butlink) {
						output += 'butlink="'+butlink+'" ';
					}
                    if(toolpos) {
						output += 'toolpos="'+toolpos+'" ';
					}
				output += ']'+tooltext+'[/tooltip]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(tooltip.init, tooltip);

	</script>
	<title>Add Tooltip</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
		jQuery(function(){
		  jQuery("#animcontent").load("animation.html"); 
		});
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="type">Tooltip Type:</label>
		<select id="type" name="type">
			<option value="button">Button</option>
			<option value="text">Text</option>
		</select>
	</p>
	<p>
		<label for="buttext">Title</label>
		<input type="text" id="buttext" name="buttext" />
	</p>
        <p>
		<label for="butlink">Button Link: </label>
		<input type="text" id="butlink" name="butlink" />
	</p>
	<p>
		<label for="butcolor">Button Color :</label>
		<select id="butcolor" name="butcolor">
			<option value="default">Default</option>
			<option value="primary">Primary</option>
			<option value="info">Info</option>
			<option value="success">Success</option>
			<option value="warning">Warning</option>
			<option value="danger">Danger</option>
		</select>
	</p>
	<p>
		<label for="titleCustomColor"> Custom color :</label>
		<div id="titleCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="titleCustomColor" id="titleCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#titleCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#titleCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#titleCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
	<p>
		<label for="toolpos">Tooltip Position:</label>
		<select  id="toolpos" name="toolpos">
			<option value="tl:bl" >Top Left</option>
			<option value="tc:bc">Top Center</option>
			<option value="tr:br">Top Right</option>
			<option value="bl:tl">Bottom Left</option>
			<option value="bc:tc">Bottom Center</option>
			<option value="br:tr">Bottom Right</option>
			<option value="ml:mr">Middle Left</option>
			<option value="mr:ml">Middle Right</option>
		</select>
	</p>
	<p>
		<label for="tooltext">Tooltip Content:</label>
		<textarea id="tooltext" name="tooltext" col="20"></textarea>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:tooltip.insert(tooltip.e)">Insert</a></div>
<!--/*************************************/ -->
<?php } elseif( $page == 'lightbox' ){
?>
<script type="text/javascript">
	var lightbox = {
		e: '',
		init: function(e) {
			lightbox.e = e;
			tinyMCEPopup.resizeToInnerSize();
		},
		insert: function createGalleryShortcode(e) {
			var type=jQuery('#type').val();
			var thumbnail=jQuery('#lboxthumb-img').val();
			var src=jQuery('#src').val();
			var caption=jQuery('#caption').val();
			var anim=jQuery('#anim').val();    
			var alignment=jQuery('#alignment').val();   
			
			var addclass=jQuery('#class').val();
						   
			var output = '[lightbox ';
				if (anim){
					output+= 'anim="'+anim+'" ';
				}
				if(addclass){
					output+='class="'+addclass+'" ';
				}
				if(alignment){
					output+='alignment="'+alignment+'" ';
				}
				if(type) {
					output += 'type="'+type+'" ';
				}
				if(image) {
					output += 'image="'+image+'" ';
				}
				if(thumbnail) {
					output += 'thumbnail="'+thumbnail+'" ';
				}
				if(src) {
					output += 'src="'+src+'" ';
				}
				output += ']'+caption+'[/lightbox]';
			tinyMCEPopup.execCommand('mceReplaceContent', false, output);
			tinyMCEPopup.close();
			
		}
	}
	tinyMCEPopup.onInit.add(lightbox.init, lightbox);

</script>
<title>Add Lightbox</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
		jQuery(function(){
		  jQuery("#animcontent").load("animation.html"); 
		});
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="type">Type:</label>
		<select id="type" name="type">
			<option value="image">Image</option>
			<option value="video">Video</option>
			<option value="iframe">Iframe</option>
		</select>
	</p>
	<div class="wrap-list">
		<label for="upload_lboxthumb_button">Thumbnail image:</label>
		<input id="lboxthumb-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="lboxthumb" value="" />
		<input id="upload_lboxthumb_button" type="button" class="small_button" value="Upload" />
		<div id="lboxthumb-preview" class="img-preview" <?php if(!weblusive_get_option('lboxthumb')) echo 'style="display:none;"' ?>>
			<img src="<?php if(weblusive_get_option('lboxthumb')) echo weblusive_get_option('lboxthumb'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" />
			<a class="del-img" title="Delete"></a>
		</div>
		<div class="clear"></div>
	</div>
	
	
	<p id="src-holder">
		<label for="src">Lightbox URL :</label>
		<input type="text" id="src" name="src" value=""/>
	</p>
	<p>
		<label for="caption">Lightbox Caption:</label>
		<textarea   id="caption" name="caption" style="width: 300px;height: 150px;"></textarea>
	</p>
	
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:lightbox.insert(lightbox.e)">Insert</a></div>
<!--/*************************************/ 

<!--/*************************************/ -->

<?php } elseif( $page == 'regimage' ){

?>
	<script type="text/javascript">
		var regimage = {
			e: '',
			init: function(e) {
				regimage.e = e;
				tinyMCEPopup.resizeToInnerSize();
				
			},
			insert: function createGalleryShortcode(e) {
				var image=jQuery('#regimage-img').val();
				var imgstyle=jQuery('#imgstyle').val();
				var imgtrans=jQuery('#imgtrans').val();
				var imgzoom=jQuery('#imgzoom').val();
				var bgover=jQuery('#bgover').val();
				var BgCustomColor=jQuery('#BgCustomColor').val();
				var alignment=jQuery('#alignment').val();
				var anim=jQuery('#anim').val();     
				var addclass=jQuery('#class').val();
				var alt=jQuery('#alt').val();
				var link=jQuery('#link').val();
                var ovcontent=jQuery('#ovcontent').val();                
				
				var output = '[regimage ';
					if (anim){
						output+= 'anim="'+anim+'" ';
					}
					if(addclass){
						output+='class="'+addclass+'" ';
					}
					if(image) {
						output += 'image="'+image+'" ';
					}
					if (imgstyle){
						output+= 'imgstyle="'+imgstyle+'" ';
					}
					if(imgtrans){
						output+='imgtrans="'+imgtrans+'" ';
					}
					if(imgzoom){
						output+='imgzoom="'+imgzoom+'" ';
					}
					if(bgover){
						output+='bgover="'+bgover+'" ';
					}
					if(BgCustomColor){
						output+='cusbgover="'+BgCustomColor+'" ';
					}
					if (alt){
						output+= 'alt="'+alt+'" ';
					}
					if (link){
						output+= 'link="'+link+'" ';
					}
					
					if(alignment) {
						output += 'alignment="'+alignment+'" ';
					}
					output += ' ]';
                    if(ovcontent){
						output+=ovcontent;
					}
				output += '[/regimage]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
			}
		}
		tinyMCEPopup.onInit.add(regimage.init, regimage);

	</script>
	<title>Add Image</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
		jQuery(function(){
		  jQuery("#animcontent").load("animation.html"); 
		});
    </script>
    <p id="animcontent"></p>
	<div class="wrap-list">
		<label for="upload_regimage_button">Image:</label>
		<input id="regimage-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="regimage-img" value="" />
		<input id="upload_regimage_button" type="button" class="small_button" value="Upload" />
		<div id="regimage-preview" class="img-preview" <?php if(!weblusive_get_option('regimage')) echo 'style="display:none;"' ?>>
			<img src="<?php if(weblusive_get_option('regimage')) echo weblusive_get_option('regimage'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" />
			<a class="del-img" title="Delete"></a>
		</div>
		<div class="clear"></div>
	</div>
	<p>
			<label for="imgstyle">Image style</label>
			<select  id="imgstyle" name="imgstyle">
				<option value="">None</option>
				<option value="img-rounded">Rounded</option>
				<option value="img-thumbnail">Thumbnail</option>
			</select>
		</p>
		<p>
			<label for="imgtrans">Image transparency</label>
			<select  id="imgtrans" name="imgtrans">
				<option value="">No</option>
				<option value="img-transparency">Yes</option>
			</select>
		</p>
		<p>
			<label for="imgzoom">Image Zoom</label>
			<select  id="imgzoom" name="imgzoom">
				<option value="">No</option>
				<option value="img-zoom">Yes</option>
			</select>
		</p>
		<p>
			<label for="bgover">Overlay color</label>
			<select  id="bgover" name="bgover">
				<option value="">None</option>
				<option value="bg-color-default">Default</option>
				<option value="bg-color-success">Success</option>
				<option value="bg-color-info">Info</option>
				<option value="bg-color-warning">Warning</option>
				<option value="bg-color-white">White</option>
				<option value="bg-overlay bg-overlay-gdark">Dark</option>
			</select>
		</p>
		<p>
		<label for="BgCustomColor">Overlay custom color :</label>
		<div id="BgCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="BgCustomColor" id="BgCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#BgCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#BgCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#BgCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
	<p>
		<label for="alignment">Alignment:</label>
		<select id="alignment" name="alignment">
			<option value="alignnone">None</option> 
			<option value="alignleft">Left</option>
			<option value="aligncenter">Center</option>
			<option value="alignright">Right</option>
		</select>
	</p>
	<p>
		<label for="alt">Alt text</label>
		<input id="alt" name="alt" type="text" value="" />
	</p>
	
	<p>
		<label for="link">Link URL (Optional)</label>
		<input id="link" name="link" type="text" value="" />
	</p>
	<p>
		<label for="ovcontent">Overlay Content</label>
		<textarea id="ovcontent" name="ovcontent" ></textarea>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
	
</form>
<div class="mce-foot"><a class="add" href="javascript:regimage.insert(regimage.e)">Insert</a></div>

<!--/*************************************/ 

<!--/*************************************/ -->
<?php } elseif( $page == 'fullbg' ){
?>
	<script type="text/javascript">
		var fullbg = {
			e: '',
			init: function(e) {
				fullbg.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var bgcolor=jQuery('#ProgressCustomColor').val();
				var bgimage=jQuery('#bgimage-img').val();
				var bgrepeat=jQuery('#bgrepeat').val();
				var custompadding=jQuery('#custompadding').val();
				var scrollspeed=jQuery('#scrollspeed').val();    
				var padding=jQuery('#fullbgpadding').val();  
				var type=jQuery('#fullbgtype').val();	
				var videourl=jQuery('#videourl').val(); 
				var autoplay=jQuery('#autoplay').val();
				var loop=jQuery('#loop').val();
				var mute=jQuery('#mute').val();
				var showcontrols=jQuery('#showcontrols').val();
				var quality=jQuery('#quality').val();
				var height=jQuery('#height').val();
				var addclass=jQuery('#class').val();
                                
				var output = '[fullbg';
				
				if(type){
					output+=' type="'+type+'"';
				}
				if(videourl && type == 'wvideo'){
					output+=' videourl="'+videourl+'"';
				}
				if(height && type == 'wvideo'){
					output+=' height="'+height+'"';
				}
				if(autoplay && type == 'wvideo'){
					output+=' autoplay="'+autoplay+'"';
				}
				if(loop && type == 'wvideo'){
					output+=' loop="'+loop+'"';
				}
				if(mute && type == 'wvideo'){
					output+=' mute="'+mute+'"';
				}
				if(showcontrols && type == 'wvideo'){
					output+=' showcontrols="'+showcontrols+'"';
				}
				if(quality && type == 'wvideo'){
					output+=' quality="'+quality+'"';
				}
				
				if(scrollspeed && type == 'parallax'){
					output+=' scrollspeed="'+scrollspeed+'"';
				}
				if(addclass){
					output+=' class="'+addclass+'"';
				}
				if(bgcolor) {
					output += ' bgcolor="'+bgcolor+'"';
				}
                if(bgimage) {
					output += ' bgimage="'+bgimage+'"';
				}
                if(bgrepeat) {
					output += ' bgrepeat="'+bgrepeat+'"';
				}
				if(padding) {
					output += ' padding="'+padding+'"';
				}
				if(custompadding) {
					output += ' custompadding="'+custompadding+'"';
				}
				var notopborder = jQuery('#notopborder:checked').val();
				if (notopborder === undefined) notopborder = '';
				var nobottomborder = jQuery('#nobottomborder:checked').val();
				if (nobottomborder === undefined) nobottomborder = '';
				
				if(notopborder) {
					output += ' notopborder="'+notopborder+'"';
				}
				if(nobottomborder) {
					output += ' nobottomborder="'+nobottomborder+'"';
				}
				
				output += '][/fullbg]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
			}
		}
		tinyMCEPopup.onInit.add(fullbg.init, fullbg);

	</script>
	<title>Add Full Width Background(parallax)</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
    jQuery(function(){
		jQuery("#scrollspeed-wrapper").hide();
		jQuery("#video-elements").hide();
		
		jQuery("#fullbgtype").change(function(){
			var selected = jQuery('#fullbgtype').val();
			if (selected == 'parallax'){
				jQuery("#scrollspeed-wrapper").show();
			}
			else{
				jQuery("#scrollspeed-wrapper").hide();
			}
			if (selected == 'wvideo'){
				jQuery("#video-elements").show();
			}
			else{
				jQuery("#video-elements").hide();
			}
			
		});
    });
    </script>
    
	<p>
		<label for="fullbgtype">Type:</label>
		<select id="fullbgtype" name="fullbgtype">
			<option value="">Regular</option>
			<option value="wvideo">Regular (With video)</option>
			<option value="parallax">Parallax</option>
		</select>
	</p>
	<div id="video-elements">
		<p>
			<label for="videourl">Youtube video URL:</label>
			<input type="text" id="videourl" name="videourl" value=""/>
		</p>
		<p>
			<label for="autoplay">Video autoplay:</label>
			<select id="autoplay" name="autoplay">
				<option value="true">Yes</option>
				<option value="false">No</option>
			</select>
		</p>
		<p>
			<label for="loop">Video loop:</label>
			<select id="loop" name="loop">
				<option value="true">Yes</option>
				<option value="false">No</option>
			</select>
		</p>
		<p>
			<label for="mute">Video Mute:</label>
			<select id="mute" name="mute">
				<option value="true">Yes</option>
				<option value="false">No</option>
			</select>
		</p>
		<p>
			<label for="showcontrols">Video show controls:</label>
			<select id="showcontrols" name="showcontrols">
				<option value="true">Yes</option>
				<option value="false">No</option>
			</select>
		</p>
		<p>
			<label for="height">Force fixed height:</label>
			<input type="text" id="height" name="height" value=""/>px
		</p>
		<p>
			<label for="quality">Video quality:</label>
			<select id="quality" name="quality">
				<option value="default">Default</option>
				<option value="small">Small</option>
				<option value="medium">Medium</option>
				<option value="large">Large</option>
				<option value="hd720">HD720</option>
				<option value="hd1080">HD1080</option>
				<option value="highres">High res</option>
			</select>
		</p>
	</div>
	<p id="scrollspeed-wrapper">
		<label for="scrollspeed">Scrolling speed:</label>
		<select id="scrollspeed" name="scrollspeed">
			<option value="0.1">0.1</option>
			<option value="0.2">0.2</option>
			<option value="0.3">0.3</option>
			<option value="0.4">0.4</option>
			<option value="0.5">0.5</option>
			<option value="0.6" selected="selected">0.6</option>
			<option value="0.7">0.7</option>
			<option value="0.8">0.8</option>
			<option value="0.9">0.9</option>
			<option value="1">1</option>
			<option value="2">2</option>
		</select>
	</p>
	<p>
		<label for="upload_thumbnail_button">Background image:</label>
		<input id="bgimage-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="bgimageimg" value="" />
		<input id="upload_bgimage_button" type="button" class="small_button" value="Upload" />
		<div id="bgimage-preview" class="img-preview" <?php if(!weblusive_get_option('bgimageimg')) echo 'style="display:none;"' ?>>
			<img src="<?php if(weblusive_get_option('bgimageimg')) echo weblusive_get_option('bgimageimg'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" />
			<a class="del-img" title="Delete"></a>
		</div>
		<div class="clear"></div>
	</p>
	<label for="ProgressCustomColorcolorSelector">Background color:</label>
	<div id="ProgressCustomColorcolorSelector" class="color-pic">
		<div></div>
	</div>
	<input style="width:80px; margin-right:5px;"  name="ProgressCustomColor" id="ProgressCustomColor" type="text" value="" />			
	<script>
		jQuery(document).ready(function() {
			jQuery('#ProgressCustomColorcolorSelector').ColorPicker({
				onShow: function (colpkr) {
					jQuery(colpkr).fadeIn(500);
					return false;
				},
				onHide: function (colpkr) {
					jQuery(colpkr).fadeOut(500);
					return false;
				},
				onChange: function (hsb, hex, rgb) {
					jQuery('#ProgressCustomColorcolorSelector div').css('backgroundColor', '#' + hex);
					jQuery('#ProgressCustomColor').val('#'+hex);
				}
			});
		});
	</script>
	<p>
		<label for="bgrepeat">Background repeat:</label>
		<select  id="bgrepeat" name="bgrepeat">
			<option value="repeat">Repeat</option>
			<option value="repeat-x">Repeat-X</option>
			<option value="repeat-y">Repeat-Y</option>
			<option value="no-repeat">No-repeat</option>
		</select>
	</p>
	<p>
		<label for="fullbgpadding">Padding:</label>
		<select id="fullbgpadding" name="fullbgpadding">
			<option value="">None</option>
			<option value="padding-xsmall">Very Small</option>
			<option value="padding-small">Small</option>
			<option value="padding-medium">Medium</option>
			<option value="padding-large">Large</option>
		</select>
	</p>
	<p>
		<label for="custompadding">Custom padding:</label>
		<input type="text" maxlength="3" style="width:50px" id="custompadding" name="custompadding" value=""/> px
	</p>
	<p>
		<label>Borders :</label>
		<input type="checkbox" name="notopborder" id="notopborder" value="notopborder" /><label class="inner-label" for="notopborder">No top border</label>
		<input type="checkbox" name="nobottomborder" id="nobottomborder" value="nobottomborder" /><label class="inner-label" for="nobottomborder">No bottom border</label>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:fullbg.insert(fullbg.e)">Insert</a></div>
<!--/*************************************/ -->

<!--/*************************************/ -->
<?php } elseif( $page == 'smicon' ){
?>
	<script type="text/javascript">
		var smicon = {
			e: '',
			init: function(e) {
				smicon.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var smiconFamily = jQuery('#smiconFamily').val();
				var smiconIcon = jQuery('#smiconIcon').val();
				var position=jQuery('#position').val();
				var smiconType = jQuery('#smiconType').val();
				var smiconColor = jQuery('#smiconColor').val();
				var smiconLink = jQuery('#smiconLink').val();
				var customcolor = jQuery('#IconCustomColor').val();
				var smiconSize = jQuery('#smiconSize').val();
				var bgcolor = jQuery('#bgcolor').val();
				var bgcustomcolor = jQuery('#iconcustombg').val();
				
				var anim=jQuery('#anim').val();     
				var addclass=jQuery('#class').val();
               
				var output = '[smicon ';
					if (anim){
						output+= 'anim="'+anim+'" ';
					}   
					if (smiconFamily){
						output+= 'family="'+smiconFamily+'" ';
					}   
					if(addclass){
						output+='class="'+addclass+'" ';
					}
					if(smiconIcon) {
						output += 'icon="'+smiconIcon+'" ';
					}
					if(position){
						output += 'position="'+position+'" ';
					}
					if(smiconLink) {
						output += 'link="'+smiconLink+'" ';
					}
					
					if(smiconType) {
						output += 'type="'+smiconType+'" ';
					}
                    if(smiconColor && !customcolor) {
						output += 'color="'+smiconColor+'" ';
					}
					if(customcolor) {
						output += 'customcolor="'+customcolor+'" ';
					}
					if(smiconSize) {
						output += 'size="'+smiconSize+'" ';
					}
					if(bgcolor && !bgcustomcolor) {
						output += 'bgcolor="'+bgcolor+'" ';
					}
					if(bgcustomcolor) {
						output += 'custombgcolor="'+bgcustomcolor+'" ';
					}
					
				output += '/]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
			}
		}
		tinyMCEPopup.onInit.add(smicon.init, smicon);
	</script>
	<title>Add Icon</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
		jQuery(function(){
		  jQuery("#animcontent").load("animation.html"); 
		});
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="smiconFamily">Icon Family</label>
		<select id="smiconFamily" name="smiconFamily">
			<option value="fontawe">Font Awesome</option>
			<option value="dart">dart</option>
		</select>
	</p>
	<p>
		<label for="smiconIcon">Icon :</label>
		<input id="smiconIcon" name="smiconIcon" type="text" value=""/>
		<small><a href="<?php echo $fonturl ?>" target="blank">Icons list</a></small>
	</p>
	<p>
		<label for="position">Icon position :</label>
		<select id="position" name="position">
			<option value="">None</option>
			<option value="iconleft">Left</option>	
			<option value="iconright">Right</option>	
		</select>
	</p>
	<p>
		<label for="smiconType">Icon Wrapper</label>
		<select id="smiconType" name="smiconType">
			<option value="nowrapper">None</option>
			<option value="circle">Full Round</option>
			<option value="circle-border">Border Round</option>
			<option value="square">Full Radius</option>
			<option value="square-border">Border Radius</option>
		</select>
	</p>
	<p>
		<label for="smiconLink">Link (optional) :</label>
		<input id="smiconLink" name="smiconLink" type="text" value=""/>
	</p>
	
	<p>
		<label for="smiconColor">Color :</label>
		<select id="smiconColor" name="smiconColor">
			<option value="">None</option>
			<option value="color-default">Default</option>
			<option value="color-info">Info</option>
			<option value="color-success">Success</option>
			<option value="color-danger">Danger</option>
			<option value="color-warning">Warning</option>
			<option value="color-white">White</option>
			<option value="color-dark">Dark</option>
		</select>
	</p>
	<p>
		<label for="IconCustomColor">Icon Custom color :</label>
		<div id="IconCustomColorcolorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="IconCustomColor" id="IconCustomColor" type="text" value="" />			
		<script>
			jQuery(document).ready(function() {
				jQuery('#IconCustomColorcolorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#IconCustomColorcolorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#IconCustomColor').val('#'+hex);
					}
				});
			});
		</script>
	</p>
	<p>
		<label for="smiconSize">Size :</label>
		<select id="smiconSize" name="smiconSize">
			<option value="">Small</option>
			<option value="fa-2x">2X</option>	
			<option value="fa-3x">3X</option>	
			<option value="fa-4x">4X</option>	
			<option value="fa-5x">5X</option>	
		</select>
	</p>
	
	<p>
		<label for="bgcolor">Icon background color:</label>
		<select id="bgcolor" name="bgcolor">
			<option value="">None</option>
			<option value="bg-color-default">Default</option>
			<option value="bg-color-info">Info</option>
			<option value="bg-color-success">Success</option>
			<option value="bg-color-warning">Warning</option>
			<option value="bg-color-danger">Danger</option>
			<option value="bg-color-white">White</option>
			<option value="bg-color-dark">Dark</option>
		</select>
	</p>
	<p>
		<label for="iconcustombg">Icon Custom Background :</label>
		<div id="iconcustombgcolorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="iconcustombg" id="iconcustombg" type="text" value="" />			
		<script>
			jQuery(document).ready(function() {
				jQuery('#iconcustombgcolorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#iconcustombgcolorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#iconcustombg').val('#'+hex);
					}
				});
			});
		</script>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:smicon.insert(smicon.e)">Insert</a></div>
<!--/*************************************/ --> 
<?php } elseif( $page == 'googlemap' ){

?>
	<script type="text/javascript">
		var googlemap = {
			e: '',
			init: function(e) {
				googlemap.e = e;
				tinyMCEPopup.resizeToInnerSize();
				
			},
			insert: function createGalleryShortcode(e) {
				var anim=jQuery('#anim').val();   
				var lat=jQuery('#lat').val();
				var lon=jQuery('#lon').val();
				var address=jQuery('#address').val();
				var zoom=jQuery('#zoom').val();
				var width=jQuery('#width').val();
				var height=jQuery('#height').val();
				var maptype=jQuery('#maptype').val();
				var marker=jQuery('#marker').val();
				var markerimage=jQuery('#markerimage-img').val();
				var infowindow=jQuery('#infowindow').val();
				var traffic=jQuery('#traffic').val();
				var addclass=jQuery('#class').val();
				
				var output = '[map';
					if (anim){
						output+= ' anim="'+anim+'"';
					}
					if (lat){
						output+= ' lat="'+lat+'"';
					}
					if (address){
						output+= ' address="'+address+'"';
					}
					if (zoom){
						output+= ' z="'+zoom+'"';
					}
					if (width){
						output+= ' w="'+width+'"';
					}
					if (height){
						output+= ' h="'+height+'"';
					}
					if (maptype){
						output+= ' maptype="'+maptype+'"';
					}
					if (marker){
						output+= ' marker="'+marker+'"';
					}
					if (markerimage){
						output+= ' markerimage="'+markerimage+'"';
					}
					if (infowindow){
						output+= ' infowindow="'+infowindow+'"';
					}
					if (traffic){
						output+= ' traffic="'+traffic+'"';
					}
					if (addclass){
						output+= ' class="'+addclass+'"';
					}
					
				output += '][/map]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
			}
		}
		tinyMCEPopup.onInit.add(googlemap.init, googlemap);

	</script>
	<title>Add Google Map</title>

</head>
<body>
<form id="GalleryShortcode">
    <script> 
		jQuery(function(){
		  jQuery("#animcontent").load("animation.html"); 
		  /*tinyMCE.init({
				skin : "modern",
				mode : "exact",
				elements : "thumbcontent",
				theme: "advanced"
			});*/
		});
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="lat">Latitude</label>
		<input id="lat" name="lat" type="text" value="" />
	</p>
	<p>
		<label for="lon">Longitude</label>
		<input id="lon" name="lon" type="text" value="" />
	</p>
	<p>
		<label for="address">Address</label>
		<input id="address" name="address" type="text" value="" />
	</p>
	<p>
		<label for="zoom">Zoom:</label>
		<select id="zoom" name="zoom">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
			<option value="13">13</option>
			<option value="14" selected="selected">14</option>
			<option value="15">15</option>
			<option value="16">16</option>
			<option value="17">17</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
		</select>
	</p>
	<p>
		<label for="width">Width (in pixels)</label>
		<input id="width" name="width" type="text" value="" />
	</p>
	<p>
		<label for="height">Height (in pixels)</label>
		<input id="height" name="height" type="text" value="" />
	</p>
	<p>
		<label for="maptype">Map Type:</label>
		<select id="maptype" name="maptype">
			<option value="ROADMAP">Road</option>
			<option value="SATELLITE">Satellite</option>
			<option value="HYBRID">Hybrid</option>
			<option value="TERRAIN">Terrain</option>
		</select>
	</p>
	<p>
		<label for="marker">Marker(set address)</label>
		<input id="marker" name="marker" type="text" value="" />
	</p>
	<p>
		<label for="upload_markerimage_button">Marker image:</label>
		<input id="markerimage-img" class="img-path" type="text" size="56" style="direction:ltr; text-align:left" name="markerimage" value="" />
		<input id="upload_markerimage_button" type="button" class="small_button" value="Upload" />
		<div id="markerimage-preview" class="img-preview" <?php if(!weblusive_get_option('markerimage')) echo 'style="display:none;"' ?>>
			<img src="<?php if(weblusive_get_option('markerimage')) echo weblusive_get_option('markerimage'); else echo get_template_directory_uri().'/library/admin-panel/images/spacer.png'; ?>" alt="" />
			<a class="del-img" title="Delete"></a>
		</div>
		<div class="clear"></div>
	</p>
	<p>
		<label for="infowindow">Information window:</label>
		<textarea id="infowindow" name="infowindow" style="height:50px;"></textarea>
	</p>
	<p>
		<label for="traffic">Show Traffic:</label>
		<select id="traffic" name="traffic">
			<option value="no">No</option> 
			<option value="yes">Yes</option>
		</select>
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:googlemap.insert(googlemap.e)">Insert</a></div>
<!--/*************************************/ 

<!--/*************************************/ -->

<?php } elseif($page=='sblock') {?>
    <script type="text/javascript">
        var sblock={
            e: '',
            init: function(e){
                sblock.e=e,
                tinyMCEPopup.resizeToInnerSize();
            },
            insert: function createGalleryShortcode(e){
				var Type=jQuery('#type').val();
                var Title=jQuery('#title').val();
				var TitleCustomColor=jQuery('#titleCustomColor').val();
                var Icon=jQuery('#icon').val();
				var iconCustomColor=jQuery('#iconCustomColor').val();
                var content=jQuery('#content').val();
                var Link=jQuery('#link').val();
                var anim=jQuery('#anim').val();
                var addclass=jQuery('#class').val();
		
                var output='[sblock';
                if (anim){
                    output+= ' anim="'+anim+'"';
                }
                if(addclass){
                    output+=' class="'+addclass+'"';
                }
				if(Type){
                    output+=' type="'+Type+'"';
                }
                if(Title){
                    output+=' title="'+Title+'"';
                }
				if(TitleCustomColor){
                    output+=' tcuscolor="'+TitleCustomColor+'"';
                }
                if(Icon){
                    output+=' icon="'+Icon+'"';
                }
               
				if(iconCustomColor){
                    output+=' icuscolor="'+iconCustomColor+'"';
                }
				
                if(Link){
                    output+=' link="'+Link+'"';
                }
                output+=']'+content+'[/sblock]';
                tinyMCEPopup.execCommand('mceReplaceContent', false, output);
		tinyMCEPopup.close();
            }
        }
        tinyMCEPopup.onInit.add(sblock.init, sblock);
    </script>
    <title>Insert Service Block</title>
</head>
<body>
    <form id="GalleryShortcode">
         <script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
	  jQuery("#type").change(function(){
			var selected = jQuery('#type').val();
			if (selected == 'alter'){
				jQuery(".sblocklink").hide();
			}else{
				jQuery(".sblocklink").show(); 
			}
		});
    });
    </script>
    <p id="animcontent"></p>
	<p>
		<label for="type">Block Type:</label>
		<select id="type" name="type">
			<option value="default">Default</option>
			<option value="alter">Alternative</option>
		</select>
	</p>
	<p>
		<label for="title">Block Title:</label>
		<input type="text" id="title">
	</p>
	 <p>
		<label for="titleCustomColor">Title custom color :</label>
		<div id="titleCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="titleCustomColor" id="titleCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#titleCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#titleCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#titleCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
	 <p>
		<label for="icon">Block Icon:</label>
		<input type="text" id="icon">
		<small><a href="<?php echo $fonturl ?>" target="blank">Icons list</a></small>
	</p>
	<div id="iconwrap">
	<p>
		<label for="iconCustomColor">Icon custom color :</label>
		<div id="iconCustomColorSelector" class="color-pic">
			<div></div>
		</div>
		<input style="width:80px; margin-right:5px;"  name="iconCustomColor" id="iconCustomColor" type="text" value="" />
							
			<script>
			jQuery(document).ready(function() {
				jQuery('#iconCustomColorSelector').ColorPicker({
					onShow: function (colpkr) {
						jQuery(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						jQuery(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						jQuery('#iconCustomColorSelector div').css('backgroundColor', '#' + hex);
						jQuery('#iconCustomColor').val('#'+hex);
					}
				});
			});
				</script>
		
	</p>
	</div>
	<p class="sblocklink">
		<label for="link">Block Link:</label>
		<input type="text" id="link">
	</p>
	<p>
		<label for="content">Block Content:</label>
		<textarea id="content" style="width:200px; height:50px"></textarea>
		
	</p>
	
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
		
	</p>
</form>
<div class="mce-foot"><a class="add" href="javascript:sblock.insert(sblock.e)">Insert</a></div>
<!--/*************************************/ -->
<!--/*************************************/ -->
<?php } elseif( $page == 'pricingtable' ){ ?>

	<script type="text/javascript">
		var PricingTable = {
			e: '',
			init: function(e) {
				PricingTable.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {
				var type = jQuery('#type').val();
				var title = jQuery('#title').val();
				var currency = jQuery('#currency').val();
				var price = jQuery('#price').val();
				var period = jQuery('#period').val();
				var fbutton = jQuery('#fbutton').val();
				var fbutlink=jQuery('#fbutlink').val();
				var anim=jQuery('#anim').val();
                var addclass=jQuery('#class').val();
				

				var output = "[av_pricing";
				if (anim){
                   output+= ' anim="'+anim+'" ';
                }
                if(addclass){
                   output+=' class="'+addclass+'" ';
                }
				if(type) {
					output+= ' type="'+type+'"';
				}
				if(title) {
					output+= ' title="'+title+'"';
				}				
				if(currency) {
					output+= ' currency="'+currency+'"';
				}
				if(price) {
					output+= ' price="'+price+'"';
				}
				if(period) {
					output+= ' period="'+period+'"';
				}
				
				if(fbutton) {
					output+= ' fbutton="'+fbutton+'"';
				}
				if(fbutlink) {
					output+= ' fbutlink="'+fbutlink+'"';
				}
				
								
				output += "]";
				
				jQuery("textarea[id^=ptcontent]").each(function(intIndex, objValue) {
					//var obj = jQuery(this).get(intIndex);
					output += "[av_column]" + jQuery(this).val()+"[/av_column]";
				});
				
				
				output += '[/av_pricing]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(PricingTable.init, PricingTable);

		jQuery(document).ready(function() {
			jQuery("#add-tablefield").click(function() {
				jQuery('#PricingTableShortcodeContent').append('<p><label for="ptcontent[]">Column content</label><textarea id="ptcontent[]" name="ptcontent[]" ></textarea></p>');
			});
		});
		
	</script>
	<title>Add Pricing table</title>

</head>
<body>
<form id="PricingTableShortcode">
<div id="PricingTableShortcodeContent">
	<script> 
    jQuery(function(){
      jQuery("#animcontent").load("animation.html"); 
    });
    </script>
	<p id="animcontent"></p>
	<p>
		<label for="type">Type</label>
		<select  id="type" name="type">
			<option value="">Default</option>
			<option value="featured">Featured</option>
		</select>
	</p>
	<p>
		<label for="title"> Title:</label>
		<input type="text" id="title">
	</p>
	<p>
		<label for="currency">Currency</label>
		<input id="currency" name="currency" type="text" value="" />
	</p>
	<p>
		<label for="price">Price</label>
		<input id="price" name="price" type="text" value="" />
	</p>
	<p>
		<label for="period">Period</label>
		<input id="period" name="period" type="text" value="" />
	</p>
	<p>
		<label for="fbutton">Submit caption</label>
		<input id="fbutton" name="fbutton" type="text" value="" />
	</p>
	<p>
		<label for="fbutlink">Submit URL</label>
		<input id="fbutlink" name="fbutlink" type="text" value="" />
	</p>
	<p>
		<label for="class">Extra Class</label>
		<input id="class" name="class" type="text" value="" />
		
	</p>
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
	
	<p>
		<label for="ptcontent[]">Column content</label>
		<textarea id="ptcontent[]" name="ptcontent[]" ></textarea>
	</p>
	
	<hr style="border-bottom: 1px solid #FFF;border-top: 1px solid #ccc; border-left:0; border-right:0;" />
</div>
	<strong><a style="cursor: pointer;" id="add-tablefield">+ Add another column</a></strong>
	<p><a class="add" href="javascript:PricingTable.insert(PricingTable.e)">insert into post</a></p>
</form>
<!--/*****************************************************/-->
<?php } ?>

</body>
</html>