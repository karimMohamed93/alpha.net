<?php

/********* Custom menu ************/
add_action( 'after_setup_theme', 'bootstrap_setup' );
 
if ( ! function_exists( 'bootstrap_setup' ) ):
 
	function bootstrap_setup(){
 
		class dart_onepage_Walker extends Walker_Nav_Menu {
 
			
			public function start_lvl( &$output, $depth = 0, $args = array()) {
 
				$indent = str_repeat( "\t", $depth );
				$output	   .= "\n$indent<ul class=\"dropdown-menu\">\n";
				
			}
 
			public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
				if (isset($args->has_children))
				{
					$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
					
					if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
                        $output .= $indent . '<li role="presentation" class="divider">';
					} else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
							$output .= $indent . '<li role="presentation" class="divider">';
					} else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
							$output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
					} else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
							$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
					} else {
						$li_attributes = '';
						$class_names = $value = '';
						$classes = empty( $item->classes ) ? array() : (array) $item->classes;
						$classes[] = 'menu-item-' . $item->ID;
						$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

                        if ( $args->has_children  && $depth < 1)
                                $class_names .= ' dropdown';
						
						if ( $args->has_children && $depth >=1)
                                $class_names .= ' dropdown-submenu';

                        if ( in_array( 'current-menu-item', $classes ) )
                                $class_names .= ' current active';

                        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

                        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
                        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
		 
						
						$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
						
						$custom = get_post_custom($item->object_id);
						$pageType = isset ($custom['_weblusive_page_type']) ? @$custom['_weblusive_page_type'][0] : '1';
						$link =  $item->url; 
					
						if ($pageType == '1' || $pageType=='')
						{
							$link =  $item->url;
						}
						elseif ($pageType == '2')
						{
							$link = get_site_url().'/#'.slugify($item->title);
							//$link = '#'.slugify($item->title);
							//$link = @$custom['_custom_url'][0];
						}
						else
						{
							$link =  $item->url;
						}
		 
						$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
						$desc  = ! empty( $item->description  ) ? ' <i class="fa '.$item->description .'"></i>' : '';
						$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
						$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
						$attributes .= ! empty( $item->url )        ? ' href="'.$link.'" ' : '';
						$attributes .= ($args->has_children && $depth == 0) ? 'class="dropdown-toggle" data-toggle="dropdown" ' : 'class="page-scroll"';
		  
						$item_output = $args->before;
						$item_output .= '<a'. $attributes .' data-target="#'.slugify($item->title).'">'.$desc;
						$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
						//$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
						$item_output .= ($args->has_children && $depth == 0) ? '<i class="fa fa-caret-down"></i></a>' : '</a>';
						$item_output .= $args->after;
		 
						$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
					}
				}
			}
 
			function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
				
				if ( !$element )
					return;
				
				$id_field = $this->db_fields['id'];
 
				//display this element
				if ( is_array( $args[0] ) ) 
					$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
				else if ( is_object( $args[0] ) ) 
					$args[0]->has_children = ! empty( $children_elements[$element->$id_field] ); 
				$cb_args = array_merge( array(&$output, $element, $depth), $args);
				call_user_func_array(array(&$this, 'start_el'), $cb_args);
 
				$id = $element->$id_field;
 
				// descend only when the depth is right and there are childrens for this element
				if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {
 
					foreach( $children_elements[ $id ] as $child ){
 
						if ( !isset($newlevel) ) {
							$newlevel = true;
							//start the child delimiter
							$cb_args = array_merge( array(&$output, $depth), $args);
							call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
						}
						$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
					}
						unset( $children_elements[ $id ] );
				}
 
				if ( isset($newlevel) && $newlevel ){
					//end the child delimiter
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
				}
 
				//end this element
				$cb_args = array_merge( array(&$output, $element, $depth), $args);
				call_user_func_array(array(&$this, 'end_el'), $cb_args);
				
			}
			
		}
 
	}
 
endif;
class pages_from_nav extends Walker_Nav_Menu
{
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) 
	{
		global $wp_query;
		$item_output = $item->object_id;
		$item_output .= ',';
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

/********* STRING MANIPULATIONS ************/

function alc_trim($text, $length, $end = '[...]') {
	$text = preg_replace('`\[[^\]]*\]`', '', $text);
	$text = strip_tags($text);
	$text = substr($text, 0, $length);
	$text = substr($text, 0, last_pos($text, " "));
	$text = $text . $end;
	return $text;
}

function last_pos($string, $needle){
   $len=strlen($string);
   for ($i=$len-1; $i>-1;$i--){
       if (substr($string, $i, 1)==$needle) return ($i);
   }
   return FALSE;
}

function limit_words($string, $word_limit, $chars = 0) {
 
	// creates an array of words from $string (this will be our excerpt)
	// explode divides the excerpt up by using a space character
	$words = '';
	
	if ($chars == 1){
		$words = (strlen($string) > $word_limit) ? substr($string,0, $word_limit).'...' : $string;
		return $words;
	}
	else{
		$words = explode(' ', $string);
		return implode(' ', array_slice($words, 0, $word_limit)).'...';
	}
}

/********** GET PAGES BY PARAMS ************/

/*-- Get root parent of a page --*/
function get_root_page($page_id) 
{
	global $wpdb;
	
	$parent = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE post_type='page' AND ID = '$page_id'");
	
	if ($parent == 0) 
		return $page_id;
	else 
		return get_root_page($parent);
}


/*-- Get page name by ID --*/
function get_page_name_by_ID($page_id)
{
	global $wpdb;
	$page_name = $wpdb->get_var("SELECT post_title FROM $wpdb->posts WHERE ID = '$page_id'");
	return $page_name;
}


/*-- Get page ID by Page Template --*/
function get_page_ID_by_page_template($template_name)
{
	global $wpdb;
	$page_ID = $wpdb->get_var("SELECT post_id FROM $wpdb->postmeta WHERE meta_value = '$template_name' AND meta_key = '_wp_page_template'");
	return $page_ID;
}

/*-- Get page content (Used for pages with custom post types) --*/
if(!function_exists('getPageContent'))
{
	function getPageContent($pageId)
	{
		if(!is_numeric($pageId))
		{
			return;
		}
		global $wpdb;
		$sql_query = 'SELECT DISTINCT * FROM ' . $wpdb->posts .
		' WHERE ' . $wpdb->posts . '.ID=' . $pageId;
		$posts = $wpdb->get_results($sql_query);
		if(!empty($posts))
		{
			foreach($posts as $post)
			{
				return nl2br($post->post_content);
			}
		}
	}
}

function youtube_id_from_url($url) {
    $pattern = 
        '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x'
        ;
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}


/* -- Get page ID by Custom Field Value -- */
function get_page_ID_by_custom_field_value($custom_field, $value)
{
	global $wpdb;
	$page_ID = $wpdb->get_var("
	    SELECT wposts.ID
    	FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
	    WHERE wposts.ID = wpostmeta.post_id 
    	AND wpostmeta.meta_key = '$custom_field' 
	    AND (wpostmeta.meta_value like '$value,%' OR wpostmeta.meta_value like '%,$value,%' OR wpostmeta.meta_value like '%,$value' OR wpostmeta.meta_value = '$value')		
    	AND wposts.post_status = 'publish' 
	    AND wposts.post_type = 'page'
		LIMIT 0, 1");

	return $page_ID;
}
/*******************************************/

/******* POSTS RELATED BY TAXONOMY *********/

function get_taxonomy_related_posts($post_id, $taxonomy, $limit, $args=array()) {
  $query = new WP_Query();
  $terms = wp_get_object_terms($post_id, $taxonomy);
  if (count($terms)) {
    $post_ids = get_objects_in_term($terms[0]->term_id,$taxonomy);
    $post = get_post($post_id);
    $args = wp_parse_args($args,array(
      'post_type' => $post->post_type, 
      'post__in' => $post_ids,
	  'exclude' => $post_id,
      'taxonomy' => $taxonomy,
      'term' => $terms[0]->slug,
	  'posts_per_page' => $limit
    ));
    $query = new WP_Query($args);
  }
  return $query;
}

/********************************************/

function dart_highlight_results($text){
     if(is_search()){
		 $sr = get_query_var('s');
		 $length = strlen ($sr);
		 if ($length > 3){
			$keys = explode(" ",$sr);
			$text = preg_replace('/('.implode('|', $keys) .')/iu', '<span class="selected">'.$sr.'</span>', $text);
		 }
     }
     return $text;
}
add_filter('the_excerpt', 'dart_highlight_results');

/************** LIST TAXONOMY ***************/

function list_taxonomy($taxonomy, $id='')
{
	$args = array ('hide_empty' => false);
	$tax_terms = get_terms($taxonomy, $args); 
	$active = '';
	$output = '<ul id="'.$id.'">';

	foreach ($tax_terms as $tax_term) {
		if ($taxonomy  == $tax_term)
		{
			$active  = ' class="active"';
		}
		$output.='<li><a href="'.esc_attr(get_term_link($tax_term, $taxonomy)) . '"'.$active.'>'.$tax_term->name.'</a></li>';
	}
	$output.='</ul>';
	
	return $output;
}

/********* HEX TO RGB CONVERSION ************/
function HexToRGB($hex, $findclosest = 0, $valueRatio = 0.8) {
	$hex = str_replace("#", "", $hex);
	$color = array();
	
	if(strlen($hex) == 3) {
		$color['r'] = hexdec(substr($hex, 0, 1) . $r);
		$color['g'] = hexdec(substr($hex, 1, 1) . $g);
		$color['b'] = hexdec(substr($hex, 2, 1) . $b);
	}
	else if(strlen($hex) == 6) {
		$color['r'] = hexdec(substr($hex, 0, 2));
		$color['g'] = hexdec(substr($hex, 2, 2));
		$color['b'] = hexdec(substr($hex, 4, 2));
	}
	
	if ($findclosest == 1){
		$color['r'] = (int)($color['r'] * $valueRatio);
		$color['g'] = (int)($color['g'] * $valueRatio);
		$color['b'] = (int)($color['b'] * $valueRatio);
	}
	
	return $color;
}

/******** CLOSEST COLOR MATCH FUNCTION ******/

	
/********************************************/

/************* COMMENTS HOOK *************/

function dart_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; 
	
	?>
    <li >
		<div class="comment">
		<?php echo get_avatar($comment, 64); ?>
		<div class="comment-body" id="comment-<?php comment_ID(); ?>">            
            <?php if ($comment->comment_approved == '0') : ?><p><em><?php esc_html_e('Your comment is awaiting moderation.', 'dart') ?></em></p><?php endif; ?>
			<h4 class="comment-author"><?php echo get_comment_author()?><?php edit_comment_link(esc_html__('(Edit)', 'dart'),'  ','') ?></h4>		
			<div class="comment-date"><?php printf(esc_html__('%1$s at %2$s', 'dart'), get_comment_date(),get_comment_time()) ?></div>
				<?php comment_text() ?>
            <div class="text-right weight-600"><?php comment_reply_link(array_merge($args, array('depth' => $depth, 'reply_text' => '<i class="fa fa-mail-reply"></i>'.esc_html__('Reply', 'dart'), 'style'=>'<ul class="com_child"', 'max_depth' => $args['max_depth']))) ?></div>
        </div>
		</div>
<?php }

add_filter('comment_reply_link', 'replace_reply_link_class');

function replace_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='comment-reply", $class);
    return $class;
}
add_filter('get_avatar','add_gravatar_class');

function add_gravatar_class($class) {
    $class = str_replace("class='avatar", "class='comment-avatar pull-left", $class);
    return $class;
}
/*****************************************/

function load_template_part($template_name, $part_name=null) {
    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}

function dart_mb_unserialize($string) {

	$string = preg_replace_callback(
		'!s:(\d+):"(.*?)";!s',
		function ($matches) {
			if ( isset( $matches[2] ) )
				return 's:'.strlen($matches[2]).':"'.$matches[2].'";';
		},
		$string
	);
	return unserialize($string);
}
?>