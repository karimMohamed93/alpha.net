<?php
function panel_options() { 

	$categories_obj = get_categories('hide_empty=0');
	$categories = array();
	foreach ($categories_obj as $pn_cat) {
		$categories[$pn_cat->cat_ID] = $pn_cat->cat_name;
	}
	
	$sliders = array();
	$custom_slider = new WP_Query( array( 'post_type' => 'weblusive_slider', 'posts_per_page' => -1 ) );
	while ( $custom_slider->have_posts() ) {
		$custom_slider->the_post();
		$sliders[get_the_ID()] = get_the_title();
	}
	
	
$save='
	<div class="mpanel-submit">
		<input type="hidden" name="action" value="test_theme_data_save" />
        <input type="hidden" name="security" value="'. wp_create_nonce("test-theme-data").'" />
		<input name="save" class="mpanel-save" type="submit" value="Save Changes" />    
	</div>'; 
?>

		
<div id="save-alert"></div>

<div class="admin-panel">
	<div class="top-nav">
		<div class="logo">
			<?php
			$customlogo = weblusive_get_option('admin_panel_logo');
			$logo = empty($customlogo) ? get_template_directory_uri().'/library/admin-panel/images/logo.png' : $customlogo;
			?>
			<img src="<?php echo esc_url($logo)?>" alt="" />
		</div>
		<div class="right-info">
			<?php global $docs_url;?>
			<a href="<?php echo esc_url($docs_url)?>" target="_blank" class="documentation-link">
				<i class="dashicons dashicons-book"></i><?php esc_html_e('Documentation', 'dart')?>
			</a>
		</div>
		<div class="clear"></div>
	</div>
	<div class="admin-panel-tabs">
		<ul>
			<li class="weblusive-tabs main-settings"><a href="#tab1"><span class="dashicons dashicons-admin-site"></span>Main Settings</a></li>
			<li class="weblusive-tabs header"><a href="#tab2"><span class="dashicons dashicons-editor-table"></span>Header</a></li>
			<li class="weblusive-tabs footer"><a href="#tab3"><span class="dashicons dashicons-align-center"></span>Footer</a></li>
			<li class="weblusive-tabs sidebars"><a href="#tab4"><span class="dashicons dashicons-align-left"></span>Sidebars</a></li>
			<li class="weblusive-tabs portfolio"><a href="#tab-portfolio"><span class="dashicons dashicons-portfolio"></span>Portfolio</a></li>
			<li class="weblusive-tabs blog"><a href="#tab8"><span class="dashicons dashicons-welcome-write-blog"></span>Blog settings</a></li>
			<li class="weblusive-tabs faq"><a href="#tab12"><span class="dashicons dashicons-lightbulb"></span>FAQ settings</a></li>
			<li class="weblusive-tabs woocommerce"><a href="#tab5"><span class="dashicons dashicons-cart"></span>Woocommerce</a></li>
			<li class="weblusive-tabs underconstruction"><a href="#tab9"><span class="dashicons dashicons-hammer"></span>Under construct.</a></li>
			<li class="weblusive-tabs styling"><a href="#tab6"><span class="dashicons dashicons-art"></span>Styling</a></li>
			<li class="weblusive-tabs advanced"><a href="#tab11"><span class="dashicons dashicons-admin-generic"></span>Advanced</a></li>
		</ul>
		<div class="clear"></div>
	</div> <!-- .admin-panel-tabs -->
	
	<div class="admin-panel-content">
	<form action="/" name="weblusive_form" id="weblusive_form">
		<div id="tab1" class="tabs-wrap">
			<h2>Main Settings</h2> <?php echo $save ?>
			<div class="weblusivepanel-item">
				<h3>Logo</h3>
				<?php
					weblusive_options(
						array( 	
						"name" => "Logo Setting",
						"id" => "logo_setting",
						"type" => "radio",
						"options" => array( "logo"=>"Custom Image Logo" ,"title"=>"Display Site Title" )
						)
					);
				
					weblusive_options(
						array(	"name" => "Custom Logo Image",
								"id" => "logo",
								"help" => "Upload an image or specify an existing URL. If left blank, the default website name will be applied.",
								"type" => "upload"));
					
					weblusive_options(
					array(	"name" => "Logo Margin top (in pixels)",
							"desc" => "",
							"id" => "logo_margin_top",
							"type" => "text"));
					
					weblusive_options(
					array(	"name" => "Logo Margin left (in pixels)",
							"desc" => "",
							"id" => "logo_margin_left",
							"type" => "text"));
				?>
			</div>
			<div class="weblusivepanel-item">
				<h3>Favicon</h3>
				<?php
					weblusive_options(
					array(	
						"name" => "Custom Favicon",
						"id" => "favicon",
						"type" => "upload")
					);
				?>
			</div>
			
			<div class="weblusivepanel-item">
				<h3>Layout type</h3>
				<?php weblusive_options(
					array(	
					"name" => "Boxed/Full",
					"id" => "layout_type",
					"type" => "select",
					"options" => array('fullwidth' => 'Full width', 'boxed' => 'Boxed')
					)
				);
				
				?>		
			</div>
			<div class="weblusivepanel-item">
				<h3>Color Scheme</h3>
				<?php weblusive_options(
					array(	
					"name" => "Light/Dark",
					"id" => "color_scheme",
					"type" => "select",
					"options" => array('light' => 'Light', 'dark' => 'Dark')
					)
				);
				?>		
			</div>
			<div class="weblusivepanel-item">
				<h3>RTL Mode</h3>
				<?php weblusive_options(
					array(	
						"name" => "Enable RTL",
						"id" => "rtl_mode",
						"help" => "Switch this on for Right to left direction languages like Arabic or Hebrew.",
						"type" => "checkbox")
					);
				?>		
			</div>
			<div class="weblusivepanel-item">
				<h3>Google Map Key</h3>
				<?php weblusive_options(
					array(
						"name" => "API Key",
						"id" => "google_map_key",
						"type" => "text"
					)
				);
				$getMap = esc_url('https://developers.google.com/maps/documentation/javascript/get-api-key#key');
				?>
				<div class="clear"></div>
				<div class="message installer-message warning gmap-warning">
					<p>The Google Maps API key is now a requirement if you are going to use Google maps
						anywhere in the website. To get one, head on to
						<a href="<?php echo $getMap?>" target="_blank">Google developer website</a>,
						click on "Get a Key" button and paste the key received into the field above.</p>
				</div>

			</div>
		</div>
		
		<div id="tab2" class="tabs-wrap">
			<h2>Header Settings</h2> <?php echo $save ?>
			<div class="weblusivepanel-item">
				<h3>Topbar Settings</h3>
				<?php
				weblusive_options(
					array(	
						"name" => "Enable topbar",
						"id" => "topbar_enable",
						"help" => "Set to display a bar on top of the header.",
						"type" => "checkbox")
					);
				weblusive_options(
					array(	
						"name" => "Enable topbar left sidebar ",
						"id" => "topbar_lsidebar_enable",
						"help" => "Will appear on left side of the topbar.",
						"type" => "checkbox")
					);
				weblusive_options(
					array(	
						"name" => "Enable topbar right sidebar ",
						"id" => "topbar_rsidebar_enable",
						"help" => "Will appear on right side of the topbar.",
						"type" => "checkbox")
					);
				?>		
			</div>
				
			<div class="weblusivepanel-item">
				<h3>Menu settings</h3>
				<?php
					weblusive_options(
						array(	
						"name" => "Disable search box",
						"id" => "disable_header_searchbox",
						"help" => "Select this option to hide the searchbox which appears on the main navigation block's right side.",
						"type" => "checkbox")
					);
					weblusive_options(
						array(	
						"name" => "Hide Woocommerce cart button",
						"id" => "disable_header_cart",
						"help" => "Select this option to hide the woccommerce cart button",
						"type" => "checkbox")
					);
					weblusive_options(
						array(	
						"name" => "Disable sticky menu",
						"id" => "disable_sticky_menu",
						"help" => "",
						"type" => "checkbox")
					);
					 
				?>
				
			</div>
			<div class="weblusivepanel-item">
				<h3>Title Settings</h3>
				<?php
					weblusive_options(
					array(	"name" => "Hide titles",
							"id" => "hide_titles",
							"type" => "checkbox")
						); 
							
					weblusive_options(
					array(	"name" => "Hide Breadcrumbs ",
							"id" => "hide_breadcrumbs",
							"type" => "checkbox")
						); 

					weblusive_options(
					array(	
						"name" => "Inner heading background image",
						"id" => "innerheading_background_image",
						"type" => "upload")
					);
				?>
			</div>
						
			<div class="weblusivepanel-item">
				<h3>Header Code</h3>
				<div class="option-item">
					<small>Paste any custom javascript code you would like to put in header in this field.</small>
					<textarea id="header_code" name="weblusive_options[header_code]" style="width:100%" rows="7"><?php echo htmlspecialchars_decode(weblusive_get_option('header_code'));  ?></textarea>				
				</div>
			</div>
		</div> 
		
		<div id="tab3" class="tabs-wrap">
			<h2>Footer Settings</h2> <?php echo $save ?>
			
			<div class="weblusivepanel-item">
				<h3>Footer Widgets</h3>
				<div class="option-item">
					<?php 
					weblusive_options(
						array( 	
						"name" => "Number of widgets",
						"id" => "footer_widgets",
						"type" => "select",
						"options" => array( "4"=>"4 widgets per column", "3"=>"3 widgets per column", "2"=>"2 widgets per column", "none"=>"Disable footer widgets" )
						)
					);
					?>
				</div>
			</div>

			<div class="weblusivepanel-item">
				<h3>Footer bottom options</h3>
				
				<div class="option-item">
					<?php 
						weblusive_options(
						array(	"name" => "Copyright text",
								"desc" => "",
								"id" => "footer_copyright",
								"type" => "textarea"));
						weblusive_options(
						array(	
							"name" => "Custom Top icon",
							"id" => "footer_gotop_icon",
							"help" => "Upload an image or specify an existing URL. Recommended size: 120x140 pixels.",
							"type" => "upload")
						);
					?>
				</div>
				<div class="option-item">
					<?php 
						weblusive_options(
						array(	"name" => "Hide 'Go To Top' Icon",
								"id" => "hide_footer_top",
								"type" => "checkbox"));
					?>
				</div>
				
			</div>
					
			<div class="weblusivepanel-item">
				<h3>Footer Code</h3>
				<div class="option-item">
					<small>Code added goes before the closing  &lt;/body&gt; tag. Useful for Google analytics code or any other script.</small>
					<textarea id="footer_code" name="weblusive_options[footer_code]" style="width:100%" rows="7"><?php echo htmlspecialchars_decode(weblusive_get_option('footer_code'));  ?></textarea>				
				</div>
			</div>	
		</div><!-- Footer Settings -->
		
		<div id="tab4" class="tab_content tabs-wrap">
			<h2>Sidebars</h2>	<?php echo $save ?>	
			
			<div class="weblusivepanel-item">
				<h3>Sidebar Position</h3>
				<div class="option-item">
					<?php
						$checked = 'checked="checked"';
						$weblusive_sidebar_pos = weblusive_get_option('sidebar_pos');
					?>
					<ul id="sidebar-position-options" class="weblusive-options">
						<li>
							<input id="_weblusive_sidebar_pos" name="weblusive_options[sidebar_pos]" type="radio" value="right" <?php if($weblusive_sidebar_pos == 'right' || !$weblusive_sidebar_pos ) echo $checked; ?> />
							<a class="checkbox-select" href="#"><img src="<?php echo get_template_directory_uri(); ?>/library/admin-panel/images/sidebar-right.png" /></a>
						</li>
						<li>
							<input id="_weblusive_sidebar_pos" name="weblusive_options[sidebar_pos]" type="radio" value="left" <?php if($weblusive_sidebar_pos == 'left') echo $checked; ?> />
							<a class="checkbox-select" href="#"><img src="<?php echo get_template_directory_uri(); ?>/library/admin-panel/images/sidebar-left.png" /></a>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="weblusivepanel-item">
				<h3>Add Sidebar</h3>
				<div class="option-item">
					<span class="label">Sidebar Name</span>
					
					<input id="sidebarName" type="text" size="56" style="direction:ltr; text-laign:left" name="sidebarName" value="" />
					<input id="sidebarAdd"  class="small_button" type="button" value="Add" />
					
					<ul id="sidebarsList">
					<?php $sidebars = weblusive_get_option( 'sidebars' ) ;
						if($sidebars){
							foreach ($sidebars as $sidebar) { ?>
						<li>
							<div class="widget-head"><?php echo $sidebar ?>  <input id="weblusive_sidebars" name="weblusive_options[sidebars][]" type="hidden" value="<?php echo $sidebar ?>" /><a class="del-sidebar"></a></div>
						</li>
							<?php }
						}
					?>
					</ul>
				</div>				
			</div>

			<div class="weblusivepanel-item" id="custom-sidebars">
				<h3>Custom Sidebars</h3>
				<?php
				
				$new_sidebars = array('primary-widget-area'=> 'Default (Primary Widget Area)', '' => 'None');
				if($sidebars){
					foreach ($sidebars as $sidebar) {
						$new_sidebars[$sidebar] = $sidebar;
					}
				}
							
				weblusive_options(				
					array(	"name" => "Blog Single Post Sidebar",
							"id" => "sidebar_post",
							"type" => "select",
							"options" => $new_sidebars ));
							
				weblusive_options(				
					array(	"name" => "Blog Archives Sidebar",
							"id" => "sidebar_archive",
							"type" => "select",
							"options" => $new_sidebars )); 
				
				weblusive_options(				
					array(	"name" => "Portfolio Single Page Sidebar",
							"id" => "sidebar_portfolio",
							"type" => "select",
							"options" => $new_sidebars ));
				weblusive_options(				
					array(	"name" => "Portfolio taxonomy Page Sidebar",
							"id" => "sidebar_portfolio_taxonomy",
							"type" => "select",
							"options" => $new_sidebars ));
				?>
			</div>
		</div> 
		
		 <!-- Woocommerce -->
        <div id="tab5" class="tab_content tabs-wrap">
            <h2>Woocommerce</h2>	<?php echo $save ?>
            <div class="weblusivepanel-item">
                <h3>Visual options</h3>
                <?php
                weblusive_options(
                    array(
                        "name" => "Products per page",
                        "id" => "products_per_page",
                        "type" => "text",
                        "help" => "The default pagination value",
                    ));
              
					weblusive_options(
                    array(
                        "name" => "Products per row",
                        "id" => "products_per_row",
                        "type" => "select",
                        "help" => "How many products to show in one row",
                        "options" => array("2" => '2', "3" => '3', "4" => "4",  "6" => "6", "1" => '1')
                    ));
					
					weblusive_options(
                    array(
                        "name" => "Product page layout",
                        "id" => "product_page_layout",
                        "type" => "select",
                        "help" => "Applies to single product page. If left or right sidebars are chosen, the sidebar to show has to be set separately by editing the 'Shop' page.",
                        "options" => array("full" => 'Full width', "left" => "Left sidebar",  "right" => "Right sidebar")
                    ));
					
					weblusive_options(
                    array(
                        "name" => "Hide related products",
                        "id" => "hide_related_products",
						"type" => "checkbox",
                        "help" => "Applies to product page"
                    ));
				  ?>
            </div>
			
        </div> <!-- End Woocommerce -->
		
		<!-- Styling -->
		<div id="tab6" class="tab_content tabs-wrap">
			<h2>Styling manager</h2>	<?php echo $save ?>	
						
						
			<div class="weblusivepanel-item">
				<h3>Custom CSS</h3>	
				<div class="option-item">
					<p><strong>Global CSS :</strong></p>
					<textarea id="weblusive_css" name="weblusive_options[css]" style="width:100%" rows="7"><?php echo weblusive_get_option('css');  ?></textarea>
				</div>	
				<div class="option-item">
					<p><strong>Portrait Tablets :</strong> 768px and above</p>
					<textarea id="weblusive_css" name="weblusive_options[css_tablets]" style="width:100%" rows="7"><?php echo weblusive_get_option('css_tablets');  ?></textarea>
				</div>
				<div class="option-item">
					<p><strong>Phones to tablets :</strong> 767px and below</p>
					<textarea id="weblusive_css" name="weblusive_options[css_wide_phones]" style="width:100%" rows="7"><?php echo weblusive_get_option('css_wide_phones');  ?></textarea>
				</div>
				<div class="option-item">
					<p><strong>Phones :</strong>480px and below</p>
					<textarea id="weblusive_css" name="weblusive_options[css_phones]" style="width:100%" rows="7"><?php echo weblusive_get_option('css_phones');  ?></textarea>
				</div>	
			</div>	

		</div> <!-- Styling -->

		<div id="tab7" class="tab_content tabs-wrap">
			<h2>Typography</h2>	<?php echo $save ?>	
			
			<div class="weblusivepanel-item">
				<h3>Character sets</h3>
				<p class="info">
					Loading additional character sets affects the performance. Use the additional character sets only if it's 
					required for the language you intend to build your website in. Example: If your website is in English, you 
					don't need any of those, if in Greek, Greek character set should be activated.
				</p>
				<?php
					weblusive_options(
						array(	"name" => "Latin Extended",
								"id" => "typography_latin_extended",
								"type" => "checkbox"));

					weblusive_options(
						array(	"name" => "Cyrillic",
								"id" => "typography_cyrillic",
								"type" => "checkbox"));

					weblusive_options(
						array(	"name" => "Cyrillic Extended",
								"id" => "typography_cyrillic_extended",
								"type" => "checkbox"));
								
					weblusive_options(
						array(	"name" => "Greek",
								"id" => "typography_greek",
								"type" => "checkbox"));
								
					weblusive_options(
						array(	"name" => "Greek Extended",
								"id" => "typography_greek_extended",
								"type" => "checkbox"));
				?>
			</div>
			
		</div> 
		<div id="tab-portfolio" class="tab_content tabs-wrap">
			<h2>Portfolio Settings</h2> <?php echo $save ?>
                        
			<div class="weblusivepanel-item">
				<h3>Single page</h3>
				<?php
								
					weblusive_options(
						array(	
							"name" => "Disable related posts",
							"id" => "portfolio_single_related",
							"type" => "checkbox",
							"help" => "Select if you don't want similar posts block to appear after the content part."
						)
					); 	
					
				?>
				<div class="related-options">
					<?php
					
					weblusive_options(
						array(	
							"name" => "Related posts title",
							"id" => "portfolio_related_title",
							"type" => "text"
						)
					); 	
					weblusive_options(
						array(	
							"name" => "Related posts limit",
							"id" => "portfolio_related_limit",
							"type" => "text",
							"help" => "If not set, will default to 5."
						)
					); 	
					?>
				</div>
			</div>
			<div class="weblusivepanel-item">
				<h3>Taxonomy (category) options</h3>
				<p class="info">Options in this block are effective only if Non-animated portfolio with pagination is selected.</p>
				<?php
					weblusive_options(
						array(	
							"name" => "Items per page",
							"id" => "portfolio_category_ppp",
							"type" => "text",
							"help" => "Applies only if Non-animated portfolio with pagination is selected via portfolio page."
						)
					); 
					weblusive_options(
						array(
							"name"=>"Number of columns",
							"id"=>"portfolio_tax_columns",
							"type"=>"select",
							"options" => array( "2"=>"2", "3"=>"3", "4"=>"4")
						)
					);
				
					weblusive_options(
						array(
							"name"=>"Layout Type",
							"id"=>"portfolio_tax_layout",
							"type"=>"select",
							"options" => array(	"1"=> "No title/excerpt/","2"=> "Show title + excerpt")
						)
					);
					
				?>
				
			</div>	
		</div> <!-- Portfolio Settings -->
		<div id="tab8" class="tab_content tabs-wrap">
			<h2>Blog Settings</h2> <?php echo $save ?>
            <div class="weblusivepanel-item">

				<h3>Meta information</h3>
				<?php
					weblusive_options(
						array(	"name" => "Hide author",
								"id" => "blog_show_author",
								"type" => "checkbox")
							); 
					weblusive_options(
						array(	"name" => "Hide date",
								"id" => "blog_show_date",
								"type" => "checkbox")
							); 
				
					weblusive_options(
						array(	"name" => "Hide comments number",
								"id" => "blog_show_comments",
								"type" => "checkbox")
							); 
					weblusive_options(
						array(	"name" => "Hide category",
								"id" => "blog_show_category",
								"type" => "checkbox")
							); 
				?>
			</div>
			
			<div class="weblusivepanel-item">

				<h3>Single page</h3>
				<?php
					weblusive_options(
						array(	"name" => "Disable author Bio block",
								"id" => "blog_disable_authorbio",
								"type" => "checkbox")
							);
					
					weblusive_options(
						array(	"name" => "Disable comments",
								"id" => "blog_disable_comments",
								"type" => "checkbox")
							); 
					
				?>
			</div>
			
			<div class="weblusivepanel-item">
				<h3>Custom Gravatar</h3>
				
				<?php
					weblusive_options(
						array(	"name" => "Custom Gravatar",
								"id" => "gravatar",
								"type" => "upload"));
				?>
			</div>	
			
		</div> <!-- Article Settings -->
		
		<div id="tab12" class="tab_content tabs-wrap">
			<h2>FAQ</h2> <?php echo $save ?>
          
			<div class="weblusivepanel-item">

				<h3>Contact Form</h3>
				<?php
					weblusive_options(
						array(	"name" => "Hide contact form",
								"id" => "faq_hide_contact",
								"type" => "checkbox")
							); 
					weblusive_options(
						array(	"name" => "Form title",
								"id" => "faq_contact_title",
								"type" => "text")
							); 
					
					weblusive_options(
					array(	"name" => "Email address",
							"id" => "faq_contact_address",
							"type" => "text",
							"hint" => "Set your email address here")
						); 
					
					weblusive_options(
					array(	"name" => "Email subject",
							"id" => "faq_contact_subject",
							"type" => "text")
						); 
					
					weblusive_options(
					array(	"name" => "Success message",
							"id" => "faq_contact_success",
							"type" => "text")
						); 
					weblusive_options(
					array(	"name" => "Error message",
							"id" => "faq_contact_error",
							"type" => "text")
						); 
					 weblusive_options(
						array(	"name" => "Form description",
								"id" => "faq_contact_text",
								"type" => "textarea")
							); 
				?>
			</div>
		</div>
		<!-- Under construction -->
		<div id="tab9" class="tab_content tabs-wrap">
			<h2>Under construction</h2>	<?php echo $save ?>	

			<div class="weblusivepanel-item">
				<h3>General settings</h3>
				<?php
					
					weblusive_options(
					array(	"name" => "Custom text",
							"id" => "uc_text",
							"type" => "textarea"));
				
					weblusive_options(
					array(	"name" => "Custom Background",
							"id" => "uc_custombg",
							"help" => "Goes under countdown block",
							"type" => "upload")
						);
					weblusive_options(
					array(	"name" => "Launching date (yyyy/mm/dd)",
							"id" => "uc_launchdate",
							"type" => "text",
							"help" => "Please insert the date in yyyy/mm/dd format otherwise the page won't work correctly. Example: 2015/07/24"));
					weblusive_options(		
						array(	"name" => "Countdown timer color",
							"id" => "uc_timercolor",
							"type" => "color",
							"help" => "If not set, white color will be applied." 
						));				
					weblusive_options(
						array(	"name" => "Enable newsletter block",
								"id" => "uc_enable_newsletter",
								"type" => "checkbox",
								"help" => "You must have 'Email newsletter' plugin installed to enable this feature.")
							); 
				?>
			</div>	
				
			
	
		</div> <!-- End under construction -->
		<div id="tab11" class="tab_content tabs-wrap">
			<h2>Advanced Settings</h2>	<?php echo $save ?>	

			<div class="weblusivepanel-item">
				<h3>Branding</h3>
				<?php
					weblusive_options(
						array(	"name" => "Worpress Login page Logo",
								"id" => "dashboard_logo",
								"type" => "upload"));
				?>
			
			</div>
			<?php
				global $array_options ;
				
				$current_options = array();
				foreach( $array_options as $option ){
					if( get_option( $option ) )
						$current_options[$option] =  get_option( $option ) ;
				}
			?>
			
			<div class="weblusivepanel-item">
				<h3>Export</h3>
				<p class="info">If you are importing previously saved content, make sure to delete the content in the "Export" field.</p>
				<div class="option-item">
					<textarea style="width:100%" rows="7"><?php echo $currentsettings = serialize( $current_options ); ?></textarea>
				</div>
			</div>
			<div class="weblusivepanel-item">
				<h3>Import</h3>
				<div class="option-item">
					<textarea id="weblusive_import" name="weblusive_import" style="width:100%" rows="7"></textarea>
				</div>
			</div>
			
		</div>
		
		<div class="mo-footer">
			<?php echo $save; ?>
		</div>
		</form>
		
	</div><!-- .admin-panel-content -->
	<div class="clear"></div>
</div><!-- .admin-panel -->
<?php
}
?>