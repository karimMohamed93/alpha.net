<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
//If the form is submitted
/*
add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );
function custom_wp_mail_from_name( $originalname ){
	return $originalname;
}
add_filter( 'wp_mail_from', 'custom_wp_mail_from' );

function custom_wp_mail_from( $originalemail )
{
//Make sure the email is from the same domain 
//as your website to avoid being marked as spam.
return $originalemail;
}
*/

function set_html_content_type() {
	return 'text/html';
}

if(isset($_POST['faqsubmit'])) {
	$name = sanitize_text_field($_POST['faqname']);
	$email = sanitize_email($_POST['faqemail']);
	$message = sanitize_text_field($_POST['faqmessage']);
	
	$subject  = weblusive_get_option('faq_contact_subject');
	$mailto = weblusive_get_option('faq_contact_address');
	$success = weblusive_get_option('faq_contact_success');
	
	//$options = explode('|', $_POST['appdetails']);
	//If there is no error, send the email
	if(!isset($hasError)) {
		if($name === '') {
			$nameError = 'Please enter your name.';
			$hasError = true;
		} 
		
		if($email === '')  {
			$mailError = 'You forgot to enter your email address.';
			$hasError = true;
		}
		//Check to make sure sure that a valid email address is submitted
		if($email === '')  {
			$mailError = 'You forgot to enter your email address.';
			$hasError = true;
		} else if (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)) {
            $mailError = 'You entered an invalid email address.';
            $hasError = true;
		}
		
	}
	if(!isset($hasError)) {
		
		$status = "";

		add_filter('wp_mail_content_type', 'set_html_content_type');
		if (!wp_mail( $mailto, $subject, $message )){
			$status =  '<span class="label label-danger">'.esc_html__('Submission error. Please make sure all fields are filled correctly.', 'dart').'</span>';
		}
		else
		{
			$status =  '<span class="label label-success">'.$success.'</span>';
		}
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
		echo $status;
		die();
	} 
	die;
} 
?>