( function( $ ) {
	
/******************global color****************************/
	wp.customize( 'global_color', function( value ) {
		value.bind( function(e, newval ) {
			
			$('a.navbar-brand,   ul.navbar-nav li.active > a, ul.navbar-nav > li.current-menu-parent>a,\
			.portfolio-overlay,.footer-bottom,#portfolio-carousel .caption, .btn.btn-primary,\
			.widget_categories ul li span.count, .footer-widget ul li.cat-item span.count,\
			 .bx-wrapper .bx-pager.bx-default-pager a.active, .widget-tab .nav-tabs>li.active>a,.tweet .tweet-time i,\
			.pagination>.active>a,   h4.panel-title a:not(.collapsed):after,.featured-tab .nav-tabs>li.active>a,\
			.tab-default .nav-tabs>li.active>a,.contact-list li i, ul.social-icons li,.team-image-wrapper-hover,.team-img-wrapper-hover,.service-content i,\
			.plan-price,.plan.featured .plan-name,.recent-post .blog-date, .recent-post .date:before,\
			 .bx-wrapper .bx-pager.bx-default-pager a.active, .heading:after, ul.navbar-nav li.nav-single,\
			.site-search, .eemail_button input[type="button"]').css('background-color', newval );
			
			$("ul.navbar-nav li a, .owl-controls .owl-prev, .owl-controls .owl-next,.headcarousel .flex-direction-nav a,\
			ul.wp-tag-cloud li a, .bx-wrapper .bx-pager.bx-default-pager a, .pagination>.active>a, .pagination>li>a, .product-links ul li a,\
			.dart-carousel-controller .left, .dart-carousel-controller .right, .bx-wrapper .bx-pager.bx-default-pager a, .product-links ul li a").hover(function(e) { 
				$(this).css("background-color", e.type === "mouseenter" ? newval : value);
			});
			
			$("ul.navbar-nav li a").focus(function(e) { 
				$(this).css("background-color",e.type === "mouseenter" ? newval: value);
			});
			
			$('#order_review_heading,\
			.woocommerce-pagination ul li span.current, .widget_product_search #searchsubmit,  .woocommerce .widget_price_filter .ui-slider-horizontal .ui-slider-range,\
			.woocommerce-page .widget_price_filter .ui-slider-horizontal .ui-slider-range, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,\
			.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle, .woocommerce .button').css('cssText', 'background:' + newval + '!important');
			
			$('.dropdown-menu>li.active>a, #isotope-filter a.active, .social-widget a, \
			 .blog-img-icon a,.widget_categories ul li:before, .footer-widget ul li.cat-item:before, .post-meta i,\
			.pagination li a:not(.active), a.comment-reply,h4.panel-title a:not(.collapsed),ul.divider li:before,ul.circle li:before,\
			.team-wrapper .team-content h3.name,.recent-post .post-body:after,\
			.service-box i, .feature-box .feature-icon i,.testimonial-slide .testimonial-content h3.name, .testimonial-slide .testimonial-text:before,\
			.testimonial-slide .testimonial-text:after, .rating i').css('color', newval);
		
			$(".dropdown-menu>li>a, #isotope-filter a, .portfolio-overlay-btn a, ul.divider li a,  a.read-more,\
				.widget_alc_works h4.entry-title a,.widget_alc_blogposts h4.entry-title a,.widget-tab h4.entry-title a,\
				.tweet a, ul.social-icons li a, .team-image-wrapper .social-icons a").hover(function(e) { 
				$(this).css("color", e.type === "mouseenter" ? newval : value);
			});
			$("a, .dropdown-menu>li>a").focus(function(e) { 
				$(this).css("color",e.type === "mouseenter" ? newval: value);
			});
			$('blockquote,.form-control:focus,.page-title:after,\
			.page-numbers > li > span.current, .page-numbers > li:hover a, .page-numbers > li:hover span,\
			.summary .yith-wcwl-add-to-wishlist, .summary .compare').css('border-color', newval );
			
			$('.featured-tab .nav-tabs>li.active>a:after').css('border-left-color', newval );
			$('.plan.featured a.btn').css('border-bottom-color', newval );
		} );
	} );
/****************Custom background**************************/
wp.customize( 'bg_image', function( value ) {
		value.bind( function( newval ) {
			$('.body-inner').css('background-image', 'url('+newval+')');
		} );
	} );
wp.customize( 'bg_color', function( value ) {
		value.bind( function( newval ) {
			$('.body-inner').css('background-color', newval );
		} );
	} );
wp.customize( 'bg_repeat', function( value ) {
		value.bind( function( newval ) {
			$('.body-inner').css('background-repeat', newval );
		} );
	} );
wp.customize( 'bg_att', function( value ) {
		value.bind( function( newval ) {
			$('.body-inner').css('background-attachment', newval );
		} );
	} );
wp.customize( 'bg_hor', function( value ) {
		value.bind( function( newval ) {
			$('.body-inner').css('background-position-x', newval );
		} );
	} );
wp.customize( 'bg_ver', function( value ) {
		value.bind( function( newval ) {
			$('.body-inner').css('background-position-y', newval );
		} );
	} );
wp.customize( 'bg_size', function( value ) {
		value.bind( function( newval ) {
			if(newval){
			$('.body-inner').css('background-size', 'cover' );
		}
		} );
	} );

/************** Top navigation section ******************/
wp.customize( 'topnav_bg_image', function( value ) {
	value.bind( function( newval ) {
		$('.top-info').css('background-image', 'url('+newval+')');
	} );
} );
wp.customize( 'topnav_bg_color', function( value ) {
	value.bind( function( newval ) {
		$('.top-info').css('background-color', newval );
	} );
} );
wp.customize( 'topnav_bg_repeat', function( value ) {
	value.bind( function( newval ) {
		$('.top-info').css('background-repeat', newval );
	} );
} );	

wp.customize( 'topnav_link_color', function( value ) {
	value.bind( function( newval ) {
		$('.top-info .topbar-sidebar a').css('color', newval);
	} );
} );

wp.customize( 'topnav_link_color_hov', function( value ) {
	value.bind( function( newval ) {
		$(".top-info .topbar-sidebar a").hover(function(e) { 
			$(this).css("color", e.type === "mouseenter" ? newval : value);
		});
	} );
} );
wp.customize( 'topnav_text', function( value ) {
	value.bind( function( newval ) {
		$('.top-info .topbar-sidebar, .top-info .topbar-sidebar p, .top-info .topbar-sidebar li').css('color', newval);
	} );
} );
/*
wp.customize( 'topnav_dropdown_bg_hov', function( value ) {
	value.bind( function( newval ) {
		$(".header-top ul.dropdown-menu li a").hover(function(e) { 
			$(this).css("background-color", e.type === "mouseenter" ? newval : value);
		});
	} );
} );

wp.customize( 'topnav_dropdown_color', function( value ) {
	value.bind( function( newval ) {
		$('.header-top ul.dropdown-menu li a').css('color', newval);
	} );
} );

wp.customize( 'topnav_dropdown_bg', function( value ) {
	value.bind( function( newval ) {
		$('.header-top ul.dropdown-menu').css('background', newval);
	} );
} );


		
/****************Header section**************************/
wp.customize( 'header_bg_image', function( value ) {
		value.bind( function( newval ) {
			$('#header').css('background', 'url('+newval+')');
		} );
	} );
wp.customize( 'header_bg_color', function( value ) {
		value.bind( function( newval ) {
			$('#header').css('background-color', newval );
		} );
	} );
wp.customize( 'header_bg_repeat', function( value ) {
		value.bind( function( newval ) {
			$('#header').css('background-repeat', newval );
		} );
	} );
wp.customize( 'header_bg_att', function( value ) {
		value.bind( function( newval ) {
			$('#header').css('background-attachment', newval );
		} );
	} );
wp.customize( 'header_bg_hor', function( value ) {
		value.bind( function( newval ) {
			$('#header').css('background-position-x', newval );
		} );
	} );
wp.customize( 'header_bg_ver', function( value ) {
		value.bind( function( newval ) {
			$('#header').css('background-position-y', newval );
		} );
	} );
/*****************************Content styling*******************/
wp.customize( 'title_content_bg_image', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content').css('cssText', 'background:url('+newval+')!important');

		} );
	} );
wp.customize( 'title_content_bg_color', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content').css('cssText', 'background:'+newval+'!important' );

		} );
	} );
wp.customize( 'title_content_bg_repeat', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content').css('background-repeat', newval );
		} );
	} );
wp.customize( 'title_content_bg_att', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content').css('background-attachment', newval );
		} );
	} );
wp.customize( 'title_content_bg_hor', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content').css('background-position-x', newval );
		} );
	} );
wp.customize( 'title_content_bg_ver', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content').css('background-position-y', newval );
		} );
	} );
	/***********************promo*********************/
wp.customize( 'promo_bg_image', function( value ) {
		value.bind( function( newval ) {
			$('#banner').css('background', 'url('+newval+')');
		} );
	} );
wp.customize( 'promo_bg_color', function( value ) {
		value.bind( function( newval ) {
			$('#banner').css('background-color', newval );
		} );
	} );
wp.customize( 'promo_bg_repeat', function( value ) {
		value.bind( function( newval ) {
			$('#banner').css('background-repeat', newval );
		} );
	} );
wp.customize( 'promo_bg_att', function( value ) {
		value.bind( function( newval ) {
			$('#banner').css('background-attachment', newval );
		} );
	} );
wp.customize( 'promo_bg_hor', function( value ) {
		value.bind( function( newval ) {
			$('#banner').css('background-position-x', newval );
		} );
	} );
wp.customize( 'promo_bg_ver', function( value ) {
		value.bind( function( newval ) {
			$('#banner').css('background-position-y', newval );
		} );
	} );
/***************************Footer**************/
wp.customize( 'footer_bg_image', function( value ) {
		value.bind( function( newval ) {
			$('#footer').css('background', 'url('+newval+')');
		} );
	} );
wp.customize( 'footer_bg_color', function( value ) {
		value.bind( function( newval ) {
			$('#footer').css('background-color', newval );
		} );
	} );
wp.customize( 'footer_bg_repeat', function( value ) {
		value.bind( function( newval ) {
			$('#footer').css('background-repeat', newval );
		} );
	} );
wp.customize( 'footer_bg_att', function( value ) {
		value.bind( function( newval ) {
			$('#footer').css('background-attachment', newval );
		} );
	} );
wp.customize( 'footer_bg_hor', function( value ) {
		value.bind( function( newval ) {
			$('#footer').css('background-position-x', newval );
		} );
	} );
wp.customize( 'footer_bg_ver', function( value ) {
		value.bind( function( newval ) {
			$('#footer').css('background-position-y', newval );
		} );
	} );
wp.customize( 'footer_title_color', function( value ) {
		value.bind( function( newval ) {
			$('.footer-widget>h3.footer-title').css('color', newval );
		} );
	} );
wp.customize( 'footer_links_color', function( value ) {
		value.bind( function( newval ) {
			$('#footer .footer-widget a, #footer .footer-widget li a').css('color', newval );
		} );
	} );
wp.customize( 'footer_links_color_hov', function( value ) {
		value.bind( function( newval ) {
			$("#footer .footer-widget a, #footer .footer-widget li a").hover(function(e) { 
				$(this).css("color", e.type === "mouseenter" ? newval : value);
			});
		} );
	} );
	
	
wp.customize( 'footerbottom_bg_image', function( value ) {
		value.bind( function( newval ) {
			$('.footer-bottom').css('background', 'url('+newval+')');
		} );
	} );
wp.customize( 'footerbottom_bg_color', function( value ) {
		value.bind( function( newval ) {
			$('.footer-bottom').css('background-color', newval );
		} );
	} );
wp.customize( 'footerbottom_bg_repeat', function( value ) {
		value.bind( function( newval ) {
			$('.footer-bottom').css('background-repeat', newval );
		} );
	} );
wp.customize( 'footerbottom_bg_att', function( value ) {
		value.bind( function( newval ) {
			$('.footer-bottom').css('background-attachment', newval );
		} );
	} );
wp.customize( 'footerbottom_bg_hor', function( value ) {
		value.bind( function( newval ) {
			$('.footer-bottom').css('background-position-x', newval );
		} );
	} );
wp.customize( 'footerbottom_bg_ver', function( value ) {
		value.bind( function( newval ) {
			$('.footer-bottom').css('background-position-y', newval );
		} );
	} );

wp.customize( 'footerbottom_text_color', function( value ) {
	value.bind( function( newval ) {
		$('.footer-bottom, .footer-bottom p, .footer-bottom li, .copyright-info').css('color', newval );
	} );
} );	
wp.customize( 'footerbottom_links_color', function( value ) {
	value.bind( function( newval ) {
		$('.footer-bottom a, .copyright-info a').css('color', newval );
	} );
} );	
	
wp.customize( 'footerbottom_links_color_hov', function( value ) {
	value.bind( function( newval ) {
		$(".footer-bottom a, .copyright-info a").hover(function(e) { 
			$(this).css("color", e.type === "mouseenter" ? newval : value);
		});
	} );
} );	


/*********************************Links**********************/
wp.customize( 'link_color', function( value ) {
		value.bind( function( newval ) {
			$('a').css('color', newval );
		} );
	} );
wp.customize( 'link_decor', function( value ) {
		value.bind( function( newval ) {
			$('a').css('text-decoration', newval);
		} );
	} );
wp.customize( 'link_color_hov', function( value ) {
		value.bind( function( newval ) {
			$('a').hover(function(){
				$(this).css("color",newval);

			});
		} );
	} );
wp.customize( 'link_decor_hov', function( value ) {
		value.bind( function( newval ) {
			$('a').hover(function(){
				$(this).css("text-decoration",newval);

			});
		} );
	} );
/********************Main Navigation*******************/
wp.customize( 'nav_bg_image', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.nav.navbar-nav').css('background', 'url('+newval+')');
		} );
	} );
wp.customize( 'nav_bg_color', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.nav.navbar-nav').css('background-color', newval );
		} );
	} );
wp.customize( 'nav_bg_repeat', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.nav.navbar-nav').css('background-repeat', newval );
		} );
	} );
wp.customize( 'nav_bg_att', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.nav.navbar-nav').css('background-attachment', newval );
		} );
	} );
wp.customize( 'nav_bg_hor', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.nav.navbar-nav').css('background-position-x', newval );
		} );
	} );
wp.customize( 'nav_bg_ver', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.nav.navbar-nav').css('background-position-y', newval );
		} );
	} );

wp.customize( 'nav_links_color', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.navbar-nav>li>a').css('color', newval );
		} );
	} );

wp.customize( 'nav_links_color_hover', function( value ) {
		value.bind( function(e, newval ) {
			$('nav ul.navbar-nav>li>a').hover(function(e){
				$(this).css("color", e.type === "mouseenter" ? newval : value);
			});
		} );
	} );

wp.customize( 'nav_current_links_color', function( value ) {
	value.bind( function(e, newval ) {
		$('nav ul.navbar-nav li.active > a').css('cssText', 'color:' + newval + '!important');
		$('nav ul.navbar-nav li.active > a').hover(function(e){
			$(this).css("color", e.type === "mouseenter" ? newval : value);
		});
	} );
} );

wp.customize( 'sub_nav_hover_background', function( value ) {
	value.bind( function(e, newval ) {
		$('nav ul.navbar-nav ul.dropdown-menu > li').hover(function(e){
			$(this).css("background-color", e.type === "mouseenter" ? newval : value);
		});
	} );
} );
wp.customize( 'sub_nav_hover_color', function( value ) {
	value.bind( function(e, newval ) {
		$('nav ul.navbar-nav ul.dropdown-menu > li> a').hover(function(e){
			$(this).css("color", e.type === "mouseenter" ? newval : value);
		});
	} );
} );	
wp.customize( 'nav_current_bg', function( value ) {
	value.bind( function( newval ) {
		$('nav ul.navbar-nav li.active > a').css('cssText', 'background:' + newval + '!important');
	} );
} );
	
wp.customize( 'sub_nav_background', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.navbar-nav ul.dropdown-menu>li').css('background-color', newval );
		} );
	} );
wp.customize( 'sub_nav_color', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.navbar-nav ul.dropdown-menu > li> a').css('color', newval);
		} );
	} );
wp.customize( 'nav_separator', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.navbar-nav ul.dropdown-menu > li> a').css('border-bottom', '1px solid'+newval);
		} );
	} );
	
/**************************TYPOGRAPHY***************************************/
//get font name 
function get_font(font){
	var fullFont=font;
	var mainFont=fullFont.split(':');
	var font_name=mainFont[0];
	var font_type=mainFont[1];
	var font_name_link='';
	if( font_type == 'non-google' ){
	}
	else if( font_type == 'early-google'){
		 font_name_link = mainFont[0].replace(" ","");
		$('head').append('<link href="http://fonts.googleapis.com/earlyaccess/'+font_name_link+'.css" rel="stylesheet" type="text/css">');
	}else{
		font_name_link =  mainFont[0].replace(" ","+");
		var font_variants = mainFont[1].replace("|",",");
		$('head').append('<link href="http://fonts.googleapis.com/css?family='+font_name_link+':'+font_variants+'" rel="stylesheet" type="text/css">');
	}
	return font_name;
}
/*main typography*/

wp.customize( 'main_typ_font', function( value ) {
	value.bind( function( newval ) {
		var font=get_font(newval);
		$('body, p, li, span, .testimonial-slide .testimonial-text').css('font-family', font);
	} );
} );
wp.customize( 'main_typ_col', function( value ) {
	value.bind( function( newval ) {
		$('body, p, li, span, .testimonial-slide .testimonial-text').css('color', newval );
	} );
} );
wp.customize( 'main_typ_size', function( value ) {
	value.bind( function( newval ) {
		$('body, p, li, span, .testimonial-slide .testimonial-text').css('font-size', newval+'px' );
	} );
} );
wp.customize( 'main_typ_weight', function( value ) {
	value.bind( function( newval ) {
		$('body, p, li, span, .testimonial-slide .testimonial-text').css('font-weight', newval );
	} );
} );
wp.customize( 'main_typ_style', function( value ) {
	value.bind( function( newval ) {
		$('body, p, li, span, .testimonial-slide .testimonial-text').css('font-style', newval );
	} );
} );
/*textual logo*/
wp.customize( 'log_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('a.navbar-brand').css('font-family', font);
		} );
	} );
wp.customize( 'log_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('a.navbar-brand').css('color', newval );
		} );
	} );
wp.customize( 'log_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('a.navbar-brand').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'log_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('a.navbar-brand').css('font-weight', newval );
		} );
	} );
wp.customize( 'log_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('a.navbar-brand').css('font-style', newval );
		} );
	} );
/*main Navigation*/
wp.customize( 'nav_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('nav ul.navbar-nav li a').css('font-family', font);
		} );
	} );

wp.customize( 'nav_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.navbar-nav li a').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'nav_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.navbar-nav li a').css('font-weight', newval );
		} );
	} );
wp.customize( 'nav_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('nav ul.navbar-nav li a').css('font-style', newval );
		} );
	} );
/*h1 styling*/
wp.customize( 'h1_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('h1, h1 a').css('font-family', font);
		} );
	} );
wp.customize( 'h1_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('h1').css('color', newval );
		} );
	} );
wp.customize( 'h1_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('h1').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'h1_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('h1').css('font-weight', newval );
		} );
	} );
wp.customize( 'h1_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('h1').css('font-style', newval );
		} );
	} );
/*h2 styling*/
wp.customize( 'h2_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('h2').css('font-family', font);
		} );
	} );
wp.customize( 'h2_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('h2').css('color', newval );
		} );
	} );
wp.customize( 'h2_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('h2').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'h2_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('h2').css('font-weight', newval );
		} );
	} );
wp.customize( 'h2_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('h2').css('font-style', newval );
		} );
	} );
/*h3 styling*/
wp.customize( 'h3_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('h3').css('font-family', font);
		} );
	} );
wp.customize( 'h3_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('h3').css('color', newval );
		} );
	} );
wp.customize( 'h3_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('h3').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'h3_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('h3').css('font-weight', newval );
		} );
	} );
wp.customize( 'h3_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('h3').css('font-style', newval );
		} );
	} );
/*h4 styling*/
wp.customize( 'h4_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('h4').css('font-family', font);
		} );
	} );
wp.customize( 'h4_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('h4').css('color', newval );
		} );
	} );
wp.customize( 'h4_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('h4').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'h4_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('h4').css('font-weight', newval );
		} );
	} );
wp.customize( 'h4_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('h4').css('font-style', newval );
		} );
	} );
/*h5 styling*/
wp.customize( 'h5_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('h5').css('font-family', font);
		} );
	} );
wp.customize( 'h5_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('h5').css('color', newval );
		} );
	} );
wp.customize( 'h5_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('h5').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'h5_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('h5').css('font-weight', newval );
		} );
	} );
wp.customize( 'h5_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('h5').css('font-style', newval );
		} );
	} );
/*h6 styling*/
wp.customize( 'h6_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('h6').css('font-family', font);
		} );
	} );
wp.customize( 'h6_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('h6').css('color', newval );
		} );
	} );
wp.customize( 'h6_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('h6').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'h6_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('h6').css('font-weight', newval );
		} );
	} );
wp.customize( 'h6_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('h6').css('font-style', newval );
		} );
	} );

/*Page title styling*/
wp.customize( 'ptit_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('.inner-title-content h2').css('font-family', font);
		} );
	} );

wp.customize( 'ptit_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content h2').css('color', newval );
		} );
	} );
wp.customize( 'ptit_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content h2').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'ptit_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content h2').css('font-weight', newval );
		} );
	} );
wp.customize( 'ptit_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('.inner-title-content h2').css('font-style', newval );
		} );
	} );
/*breadcrumb styling*/
wp.customize( 'bred_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('ul.breadcrumb li a, ul.breadcrumb li').css('font-family', font);
		} );
	} );
wp.customize( 'bred_typ_col', function( value ) {
	value.bind( function( newval ) {
		$('ul.breadcrumb li a, ul.breadcrumb li').css('color', newval );
	} );
} );
wp.customize( 'bred_typ_size', function( value ) {
	value.bind( function( newval ) {
		$('ul.breadcrumb li a, ul.breadcrumb li').css('font-size', newval+'px' );
	} );
} );
wp.customize( 'bred_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('ul.breadcrumb li a, ul.breadcrumb li').css('font-weight', newval );
		} );
	} );
wp.customize( 'bred_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('ul.breadcrumb li a, ul.breadcrumb li').css('font-style', newval );
		} );
	} );
/*Widget title styling*/
wp.customize( 'wtit_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('.sidebar h3.widget-title').css('font-family', font);
		} );
	} );

wp.customize( 'wtit_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('.sidebar h3.widget-title').css('color', newval );
		} );
	} );
wp.customize( 'wtit_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('.sidebar h3.widget-title').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'wtit_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('.sidebar h3.widget-title').css('font-weight', newval );
		} );
	} );
wp.customize( 'wtit_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('.sidebar h3.widget-title').css('font-style', newval );
		} );
	} );
/*Footer Widget title styling*/
wp.customize( 'ftit_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('.footer-widget>h3.footer-title').css('font-family', font);
		} );
	} );

wp.customize( 'ftit_typ_col', function( value ) {
		value.bind( function( newval ) {			
			$('.footer-widget>h3.footer-title').css('color', newval );
		} );
	} );
wp.customize( 'ftit_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('.footer-widget>h3.footer-title').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'ftit_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('.footer-widget>h3.footer-title').css('font-weight', newval );
		} );
	} );
wp.customize( 'ftit_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('.footer-widget>h3.footer-title').css('font-style', newval );
		} );
	} );
	
	

/*************************ONE PAGER**********************/
/*Alternative Page title styling*/
wp.customize( 'altptit_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('h2.onetitle').css('font-family', font);
		} );
	} );

wp.customize( 'altptit_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('h2.onetitle').css('color', newval );
		} );
	} );
wp.customize( 'altptit_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('h2.onetitle').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'altptit_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('h2.onetitle').css('font-weight', newval );
		} );
	} );
wp.customize( 'altptit_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('h2.onetitle').css('font-style', newval );
		} );
	} );
/*Tagline styling*/
wp.customize( 'tag_typ_font', function( value ) {
		value.bind( function( newval ) {
			var font=get_font(newval);
			$('h3.onesubtitle').css('font-family', font);
		} );
	} );

wp.customize( 'tag_typ_col', function( value ) {
		value.bind( function( newval ) {
			$('h3.onesubtitle').css('color', newval );
		} );
	} );
wp.customize( 'tag_typ_size', function( value ) {
		value.bind( function( newval ) {
			$('h3.onesubtitle').css('font-size', newval+'px' );
		} );
	} );
wp.customize( 'tag_typ_weight', function( value ) {
		value.bind( function( newval ) {
			$('h3.onesubtitle').css('font-weight', newval );
		} );
	} );
wp.customize( 'tag_typ_style', function( value ) {
		value.bind( function( newval ) {
			$('h3.onesubtitle').css('font-style', newval );
		} );
	} );
} )( jQuery );


