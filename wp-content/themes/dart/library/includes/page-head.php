<?php
global $get_meta, $post;
if (empty($get_meta)) {
	if(function_exists('is_woocommerce') && is_woocommerce()){
		$id = get_option('woocommerce_shop_page_id');
		$get_meta = get_post_custom($id);
	}
	else{
	
		$get_meta = get_post_custom($post->ID);
	}
}	
if(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] != 'none' ):
	
	$orig_post = $post;
	
	if(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] == 'revslider' ){
	
		$revslider =  $get_meta["_weblusive_revslider"][0];
		if( !empty($revslider) ){
			if (function_exists('putRevSlider'))
			{	
				echo '<div class="slider-wrapper">'.do_shortcode($revslider).'<div class="clearfix"></div></div>';
				//echo do_shortcode($revslider);
			}
			else
			{
				echo '<p>Please install the "Revolution slider" plugin to be able to display it on the page.</p>';
			}
		}
	}

	elseif(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] == 'vimeovideo'){
		$video = isset ($get_meta["_weblusive_vimeo"]) ? $get_meta["_weblusive_vimeo"][0] : '';
		?>
				<?php if($video):?>
					<div class="flex-video vimeo">
						<iframe src="http://player.vimeo.com/video/<?php echo $video?>" height="350" ></iframe>
					</div>
				<?php else: ?>
					<p class="label label-warning"><?php esc_html_e('Vimeo video chosen from post head options but no video ID provided. Please make sure to provide a video ID to display it.', 'dart')?></p>
				<?php endif?>
	<?php 
	}
	elseif(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] == 'youtubevideo'){
		$video = $get_meta["_weblusive_youtube"][0];
		?>
		<div class="flex-video youtube">
			<?php if($video):?>
				<iframe src="http://www.youtube.com/embed/<?php echo $video?>" height="350" ></iframe>
			<?php else: ?>
				<p><?php esc_html_e('Youtube video was chosen from post head options but no video ID provided. Please make sure to provide a video ID  to display it.', 'dart')?></p>
			<?php endif?>
		</div>

	<?php 
	}
	
	
	elseif(isset( $get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] == 'slider' && !empty( $get_meta['_weblusive_post_slider'][0] ) ){
		
		
		$custom_slider_args = array( 'post_type' => 'themeplaza_slider', 'p' => $get_meta['_weblusive_post_slider'][0] );
		$custom_slider = new WP_Query( $custom_slider_args );
		
		while ( $custom_slider->have_posts() ) : $custom_slider->the_post();
			$custom = get_post_custom($post->ID);
		//	$slidertype = isset ($custom["_weblusive_slider_type"]) ? $custom["_weblusive_slider_type"][0] : '1';
			$slider = isset ($custom["custom_slider"]) ? unserialize( $custom["custom_slider"][0]) : '';
			$number = count($slider);
			$sliderAutoplay = isset ($custom["autoslide"]) ? stripslashes($custom["autoslide"][0]) : 'true';
			$sliderInterval = isset ($custom["interval"]) ? stripslashes($custom["interval"][0]) : '5000';
				
		if( $slider ):
		$rtl = weblusive_get_option('rtl_mode');
		if ($rtl){
			$rtl = 'true';
		}
		else{
			$rtl = 'false';
		}
		?>
	<div class="row">
			<div class="col-md-12">
			
				<div id="main-slide" class="headcarousel carousel flexslider flexslider-rtl">
					<ul class="slides">
							<?php foreach( $slider as $slide ): 
								$image =  wp_get_attachment_image_src( $slide['id'], 'full');
								$width = isset($image[1]) ? $image[1] : '';
								$height = isset($image[2]) ? $image[2] : '';

								$finalimage = empty($image) ? '' : $image[0];
								$caption = empty($slide['caption']) ? '' : ($slide['caption']) ;
								?>
								<li>
									<?php if( !empty( $slide['link'] ) ):?><a href="<?php echo $slide['link']?>"><?php endif?>
										<img src="<?php echo $finalimage ?>" class="img-responsive" alt="" width="<?php echo $width?>" height="<?php echo $height?>" />
									<?php if( !empty( $slide['link'] ) ):?></a><?php endif?>	
									<div class="flex-caption slider-content">
										<div class="col-md-12 text-center">
										<?php if($caption):?>
											<?php echo $caption ?>
										<?php endif; ?>
										</div>
									</div>
								</li>
							<?php endforeach; ?>
						</ul>
				</div>
			</div>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.headcarousel').flexslider({
				animation:'slide',
				controlNav:true,
				rtl: <?php echo $rtl?>,
				direction: '<?php echo $rtl ? 'vertical' : 'horizontal'?>',
				directionNav:true,
				prevText:'<span><i class="fa fa-angle-left"></i></span>',
				nextText:'<span><i class="fa fa-angle-right"></i></span>',
				slideshow : <?php echo $sliderAutoplay?>,
				slideshowSpeed:<?php echo $sliderInterval?>
				
			});
		})
		
	</script>
</div>
		<?php else:?>
			<div class="col-md-12">
				<div class="no-slider">
					<p class="btn btn-warning">
						<i class="fa fa-warning"></i>
						<?php esc_html_e('No slider items were found in the selected slider. Please make sure to create some via "Slider" section in your admin panel.', 'dart');?>
					</p>
				</div>
			</div>
		<?php endif;
	endwhile; ?>
<?php }

	$post = $orig_post;
	wp_reset_query();
	
 endif; ?>
