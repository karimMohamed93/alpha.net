<?php 
$pageId = $post->ID;
get_template_part('portfolio_header');
$_SESSION['dart_page_id'] = $pageId;

$get_meta = get_post_custom($post->ID);

$weblusive_sidebar_pos = $get_meta["_weblusive_sidebar_pos"][0];
$layout =  isset($get_meta["_portfolio_item_layout"][0]) ? $get_meta["_portfolio_item_layout"][0] : '1'; 
get_template_part( 'library/includes/page-head' ); 

$portfolio_type = get_post_meta($post->ID, "_portfolio_type", $single = false);
$paginationEnabled = (isset($portfolio_type) && !(empty ($portfolio_type))) ? $portfolio_type[0] : 0;
$page_template_name = get_post_meta($post->ID,'_wp_page_template',true); 


	$itemsize = '292x300';	
	$thumbsize = 'portfolio-4-cols';
	// Check which layout was selected
	switch($page_template_name)
	{
		
		case 'portfolio-template-2columns.php':
			$itemsize = '600x850';	
			$thumbsize = 'portfolio-2-cols';
			$contcClass='col-sm-6';
		break;
		case 'portfolio-template-3columns.php':
			$itemsize = '450x600';	
			$thumbsize = 'portfolio-3-cols';
			$contcClass='col-sm-4';
		break;
		
		case 'portfolio-template-4columns.php':
			$itemsize = '292x300';	
			$thumbsize = 'portfolio-4-cols';
			$contcClass='col-sm-3';
		break;
	}


if( get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = true) != '' && $paginationEnabled ) 
{ 
	$items_per_page = get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = false);
} 
else 
{ // else don't paginate
	$items_per_page[0] = 777;
}
$loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => $items_per_page[0])); 
?>
<div class="portfolio-page" id="portfolio-page">
	<div class="row">
	<?php if ($weblusive_sidebar_pos == 'left'): ?>
		<div id="sidebar" class="col-sm-4 col-md-3"><div class="sidebar sidebar-left"><?php get_sidebar(); ?></div></div>
	<?php endif?>
	<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-12<?php else:?>col-sm-8 col-md-9<?php endif?> main-content">
		<div class="row text-center">
			<ul id="isotope-filter" data-option-key="filter" class="option-set list-inline text-center <?php if (!$paginationEnabled):?><?php echo 'non-paginated' ?><?php endif?>">
				<?php if ($paginationEnabled):?>
					<li><a href="<?php echo get_page_link($pageId) ?>" class="filter active"><?php esc_html_e('Show All', 'dart')?></a></li>
					<?php 
						$cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true);
						$MyWalker = new PortfolioWalker2();
						$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'show_count' => '1');
						$categories = wp_list_categories ($args);
					?>
				<?php else: ?>
					<li><a href="#isotope-filter" class="filter active" data-filter="*"><?php esc_html_e('Show All', 'dart')?></a></li>
					<?php 
						$cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true);                                                 
						$MyWalker = new PortfolioWalker();
						$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'show_count' => '1');
						$categories = wp_list_categories ($args);
					?>
				<?php endif ?>
			</ul>
			</div>
			<div class="row">
				<div id="isotope" class="isotope portfolio-page-items <?php echo $thumbsize;?>">
				<?php if( $cats == '' ): ?>
					<p><?php esc_html_e('No categories selected. To fix this, please login to your WP Admin area and set
						the categories you want to show by editing this page and setting one or more categories 
						in the multi checkbox field "Portfolio Categories".', 'dart')?>
					</p>
				<?php else: ?>		
					<?php 
						// If the user hasn't set a number of items per page, then use JavaScript filtering
						if( $items_per_page == 777 ) : endif; 
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						//  query the posts in selected terms
						$portfolio_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'portfolio_category');
					 ?>
					 <?php if (!empty($portfolio_posts_to_query)):			
						$wp_query = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'menu_order', 'order' => 'ASC', 'post__in' => $portfolio_posts_to_query, 'paged' => $paged, 'showposts' => $items_per_page[0] ) ); 
						
						if ($wp_query->have_posts()):  ?>
						<?php while ($wp_query->have_posts()) : 							
							$wp_query->the_post();
							$custom = get_post_custom($post->ID);
								 
							// Get the portfolio item categories
							$cats = wp_get_object_terms($post->ID, 'portfolio_category');
												   
													
							if ($cats):
								$cat_slugs = '';
								foreach( $cats as $cat ) {$cat_slugs .= $cat->slug . " ";}
							endif;
							?>
				
							<?php 
								$link = ''; 
								$attachments = ''; 
								$thumbnail = get_the_post_thumbnail($post->ID, $thumbsize); 
								$size = isset($custom["_portfolio_item_size"][0]) ? $custom["_portfolio_item_size"][0] : '';
								
							?>
							<div class="<?php echo $contcClass?> <?php echo $cat_slugs; ?>">
								<div class="portfolio-overlay">
									<div class="portfolio-overlay-btn">
										<?php if (!$attachments) : ?>
											<?php if (!empty($custom['_portfolio_video'][0])) : $link = $custom['_portfolio_video'][0]; ?>
												<a href="<?php echo $link ?>" data-rel="prettyPhoto" title="<?php the_title() ?>">
													<i class="fa fa-film"></i>
												</a>
												<?php if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :?>
													<a href="<?php echo  get_permalink(get_the_ID()); ?>" title="<?php the_title() ?>">
														<i class="fa fa-link"></i>
													</a>
												<?php endif; ?>
											<?php elseif (isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '') : ?>
												<a href="<?php echo $custom['_portfolio_link'][0] ?>" title="<?php the_title() ?>">
													<i class="fa fa-external-link"></i>
												</a>
												<?php if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :?>
													<a href="<?php echo  get_permalink(get_the_ID()); ?>" title="<?php the_title() ?>">
														<i class="fa fa-link"></i>
													</a>
												<?php endif; ?>
											<?php elseif (isset($custom['_portfolio_no_lightbox'][0]) && $custom['_portfolio_no_lightbox'][0] != '') : $link = get_permalink(get_the_ID()); ?>
												<a href="<?php echo $link; ?>" title="<?php the_title() ?>">
													<i class="fa fa-link"></i>
												</a>
											<?php else : ?>
												<?php
													$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
													$link = $full_image[0];
													?>
													<a data-rel="prettyPhoto" href="<?php echo $link ?>" title="<?php the_title() ?>">
														<i class="fa fa-search"></i>
													</a>
												<?php if (isset($custom['_portfolio_include_link'][0]) && $custom['_portfolio_include_link'][0] != '') :?>
													<a href="<?php echo  get_permalink(get_the_ID()); ?>" title="<?php the_title() ?>">
														<i class="fa fa-link"></i>
													</a>
												<?php endif; ?>
											<?php endif; ?>       
										<?php endif ?>
									</div>
									<?php if ($layout == '2'):?>
									<div class="portfolio-item-content">
                            			<h4 class="portfolio-item-title"><a href="<?php the_permalink()?>"><?php the_title() ?></a></h4>
										<p><?php echo limit_words(get_the_excerpt(), 5) ?></p>
									</div>	
									<?php endif?>
								</div>
								<div class="image-thumb">
									<?php if (!empty($thumbnail)): ?>
										<?php the_post_thumbnail($thumbsize, array('class' => 'cover')); ?>
									<?php else :?>
										<img src="http://placehold.it/<?php echo $itemsize?>" alt="<?php _e ('No preview image', 'dart') ?>" />
									<?php endif?>
								</div>
							</div>	
						<?php endwhile; ?>
					<?php endif;?>
				<?php endif;?>
			<?php endif?>		
			</div>
		</div>
			<?php if ($paginationEnabled ):?>
				<?php if ( $wp_query->max_num_pages > 1 ): ?>
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center pagination-wrapper">
							<?php include(dart_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
							<div class="clearfix"></div>
						</div>
					</div>
				<?php endif?>
			<?php endif?>	
	</div>
	<?php if ($weblusive_sidebar_pos == 'right'): ?>
	<div id="sidebar" class="col-sm-4 col-md-3"><div class="sidebar sidebar-right"><?php get_sidebar(); ?></div></div>
	<?php endif?>
	</div>
</div>
<?php echo do_shortcode(getPageContent($pageId)); ?>
<?php get_template_part('portfolio_footer'); ?>