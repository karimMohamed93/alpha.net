<?php /* Template Name: Regular page */ 

get_header();?>

<?php
	$get_meta = get_post_custom($post->ID);
	$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
	
	get_template_part( 'inner-header', 'content'); 
	
	
	$woopage = false;
	if(function_exists('is_woocommerce')) {
		
		if(is_checkout() || is_account_page() || is_cart()){
			$woopage = true;
		}
	}
?>
<section class="main-wrapper pagecustom-<?php echo $post->ID?>"> 
	<div class="container">
		<div class="row">
			<?php if ($weblusive_sidebar_pos == 'left'): ?><div id="sidebar" class="col-sm-4 col-md-3"><div class="sidebar sidebar-left"><?php get_sidebar(); ?></div></div><?php endif?>
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>col-md-12<?php else:?>col-sm-8 col-md-9 <?php endif?> main-content">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . esc_html__( 'Pages:', 'dart' ), 'after' => '</div>' ) ); ?>
			</div>
			<?php endwhile; ?>	
			<?php if ($weblusive_sidebar_pos == 'right'): ?><div id="sidebar" class="col-sm-4 col-md-3"><div class="sidebar sidebar-right"><?php get_sidebar(); ?></div></div><?php endif?>
        </div>
    </div>
</section>
<?php get_footer();?>