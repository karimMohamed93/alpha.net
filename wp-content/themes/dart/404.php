<?php /* 404 (Not found) page template */ 

get_header();
$bgimage=weblusive_get_option('innerheading_background_image'); 
$hidetitles = weblusive_get_option('hide_titles');
if ($bgimage) {
	$bgimg=$bgimage;
}else{
	$bgimg=get_template_directory_uri() .'/images/banner/banner4.jpg' ;
}
?>
<?php if (!$hidetitles):?>
<div id="inner-header">
	<img src="<?php echo $bgimg;?>" alt ="banner" />
</div>
<!-- Subpage title start -->
<section id="inner-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner-title-content">
					<h2><?php esc_html_e('Error 404 Page', 'dart')?></h2>
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
	          	</div>
	        </div>
		</div>
	</div>
</section>
<div class="gap-40"></div>
<?php endif; ?>
<section id="main-container">
	<div class="container text-center">
		<div class="error-block">
			<h1 class="error-title">
				<?php esc_html_e('404', 'dart')?>
			</h1>
			<h2><?php esc_html_e('Oops... Page Not Found!', 'dart')?></h2>
			<p class="lead">
				<?php esc_html_e('We\'re sorry, but the page you were looking for doesn\'t exist.', 'dart')?>
			</p>
			<a href="javascript: history.go(-1)" class="btn btn-primary btn-lg"><?php esc_html_e('Go Back to Previous Page', 'dart')?></a>
		</div>
	</div><!--/ container end -->
</section>
<?php get_footer();?>
