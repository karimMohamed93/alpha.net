<?php
/********************* GOOGLE MAP **********************/

/***********************************/
vc_map( array(
   "name" => esc_html__("Google Map", "dart"),
   "base" => "map",
   "class" => "",
   "icon" => "icon-wpb-map-pin",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
      
      array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Latitude", "dart"),
         "param_name" => "lat"
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Longitude", "dart"),
         "param_name" => "lon"
      ),
	   array(
         "type" => "textfield",
		 "holder"=>'div',
         "class" => "",
         "heading" => esc_html__("Address", "dart"),
         "param_name" => "address"
      ),
	   array(
		 "type" => "dropdown",	
		  "class" => "",
		  "heading" => esc_html__("Zoom", "dart"),
		  "param_name" => "z",
		  "value"=>array("1"=>"1", "2"=>"2", "3"=>"3", "4"=>"4", "5"=>"5", "6"=>"6", "7"=>"7", "8"=>"8", "9"=>"9", "10"=>"10", "11"=>"11", "12"=>"12", "13"=>"13", "14"=>"14", "15"=>"15", "16"=>"16", "17"=>"17", "18"=>"18", "19"=>"19", "20"=>"20")
		 ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Width(in pixels)", "dart"),
         "param_name" => "w"
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Height(in pixels)", "dart"),
         "param_name" => "h"
      ),
	    array(
		 "type" => "dropdown",	
		  "class" => "",
		  "heading" => esc_html__("Map Type", "dart"),
		  "param_name" => "maptype",
		  "value"=>array("Road"=>"ROADMAP", "Satellite"=>"SATELLITE", "Hybrid"=>"HYBRID", "Terrain"=>"TERRAIN")
		 ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Marker(set address)", "dart"),
         "param_name" => "marker"
      ),
	   array(
         "type" => "attach_image",
         "class" => "",
         "heading" => esc_html__("Marker image: ", "dart"),
         "param_name" => "markerimage"
      ),
	  
	   array(
         "type" => "textarea",
         "class" => "",
         "heading" => esc_html__("Information window:", "dart"),
         "param_name" => "infowindow"
      ),
	   array(
		 "type" => "dropdown",	
		  "class" => "",
		  "heading" => esc_html__("Show Traffic:", "dart"),
		  "param_name" => "traffic",
		  "value"=>array("No"=>"no", "Yes"=>"yes")
		 ),
        array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      ),
   )
) );

/************************************************/
?>