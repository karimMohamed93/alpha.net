<?php

/****************************Modal BOX****************/

vc_map( array(
   "name" => esc_html__("Modal box", "dart"),
   "base" => "reveal",
   "class" => "",
   "icon" => "icon-wpb-my_reveal",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
       array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Type", "dart"),
         "param_name" => "type",
         "value" => array("Button"=>"btn","Link Style"=>""),
         "description" => esc_html__("Choose button type", "dart")
      ),
       array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Color", "dart"),
         "param_name" => "color",
         "value" => array("Default"=>"btn-default","Primary"=>"btn-primary", "Info"=>"btn-info", "Success"=>"btn-success","Warning"=>"btn-warning", "Danger"=>"btn-danger"),
         "description" => esc_html__("Choose button color.", "dart")
      ),
      array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Size", "dart"),
         "param_name" => "size",
         "value" => array("Large"=>"btn-lg", "Default"=>"", "Small"=>"btn-sm","Very Small"=>"btn-xs"),
         "description" => esc_html__("Choose button size.", "dart")
      ),
      
      array(
         "type" => "textfield",
         "holder" =>'div',
         "class" => "",
         "heading" => esc_html__("Button Text", "dart"),
         "param_name" => "button"
      ),
      array(
         "type" => "textfield",
         
         "class" => "",
         "heading" => esc_html__("Box Title", "dart"),
         "param_name" => "revtitle"
      ),
	   array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Hide close button", "dart"),
         "param_name" => "noclosebutton",
         "value" => array("No"=>"false", "Yes"=>"true")
      ),
       array(
         "type" => "textarea_html",
         
         "class" => "",
         "heading" => esc_html__("Content", "dart"),
         "param_name" => "content",
         "description" => esc_html__(" Box content.", "dart")
      ),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      )
   )
) );

/************************************************/
?>