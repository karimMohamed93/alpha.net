<?php
/*********************Divider************************/

/*************************************************/
vc_map( array(
   "name" => esc_html__("Divider", "dart"),
   "base" => "divider",
   "class" => "",
   "icon" => "icon-wpb-my_divider",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
        
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Type", "dart"),
         "param_name" => "type",
         "value" => array("default"=>"", "Blank Spacer"=>"blank-spacer"),
      ),
       
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Size", "dart"),
         "param_name" => "size",
         "value" => array("Small"=>"gap-20","Medium"=>"gap-30","Large"=>"gap-40"),
		   "description" => esc_html__("For Blank Spacer", "dart")
      ),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      ),
   )
) );
/*********************************************/
?>