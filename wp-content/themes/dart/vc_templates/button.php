<?php

/******************* BUTTONS ********************/

vc_map( array(
   "name" => esc_html__("Button", "dart"),
   "base" => "button",
   "class" => "",
   "icon" => "icon-wpb-my_button",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
        array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Animation", "dart"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => esc_html__(" Animation.", "dart")
      ),
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Color", "dart"),
         "param_name" => "color",
         "value" => array("Default"=>"btn-default","Primary"=>"btn-primary", "Info"=>"btn-info", "Success"=>"btn-success","Warning"=>"btn-warning", "Danger"=>"btn-danger")
      ),
      array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Size", "dart"),
         "param_name" => "size",
         "value" => array("Large"=>"btn-lg", "Default"=>"defaultsize", "Small"=>"btn-sm","Very small"=>"btn-xs"),
         "description" => esc_html__("Choose button size.", "dart")
      ),
      
      
      array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Status", "dart"),
         "param_name" => "status",
         "value" => array("Enabled"=>"","Disabled"=>"disabled"),
         "description" => esc_html__(" Choose button status.", "dart")
      ),
      array(
         "type" => "textfield",
         
         "class" => "",
         "heading" => esc_html__("Link", "dart"),
         "param_name" => "link",
         "description" => esc_html__(" Link to.", "dart")
      ),
       array(
         "type" => "textfield",
         "holder"=>"div",
         "class" => "",
         "heading" => esc_html__("Text", "dart"),
         "param_name" => "content",
         "description" => esc_html__(" Write button text.", "dart")
      ),
	   
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Icon position", "dart"),
         "param_name" => "ipos",
         "value" => array("Left"=>"icon-left","right"=>"icon-right"),
      ),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Icon", "dart"),
         "param_name" => "icon"
      ),
        array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      )
   )
) );

/************************************************/
?>