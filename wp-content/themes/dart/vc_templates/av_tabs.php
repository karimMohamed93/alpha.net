<?php
class WPBakeryShortCode_Tabgroup extends WPBakeryShortCodesContainer{

}

/*******************************************/
class WPBakeryShortCode_Tab extends WPBakeryShortCode {

}
/**********************************************************/
vc_map( array(
	"name" => esc_html__("Tabs", "dart"),
	"show_settings_on_create" => true,
	'is_container' => true,
	"content_element" => true,
	"as_parent" => array('only' => 'tab'),
	"base" => "tabgroup",
	"class" => "",
	"icon" => "icon-wpb-my_tabs",
	'admin_enqueue_css' => array(get_template_directory_uri() . '/vc_templates/shortcodes.css'),
	"category" => esc_html__('Dart', "dart"),
	"params" => array(
		array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Animation", "dart"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => esc_html__(" Animation.", "dart")
      ),
		array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Style", "dart"),
         "param_name" => "style",
         "value"=>array("Default"=>"nav-tabs", "Alternative"=>"nav-stacked")
      ),
	   
        array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class"
      ),
   ),
	"js_view" => 'VcColumnView'
) );
/**********************************************************/
vc_map( array(
	'name' => esc_html__( 'Tab Item', 'dart' ),
	'base' => 'tab',
	"icon" => "icon-wpb-ui-accordion",
	"as_child" => array('only' => 'tabgroup'),
	'content_element' => true,
	'params' => array(
		array(
		 "type" => "textfield",
		"holder"=>"div",
		  "class" => "",
		  "heading" => esc_html__("Title", "dart"),
		  "param_name" => "title"
		 ),
		array(
		 "type" => "textfield",
		  "class" => "",
		  "heading" => esc_html__("Secondary Title(optional)", "dart"),
		  "param_name" => "sectitle",
		  "description" => esc_html__(' <small>For Alternative Style</small>', "dart")
		 ),
		array(
		 "type" => "textfield",
		  "class" => "",
		  "heading" => esc_html__("Block icon", "dart"),
		  "param_name" => "icon",
		  "description" => '<small>'.esc_html__('Choose your icon from', "dart").'<a href="'.esc_url('http://fortawesome.github.io/Font-Awesome/icons/').'" target="blank">'.esc_html__('this list - ', "dart").'</a></small>'
		 ),
		array(
		  "type" => "textarea_html",
		  "class" => "",
		  "heading" => esc_html__("Tab Content", "dart"),
		  "param_name" => "content"
		 ),
		)
	))
?>