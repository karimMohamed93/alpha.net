<?php
/************** SHOP PRODUCT CAROUSEL ************/

/*******************************************/
vc_map( array(
   "name" => esc_html__("Woocommerce products listing", "dart"),
   "base" => "productcar",
   "class" => "",
   "icon" => "icon-wpb-my_productcar",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Animation", "dart"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => esc_html__(" Animation.", "dart")
      ),
	  array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Product ID(s)", "dart"),
         "param_name" => "prod_ids",
		 "description" => esc_html__('Separate product IDs with commas. E.g.: 4,18,22', "dart")
      ),
	  array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Product tag(s)", "dart"),
         "param_name" => "prod_tags",
		 "description" => esc_html__('Separate product tags with commas. E.g.: technology, furniture', "dart")
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Category ID(s)", "dart"),
         "param_name" => "cat_ids",
		 "description" => esc_html__('Separate category IDs with commas. E.g.: 4,18,22', "dart")
      ),
	  array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Category slug(s)", "dart"),
         "param_name" => "cat_slugs",
		 "description" => esc_html__('Separate category slugs with commas. E.g.: sofa,chair,table', "dart")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Items limit", "dart"),
         "param_name" => "limit",
		 "value"=>"12"
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Items to Show", "dart"),
         "param_name" => "items",
		 "value"=>"4"
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => esc_html__("Automatic sliding", "dart"),
         "param_name" => "automatic",
         "value"=>array("No"=>"false", "Yes"=>"true")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Sliding interval", "dart"),
         "param_name" => "interval",
		 "value"=>"2500"
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => esc_html__("Show arrows", "dart"),
         "param_name" => "showarrows",
         "value"=>array("Yes"=>"true", "No"=>"false")
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => esc_html__("Order", "dart"),
         "param_name" => "order",
         "value"=>array("Descending"=>"DESC", "Ascending"=>"ASC")
      ),
	  array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => esc_html__("Order by", "dart"),
         "param_name" => "orderby",
         "value"=>array("Default sorting"=>"menu_order", "Sort by popularity"=>"popularity", "Sort by average rating"=>"rating", "Sort by newness"=>"date", "Sort by price: low to high"=>"price", "Sort by price: high to low"=>"price-desc")
      ),
        array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      )
   )
) );
?>
