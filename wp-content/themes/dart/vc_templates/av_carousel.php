<?php
/****************** content carousel ********************/
class WPBakeryShortCode_Carousel extends WPBakeryShortCodesContainer{

}

/***/
class WPBakeryShortCode_Caritem extends WPBakeryShortCode {
    
}

/****/
vc_map( array(
   "name" => esc_html__("Content Carousel", "dart"),
	"show_settings_on_create" => true,
	'is_container' => true,
	"content_element" => true,
	"as_parent" => array('only' => 'caritem'),
   "base" => "carousel",
   "class" => "",
   "icon" => "icon-wpb-my_av_carousel",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Animation", "dart"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => esc_html__(" Animation.", "dart")
      ),
	   
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Style", "dart"),
         "param_name" => "style",
         "value" => array("Default"=>"","Logo"=>"logo")
      ),
       
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Navigation", "dart"),
         "param_name" => "nav",
         "value" => array("Yes"=>"true", "No"=>"false")
      ),
	   
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Autoplay", "dart"),
         "param_name" => "auto",
         "value" => array("Yes"=>"true", "No"=>"false")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Autoplay speed(milliseconds)", "dart"),
         "param_name" => "speed",
         "value" => '3000'
      ),
	  
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Slide Items", "dart"),
         "param_name" => "sitems",
         "value" => '3'
      ),
      array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      ),
   ),
	"js_view" => 'VcColumnView'
) );
/**********************************************************/
vc_map( array(
	'name' => esc_html__( 'Carousel Item', 'dart' ),
	'base' => 'caritem',
	"icon" => "icon-wpb-ui-accordion",
	"as_child" => array('only' => 'carousel'),
	'content_element' => true,
	'params' => array(
		array(
		 "type" => "textarea_html",
		  "holder"=>"div",	
		  "class" => "",
		  "heading" => esc_html__("Content", "dart"),
		  "param_name" => "content"
		 ),		
		)
	))
?>
