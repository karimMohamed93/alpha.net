<?php
/****** SHOW POSTS BY CATEGORY AND COUNT ********/

vc_map( array(
   "name" => esc_html__("Blog listing", "dart"),
   "base" => "list_posts",
   "class" => "",
   "icon" => "icon-wpb-my_bloglist",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Animation", "dart"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => esc_html__(" Animation.", "dart")
      ),
      array(
         "type" => "textfield",
         
         "class" => "",
         "heading" => esc_html__("items limit", "dart"),
         "param_name" => "limit",
          "value"=>"5"
      ),
       array(
         "type" => "textfield",
         
         "class" => "",
         "heading" => esc_html__("Category", "dart"),
         "param_name" => "category",
         "description"=>esc_html__("Specify category Id or leave blank to display items from all categories.", "dart")
      ),
      array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Post Order", "dart"),
         "param_name" => "order",
         "value"=>array("Descending"=>"DESC", "Ascending"=>"ASC")
      ),
       array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Order by", "dart"),
         "param_name" => "orderby",
         "value"=>array("Date"=>"date", "ID"=>"id", "Author"=>"author","Title"=>"title","Number of comments"=>"comment_count","Randomly"=>"rand")
      ),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      ),
   )
) );
/*************************************************/
?>