<?php
/***************** TEAM MEMBERS *******************/
function vc_teammember($atts, $content=NULL){
    extract(shortcode_atts(array(
        'style'=>'',
        'name'=>'',
        'position'=>'',
		'photo'=>'',
		'imgstyle'=>'img-responsive',
        'anim'=>'',
        'class'=>'',
		'cusbgover'=>'',
		'fb'=>'',
        'tw'=>'',
        'gpl'=>'',
    ), $atts));
	
	$image = wp_get_attachment_image_src($photo, 'full');
	
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
	$cusbgover = (isset($cusbgover) && !empty($cusbgover))  ? ' background-color:'.$cusbgover.' !important;' : '';
		do_shortcode ($content);
		$tmClass=($imgstyle=='img-circle')? 'team-wrapper' : 'page-team-wrapper';
		$imgClass=($imgstyle=='img-circle')? 'team-img-wrapper' : 'team-image-wrapper';
		$hoveClass=($imgstyle=='img-circle')? 'team-img-wrapper-hover' : 'team-image-wrapper-hover';
		$out= '<div class="'.$tmClass.' '.$style.' '.$anim.' '.$class.'">
				<div class="'.$imgClass.'">
					<div class="'.$hoveClass.'" style="'.$cusbgover.'">
						<div class="social-icons">';
							if($fb) $out.= '<a href="'.$fb.'" title="Facebook"><i class="fa fa-facebook "></i></a>';
							if($tw) $out.= '<a href="'.$tw.'" title="Twitter"><i class="fa fa-twitter"></i></a>';
							if($gpl) $out.= '<a href="'.$gpl.'" title="Google+"><i class="fa fa-google-plus"></i></a>';
				$out.='</div>
					</div>
					<img alt="'.$name.'" src="' . $image[0] . '" class="'.$imgstyle.'">
				</div>
				<div class="team-content">
				    <h3 class="name">
				      '.$name.' 
				      <span>'.$position.'</span>
					</h3>
					<p class="team-text">'.do_shortcode ($content).'</p>
				</div>
			</div>';
    return $out;
}


/*****************************************************/
vc_map( array(
   "name" => esc_html__("Team Member", "dart"),
   "base" => "vc_teammember",
   "class" => "",
   "icon" => "icon-wpb-my_teammember",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
        array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Animation", "dart"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => esc_html__(" Animation.", "dart")
      ),
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Style", "dart"),
         "param_name" => "style",
         "value" => array("Default"=>"","Centered"=>"text-center")
      ),
      array(
         "type" => "textfield",
         "holder"=>"div",
         "class" => "",
         "heading" => esc_html__("Name", "dart"),
         "param_name" => "name"
      ),
      array(
         "type" => "textfield",
         
         "class" => "",
         "heading" => esc_html__("Position", "dart"),
         "param_name" => "position"
      ),
       array(
         "type" => "attach_image",
         
         "class" => "",
         "heading" => esc_html__("Photo ", "dart"),
         "param_name" => "photo"
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Image Style", "dart"),
         "param_name" => "imgstyle",
         "value" => array("Default"=>"img-responsive","Circle"=>"img-circle")
      ),
	  
	    array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => esc_html__("Overlay custom color", "dart"),
            "param_name" => "cusbgover",
            "value" => ''
         ),
        array(
         "type" => "textfield",     
         "class" => "",
         "heading" => esc_html__("Link to Facebook profile(optional)", "dart"),
         "param_name" => "fb"
      ),
        array(
         "type" => "textfield",  
         "class" => "",
         "heading" => esc_html__("Link to Twitter profile(optional)", "dart"),
         "param_name" => "tw"
      ),
       
        array(
         "type" => "textfield",    
         "class" => "",
         "heading" => esc_html__("Link to Google+ profile(optional)", "dart"),
         "param_name" => "gpl"
      ),
        array(
         "type" => "textarea_html",         
         "class" => "",
         "heading" => esc_html__("Member description", "dart"),
         "param_name" => "content"
      ),
        array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      ),
   )
) );
/*********************************************/
?>