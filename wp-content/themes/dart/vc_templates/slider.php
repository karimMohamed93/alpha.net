<?php
/****************** SLIDER ********************/
class WPBakeryShortCode_Slider extends WPBakeryShortCodesContainer {
	
}


/*******************************/
class WPBakeryShortCode_Slideritem extends WPBakeryShortCode {
	
}

/****/
vc_map( array(
   "name" => esc_html__("Image Slider", "dart"),
   "base" => "slider",
	"as_parent" => array('only' => 'slideritem'),
   "show_settings_on_create" 	=> true,
   "class" => "",
   "icon" => "icon-wpb-my_slider",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
	"content_element" => true,
	"is_container"=> true,
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Animation", "dart"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => esc_html__(" Animation.", "dart")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Type", "dart"),
         "param_name" => "type",
         "value" => array("Single"=>"single", "Carousel"=>"carousel")
      ),
       
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Navigation", "dart"),
         "param_name" => "nav",
         "value" => array("Yes"=>"true", "No"=>"false")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Pagination", "dart"),
         "param_name" => "pag",
         "value" => array("Yes"=>"true", "No"=>"false"),
		 "description" => esc_html__("For Single Type", "dart")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Autoplay", "dart"),
         "param_name" => "auto",
         "value" => array("Yes"=>"true", "No"=>"false")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Autoplay speed(milliseconds)", "dart"),
         "param_name" => "speed",
         "value" => '3000'
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Slide Items for carousel", "dart"),
         "param_name" => "sitems",
         "value" => '3',
		"description" => esc_html__("For Carousel Type", "dart")
      ),
      array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      ),
   ),
	"js_view" => 'VcColumnView'
) );
/************************************************/
vc_map( array(
	"name" => esc_html__("Slider Item", "dart"),
	"base" => "slideritem",
	"class" => "",
	"content_element" => true,
	"icon" => "icon-wpb-ui-accordion",
	"category" => esc_html__('Content', "dart"),
	"as_child" => array('only' => 'slider'),
	"params" => array(
      array(
         "type" => "textfield",
		 'holder'=>'div',
         "class" => "",
         "heading" => esc_html__("Name", "dart"),
         "param_name" => "title"
      ),
       
		
       array(
         "type" => "attach_image",
         "class" => "",
         "heading" => esc_html__("Image ", "dart"),
         "param_name" => "image"
      ),

	array(
         "type" => "textarea_html",        
         "class" => "",
         "heading" => esc_html__("Slide Caption", "dart"),
         "param_name" => "content"
      ),
   )
) );
/*********************************************/
?>