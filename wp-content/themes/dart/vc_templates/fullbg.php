<?php

/***************** Parallax *******************/

class WPBakeryShortCode_Fullbg extends WPBakeryShortCodesContainer{
	
}

/*******************************************/

vc_map( array(
   "name" => esc_html__("Fullwidth background", "dart"),
   "base" => "fullbg",
   "icon" => "icon-wpb-my_fullbg",
	'is_container' => true,
    'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Type", "dart"),
         "param_name" => "type",
         "value"=>array("Regular"=>"", "Regular (With video)"=>"wvideo", "Parallax"=>"parallax")
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Youtube video URL", "dart"),
         "param_name" => "videourl",
         "description" => esc_html__('For Video only', "dart")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Video autoplay", "dart"),
         "param_name" => "autoplay",
         "value"=>array("Yes"=>"true", "No"=>"false"),
		 "description" => esc_html__('For Video only', "dart")
      ),
	    array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Video loop", "dart"),
         "param_name" => "loop",
         "value"=>array("Yes"=>"true", "No"=>"false"),
		 "description" => esc_html__('For Video only', "dart")
      ),
	    array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Video Mute", "dart"),
         "param_name" => "mute",
         "value"=>array("Yes"=>"true", "No"=>"false"),
		 "description" => esc_html__('For Video only', "dart")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Video show controls", "dart"),
         "param_name" => "showcontrols",
         "value"=>array("Yes"=>"true", "No"=>"false"),
		 "description" => esc_html__('For Video only', "dart")
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Force fixed height", "dart"),
         "param_name" => "height",
		 "value"=>"300",
         "description" => esc_html__('For Video only', "dart")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Video quality", "dart"),
         "param_name" => "quality",
         "value"=>array("Default"=>"default", "Small"=>"small", "Medium"=>"medium", "Large"=>"large", "HD720"=>"hd720", "HD1080"=>"hd1080", "High res"=>"highres"),
		 "description" => esc_html__('For Video only', "dart")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Scrolling speed", "dart"),
         "param_name" => "scrollspeed",
         "value" => array("0.1"=>"0.1","0.2"=>"0.2","0.3"=>"0.3","0.4"=>"0.4","0.5"=>"0.5","0.6"=>"0.6","0.7"=>"0.7","0.8"=>"0.8","0.9"=>"0.9","1"=>"1","2"=>"2")
      ),
	   array(
         "type" => "attach_image",
         "class" => "",
         "heading" => esc_html__("Background image", "dart"),
         "param_name" => "bgimage"
      ),
      array(
         "type" => "colorpicker",
         "class" => "",
         "heading" => esc_html__("Background color", "dart"),
         "param_name" => "bgcolor"
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Background repeat", "dart"),
         "param_name" => "bgrepeat",
         "value"=>array("No repeat"=>"no-repeat", "Repeat"=>"repeat", "Repeat-x"=>"repeat-x", "Repeat-y"=>"repeat-y")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Padding", "dart"),
         "param_name" => "padding",
         "value"=>array("None"=>"", "Very Small"=>"padding-xsmall", "Small"=>"padding-small", "Medium"=>"padding-medium", "Large"=>"padding-large")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Custom padding", "dart"),
         "param_name" => "custompadding"
      ),
	   array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'No Top Border', 'dart' ),
			'param_name' => 'notopborder',
			'value' => array( esc_html__( 'No Top Border', 'dart' ) => 'notopborder' )
		),
	   array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'No Bottom Porder', 'dart' ),
			'param_name' => 'nobottomborder',
			'value' => array( esc_html__( 'No Bottom Border', 'dart' ) => 'nobottomborder' )
		),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      )
   ),
	"js_view" => 'VcColumnView'
) );
/*********************************************/
?>