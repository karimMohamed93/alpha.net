<?php
/************ FEATURED BLOCK****************/

/**************************************************/
vc_map( array(
   "name" => esc_html__("Featured block", "dart"),
   "base" => "fblock",
   "class" => "",
   "icon" => "icon-wpb-my_fblock",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => esc_html__('Dart', "dart"),
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Animation", "dart"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => esc_html__(" Animation", "dart")
      ),
        array(
         "type" => "dropdown",
         "class" => "",
         "heading" => esc_html__("Type", "dart"),
         "param_name" => "type",
         "value"=>array("Default"=>"default", "Alternative"=>"alter")
      ),
      array(
         "type" => "textfield",
		  "holder"=>"div",
         "class" => "",
         "heading" => esc_html__("Block title", "dart"),
         "param_name" => "title"
      ),
      
	   array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => esc_html__("Title custom color", "dart"),
            "param_name" => "tcuscolor",
            "value" => ''
         ),
      array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Block icon", "dart"),
         "param_name" => "icon"
	 ),
	  array(
         "type" => "attach_image",
         "class" => "",
         "heading" => esc_html__("Image (will override icon set)", "dart"),
         "param_name" => "image"
      ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => esc_html__("Icon custom color", "dart"),
            "param_name" => "icuscolor",
            "value" => ''
         ),
    
	    array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => esc_html__("Icon Background custom color", "dart"),
            "param_name" => "icusbgcolor",
            "value" => '',
			"description" => esc_html__('<small>Only For Default Type</small>', "dart")
         ),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Link to", "dart"),
         "param_name" => "link",
		 "description" => esc_html__(' <small>Only For Default Type</small>', "dart")

      ),
	     array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => esc_html__("Divider", "dart"),
         "param_name" => "divider",
         "value"=>array("No"=>"", "Yes"=>"fdivider")
      ),
        array(
         "type" => "textarea_html",
         "class" => "",
         "heading" => esc_html__("Content", "dart"),
         "param_name" => "content"
      ),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => esc_html__("Extra class", "dart"),
         "param_name" => "class",
         "description" => esc_html__(' Extra class name', "dart")
      )
   )
) );
/*********************************************/
?>