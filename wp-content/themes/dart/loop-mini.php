<?php /*** The loop that displays posts.***/ 

$get_meta = get_post_custom($post->ID);
$showdate = weblusive_get_option('blog_show_date'); 
$showcomments = weblusive_get_option('blog_show_comments'); 
$showcategory = weblusive_get_option('blog_show_category'); 
$showauthor = weblusive_get_option('blog_show_author');
$showrmtext = weblusive_get_option('blog_show_rmtext'); 
$day = get_the_time('d'); $month = get_the_time('m'); $year = get_the_time('Y');
$counter = 0;
?>

<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php esc_html_e( 'Not Found', 'dart'); ?></h1>
		<div class="entry-content">
			<p><?php esc_html_e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'dart'); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php else:?>
	
	<?php $counter=0; while ( have_posts() ) : the_post(); 
		//$get_meta = get_post_custom($post->ID);
		
		$mediatype = isset($get_meta["_blog_mediatype"]) ? $get_meta["_blog_mediatype"][0] : 'image';
		$videoId = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : ''; 
		$autoplay =  isset($get_meta["_blog_videoap"]) ? $get_meta["_blog_videoap"][0] : '0';
	//	$thumbnail = get_the_post_thumbnail($post->ID, 'portfolio-4-col');
		
		?>
	<?php if($counter == 0 || $counter % 4 == 0) echo "<div class='row'>"; ?>
	<div class="col-sm-3 col-xs-6 wow slideInLeft">
		<div class="media recent-post">
		<?php
		$thumbnail = get_the_post_thumbnail($post->ID, 'blog-thumb-2');
		$postmeta = get_post_custom($post->ID);
		if (!empty($thumbnail) && 'gallery' !== get_post_format( $post->ID )):
			the_post_thumbnail('blog-thumb-2');
		elseif (( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) )):
			global $wp_embed;
			$post_embed = '';
			$video = isset($postmeta["_blog_video"]) ? $postmeta["_blog_video"][0] : ''; 
			$videoself = isset($postmeta["_blog_video_selfhosted"]) ? $postmeta["_blog_video_selfhosted"][0] : ''; 
			if ($video || $videoself): ?>
				<div class="full-video">
					<?php 
					if ($video):
						$post_embed = $wp_embed->run_shortcode('[embed width="268" height="186"]'.$video.'[/embed]'); 
					else:
						$post_embed = do_shortcode($videoself);
					endif;
						echo $post_embed; 
					?>
				</div>	
					<?php	else: ?>
						<div class="alert alert-danger fade in">
							<?php esc_html_e('Video post format was chosen but no url or embed code provided. Please fix this by providing it.', 'dart') ?>
						</div>
					<?php endif; ?>
					<?php
					elseif(( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) )):?>
					<div class="page-slider-wrapper">
						<?php
						$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); 
							$argsThumb = array(
								'order'          => 'ASC',
								'posts_per_page'  => 99,
								'post_type'      => 'attachment',
								'post_parent'    => $post->ID,
								'post_mime_type' => 'image',
								'post_status'    => null,
								//'exclude' => get_post_thumbnail_id()
								);
					?>
					<ul class="bxslider"><?php
							$attachments = get_posts($argsThumb);
							if ($attachments) {
								foreach ($attachments as $attachment) {
									$image = wp_get_attachment_url($attachment->ID, 'full', false, false);
									$alt = $attachment->post_excerpt;
									echo '<li><img src="'.$image.'" alt="'.$alt.'"></li>';
								}
							}
							?>					
					</ul>
					</div>
					<?php else: ?>
						<a href="<?php echo get_permalink($post->ID) ?>" >
							<img src = "http://placehold.it/267x186" alt="<?php  esc_html__('No image', 'dart') ?>" />
						</a>
					<?php	endif; ?>
			<div class="blog-date">
				<span class="date"><?php echo get_the_time('d', $post->ID); ?>
					<span><?php echo get_the_time('M', $post->ID); ?></span>
				</span>
			</div>
			<div class="media-body post-body">
				<h3><a href="<?php the_permalink()?>"><?php the_title()?></a></h3>
				<p class="post-meta">
					<span class="post-meta-author"><a href="#"><?php esc_html_e('Posted by:', 'dart')?> <?php echo get_the_author();?></a></span>
					<span class="post-meta-comments"><a href="#"><?php echo get_comments_number($post->ID );?><?php esc_html_e(' comments', 'dart')?></a></span>
				</p>
				<div class="post-excerpt">
					<p><?php echo do_shortcode(limit_words(get_the_excerpt(), 10)); ?></p>
					<a href="<?php the_permalink()?>" class="read-more"><?php esc_html_e('Read More ', 'dart') ?><i class="fa fa fa-long-arrow-right"></i></a>
				</div>
			</div>
		</div>
	</div>
	<?php if ($counter >0 && ($counter+1) % 4 == 0): echo '</div>'; endif;	 ?>
	<?php $counter++; endwhile;?>
<?php endif; ?>
