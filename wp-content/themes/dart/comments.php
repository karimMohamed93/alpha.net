<?php
 
// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');
 
if ( post_password_required() ) : ?>
	<p class="nocomment"><?php esc_html_e( 'This post is password protected. Enter the password to view any comments.', 'dart' ); ?></p>
	
	<?php return;
endif;
?>
 
<?php if ( have_comments() ) : ?>
	<h3 class="comments-counter">
		<?php printf( _n( '(%1$s) Comment', '(%1$s) Comments', get_comments_number(), 'dart' ),number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );?>
	</h3>
	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<div class="navigation">
			<div class="nav-previous"><?php previous_comments_link( esc_html__( '<span class="meta-nav">&larr;</span> Older Comments', 'dart' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'dart' ) ); ?></div>
		</div> <!-- .navigation -->
	<?php endif; ?>
	 
	<ul class="comments-list">
	    <?php wp_list_comments('callback=dart_comment'); ?>
	</ul>

<?php else : // this is displayed if there are no comments so far ?>
	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->
	<?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"><?php esc_html_e( 'Comments are closed.', 'dart' ); ?></p>
	 <?php endif; ?>
<?php endif; ?>

<?php if ('open' == $post->comment_status) : ?>
	 <div class="comments-form">
		<h3><?php comment_form_title( 'Leave a comment', 'Leave a Reply to %s' ); ?></h3> 
		<div class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></div>
		<div class="row">
			<div class="col-md-12">      
				<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
					<p class="bottom20">
						<?php esc_html_e('You must be logged in to post a comment.', 'dart')?> 
						<a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">
							<?php esc_html_e('Click here to login.', 'dart')?> 
						</a> 
					</p>
				<?php else : ?> 
					<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" class="comment-form" role="form">
							<?php if ( $user_ID ) : ?>				 
									<p class="com_logined_text">
										<?php esc_html_e('Logged in as', 'dart')?> 
										<a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. 
										<a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account"><?php esc_html_e('Log out', 'dart')?> &raquo;</a>
									</p>
							<?php else : ?>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label><?php esc_html_e('Your Name', 'dart')?> </label>
										 <input class="form-control" type="text" name="author" id="author"  title="<?php esc_html_e('Name', 'dart')?>" value="<?php echo $comment_author; ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label><?php esc_html_e('Your Email', 'dart')?> </label>
										<input class="form-control" type="email" name="email" id="email"  title="<?php esc_html_e('Email', 'dart')?>" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?>>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label><?php esc_html_e('Your Website', 'dart')?> </label>
										<input class="form-control" type="text" name="website" id="website"  title="<?php esc_html_e('Website', 'dart')?>" value="<?php echo isset($comment_author_website) ? $comment_author_website : '' ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?>>
									</div>
								</div>
							</div>
							<?php endif; ?>
						
							<div class="form-group">
								<label><?php esc_html_e('Message', 'dart')?> </label>
							    <textarea name="comment" class="form-control" title="<?php esc_html_e('Your Comment', 'dart')?>"  id="comment" rows="8" tabindex="4"></textarea>
							</div>
							<div>
								<br/>
								<input type="submit" class="btn btn-primary" value="<?php esc_html_e('Post Comment', 'dart')?>" />
								<?php comment_id_fields(); ?>
								<?php do_action('comment_form', $post->ID); ?>
							</div>
					</form>
				<?php endif;  ?>
			</div>
		</div>
	</div>
<?php endif;  ?>
