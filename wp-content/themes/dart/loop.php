<?php /*** The loop that displays posts.***/ 

//$get_meta = get_post_custom($post->ID);

?>

<?php while ( have_posts() ) : the_post(); 
	$get_meta = get_post_custom($post->ID);
	
	
	$thumbnail = get_the_post_thumbnail($post->ID, 'blog-default');
	?>
	<!-- ************* POST FORMAT IMAGE ************** -->
	<?php if ( ( function_exists( 'get_post_format' ) && 'image' == get_post_format( $post->ID ) )  ) : ?> 
	<div <?php post_class('blog-post post-format-image'); ?>>
		<div class="post-media">
			<div class="img-overlay"></div>
			<div class="blog-img-icon">
				<a href="<?php the_permalink()?>"><i class="fa fa-link"></i></a>
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
				<a href="<?php echo $url;?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
			</div>
			<?php $thumbnail = get_the_post_thumbnail(); if(!empty($thumbnail)) : ?>
					<?php the_post_thumbnail('blog-default', array( 'class' => 'img-responsive' )); ?>
			<?php endif?>
		</div>
		<div class="entry-header clearfix">
			<h2 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
			<div class="post-meta">
				<?php $meta = dart_meta($post); echo $meta ?>
			</div> 
		</div>
		<div class="entry-content">
			<?php echo do_shortcode(get_the_excerpt()); ?>
		</div>
		<div class="entry-footer">
			<a  href="<?php the_permalink()?>" class="read-more"><?php esc_html_e('Read More ', 'dart') ?><i class="fa fa-long-arrow-right"></i></a>
		</div>
	</div>
	<!-- ************* POST FORMAT LINK ************** -->
	<?php elseif( ( function_exists( 'get_post_format' ) && 'link' == get_post_format( $post->ID ) )  ) : ?> 
	<div <?php post_class('blog-post post-format-link'); ?>>
		<?php $link = isset($get_meta["_blog_link"]) ? $get_meta["_blog_link"][0] : ''; ?>
		<div class="entry-header clearfix">
			<h2 class="entry-title"><a href="<?php echo $link ?>"><?php the_title(); ?></a></h2>
			<div class="post-meta">
				<?php $meta = dart_meta($post); echo $meta ?>
			</div> 
		</div>
	</div>
	<!-- ************* POST FORMAT AUDIO ************** -->
	<?php elseif( ( function_exists( 'get_post_format' ) && 'audio' == get_post_format( $post->ID ) )  ) : ?> 
	<div <?php post_class('blog-post post-format-music'); ?>>
		<div class="post-media">
			<?php $audio = isset($get_meta["_blog_audio"]) ? $get_meta["_blog_audio"][0] : ''; ?>
			<?php if ($audio):?>
				<?php echo do_shortcode($audio); ?>
			<?php else:?>
				<div class="alert alert-danger fade in">
					<?php esc_html_e('Audio post format was chosen but no embed code provided. Please fix this by providing it.', 'dart') ?>
				</div>
			<?php endif?>
		</div>
		<div class="entry-header clearfix">
			<h2 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
			<div class="post-meta">
				<?php $meta = dart_meta($post); echo $meta ?>
			</div> 
		</div>
		<div class="entry-content">
			<?php echo do_shortcode(get_the_excerpt()); ?>
		</div>
		<div class="entry-footer">
			<a  href="<?php the_permalink()?>" class="read-more"><?php esc_html_e('Read More ', 'dart') ?><i class="fa fa-long-arrow-right"></i></a>
		</div>
	</div>

	<!-- ************* POST FORMAT QUOTE ************** -->
	<?php elseif( ( function_exists( 'get_post_format' ) && 'quote' == get_post_format( $post->ID ) )  ) : ?> 
	<div <?php post_class('blog-post post-format-quote'); ?>>
		<div class="entry-header clearfix">
			<h2 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
			<div class="post-meta">
				<?php $meta = dart_meta($post); echo $meta ?>
			</div> 
		</div>
		<div class="entry-content">
			<?php $quote = isset($get_meta["_blog_quote"]) ? $get_meta["_blog_quote"][0] : ''; ?>
			<?php if ($quote):?>
				<?php echo htmlspecialchars_decode(do_shortcode($quote)); ?>
			<?php else:?>
				<div class="alert alert-danger fade in">
					<?php esc_html_e('Quote post format was chosen but no url or embed code provided. Please fix this by providing it.', 'dart') ?>
				</div>
			<?php endif?>
		</div>
		<div class="entry-footer">
			<a  href="<?php the_permalink()?>" class="read-more"><?php esc_html_e('Read More ', 'dart') ?><i class="fa fa-long-arrow-right"></i></a>
		</div>
	</div>
	<!-- ************* POST FORMAT VIDEO ************** -->
	<?php elseif( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) )  ) : ?> 
		<div <?php post_class('blog-post post-format-video'); ?>>
			<div class="blog-video-wrapper">
					<?php
					global $wp_embed;
					$post_embed = '';
					$video = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : ''; 
					$videoself = isset($get_meta["_blog_video_selfhosted"]) ? $get_meta["_blog_video_selfhosted"][0] : ''; 
					if ($video || $videoself): ?>
						<div class="full-video">
							<?php
								if ($video):
									$post_embed = $wp_embed->run_shortcode('[embed width="500" height="400"]'.$video.'[/embed]'); 
								else:
									$post_embed =do_shortcode($videoself);
								endif;
								echo $post_embed; 
							?>
						</div>	
					<?php else:?>
						<div class="alert alert-danger fade in">
							<?php esc_html_e('Video post format was chosen but no url or embed code provided. Please fix this by providing it.', 'dart') ?>
						</div>
					<?php endif?>
			</div>
			<div class="entry-header clearfix">
				<h2 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<div class="post-meta">
					<?php $meta = dart_meta($post); echo $meta ?>
				</div> 
			</div>
			<div class="entry-content">
				<?php echo do_shortcode(get_the_excerpt()); ?>
			</div>
			<div class="entry-footer">
				<a  href="<?php the_permalink()?>" class="read-more"><?php esc_html_e('Read More ', 'dart') ?><i class="fa fa-long-arrow-right"></i></a>
			</div>
		</div>
	<!-- ************* POST FORMAT GALLERY ************** -->
	<?php elseif( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) )  ) : ?> 
		<div <?php post_class('blog-post post-format-gallery'); ?>>
			<div class="page-slider-wrapper">
				<?php
					$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); 
					$argsThumb = array(
						'order'          => 'ASC',
						'posts_per_page'  => 99,
						'post_type'      => 'attachment',
						'post_parent'    => $post->ID,
						'post_mime_type' => 'image',
						'post_status'    => null,
						//'exclude' => get_post_thumbnail_id()
					);
				?>
				<ul class="bxslider">
					<?php 
						$attachments = get_posts($argsThumb);
						if ($attachments) {
							foreach ($attachments as $attachment) {
								$image = wp_get_attachment_url($attachment->ID, 'full', false, false);
								$alt = $attachment->post_excerpt;
								echo '<li><img src="'.$image.'" alt="'.$alt.'" alt="blog"></li>';
							}
						}
						?>
				</ul>
			</div>
			<div class="entry-header clearfix">
				<h2 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<div class="post-meta">
					<?php $meta = dart_meta($post); echo $meta ?>
				</div> 
			</div>
			<div class="entry-content">
				<?php echo do_shortcode(get_the_excerpt()); ?>
			</div>
			<div class="entry-footer">
				<a  href="<?php the_permalink()?>" class="read-more"><?php esc_html_e('Read More ', 'dart') ?><i class="fa fa-long-arrow-right"></i></a>
			</div>
		</div>
	<?php else : ?>
	
	<!-- ************* POST FORMAT STANDARD ************** -->
	<div <?php post_class('blog-post post-format-standard'); ?>>
		<?php $thumbnail = get_the_post_thumbnail(); if(!empty($thumbnail)) : ?>
		<div class="post-media">
			<?php the_post_thumbnail('blog-default', array( 'class' => 'img-responsive' )); ?>
		</div>
		<?php endif?>
		<div class="entry-header clearfix">
			<h2 class="entry-title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
			<div class="post-meta">
				<?php $meta = dart_meta($post); echo $meta ?>
			</div> 
		</div>
		<div class="entry-content">
			<?php echo do_shortcode(get_the_excerpt()); ?>
		</div>
		<div class="entry-footer">
			<a  href="<?php the_permalink()?>" class="read-more"><?php esc_html_e('Read More ', 'dart') ?><i class="fa fa-long-arrow-right"></i></a>
		</div>
	</div>
	<?php endif; ?>
<?php endwhile;?>
<?php if ( $wp_query->max_num_pages > 1 ): ?>
	<div class="paging">
		<?php include(dart_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
	</div>
<?php endif?>
	
