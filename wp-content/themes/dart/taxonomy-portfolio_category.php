<?php get_header();
get_template_part('portfolio_header'); 
	//global $paged;
 	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
	
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	if ( get_query_var('paged') ) {
		$paged = get_query_var('paged');
	} elseif ( get_query_var('page') ) {
		$paged = get_query_var('page');
	} else {
		$paged = 1;
	}
	$pageId = $_SESSION['dart_page_id'];
	$sidebarPos = weblusive_get_option('sidebar_pos');
	$sidebar = weblusive_get_option('sidebar_portfolio_taxonomy');
	if(empty($sidebar)) $sidebarPos = 'full';
	
	$page_template_name = ''; 
	$columns = weblusive_get_option('portfolio_tax_columns') ? weblusive_get_option('portfolio_tax_columns') : '3'; 
	$layout = weblusive_get_option('portfolio_tax_layout') ? weblusive_get_option('portfolio_tax_layout') : '1'; 
	
	$itemsize = '292x300';	
	$thumbsize = 'portfolio-4-cols';
	// Check which layout was selected
	switch($columns)
	{
		
		case '2':
			$itemsize = '600x850';	
			$thumbsize = 'portfolio-2-cols';
			$contcClass='col-sm-6';
		break;
		case '3':
			$itemsize = '450x600';	
			$thumbsize = 'portfolio-3-cols';
			$contcClass='col-sm-4';
		break;
		
		case '4':
			$itemsize = '292x300';	
			$thumbsize = 'portfolio-4-cols';
			$contcClass='col-sm-3';
		break;
	}
?>
<?php //echo do_shortcode($term->description); ?> 
<?php //echo category_description(); ?>
<div class="portfolio-page" id="portfolio-page">
<div class="row">
	<?php if (!empty($sidebar) && $sidebarPos == 'left'): ?>
		<div id="sidebar" class="col-sm-4 col-md-3"><div class="sidebar sidebar-left"><?php get_sidebar(); ?></div></div>
	<?php endif?>
	<div class="<?php if ($sidebarPos == 'full' || empty($sidebar)):?>col-md-12<?php else:?>col-sm-8 col-md-9<?php endif?> main-content">
		<div class="row text-center">
			<ul id="isotope-filter"  class="option-set list-inline text-center">
				<li><a href="<?php echo get_page_link($pageId) ?>" class="filter"><?php esc_html_e('All', 'dart')?></a></li> 
				<?php 
					$cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true);
					$MyWalker = new PortfolioWalker2();
					$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'show_count' => '1');
					$categories = wp_list_categories ($args);
				?>
			</ul>
		</div>
		<div class="row">
			<div id="isotope" class="portfolio-grid   <?php echo $thumbsize;?>">
				<?php
				$counter=1;
				if ($wp_query->have_posts()):
					while ($wp_query->have_posts()) : 							
						$wp_query->the_post();
						$custom = get_post_custom($post->ID);
						// Get the portfolio item categories
						$cats = wp_get_object_terms($post->ID, 'portfolio_category');
						if ($cats):
							$cat_slugs = '';
							foreach( $cats as $cat ) {$cat_slugs .= $cat->slug . " ";}
						endif;
						$attachments = ''; 
						$thumbnail = get_the_post_thumbnail($post->ID, $thumbsize); 

						?>
					
						<div class="<?php echo $contcClass?> <?php echo $cat_slugs; ?>">
							<div class="portfolio-overlay">
								<div class="portfolio-overlay-btn">
									<?php if (!$attachments) : ?>
										<?php if (!empty($custom['_portfolio_video'][0])) : $link = $custom['_portfolio_video'][0]; ?>
											<a href="<?php echo $link ?>" data-rel="prettyPhoto" title="<?php the_title() ?>">
												<i class="fa fa-film"></i>
											</a>
										<?php elseif (isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '') : ?>
											<a href="<?php echo $custom['_portfolio_link'][0] ?>" title="<?php the_title() ?>">
												<i class="fa fa-external-link"></i>
											</a>
										<?php elseif (isset($custom['_portfolio_no_lightbox'][0]) && $custom['_portfolio_no_lightbox'][0] != '') : $link = get_permalink(get_the_ID()); ?>
											<a href="<?php echo $link; ?>" title="<?php the_title() ?>">
												<i class="fa fa-link"></i>
											</a>
										<?php else : ?>
											<?php
											$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
											$link = $full_image[0];
											?>
											<a data-rel="prettyPhoto" href="<?php echo $link ?>" title="<?php the_title() ?>">
												<i class="fa fa-search"></i>
											</a>
										<?php endif; ?>       
									<?php endif ?>
								</div>
								<?php if ($layout == '2'):?>
									<div class="portfolio-item-content">
                            			<h4 class="portfolio-item-title"><a href="<?php the_permalink()?>"><?php the_title() ?></a></h4>
										<p><?php echo limit_words(get_the_excerpt(), 5) ?></p>
									</div>	
								<?php endif?>
							</div>
							<div class="image-thumb">
								<?php if (!empty($thumbnail)): ?>
									<?php the_post_thumbnail($thumbsize, array('class' => 'cover')); ?>
								<?php else :?>
									<img src="http://placehold.it/<?php echo $itemsize?>" alt="<?php _e ('No preview image', 'dart') ?>" />
								<?php endif?>
							</div>
						</div>	
					<?php $counter++; endwhile; ?>				
				<?php endif?>
			</div>
			</div>
			<?php if ( $wp_query->max_num_pages > 1 ): ?>
				<div class="row">
					<div class="col-sm-12">
						<div class="text-center">
						<?php include(dart_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
						</div>
					</div>
				</div>
			<?php endif?>
		
	</div>
	<?php if (!empty($sidebar) && $sidebarPos == 'right'): ?>
		<div id="sidebar" class="col-sm-4 col-md-3"><div class="sidebar sidebar-right"><?php get_sidebar(); ?></div></div>
	<?php endif?>
</div>
</div>
<?php get_template_part('portfolio_footer'); ?>
<?php get_footer() ?>