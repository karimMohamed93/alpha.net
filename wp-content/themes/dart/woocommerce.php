<?php /* Template Name: Woocommerce page */ 
get_header();
$id = get_option('woocommerce_shop_page_id');
$get_meta = get_post_custom($id);
$sidebar = isset( $get_meta['_weblusive_sidebar_post'][0]) ? $get_meta["_weblusive_sidebar_post"][0] : 'Primary Widget Area';
$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';

$hidetitles = weblusive_get_option('hide_titles');
$hidebreadcrumbs = weblusive_get_option('hide_breadcrumbs');

$tagline = isset( $get_meta['_weblusive_tagline'][0]) ? $get_meta["_weblusive_tagline"][0] : '';
$alternativetitle = isset( $get_meta['_weblusive_altpagetitle'][0]) ? $get_meta["_weblusive_altpagetitle"][0] : '';
$hideheading = isset( $get_meta['_weblusive_hide_innerheading'][0]) ? $get_meta["_weblusive_hide_innerheading"][0] : '';

$layout = weblusive_get_option('innerheading_layout');
if (!$layout) $layout = "1";
$bg = weblusive_get_option('innerheading_background_type'); 
$prodlayout =  weblusive_get_option('product_page_layout'); 

$percolumn = weblusive_get_option('products_per_row');
if (empty($percolumn)) $percolumn = 'columns-3';

$bg = weblusive_get_option('innerheading_background_type'); 
$bgimage=weblusive_get_option('innerheading_background_image');
$postBg = isset( $get_meta['_weblusive_header_bg'][0]) ? $get_meta["_weblusive_header_bg"][0] : '';
$pHead=isset( $get_meta['_weblusive_post_head'][0]) ? $get_meta["_weblusive_post_head"][0] : '';
$image_id = @get_post_thumbnail_id($id);  
$image_url = wp_get_attachment_image_src($image_id,'full');  
$image_url = $image_url[0];
if($image_id && is_page()){
	$bgimg=$image_url;
}elseif ($bgimage) {
	$bgimg=$bgimage;
}elseif ($postBg) {
	$bgimg=$postBg;
}else{
	$bgimg=get_template_directory_uri() .'/images/banner/banner1.jpg' ;
}
?>

<div class="pageHead">
	<div id="inner-header" <?php if($pHead!=='none'):?>style="background: url(<?php echo $bgimg;?>) no-repeat; padding-top: 200px"<?php endif;?>>
		<?php if($pHead== 'none'):?>
		<img src="<?php echo $bgimg;?>" alt="banner">
		<?php  else : ?>
		<div class="container">
			<?php get_template_part( 'library/includes/page-head' ); ?>
		</div>
		<?php endif; ?>
	</div>

	<?php if (!$hideheading):?>
		<section id="inner-title">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="inner-title-content">
							<?php if (!$hidetitles):?><h2><?php if(function_exists('woocommerce_breadcrumb')){woocommerce_page_title();} ?></h2><?php endif; ?>
							<?php if (!$hidebreadcrumbs):?><?php if(function_exists('woocommerce_breadcrumb')){ woocommerce_breadcrumb(); } ?><?php endif?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="gap-40"></div>
	<?php endif; ?>
</div>
<section class="main-wrapper pagecustom-<?php echo $id?>"> 
	<div class="container">
		<div class="row">
			<?php if(function_exists('is_product')):?>
				<?php if (($weblusive_sidebar_pos == 'left' && !is_product()) || (is_product() && $prodlayout == 'left')): ?>
					<aside id="sidebar" class="col-sm-4 col-md-3"><div class="sidebar sidebar-left"><?php dynamic_sidebar($sidebar); ?></div></aside>
				<?php endif?>
				<div class="<?php echo (($weblusive_sidebar_pos == 'full' && !is_product()) || (is_product() && $prodlayout == 'full')) ? 'col-md-12' : 'col-sm-8 col-md-9'?> main-content shop-section woocommerce-page <?php echo $percolumn?>">
					<?php woocommerce_content(); ?>
				</div>
				<?php if (($weblusive_sidebar_pos == 'right' && !is_product()) || (is_product() && $prodlayout == 'right')): ?>
					<aside id="sidebar" class="col-sm-4 col-md-3"><div class="sidebar sidebar-right"><?php dynamic_sidebar($sidebar); ?></div></aside>
				<?php endif?>
			<?php else:?>
				<div class="col-md-12">Please install the Woocommerce plugin to use this page template.</div>
			<?php endif?>
		</div>
	</div>
</div>
<?php get_footer();?>