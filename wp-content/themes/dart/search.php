<?php /** The template for displaying FAQ search results. **/
get_header();
$sidebar = weblusive_get_option('sidebar_archive');
$weblusive_sidebar_pos = weblusive_get_option('sidebar_pos');
$search_refer = isset($_GET["post_type"]) ? $_GET["post_type"] : 'post';

$hidetitles = weblusive_get_option('hide_titles');
$hidebreadcrumbs = weblusive_get_option('hide_breadcrumbs');
$bgimage=weblusive_get_option('innerheading_background_image'); 
if ($bgimage) {
	$bgimg=$bgimage;
}else{
	$bgimg=get_template_directory_uri() .'/images/banner/banner1.jpg' ;
}
?>

<div id="inner-header">
	<img src="<?php echo $bgimg;?>" alt ="banner" />
</div>
<?php if (!$hidetitles):?>
<!-- Subpage title start -->
<section id="inner-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner-title-content">
				<?php if (!$hidetitles):?>	
					<h2><?php esc_html_e('Search Results', 'dart') ?></h2>
					<?php endif; ?>
		        	<?php if (!$hidebreadcrumbs):?>
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					<?php endif?>
	          	</div>
	        </div>
		</div>
	</div>
</section>
<?php endif; ?>
<div class="gap-40"></div>
<section id="main-container"> 
	<div class="container">
		<div class="row">
			<?php if ($weblusive_sidebar_pos == 'left' && !empty($sidebar)):?><div id="sidebar" class="col-md-4"><div class="sidebar sidebar-left"><?php get_sidebar(); ?></div></div><?php endif?>
			<div class="<?php if ($weblusive_sidebar_pos == 'full' || empty($sidebar)):?>col-md-12<?php else:?>col-md-8<?php endif?>">
				<?php   $args = array('s' => $s, 'paged' => $paged); query_posts($args);
				if ( have_posts() ) : ?>
					<?php 
						if ($search_refer == 'faq') 
						{
							get_template_part( 'loop', 'faq' );
						}	
						else
						{
							$allsearch = new WP_Query("s=$s&showposts=-1"); 
							
							$key = esc_html($s, 1); 
							$count = $allsearch->post_count;
							?>
							<div class="row">  
								<div class="col-md-6 col-md-offset-3">
									<div class="panel panel-default">
										<div class="panel-body">
											<h3 class="text-center"><?php esc_html_e('New Search', 'dart')?></h3>
											<form id="regular-searchform" action="<?php echo site_url() ?>" method="get">
											<div class="input-group">
												<span class="input-group-btn"><button class="btn btn-primary" type="submit" id="searchsubmit"><i class="fa fa-search"></i></button></span>
												<input id="s" maxlength="150" name="s" size="20" class="form-control" value="" type="text" >
												<input name="post_type" type="hidden" value="post" />
											</div>
											</form>
											<p class="text-center"><?php esc_html_e('If you are not happy with the results bellow please do another search.', 'dart')?></p>                                
										</div>
									</div>
								</div>      
							</div>
							<p class="lead">
								<?php echo $count ?><?php esc_html_e(' Search Result for: ', 'dart')?><strong><?php echo $key;?></strong>
								
							</p>
							<div class="search-results">
								<?php get_template_part( 'loop', 'search' );?>
							</div>
					<?php	}
					?>
				<?php else : ?>
					<div id="post-0" class="post no-results not-found">
						<div class="row">  
							<div class="col-md-6 col-md-offset-3">
									<div class="panel panel-default">
										<div class="panel-body">
											<h3 class="text-center"><?php esc_html_e('Nothing found', 'dart') ?></h3>
											<form id="regular-searchform" action="<?php echo site_url() ?>" method="get">
												<div class="input-group">
													<span class="input-group-btn"><button class="btn btn-primary" type="submit" id="searchsubmit"><i class="fa fa-search"></i></button></span>
													<input id="s" maxlength="150" name="s" size="20" class="form-control" value="" type="text" >
													<input name="post_type" type="hidden" value="post" />
												</div>
											</form>
											<p class="text-center"><?php esc_html_e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'dart') ?></p>                                
										</div>
									</div>
							</div>         
						</div>
					</div><!-- #post-0 -->
				<?php endif; ?>
			</div>	
            <?php if ($weblusive_sidebar_pos == 'right'  && !empty($sidebar)): ?><div id="sidebar" class="col-md-4"><div class="sidebar sidebar-right"><?php get_sidebar(); ?></div></div><?php endif?>
        </div>
    </div>
</section>
<?php get_footer(); ?>